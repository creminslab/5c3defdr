3DeFDR-5C
==========

A library for identifying differential loops in 5C data.

For complete documentation, consult [the user guide](https://www.dropbox.com/s/xfa4qsqeloo1tim/diff3DeFDR_user_guide.pdf?dl=0).

3DeFDR-5C is made available under the MIT license, see `LICENSE` for details.

The method is 3DeFDR-5C - and the method is matched to the coding package called diff3defdr (below).

Quick-start
-----------

### Docker image build

    $ docker build -t creminslab/diff3defdr:1.0 .

### Docker tutorial

    $ mkdir diff3DeFDR-tutorial
    $ cd diff3DeFDR-tutorial
    $ docker run --rm -it -v ${PWD}:/diff3DeFDR-tutorial creminslab/diff3defdr:1.0

Then in the container:

```
# create directories
cd /diff3DeFDR-tutorial
mkdir input

# download input data
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE85185&format=file&file=GSE85185%5FBED%5F314%2DES%2DNPC%2DLOCI%5Fmm9%2Ebed%2Egz' | gunzip -c > input/primermap.bed
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259911&format=file&file=GSM2259911%5FES%5F2i%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/es2i_rep1.counts
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259912&format=file&file=GSM2259912%5FES%5F2i%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/es2i_rep2.counts
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259913&format=file&file=GSM2259913%5FES%5Fserum%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/esserum_rep1.counts
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259914&format=file&file=GSM2259914%5FES%5Fserum%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/esserum_rep2.counts
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259915&format=file&file=GSM2259915%5FpNPC%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/npc_rep1.counts
wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259916&format=file&file=GSM2259916%5FpNPC%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/npc_rep2.counts

# copy default config
diff3defdr

# run
diff3defdr diff3DeFDR.cfg
```

The full tutorial takes about one hour to run. Output files and plots will
appear in `output/`.
