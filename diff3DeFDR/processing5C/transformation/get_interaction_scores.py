import copy

import numpy as np

from transformation_name import get_output_extension


def get_interaction_scores(experiment, save_intermediates=True, output_dir=None, compressed=False):
    """
    Convert modeled p-values to interaction scores using -10*log2(p-value) transformation.

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing p-values to transform
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    output_dir : str, optional
        Where to save resulting counts files if saving intermediates
    compressed : bool, optional
        Whether to save output countsfiles as compressed .npz files or uncompressed .counts files

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing interaction scores
    """
    print('get_interaction_scores: computing interaction scores')
    pvalues = experiment.counts_list
    int_scores = [{region: -10*np.log2(p[region]) for region in pvalues[0].keys()} for p in pvalues]

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts(int_scores)

    if save_intermediates:
        if compressed:
            updated_experiment.save_counts_as_npz(output_dir, ext=get_output_extension('interaction_scores'))
        else:
            updated_experiment.save_counts(output_dir, ext=get_output_extension('interaction_scores'))
    return updated_experiment
