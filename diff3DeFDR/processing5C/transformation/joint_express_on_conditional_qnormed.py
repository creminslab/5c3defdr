import copy

import diff3DeFDR.common as md
from transformation_name import get_output_extension


def joint_express_on_exp(experiment, save_intermediates=True, output_dir=None, compressed=False):
    """
    Apply joint express balancing to counts matrices in an ExperimentSet object

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing counts to transform
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    output_dir : str, optional
        Where to save resulting counts files if saving intermediates
    compressed : bool, optional
        Whether to save output countsfiles as compressed .npz files or uncompressed .counts files

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing transformed counts
    """
    trimmed_primermap = experiment.mapping
    qnormed_counts_superdict = experiment.superdict()

    sorted_labels = sorted(experiment.labels)
    sorted_counts_list = [qnormed_counts_superdict[l] for l in sorted_labels]

    # precompute fragment expected model
    expected_counts = [md.make_expected_matrix_fragment(i, trimmed_primermap, md.polyfit_model_fragments,
                                                        is_global=False) for i in sorted_counts_list]

    # reshape
    reshaped_observed_counts = {region: [i[region] for i in sorted_counts_list] for region in sorted_counts_list[0]}
    reshaped_expected_counts = {region: [i[region] for i in expected_counts] for region in sorted_counts_list[0]}

    # joint normalize
    print 'joint_express_on_conditional_qnormed'
    raw_balanced_counts, _, _ = md.joint_express_normalize(reshaped_observed_counts, reshaped_expected_counts)

    # reshape
    joint_expressed_counts_dict = {sorted_labels[i]: {region: raw_balanced_counts[region][i]
                                                      for region in sorted_counts_list[0]}
                                   for i, _ in enumerate(sorted_counts_list)}

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts([joint_expressed_counts_dict[l] for l in experiment.labels])

    # write output
    if save_intermediates:
        if compressed:
            updated_experiment.save_counts_as_npz(output_dir=output_dir, ext=get_output_extension('joint_express'))
        else:
            updated_experiment.save_counts(output_dir=output_dir, ext=get_output_extension('joint_express'))

    return updated_experiment