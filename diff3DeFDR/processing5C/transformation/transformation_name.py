import os
import pandas as pd

this_files_dir = os.path.dirname(os.path.realpath(__file__))
transform_names = pd.read_csv(os.path.join(this_files_dir, 'transformation_names.csv'),header=0,index_col=0)


def valid_transformation_name(transformation_name):
    '''
    Convenience method for checking that one is using a valid transformation name

    Parameters
    ----------
    transformation_name : str

    Returns
    -------
    bool
    '''
    valid = transformation_name in transform_names.index
    if not valid:
        print 'Invalid transformation name. Valid names include: {}'.format(transform_names.index.tolist())
    return valid


def get_plotting_name(transformation_name):
    '''
    For a given transformation output by the 5C Processing Pipeline, returns the corresponding name needed for
    accessing the data in a DataSet object, especially for the purpose of plotting heatmaps using an appropriate
    color-map.

    Parameters
    ----------
    transformation_name : str
    Options include 'joint_express', 'observed', 'global_expected', 'donut_expected', 'lower_left_expected',
    'max_donut_lower_left_expected', 'observed_over_expected', 'pvalues', 'interaction_scores', and
    'trimmed_interaction_scores'.

    Returns
    -------
    plotting name : str
        Useful for knowing what colorscale and colormap to use when plotting this data in a heatmap
    '''
    return transform_names.loc[transformation_name, 'plotting_data_type']

def get_output_extension(transformation_name):
    '''
    For a given transformation output by the 5C Processing Pipeline, returns the corresponding name needed for
    retrieving the data from file.

    Parameters
    ----------
    transformation_name : str
    Options include 'joint_express', 'observed', 'global_expected', 'donut_expected', 'lower_left_expected',
    'max_donut_lower_left_expected', 'observed_over_expected', 'pvalues', 'interaction_scores', and
    'trimmed_interaction_scores'.

     Returns
    -------
    output extension : str
        The suffix used to store counts of this type in an intermediate counts directory
    '''
    return transform_names.loc[transformation_name, 'save_ext']