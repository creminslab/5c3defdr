import os

import copy
import numpy as np

import diff3DeFDR.common as md
from transformation_name import get_output_extension


def find_obs_over_max_donut_ll(observed_experiment, expected_experiment, save_intermediates=True, output_dir=None,
                               donut_ext=None, lower_left_ext=None, max_ll_donut_ext=None,
                               observed_over_expected_ext=None):
    """
    Divides observed counts by an expected model that accounts for global and local background signal to isolate
    looping signal.

    Parameters
    ----------
    output_dir : str
        Where to save resulting counts files if saving intermediates
    observed_experiment : ExperimentSet
        ExperimentSet object  containing observed counts
    expected_experiment : ExperimentSet
        ExperimentSet object containing global expected counts
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing observed/expected counts
    """
    label_order = observed_experiment.labels
    observed_superdict = observed_experiment.superdict()
    expected_superdict = expected_experiment.superdict()

    observed_counts = [observed_superdict[k] for k in label_order]
    expected_counts = [expected_superdict[k] for k in label_order]

    # propagate nans between observed and expected counts dicts
    print('propagating nans')
    for i in xrange(len(observed_counts)):
        observed_counts[i], expected_counts[i] = md.propagate_nans(observed_counts[i], expected_counts[i])

    # get regional expected donut
    print('calculating donut expected')
    donut_expected = [{region: md.donut_filt(observed_counts[i][region], expected_counts[i][region])
                       for region in observed_counts[0].keys()}
                      for i in xrange(len(observed_counts))]

    # write output
    updated_experiment = copy.deepcopy(observed_experiment)
    updated_experiment.counts_list = donut_expected
    if save_intermediates:
        if donut_ext is None:
            donut_ext = get_output_extension('donut_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=donut_ext)

    # calculate lower left donut
    print('calculating lower left donut expected')
    lower_left_expected = [{region: md.lower_left_filt(observed_counts[i][region], expected_counts[i][region])
                            for region in observed_counts[0].keys()}
                           for i in xrange(len(observed_counts))]

    # write output
    updated_experiment.counts_list = lower_left_expected
    if save_intermediates:
        if lower_left_ext is None:
            lower_left_ext = get_output_extension('lower_left_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=lower_left_ext)

    # calculate max(donut,ll)
    print('calculating max of donut and lower left donut expected')
    max_donut_ll = [{region: np.fmax(donut_expected[i][region], lower_left_expected[i][region])
                     for region in donut_expected[0].keys()}
                    for i in xrange(len(donut_expected))]

    # write output
    updated_experiment.counts_list = max_donut_ll
    if save_intermediates:
        if max_ll_donut_ext is None:
            max_ll_donut_ext = get_output_extension('max_donut_lower_left_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=max_ll_donut_ext)

    # calculate obs/exp
    print('calculating observed/expected ratio')
    obs_over_exp = [{region: np.subtract(observed_counts[i][region], max_donut_ll[i][region])
                     for region in max_donut_ll[0].keys()}
                    for i in xrange(len(max_donut_ll))]

    # write output
    updated_experiment.counts_list = obs_over_exp
    if save_intermediates:
        if observed_over_expected_ext is None:
            observed_over_expected_ext = get_output_extension('observed_over_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=observed_over_expected_ext)

    return updated_experiment


def find_obs_over_max_donut_ll_with_min(observed_experiment, expected_experiment, save_intermediates=True, output_dir=None,
                               donut_ext=None, lower_left_ext=None, max_ll_donut_ext=None,
                               observed_over_expected_ext=None, logged=None,
                               min_counts=0.0):
    """
    Divides observed counts by an expected model that accounts for global and local background signal to isolate
    looping signal.

    Parameters
    ----------
    output_dir : str
        Where to save resulting counts files if saving intermediates
    observed_experiment : ExperimentSet
        ExperimentSet object  containing observed counts
    expected_experiment : ExperimentSet
        ExperimentSet object containing global expected counts
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing observed/expected counts
    """
    label_order = observed_experiment.labels
    observed_superdict = observed_experiment.superdict()
    expected_superdict = expected_experiment.superdict()

    observed_counts = [observed_superdict[k] for k in label_order]
    expected_counts = [expected_superdict[k] for k in label_order]

    # filter out low observed counts
    if min_counts > 0:
        for region in observed_counts[0].keys():
            for i, rep in enumerate(observed_counts):
                observed_counts[i][region][np.where(observed_counts[i][region] < min_counts)] = np.nan

    # propagate nans between observed and expected counts dicts
    print('propagating nans')
    for i in xrange(len(observed_counts)):
        observed_counts[i], expected_counts[i] = md.propagate_nans(observed_counts[i], expected_counts[i])

    # get regional expected donut
    print('calculating donut expected')
    donut_expected = [{region: md.donut_filt(observed_counts[i][region], expected_counts[i][region])
                       for region in observed_counts[0].keys()}
                      for i in xrange(len(observed_counts))]

    # write output
    updated_experiment = copy.deepcopy(observed_experiment)
    updated_experiment.counts_list = donut_expected
    if save_intermediates:
        if donut_ext is None:
            donut_ext = get_output_extension('donut_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=donut_ext)

    # calculate lower left donut
    print('calculating lower left donut expected')
    lower_left_expected = [{region: md.lower_left_filt(observed_counts[i][region], expected_counts[i][region])
                            for region in observed_counts[0].keys()}
                           for i in xrange(len(observed_counts))]

    # write output
    updated_experiment.counts_list = lower_left_expected
    if save_intermediates:
        if lower_left_ext is None:
            lower_left_ext = get_output_extension('lower_left_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=lower_left_ext)

    # calculate max(donut,ll)
    print('calculating max of donut and lower left donut expected')
    max_donut_ll = [{region: np.fmax(donut_expected[i][region], lower_left_expected[i][region])
                     for region in donut_expected[0].keys()}
                    for i in xrange(len(donut_expected))]

    # write output
    updated_experiment.counts_list = max_donut_ll
    if save_intermediates:
        if max_ll_donut_ext is None:
            max_ll_donut_ext = get_output_extension('max_donut_lower_left_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=max_ll_donut_ext)

    # calculate obs/exp
    print('calculating observed/expected ratio')
    if logged is None:
        obs_over_exp = [{region: np.power(2, np.subtract(np.log2(observed_counts[i][region] + 1),
                                                         np.log2(max_donut_ll[i][region] + 1)))-1
                         for region in max_donut_ll[0].keys()}
                        for i in xrange(len(max_donut_ll))]
    else:
        obs_over_exp = [{region: np.subtract(observed_counts[i][region], max_donut_ll[i][region])
                         for region in max_donut_ll[0].keys()}
                        for i in xrange(len(max_donut_ll))]

    # write output
    updated_experiment.counts_list = obs_over_exp
    if save_intermediates:
        if observed_over_expected_ext is None:
            observed_over_expected_ext = get_output_extension('observed_over_expected')
        updated_experiment.save_counts(output_dir=output_dir, ext=observed_over_expected_ext)
