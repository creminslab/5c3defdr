import os

import numpy as np

import diff3DeFDR.common as md
from transformation_name import get_output_extension

# Input file extensions
bed_ext = 'binned_pixelmap'

def bin_smooth_counts(experiment,
                      bin_step=4000, neighbor_radius=8000, wipe_window_width=16000, filter_function=None, logged=2,
                      save_intermediates=True, bed_dir=None, counts_dir=None, compressed=False):
    """
    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing counts to filter
    bin_step : int
        The step size of the bins, in bp.
    neighbor_radius : int
        This is half the width of the smoothing window where nearby fragments
        within this window will be smoothed over by the filter function.
    wipe_window_width : int
        The width of the filtering window in base pairs for the removal of empty bins.
    filter_function : func
        What smoothing filter to apply.
    logged : int
        Base that the output counts will be logged to.
    save_intermediates : bool
        True = Save resulting filtered counts to file in counts_dir and binned pixelmap to bed_dir
    bed_dir :  str
        Where to save resulting binned pixelmap
    counts_dir : str
        Where to save resulting counts files if saving intermediates
    compressed : bool, optional
        Whether to save output countsfiles as compressed .npz files or uncompressed .counts files

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing filtered counts
    """
    print 'bin_and_smooth_counts: Generate new pixelmap to establish bin mapping for all regions'

    pixelmap = {region: md.generate_pixelmap_from_primermap(experiment.mapping[region],
                                                            bin_step,
                                                            region_name=region)
                for region in experiment.counts_list[0].keys()}

    print 'bin_and_smooth_counts: Bin fragment level data'

    if filter_function is None:
        filter_function = md.arithmetic_mean_filter()

    if logged is None:
        binned_counts = [
            {region: md.fragment_to_bin_filter(experiment.counts_list[i][region],
                                               filter_function, pixelmap[region],
                                               experiment.mapping[region],
                                               neighborhood_radius=neighbor_radius)
             for region in experiment.counts_list[0].keys()}
            for i in xrange(len(experiment.counts_list))]
    else:
        binned_counts = [{region: md.fragment_to_bin_filter(np.log(experiment.counts_list[i][region] + 1)/np.log(logged),
                                                            filter_function, pixelmap[region],
                                                            experiment.mapping[region],
                                                            neighborhood_radius=neighbor_radius)
                          for region in experiment.counts_list[0].keys()}
                         for i in xrange(len(experiment.counts_list))]

    print 'bin_and_smooth_counts: Wipe empty bins'

    try:
        binned_counts = [md.wipe_unsmoothable_columns(binned_counts[i],
                                                      experiment.mapping, pixelmap,
                                                      wipe_window_width)
                         for i in xrange(len(binned_counts))]
    except:
        print '--failed'

    if save_intermediates:
        new_bed_name = '{}_{}.bed'.format(experiment.bed_name, bed_ext)
        new_bed_path = os.path.join(bed_dir, new_bed_name)
        updated_experiment = md.ExperimentSet(counts_list=binned_counts,
                                              conditions=experiment.conditions,
                                              mapping=pixelmap,
                                              bed_path=new_bed_path,
                                              labels=experiment.labels)
        updated_experiment.bed_name = new_bed_name


        md.write_pixelmap(pixelmap, new_bed_path)

        if compressed:
            updated_experiment.save_counts_as_npz(output_dir=counts_dir, ext=get_output_extension('observed'))
        else:
            updated_experiment.save_counts(output_dir=counts_dir, ext=get_output_extension('observed'))
    else:
        updated_experiment = md.ExperimentSet(counts_list=binned_counts,
                                              conditions=experiment.conditions,
                                              mapping=pixelmap,
                                              labels=experiment.labels)
    return updated_experiment
