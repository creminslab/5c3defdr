import copy

import numpy as np

import diff3DeFDR.common as md
from transformation_name import get_output_extension


def trim_diagonal(experiment, distance_threshold=20000, wipe_value=np.nan, distance_span='start-to-start',
                  save_intermediates=True, output_dir=None, compressed=False):
    """
    Trim near-diagonal interactions from a set of regional interaction score matrices in an ExperimentSet object.

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing counts to filter
    distance_threshold : int
        Interacting bins whose genomic distance falls below this
        value will be classified as "short-range" and their indices will be wiped.
    wipe_value = any
        The value that will fill in the index containing the short-range
        interaction
    distance_span = str
        Specifies how genomic distance between two interacting bins will be
        determined.
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    output_dir : str, optional
        Where to save resulting counts files if saving intermediates
    compressed : bool, optional
        Whether to save output countsfiles as compressed .npz files or uncompressed .counts files

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing filtered counts
    """
    interaction_scores = experiment.counts_list

    print('trim_diagonal: trimming diagonal')
    diag_trimmed = []
    for rep in interaction_scores:
        trimmed_rep = {}
        for region in rep:
            trimmed_rep[region],_ = md.wipe_short_range_interactions(
                rep[region], experiment.mapping[region],
                distance_span=distance_span, wipe_value=wipe_value, distance_threshold=distance_threshold)
        diag_trimmed.append(trimmed_rep)

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts(diag_trimmed)

    if save_intermediates:
        if compressed:
            updated_experiment.save_counts_as_npz(output_dir, ext=get_output_extension('trimmed_interaction_scores'))
        else:
            updated_experiment.save_counts(output_dir, ext=get_output_extension('trimmed_interaction_scores'))
    return updated_experiment
