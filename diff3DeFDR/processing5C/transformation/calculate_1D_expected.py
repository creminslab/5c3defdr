import copy

import numpy as np

import diff3DeFDR.common as md
from transformation_name import get_output_extension

def calculate_1D_expected(experiment, logged=2, is_global=True, save_intermediates=True, output_dir=None,
                          compressed=True):
    """
    Computes a bin-level global expected model from observed counts.

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object  containing observed counts for which we will compute a global expected counts matrix
    logged : int
        Base that the input counts have been logged to. If they have not been, set to None. Output counts will be
        logged to the same base.
    is_global : bool
        If True, 1D expected will be global, else it will be regional.
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    output_dir : str, optional
        Where to have output counts to if save_intermediates is enabled.
    compressed : bool, optional
        Whether to save output countsfiles as compressed .npz files or uncompressed .counts files

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing global expected counts
    """
    observed_counts = experiment.counts_list
    regions = observed_counts[0].keys()

    if logged is not None:
        observed_counts = [{region: np.power(logged, c[region]) - 1  for region in regions}
                           for c in observed_counts]

    print 'calculate_1D_expected: Precomputing bin-level 1D expected model (global={})'.format(is_global)

    # Expected is computed in log space, but resulting expected is returned unlogged
    expected_counts = [md.make_expected_matrix_binned(c,
                                                      md.lowess_empirical_model_bins,
                                                      is_global=is_global,
                                                      log_base=2)
                       for c in observed_counts]

    # Now we log it if we would prefer
    if logged is not None:
        expected_counts = [{region: np.log(c[region] + 1)/np.log(logged) for region in regions}
                           for c in expected_counts]

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts(expected_counts)

    if save_intermediates:
        if compressed:
            updated_experiment.save_counts_as_npz(output_dir=output_dir, ext=get_output_extension('global_expected'))
        else:
            updated_experiment.save_counts(output_dir=output_dir, ext=get_output_extension('global_expected'))

    return updated_experiment

