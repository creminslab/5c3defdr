import copy

import diff3DeFDR.common as md


def nonconditional_qnorm(experiment, save_intermediates=True, output_dir=None, save_ext='qn'):
    """
    Quantile normalize counts.

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing counts to normalized
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    output_dir : str, optional
        Where to save resulting counts files if saving intermediates
    save_ext : str, optional

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing normalized counts
    """
    trimmed_primermap = experiment.mapping
    trimmed_counts_superdict = experiment.superdict()

    print('qnorm: quantile normalizing')
    qnormed_counts_superdict = md.qnorm_counts_superdict(trimmed_counts_superdict, trimmed_primermap,
                                                         regional=True)

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts([qnormed_counts_superdict[l] for l in experiment.labels])
    updated_experiment.counts_ext = save_ext

    # write output
    if save_intermediates:
        updated_experiment.save_counts(output_dir=output_dir, ext=save_ext)

    return updated_experiment