import os
from copy import deepcopy


from diff3DeFDR.common import ExperimentSet, trim_matrix, trim_primers, write_primermap


def trim_augmented_primers_and_counts(experiment, min_sum=10.0, min_frac=None, handpicked_ranges=[],
                                      save_intermediates=True, output_dir=None, save_ext='trimmed',
                                      extra_column_names=['length', '% GC']):
    """
    Wrapper method for trimming bad primers as part of the Beagan et al 2017 pre-processing pipeline.

    Parameters
    ----------
    experiment : ExperimentSet
        ExperimentSet object containing counts to be trimmed. experiment.mapping is expected to be "augmented" with
        extra columns specifying primer length and GC content.
    min_sum : float, optional
        Primers with a total cis sum lower than this value will be trimmed. Value is passed to
        ``diff3DeFDr.common.trim_primers``.
    min_frac : float, optional
        Primers with fewer than this fraction of nonzero interactions out of all
        their finite interactions will be trimmed. Value is passed to ``diff3DeFDr.common.trim_primers``.
    handpicked_ranges : dict, optional
        Drop specific primers. Pass in the primer numbers of the primers you would like to drop in a given region.
        e.g. {"Nanog-V2": "206-", "Olig1-Olig2": "152,193,219"} will drop all primers numbered 206 and greater in
        the region Nanog-V2 and drop primers numbered 152, 193, and 219 in region Olig1-Olig2.
    save_intermediates : bool
        True = Save resulting trimmed counts and primermap to file in output_dir
    output_dir : str, optional
        Where to save resulting primermap and counts files if saving intermediates
    save_ext : str
        What suffix to save trimmed countsfiles with. What name the trimmed primermap will be saved under.
    Returns
    -------
    trimmed_exp : ExperimentSet
        ExperimentSet object containing trimmed counts matrices and corresponding trimmed primermap
    """

    counts_superdict = experiment.superdict()
    primermap = experiment.mapping

    trimmed_primermap = {}
    trimmed_indices = {}
    for region in primermap.keys():
        trimmed_primermap[region], trimmed_indices[region] = \
            trim_primers(primermap[region],
                         {rep: counts_superdict[rep][region] for rep in counts_superdict},
                         min_sum=min_sum, min_frac=min_frac)

    for region in handpicked_ranges:
        if region in primermap.keys():
            range_parts = handpicked_ranges[region].split(',')
            to_remove = []
            for r in range_parts:
                if r[-1] == '-':
                    start = int(r[:-1])
                    to_remove.extend(range(start,trimmed_primermap[region][-1]['number']+1))
                elif r[0] == '-':
                    end = int(r[1:])
                    to_remove.extend(range(0, end+1))
                else:
                    to_remove.append(int(r))
            region_trimmed_primermap = deepcopy(trimmed_primermap[region])
            trimmed_region_indices = []

            for i, primer in enumerate(trimmed_primermap[region]):
                if primer['number'] in to_remove:
                    region_trimmed_primermap.remove(primer)
                    trimmed_region_indices.append(i)
            trimmed_primermap[region] = region_trimmed_primermap
            trimmed_indices[region] = set(trimmed_region_indices)

    trimmed_counts_superdict = {rep: {} for rep in counts_superdict}
    for rep in counts_superdict.keys():
        for region in counts_superdict[rep].keys():
            trimmed_counts_superdict[rep][region] = trim_matrix(counts_superdict[rep][region],
                                                                trimmed_indices[region])

    # Save and return trimmed data
    trimmed_experiment = ExperimentSet(counts_list=[trimmed_counts_superdict[l] for l in experiment.labels],
                                       conditions=experiment.conditions,
                                       mapping=trimmed_primermap,
                                       labels=experiment.labels)
    if save_intermediates:
        trimmed_experiment.save_counts(output_dir, ext=save_ext)
        write_primermap(trimmed_primermap, os.path.join(output_dir, save_ext + '.bed'),
                        extra_column_names=extra_column_names)

    return trimmed_experiment