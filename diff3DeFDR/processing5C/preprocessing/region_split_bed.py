def region_split_bed(input_bed_path, output_bed_path, keep_regions, splits):
    """
        Split or drop regions from a primermap.

        Parameters
        ----------
        input_bed_path : str
            Path to input primermap
        output_bed_path : str
            Path to output primermap
        keep_regions : list[str]
            Regions to keep in matrices.
        splits : dict[list[dict]]
            List of 'splittings' to perform. Each element is dict specifying the current region in the matrix,
            the new name for the region, and the span of bins in the original region that will be given the new
            region name.
        """

    input = open(input_bed_path, 'r')
    output = open(output_bed_path, 'w')

    for line in input:
        # skip comments
        if line.startswith('#'):
            printout = line.strip('\n')
        else:
            temp = line.strip('\n')
            pieces = line.strip().split('\t')
            chr = pieces[0]
            start = int(pieces[1])
            end = int(pieces[2])
            primer_name = pieces[3]
            primer_name_pieces = primer_name.split('_')
            region = primer_name_pieces[2]
            bin_pieces = primer_name_pieces[4].split(':')
            bin = int(bin_pieces[0])
            printout = ''
            if region in keep_regions:
                if region in splits:
                    for change in splits[region]:
                        lower, upper = change['range']
                        if bin >= lower and bin <= upper:
                            new_region_name = change['new']
                            primer_name_pieces[2] = new_region_name
                            bin_pieces[0] = str(bin - lower)
                            bin_name = ':'.join(bin_pieces)
                            primer_name_pieces[4] = bin_name
                            primer_name = '_'.join(primer_name_pieces)
                            pieces[3] = primer_name
                            printout = '\t'.join(pieces)
                else:
                    primer_name = '_'.join(primer_name_pieces)
                    pieces[3] = primer_name
                    printout = '\t'.join(pieces)
        if printout != '':
            print >> output, "%s" % (printout)
    input.close()
    output.close()
