import csv
import pandas as pd


def convert_primer_sequence_xlsx_to_tsv(xlsx_path, tsv_path):
    """
    Convert downloaded excel file to tsv.
    """
    primer_list = pd.read_excel(xlsx_path, skiprows=[0, 1, 2], header=None)

    with open(tsv_path, 'wb') as f:
        fwriter = csv.writer(f, delimiter='\t')
        for i in xrange(0, len(primer_list), 2):
            primer_name = primer_list.loc[i, 0][1:]
            primer_sequence = primer_list.loc[i + 1, 0]
            fwriter.writerow([primer_name, primer_sequence])