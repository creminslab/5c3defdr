import ntpath
import os

from region_split_bed import region_split_bed
from region_split_counts import region_split_counts


def region_split(bed_path, counts_paths, output_dir, ext='rs', keep_regions=['Sox2', 'Klf4', 'Olig1-Olig2'], splits=None):
    """
    Split or drop regions from a primermap and its counts matrices. This is a pre-processing step specific to the
    Beagan et al. 2016 dataset.

    Parameters
    ----------
    bed_path : str
        Path to input bed file
    counts_paths : list[str]
        Path to input counts files
    output_dir : str
        Directory to put new bed and counts files in, counts file-names will be appended with the
    extension : str
        Suffix to add to the names of the output counts files. Defaults to '_rs'.
    keep_regions : list[str]
        Regions to keep in matrices.
    splits : dict[list[dict]]
        List of 'splittings' to perform. Each element is dict specifying the current region in the matrix,
        the new name for the region, and the span of bins in the original region that will be given the new
        region name.

    Returns
    -------
    Full paths to new bed file and counts files
    """
    input_bed_fname = ntpath.basename(bed_path).split('.')[0]
    output_bed_path = '{}_{}.bed'.format(os.path.join(output_dir, input_bed_fname), ext)
    output_counts_paths = ['{}_{}.counts'.format(os.path.join(output_dir, ntpath.basename(count_path).split('.')[0]),
                                                 ext)
                           for count_path in counts_paths]
    region_split_bed(input_bed_path=bed_path, output_bed_path=output_bed_path, keep_regions=keep_regions, splits=splits)
    region_split_counts(input_counts_paths=counts_paths, output_counts_paths=output_counts_paths,
                        keep_regions=keep_regions, splits=splits)

    return output_bed_path, output_counts_paths
