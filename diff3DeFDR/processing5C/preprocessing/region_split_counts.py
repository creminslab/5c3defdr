def region_split_counts(input_counts_paths, output_counts_paths, keep_regions, splits):
    """
    Split or drop regions from a set of regional counts matrices.

    Parameters
    ----------
    input_counts_paths : list[str]
        List of paths to input counts matrices
    output_counts_paths : list[str]
        Corresponding list of output paths for resulting matrices
    keep_regions : list[str]
        Regions to keep in matrices.
    splits : dict[list[dict]]
        List of 'splittings' to perform. Each element is dict specifying the current region in the matrix,
        the new name for the region, and the span of bins in the original region that will be given the new
        region name.
    """


    # Iterate through each of counts file
    for i in xrange(len(input_counts_paths)):
        input_file = open(input_counts_paths[i], 'r')
        output = open(output_counts_paths[i], 'w')

        for line in input_file:
            if line.startswith('#'):
                continue
            else:
                temp = line.strip().split('\t')
                primer_1 = temp[0]
                primer_2 = temp[1]
                count = temp[2]
                primer_1_pieces = primer_1.split('_')
                primer_2_pieces = primer_2.split('_')
                bin_1_pieces = primer_1_pieces[4].split(':')
                bin_2_pieces = primer_2_pieces[4].split(':')
                bin_1 = int(bin_1_pieces[0])
                bin_2 = int(bin_2_pieces[0])
                region = primer_1_pieces[2]

            printout = ''
            if region in keep_regions:
                if region in splits:
                    for change in splits[region]:
                        lower, upper = change['range']
                        if bin_1 >= lower and bin_1 <= upper and bin_2 >= lower and bin_2 <= upper:
                            new_region_name = change['new']
                            primer_1_pieces[2] = new_region_name
                            primer_2_pieces[2] = new_region_name

                            bin_1_pieces[0] = str(bin_1 - lower)
                            bin_2_pieces[0] = str(bin_2 - lower)
                            bin_1_name = ':'.join(bin_1_pieces)
                            bin_2_name = ':'.join(bin_2_pieces)
                            primer_1_pieces[4] = bin_1_name
                            primer_2_pieces[4] = bin_2_name

                            primer_1_name = '_'.join(primer_1_pieces)
                            primer_2_name = '_'.join(primer_2_pieces)

                            printout = '%s\t%s\t%s' % (primer_1_name, primer_2_name, count)
                else:
                    printout = '%s\t%s\t%s' % (primer_1, primer_2, count)
            if printout != '':
                print >> output, printout
        input_file.close()
        output.close()