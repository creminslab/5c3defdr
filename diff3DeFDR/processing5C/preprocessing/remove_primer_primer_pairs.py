import copy

import diff3DeFDR.common as md


def remove_primer_primer_pairs(output_dir, experiment, save_intermediates=True, count_threshold=2, num_reps=1):
    """
    Remove low-counts primer-primer pairs.

    output_dir : str
        Where to save resulting counts files if saving intermediates
    experiment : ExperimentSet
        ExperimentSet object containing counts to filter
    save_intermediates : bool
        True = Save resulting filtered counts to file in output_dir
    count_threshold : int
        Minimum number of counts at a primer-primer pair.
    num_reps : int
        Over how many replicates these below threshold counts must occur for the primer-primer pair to be wiped.

    Returns
    -------
    updated_experiment : ExperimentSet
        ExperimentSet object containing filtered counts
    """
    print('remove_primer_primer_pairs: Removing primer-primer pairs with low counts')

    counts_superdict = {experiment.labels[i]: c for i, c in enumerate(experiment.counts_list)}
    trimmed = md.remove_primer_primer_pairs(counts_superdict,
                                            experiment.mapping,
                                            count_threshold=count_threshold,
                                            num_reps=num_reps)

    updated_experiment = copy.deepcopy(experiment)
    updated_experiment.set_counts([trimmed[l] for l in experiment.labels])

    if save_intermediates:
        updated_experiment.save_counts(output_dir=output_dir, ext='rpp')

    return updated_experiment

