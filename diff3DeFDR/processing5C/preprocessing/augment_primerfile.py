import argparse

from diff3DeFDR.common import load_primermap, write_primermap


def parse_primer_sequences(primer_sequence_file, chop_tails=True):
    primer_sequence_dict = {}
    with open(primer_sequence_file, 'r') as handle:
        for line in handle:
            name, sequence = line.strip().split('\t')
            if chop_tails:
                if 'FOR' in name:
                    sequence = sequence[20:]
                elif 'REV' in name:
                    sequence = sequence[:-21]
            primer_sequence_dict[name] = sequence
    return primer_sequence_dict


def compute_gc_content(sequence):
    num_c = sequence.count('C') + sequence.count('c')
    num_g = sequence.count('G') + sequence.count('g')
    return (num_c + num_g) / float(len(sequence))


def augment_primerfile(infile, sequencefile, outfile):
    """
    Add columns for GC content and primer length to a primer file.
    """
    # parse input files
    primermap = load_primermap(infile)
    primer_sequence_dict = parse_primer_sequences(sequencefile)

    # augment primermap
    for region in primermap.keys():
        for i in range(len(primermap[region])):
            primermap[region][i]['length'] = primermap[region][i]['end'] - \
                                             primermap[region][i]['start']
            primermap[region][i]['% GC'] = compute_gc_content(
                primer_sequence_dict[primermap[region][i]['name']])

    # write primermap
    write_primermap(
        primermap, outfile, extra_column_names=['length', '% GC'])

def main():
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('sequencefile')
    parser.add_argument('outfile')
    args = parser.parse_args()

    augment_primerfile(args.infile, args.sequencefile, args.outfile)


if __name__ == '__main__':
    main()
