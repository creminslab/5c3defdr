import os

#from memory_profiler import profile
from matplotlib import pyplot as plt
import numpy as np
from scipy.stats import zscore
from scipy.optimize import curve_fit
from statsmodels.nonparametric.smoothers_lowess import lowess

import diff3DeFDR.common as dc


def joint_express_normalize(dataset, outfile=None):
    qnormed_counts_superdict = dataset.counts('counts', fill_value=np.nan)
    reps = qnormed_counts_superdict.keys()
    regions = qnormed_counts_superdict[reps[0]].keys()

    # pre-compute fragment expected model
    expected_counts_superdict = {rep: dc.make_expected_matrix_fragment(
        qnormed_counts_superdict[rep], dataset.pixelmap, dc.polyfit_model_fragments, is_global=False) for rep in reps}

    # reshape
    reshaped_observed_counts = {region: [qnormed_counts_superdict[rep][region] for rep in reps] for region in regions}
    reshaped_expected_counts = {region: [expected_counts_superdict[rep][region] for rep in reps] for region in regions}

    # joint normalize
    print 'joint_express_on_conditional_qnormed'
    raw_balanced_counts, _, _ = dc.joint_express_normalize(reshaped_observed_counts, reshaped_expected_counts)

    # reshape
    joint_express_counts_superdict = {rep: {region: raw_balanced_counts[region][i] for region in regions}
                                      for i, rep in enumerate(reps)}

    for rep in reps:
        dataset.add_column_from_counts(joint_express_counts_superdict[rep], ['joint_express', rep])

    if outfile:
        dataset.save(outfile)

    return dataset


def bin_and_smooth(dataset, outfile=None, bin_step=4000, neighbor_radius=8000, wipe_window_width=16000,
                   filter_function=None, logged=2):
    counts_superdict = dataset.counts('joint_express', fill_value=np.nan)
    reps = counts_superdict.keys()
    regions = counts_superdict[reps[0]].keys()

    primermap = dataset.pixelmap
    pixelmap = {region: dc.generate_pixelmap_from_primermap(primermap[region], bin_step, region_name=region)
                for region in regions}

    if filter_function is None:
        filter_function = dc.arithmetic_mean_filter()

    binned_counts_superdict = {
        {region: dc.fragment_to_bin_filter(
            counts_superdict[rep][region] if logged is None else np.log(counts_superdict[rep][region] + 1),
            filter_function, pixelmap[region], primermap[region], neighborhood_radius=neighbor_radius)
         for region in regions} for rep in reps}

    try:
        binned_counts_superdict = {rep: dc.wipe_unsmoothable_columns(
            binned_counts_superdict[rep], primermap, pixelmap, wipe_window_width) for rep in reps}
    except:
        print '--failed'

    dataset = dataset.from_counts_superdict(binned_counts_superdict, pixelmap, name='counts', repinfo=dataset.repinfo)

    if outfile:
        dataset.save(outfile)

    return dataset


def expected(dataset, outfile=None, logged=2, is_global=True):
    if logged is not None:
        dataset.df['unlogged_counts'] = np.power(logged, dataset.df['counts']) - 1
    counts_superdict = dataset.counts('counts' if logged is None else 'unlogged_counts', fill_value=np.nan)
    reps = counts_superdict.keys()
    regions = counts_superdict[reps[0]].keys()

    # Expected is computed in log space, but resulting expected is returned unlogged
    expected_counts_superdict = {rep: dc.make_expected_matrix_binned(
        counts_superdict[rep], dc.lowess_empirical_model_bins, is_global=is_global, log_base=2) for rep in reps}

    # Now we log it if we would prefer
    if logged is not None:
        expected_counts_superdict = {rep: {region: np.log(expected_counts_superdict[rep][region] + 1) / np.log(logged)
                                           for region in regions} for rep in reps}

    for rep in reps:
        dataset.add_column_from_counts(expected_counts_superdict[rep], ['global_expected', rep])

    if outfile:
        dataset.save(outfile)

    return dataset


def observed_over_expected(dataset, outfile=None):
    # propagate nans between observed and expected counts columns
    nan_mask = np.logical_or(np.isnan(dataset.df['counts']), np.isnan(dataset.df['expected']))
    dataset.df.loc[nan_mask, 'expected'] = np.nan
    dataset.df.loc[nan_mask, 'counts'] = np.nan

    # pull down from dataset
    obs_counts_superdict = dataset.counts('counts', fill_value=np.nan)
    exp_counts_superdict = dataset.counts('expected', fill_value=np.nan)
    reps = obs_counts_superdict.keys()
    regions = obs_counts_superdict[reps[0]].keys()

    # get regional expected donut
    print('calculating donut expected')
    donut_expected_superdict = {rep: {region: dc.donut_filt(
        obs_counts_superdict[rep][region], exp_counts_superdict[rep][region]) for region in regions} for rep in reps}

    for rep in reps:
        dataset.add_column_from_counts(donut_expected_superdict[rep], ['donut_expected', rep])
    if outfile:
        dataset.save(outfile)

    # calculate lower left donut
    print('calculating lower left donut expected')
    lower_left_expected_superdict = {rep: {region: dc.lower_left_filt(
        obs_counts_superdict[rep][region], exp_counts_superdict[rep][region]) for region in regions} for rep in reps}

    for rep in reps:
        dataset.add_column_from_counts(lower_left_expected_superdict[rep], ['lower_left_expected', rep])
    if outfile:
        dataset.save(outfile)

    # calculate max(donut,ll)
    print('calculating max of donut and lower left donut expected')
    max_donut_ll_superdict = {rep: {region: np.fmax(donut_expected_superdict[rep][region],
                                                    lower_left_expected_superdict[rep][region])
                                    for region in regions} for rep in reps}

    for rep in reps:
        dataset.add_column_from_counts(max_donut_ll_superdict[rep], ['max_donut_lower_left_expected', rep])
    if outfile:
        dataset.save(outfile)

    # calculate obs/exp
    print('calculating observed/expected ratio')
    obs_over_exp_superdict = {rep: {region: np.subtract(obs_counts_superdict[rep][region],
                                                        max_donut_ll_superdict[rep][region])
                                    for region in regions} for rep in reps}

    for rep in reps:
        dataset.add_column_from_counts(obs_over_exp_superdict[rep], ['observed_over_expected', rep])
    if outfile:
        dataset.save(outfile)
    return dataset


def logistic_pvalues(dataset, output_dir, outfile=None):
    print('get_logistic_pvalues')

    column_name = 'observed_over_expected'
    counts_superdict = dataset.counts(column_name, fill_value=np.nan)
    reps = counts_superdict.keys()
    regions = counts_superdict[reps[0]].keys()

    verb_path = 'Rscript'
    script_path = os.path.join(os.path.split(__file__)[0], 'find_logistic_pvalues.R')
    job_labels = []
    cmds = []
    pvalues_ext = 'pvalues'
    outfiles = []
    for rep in reps:
        file_path = '{}/{}{}.counts'.format(output_dir, rep, '_{}'.format(column_name))
        dc.write_counts(counts_superdict[rep], file_path, dataset.pixelmap)

        outfile = '{}/{}{}.counts'.format(output_dir, rep, '_{}'.format(pvalues_ext))
        outfiles.append(outfile)
        job_labels.append('get ' + outfile)
        cmds.append([verb_path, script_path, file_path, outfile])

    dc.job_management(job_labels, cmds, parallelized=False)

    pvalues_superdict = {}
    for i, outfile in enumerate(outfiles):
        pvalues_superdict[reps[i]] = dc.load_counts(outfile, dataset.pixelmap)

    dataset = dataset.from_counts_superdict(pvalues_superdict, dataset.pixelmap, name='pvalues', repinfo=dataset.repinfo)
    if outfile:
        dataset.save(outfile)
    return dataset


def interaction_score(dataset, outfile=None):
    dataset.df['is'] = -10 * np.log2(dataset.df['pvalues'])
    if outfile:
        dataset.save(outfile)
    return dataset


def main():
    this_files_dir = '/Volumes/LF-Slim/3DeFDR/gb_resubmission/diff3defdr/generate_submission_figures/make_figures/generate_data_to_plot'
    input_dir = os.path.join(this_files_dir, 'input')
    output_dir = os.path.join(this_files_dir, 'output')
    intermediates_dir = os.path.join(this_files_dir, 'intermediates')

    repinfo_path = os.path.join(input_dir, 'rep_info.tsv')
    real_transforms_dir = dc.check_dir(os.path.join(intermediates_dir, 'transforms_dir'))

    exp_preprocessed = dc.load_counts_into_experimentset(
        bed_path=os.path.join(real_transforms_dir, 'trimmed.bed'),
        counts_path=real_transforms_dir,
        repinfo_path=repinfo_path,
        counts_ext='qn')
    dataset = exp_preprocessed.to_DataSet()

    # Transform counts
    dataset = joint_express_normalize(dataset, outfile=os.path.join(intermediates_dir, 'dataset_jen'))
    dataset = bin_and_smooth(dataset, outfile=os.path.join(intermediates_dir, 'dataset_obs'))
    dataset = expected(dataset, outfile=os.path.join(intermediates_dir, 'dataset_global_expected'))
    dataset = observed_over_expected(dataset, outfile=os.path.join(intermediates_dir, 'dataset_obs_over_max_donut_ll'))
    dataset = logistic_pvalues(dataset, output_dir, outfile=os.path.join(intermediates_dir, 'dataset_pvalues'))
    dataset = interaction_score(dataset, outfile=os.path.join(intermediates_dir, 'dataset_int_scores'))

if __name__ == '__main__':
    main()