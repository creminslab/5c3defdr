import os

import glob

from diff3DeFDR.common import load_counts_into_experimentset
from diff3DeFDR.processing5C.preprocessing import augment_primerfile, convert_primer_sequence_xlsx_to_tsv, region_split, \
    trim_augmented_primers_and_counts, conditional_qnorm


def beagan2017_dataset_preprocessing(input_dir, primer_sequence_xlsx_path, output_dir):
    """
    Perform pre-processing specific to Beagan et al. 2017 data set.

    Parameters
    ----------
    input_dir : str
        Directory containing raw counts matrices to be pre-processed
    primer_sequence_xlsx_path : str
        Path to excel file containing the sequences of our primer set, from which we can compute GC content in order
        to perform quantile normalization conditionally on GC content as the last step of this pre-processing pipeline.
    output_dir : str
        Where to save pre-processed countsfiles and primermap.

    Return
    ------
    qn_exp : ExperimentSet
        ExperimentSet object of preprocessed counts.

    """
    print 'Performing dataset specific pre-processing.'

    # Convert primer sequences excel file (from Philips-Cremins et al. 2013 Table 2) to csv file
    print '  Loading primer sequences file.'
    primer_sequence_tsv_path = os.path.join(input_dir, 'primer_sequences.tsv')
    convert_primer_sequence_xlsx_to_tsv(xlsx_path=primer_sequence_xlsx_path, tsv_path=primer_sequence_tsv_path)

    # Create augmented primer-map
    print '  Adding primer length and GC content to primer-map file.'
    raw_data_dir = os.path.join(input_dir, 'raw_5C_data')
    raw_bed_path = glob.glob(os.path.join(raw_data_dir, '*.bed'))[0]
    augmented_bed_path = os.path.join(raw_data_dir, 'augmented_primermap.bed')
    augment_primerfile(infile=raw_bed_path, sequencefile=primer_sequence_tsv_path, outfile=augmented_bed_path)

    # Split large regions into smaller regions
    print '  Splitting large regions into smaller ones.'
    keep_regions = ['Sox2', 'Klf4', 'Olig1-Olig2', 'Nestin', 'Oct4', 'gene-desert', 'Nanog-V2']
    split_ext = 'rs'
    splits = {'Oct4': [{'range':[0, 260], 'new':'Oct4a'}, {'range':[300, 407], 'new':'Oct4b'}]}
    split_bed_path, _ = region_split(bed_path=augmented_bed_path,
                                     counts_paths=glob.glob(os.path.join(raw_data_dir, '*.counts')),
                                     output_dir=output_dir,
                                     ext=split_ext,
                                     keep_regions=keep_regions,
                                     splits=splits)

    # Trim bad primers
    print '  Trimming bad primers.'
    repinfo_path = os.path.join(input_dir, 'rep_info.tsv')
    split_exp = load_counts_into_experimentset(bed_path=split_bed_path,
                                               counts_path=output_dir,
                                               repinfo_path=repinfo_path,
                                               counts_ext=split_ext)
    trimmed_exp = trim_augmented_primers_and_counts(
        split_exp, min_sum=10.0, min_frac=None, handpicked_ranges={"Nanog-V2": "207-", "Olig1-Olig2": "152,193,219"},
        save_intermediates=True, output_dir=output_dir, save_ext='trimmed')

    # Apply conditional quantile normalization to a reference counts matrix
    print '  Conditional quantile normalization.'
    qn_exp = conditional_qnorm(trimmed_exp, output_dir=output_dir)

    return qn_exp
