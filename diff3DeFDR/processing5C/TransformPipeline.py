import os
import sys
from os.path import join

import glob
import imp

import diff3DeFDR.common as md
import transformation as tm


class TransformPipeline(object):

    def __init__(self, experiment, working_dir, compressed=False, name_parser=None):
        """
        Initiator method for 5C Processing Pipeline class. Runs default version pipeline upon object creation.

        Attributes
        ----------
        experiment: ExperimentSet
            ExperimentSet object containing pre-preprocessed counts to be transformed to interaction scores via the
            Beagan et al. 2017 variant of our 5C processing pipeline
        working_dir : str
            Where to save both intermediate transformations and final interaction score countsfiles
        load_map_kwargs : dict, optional
            Optional kwargs that might be needed to load preprocessed or raw counts from file
        compressed : bool, optional
            Whether to save output transformed counts as compressed .npz files or uncompressed .counts files
        """
        assert type(experiment) == md.ExperimentSet

        self.experiment = experiment
        self.working_dir = working_dir
        self.name_parser = name_parser
        self.compressed = compressed

        self._submission_pipeline()

    @staticmethod
    def load_intermediate_exp(working_dir, transform_type, **kwargs):
        """
        Loads an intermediate transformation of the input data from file into ExperimentSet object

        Parameters
        ----------
        working_dir : str
            Directory from which to load previously computed transformed counts from file
        intermediate_type : str
            Valid options include 'joint_express', 'observed', 'global_expected', 'donut_expected',
            'lower_left_expected', 'max_donut_lower_left_expected', 'observed_over_expected', 'pvalues',
            'interaction_scores', and 'trimmed_interaction_scores'.

        Return
        ------
        intermediate_exp: ExperimentSet
            ExperimentSet object containing counts transformed into the given intermediate data type.
        """
        assert tm.valid_transformation_name(transform_type)

        ext = tm.get_output_extension(transform_type)
        bed_type = tm.get_plotting_name(transform_type)

        bed_path = glob.glob(os.path.join(working_dir, '*.bed' if bed_type == 'raw' else '*binned*.bed'))[0]

        if bed_type == 'raw':
            if kwargs and 'name_parser' in kwargs:
                name_parser = kwargs['name_parser']
                del kwargs['name_parser']
            else:
                name_parser = md.default_primer_name_parser
        else:
            name_parser = md.default_bin_name_parser

        intermediate_exp = md.load_counts_into_exp(bed_path=bed_path,
                                                   counts_path=working_dir,
                                                   counts_ext=ext,
                                                   name_parser=name_parser)
        return intermediate_exp

    def _load_intermediate_exp(self, intermediate_type):
        return TransformPipeline.load_intermediate_exp(self.working_dir, intermediate_type,
                                                       name_parser=self.name_parser)

    def _submission_pipeline(self):
        '''
        Runs 5C Processing pipeline designed for processing Beagan et al's Genome Research data set.
        '''

        # (1) Apply joint express normalization
        jen_exp = tm.joint_express_on_exp(self.experiment, output_dir=self.working_dir, compressed=self.compressed)
        # jen_exp = self._load_intermediate_exp('joint_express')

        # (2) Bin and smooth counts => Observed counts
        obs_exp = tm.bin_smooth_counts(jen_exp, bed_dir=self.working_dir, counts_dir=self.working_dir,
                                       compressed=self.compressed)
        # obs_exp = self._load_intermediate_exp('observed')

        # (3) Compute expected counts
        expected_exp = tm.calculate_1D_expected(obs_exp, output_dir=self.working_dir, compressed=self.compressed)

        # # (4) Compute observed/expected counts
        tm.find_obs_over_max_donut_ll(observed_experiment=obs_exp, expected_experiment=expected_exp,
                                      output_dir=self.working_dir)

        # (5) Get p-values representing significance of interaction
        print('find_logistic_pvalues.R: Running this Rscript')
        tm.get_logistic_pvalues(self.working_dir,
                                observed_over_expected_ext=tm.get_output_extension('observed_over_expected'),
                                pvalues_ext=tm.get_output_extension('pvalues'))

        # TODO Would really prefer to call this Rscript in a way that returns these counts matrices
        pvalue_exp = self._load_intermediate_exp('pvalues')

        # (6) Compute interaction scores as -10*log2(p-value)
        is_exp = tm.get_interaction_scores(pvalue_exp, output_dir=self.working_dir, compressed=self.compressed)

        # (7) Trim on/near-diagonal points
        is_trimmed_exp = tm.trim_diagonal(is_exp, output_dir=self.working_dir, compressed=self.compressed)

        self.final_experiment = is_trimmed_exp


if __name__ == '__main__':
    print sys.argv[1:]
    bed_path = sys.argv[1]
    working_dir = sys.argv[2]
    ext = sys.argv[3]
    compressed = sys.argv[4] == 'True'

    name_parser_name = sys.argv[5] if len(sys.argv) > 5 else None
    name_parser_path = sys.argv[6] if len(sys.argv) > 5 else None
    name_parser = imp.load_source(name_parser_name, name_parser_path).name_parser if name_parser_path else None

    experiment = md.load_counts_into_exp(bed_path=bed_path, counts_path=working_dir, counts_ext=ext,
                                         name_parser=name_parser)
    TransformPipeline(experiment=experiment, working_dir=working_dir, compressed=compressed, name_parser=name_parser)
