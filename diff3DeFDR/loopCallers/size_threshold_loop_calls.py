import copy
import numpy as np

from diff3DeFDR.common import make_clusters


def size_threshold_loop_calls(dataset, calls, min_cluster_size=4):
    """
    Given a list of loop classifications, identifies clusters of adjacent of the same class, and returns list of
    loop calls filtered to eliminate pixels that are part of clusters including fewer than min_cluster_size pixels.

    Parameters
    ----------
    dataset : pandas.DataSet
        DataSet carrying information about pixel mapping
    calls :  pandas.Series
        List of differential loop calls for each bin
    min_cluster_size : int
        Minimum size of clusters to include final filtered loop call list

    Returns
    -------
    filtered_calls : pandas.Series
        Resulting filtered list of differential loop calls
    clusters : list[list[dict]]
        Resulting filtered list of clusters of differential loop calls
        Data structure is list of clusters, each of which is a list of loops, each of which is dictionary carrying
        the loop classification and names of the bins looping together
    """
    filtered_calls = copy.deepcopy(calls)
    classes_to_cluster = calls[calls.notnull()].unique()

    # Reformat DataFrame of loop calls into a matrix format in prep for spatial clustering of like calls
    reformatted_calls = {}
    for x in calls[calls.notnull()].index:
        left_name, right_name = dataset._split_index(x)
        region, left_index = dataset.reverse_pixelmap[left_name]
        other_region, right_index = dataset.reverse_pixelmap[right_name]
        loop_class = calls[x]

        if loop_class in classes_to_cluster :
            if region not in reformatted_calls:
                reformatted_calls[region] = []
            reformatted_calls[region].append({'x': left_index, 'y': right_index, 'color': loop_class, 'value': 1,
                                              'bin_i': left_name, 'bin_j': right_name})

    clusters = {}
    for region in reformatted_calls:
        all_clusters = np.array(make_clusters(reformatted_calls[region]))
        is_large = [len(c) >= min_cluster_size for c in all_clusters]
        clusters[region] = all_clusters[is_large]
        print '{}: num clusters = {}'.format(region, len(clusters[region]))

        small_clusters = all_clusters[np.logical_not(is_large)]
        for c in small_clusters:
            for l in c:
                filtered_calls['_'.join([l['bin_i'], l['bin_j']])] = np.nan

    return filtered_calls, clusters