import os
import copy

import numpy as np

from diff3DeFDR.common import check_dir, check_outdir, DataSet, ExperimentSet, get_differential_loop_class_names, \
    parse_repinfo
from diff3DeFDR.loopCallers.util import size_threshold_loop_calls, stats

def classify_loops_with_benchmark_pvalues(output_dir,
                                          exp_postprocessed,
                                          benchmark_data_path,
                                          fdr_threshold,
                                          background_threshold,
                                          significance_threshold,
                                          min_cluster_size):
    """
    Given a list of differential looping p-values created with ''diff3DeFDR.loopCallers.benchmarks.benchmark_stats.py'',
    returns a list of differential loop calls by applying FDR, background, significance, and cluster size thresholding
    in the same manner as 3DeFDR.

    Parameters
    ----------
    output_dir : str
        Path at which to save loop calls as a DataSet
    exp_postprocessed : ExperimentSet
        ExperimentSet object containing replicate counts matrices.
    benchmark_data_path : str
        Path to benchmark p-values created with ''diff3DeFDR.loopCallers.benchmarks.benchmark_stats.py''
    fdr_threshold : float
        FDR threshold to apply to differential looping q-values.
    background_threshold : float
        Background looping threshold (in p-value units).
    significance_threshold : float
        Significance threshold (in p-value units).
    min_cluster_size : int
        Minimum size of loop call clusters to include final loop call list. Specifically, After loop   classifications
        are obtained by applying other threshodls, adjacent pixels of the same  class are clustered together. Pixels
        included in clusters smaller than this size are clipped from list of loop classifications to obtain the final
        filtered list of loop calls.
    """
    # Load benchmarks. Mark same points background as those of 3DeFDR calls. Apply same FDR threshold.
    benchmark_label = os.path.basename(benchmark_data_path)
    dataset = DataSet.load(os.path.join(benchmark_data_path, benchmark_label))

    # Apply thresholds to benchmark's BH p-values
    all_below_sig = np.all(dataset.df['pvalue'] > significance_threshold, axis=1)
    all_below_background = np.all(dataset.df['pvalue'] > background_threshold, axis=1)
    all_above_sig = np.all(dataset.df['pvalue'] < significance_threshold, axis=1)
    any_above_sig = ~all_below_sig

    # Assign differential class if point passes thresholds, else list as constitutive, background, or uncalled (n.s.)
    differential_classes = get_differential_loop_class_names(exp_postprocessed.unique_conditions)
    has_differential_color = dataset.df.isin(differential_classes)['color']
    has_significantly_differential_qvalue = dataset.df['differential_qvalue'] <= fdr_threshold
    nonpass_mask = np.logical_and(np.logical_and(any_above_sig, has_differential_color),
                                  ~has_significantly_differential_qvalue)
    const_mask = np.logical_and(~has_significantly_differential_qvalue, all_above_sig)

    dataset.df.loc[all_below_sig, 'color'] = np.nan
    dataset.df.loc[all_below_background, 'color'] = 'background'
    dataset.df.loc[nonpass_mask, 'color'] = np.nan
    dataset.df.loc[const_mask, 'color'] = 'constitutive'
    dataset.df.loc[dataset.df['color'] == 'uncalled', 'color'] = np.nan
    dataset.df.loc[dataset.df['color'] == 'below_significance', 'color'] = np.nan
    dataset.df.loc[dataset.df['color'] == 'n.s.', 'color'] = np.nan

    # Size-threshold clusters of classified loops
    calls = dataset.df['color']
    filtered_calls, _ = size_threshold_loop_calls(dataset, calls, min_cluster_size=min_cluster_size)
    dataset.df['color'] = np.nan
    dataset.df['color'] = filtered_calls

    # Save data-set object compatible with GoldStandardPlotter
    suffix = ['dataset_benchmark={}',
              'fdr-threshold={}',
              'background-threshold={}',
              'significance-threshold={}',
              'min-cluster-size={}']
    save_name = '_'.join(suffix).format(benchmark_label, fdr_threshold, background_threshold, significance_threshold,
                                        min_cluster_size)
    dataset_filename = check_outdir(os.path.join(output_dir, 'classification_results', save_name, 'dataset.tsv'))
    DataSet.save_set(dataset, dataset_filename)


def generate_benchmark_pvalues(input_dir, output_dir, exp_is, pvalue_transform='is'):
    """
    A wrapper for use of ExperimentSet objects of counts with benchmark_stats.py
    Computes ANOVA and LRT differential looping p-values over two different transformations of modeled p-values
    (interaction score and interaction z-score) and saves them to file in {benchmark_data_dir}.

    Input modeled p-values are copied to {benchmark_data_dir}/input (as saved DataSet objects) for convenience.
    Output benchmark p-values are saved to {benchmark_data_dir}/output (as saved DataSet objects).

    Parameters
    ----------
    input_dir : str
        Input directory to retrieve replicate info from
    output_dir : str
        Path to write p-values to
    exp_is : ExperimentSet
        ExperimentSet of interaction score values for a real replicate set
    """
    check_dir(output_dir)
    assert type(exp_is) == ExperimentSet

    # Convert input diagonal trimmed interaction scores to a format compatible with benchmark_stats.py
    exp_pval = copy.deepcopy(exp_is)
    exp_pval.counts_list = [{r: 2 ** (i[r] / -10.0) for r in i.keys()}
                            for i in exp_is.counts_list]

    repinfo = parse_repinfo(os.path.join(input_dir, 'rep_info.tsv'))
    d = DataSet.from_counts_superdict(counts_superdict=exp_pval.superdict(),
                                      pixelmap=exp_is.mapping,
                                      repinfo=repinfo,
                                      name='pvalue')

    input_data_path = os.path.join(output_dir, 'input', 'logistic_pvalues')
    d.save(input_data_path)

    # Generate differential looping pvalues and qvalues using each benchmarking approach
    results_dir = check_dir(os.path.join(output_dir, 'output'))
    for stat_type in ['ANOVA', 'LRT']:
        state_transform_combo = '_'.join([stat_type, pvalue_transform])
        stat_path = os.path.join(results_dir, state_transform_combo, state_transform_combo)
        stats(dataset_filename=input_data_path,
                        outfile=stat_path,
                        test_statistic=stat_type,
                        transform=pvalue_transform,
                        aggregator='min',
                        null_model=None,
                        adaptive=False)