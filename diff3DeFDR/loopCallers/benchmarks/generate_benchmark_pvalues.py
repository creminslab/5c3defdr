import os
import copy

from diff3DeFDR.common import ExperimentSet, check_dir, DataSet, parse_repinfo
from benchmark_stats import benchmark_stats


def generate_benchmark_pvalues(input_dir, output_dir, exp_is, pvalue_transform='is'):
    """
    A wrapper for use of ExperimentSet objects of counts with benchmark_stats.py
    Computes ANOVA and LRT differential looping p-values over two different transformations of modeled p-values
    (interaction score and interaction z-score) and saves them to file in {benchmark_data_dir}.

    Input modeled p-values are copied to {benchmark_data_dir}/input (as saved DataSet objects) for convenience.
    Output benchmark p-values are saved to {benchmark_data_dir}/output (as saved DataSet objects).

    Parameters
    ----------
    input_dir : str
        Input directory to retrieve replicate info from
    output_dir : str
        Path to write p-values to
    exp_is : ExperimentSet
        ExperimentSet of interaction score values for a real replicate set
    """
    check_dir(output_dir)
    assert type(exp_is) == ExperimentSet

    # Convert input diagonal trimmed interaction scores to a format compatible with benchmark_stats.py
    exp_pval = copy.deepcopy(exp_is)
    exp_pval.counts_list = [{r: 2 ** (i[r] / -10.0) for r in i.keys()}
                            for i in exp_is.counts_list]

    repinfo = parse_repinfo(os.path.join(input_dir, 'rep_info.tsv'))
    d = DataSet.from_counts_superdict(counts_superdict=exp_pval.superdict(),
                                      pixelmap=exp_is.mapping,
                                      repinfo=repinfo,
                                      name='pvalue')

    input_data_path = os.path.join(output_dir, 'input', 'logistic_pvalues')
    d.save(input_data_path)

    # Generate differential looping pvalues and qvalues using each benchmarking approach
    results_dir = check_dir(os.path.join(output_dir, 'output'))
    for stat_type in ['ANOVA', 'LRT']:
        state_transform_combo = '_'.join([stat_type, pvalue_transform])
        stat_path = os.path.join(results_dir, state_transform_combo, state_transform_combo)
        benchmark_stats(dataset_filename=input_data_path,
                        outfile=stat_path,
                        test_statistic=stat_type,
                        transform=pvalue_transform,
                        aggregator='min',
                        null_model=None,
                        adaptive=False)