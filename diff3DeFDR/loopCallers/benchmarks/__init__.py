"""
Parametric approaches for performing differential looping analysis.
Used to benchmark the performance of 3DeFDR.
"""

from benchmark_stats import *
from generate_benchmark_pvalues import *
from classify_loops_with_benchmark_pvalues import *