import os

import numpy as np

from diff3DeFDR.common import check_outdir, DataSet, get_differential_loop_class_names
from diff3DeFDR.loopCallers.size_threshold_loop_calls import size_threshold_loop_calls


def classify_loops_with_benchmark_pvalues(output_dir,
                                          exp_postprocessed,
                                          benchmark_data_path,
                                          fdr_threshold,
                                          background_threshold,
                                          significance_threshold,
                                          min_cluster_size):
    """
    Given a list of differential looping p-values created with ''diff3DeFDR.loopCallers.benchmarks.benchmark_stats.py'',
    returns a list of differential loop calls by applying FDR, background, significance, and cluster size thresholding
    in the same manner as 3DeFDR.

    Parameters
    ----------
    output_dir : str
        Path at which to save loop calls as a DataSet
    exp_postprocessed : ExperimentSet
        ExperimentSet object containing replicate counts matrices.
    benchmark_data_path : str
        Path to benchmark p-values created with ''diff3DeFDR.loopCallers.benchmarks.benchmark_stats.py''
    fdr_threshold : float
        FDR threshold to apply to differential looping q-values.
    background_threshold : float
        Background looping threshold (in p-value units).
    significance_threshold : float
        Significance threshold (in p-value units).
    min_cluster_size : int
        Minimum size of loop call clusters to include final loop call list. Specifically, After loop   classifications
        are obtained by applying other threshodls, adjacent pixels of the same  class are clustered together. Pixels
        included in clusters smaller than this size are clipped from list of loop classifications to obtain the final
        filtered list of loop calls.
    """
    # Load benchmarks. Mark same points background as those of 3DeFDR calls. Apply same FDR threshold.
    benchmark_label = os.path.basename(benchmark_data_path)
    dataset = DataSet.load(os.path.join(benchmark_data_path, benchmark_label))

    # Apply thresholds to benchmark's BH p-values
    all_below_sig = np.all(dataset.df['pvalue'] > significance_threshold, axis=1)
    all_below_background = np.all(dataset.df['pvalue'] > background_threshold, axis=1)
    all_above_sig = np.all(dataset.df['pvalue'] < significance_threshold, axis=1)
    any_above_sig = ~all_below_sig

    # Assign differential class if point passes thresholds, else list as constitutive, background, or uncalled (n.s.)
    differential_classes = get_differential_loop_class_names(exp_postprocessed.unique_conditions)
    has_differential_color = dataset.df.isin(differential_classes)['color']
    has_significantly_differential_qvalue = dataset.df['differential_qvalue'] <= fdr_threshold
    nonpass_mask = np.logical_and(np.logical_and(any_above_sig, has_differential_color),
                                  ~has_significantly_differential_qvalue)
    const_mask = np.logical_and(~has_significantly_differential_qvalue, all_above_sig)

    dataset.df.loc[all_below_sig, 'color'] = np.nan
    dataset.df.loc[all_below_background, 'color'] = 'background'
    dataset.df.loc[nonpass_mask, 'color'] = np.nan
    dataset.df.loc[const_mask, 'color'] = 'constitutive'
    dataset.df.loc[dataset.df['color'] == 'uncalled', 'color'] = np.nan
    dataset.df.loc[dataset.df['color'] == 'below_significance', 'color'] = np.nan
    dataset.df.loc[dataset.df['color'] == 'n.s.', 'color'] = np.nan

    # Size-threshold clusters of classified loops
    calls = dataset.df['color']
    filtered_calls, _ = size_threshold_loop_calls(dataset, calls, min_cluster_size=min_cluster_size)
    dataset.df['color'] = np.nan
    dataset.df['color'] = filtered_calls

    # Save data-set object compatible with GoldStandardPlotter
    suffix = ['dataset_benchmark={}',
              'fdr-threshold={}',
              'background-threshold={}',
              'significance-threshold={}',
              'min-cluster-size={}']
    save_name = '_'.join(suffix).format(benchmark_label, fdr_threshold, background_threshold, significance_threshold,
                                        min_cluster_size)
    dataset_filename = check_outdir(os.path.join(output_dir, 'classification_results', save_name, 'dataset.tsv'))
    DataSet.save_set(dataset, dataset_filename)