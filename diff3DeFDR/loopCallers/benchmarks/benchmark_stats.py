import argparse
import os
import itertools

import numpy as np
import scipy.stats as stats
from scipy.special import erfinv
from statsmodels.sandbox.stats.multicomp import multipletests


from diff3DeFDR.common import DataSet
from diff3DeFDR.plotters import filter_near_diagonal, filter_by_min_max_pvalue, epvalue


def is_lrt_stat(is_matrix, reps_per_condition=2):
    """
    Computes the LRT statistic.

    Assumes that the columns of is_matrix are ordered by condition and that
    there are the same number of reps in each condition.

    Parameters
    ----------
    is_matrix : np.ndarray
        Columns are replicates, rows are FFLJs. Entries are IS values.
    reps_per_condition : int
        Number of reps in each condition.

    Returns
    -------
    vector
        The vector of LRT statistics. Same length as is_matrix.
    """
    # precompute a term that shows up in many subsequent equations
    erfinv_term = erfinv(2**(1 - (is_matrix/10)) - 1)

    # prepare MLE parameter estimates
    mu_0 = np.mean(np.sqrt(2) * erfinv_term, axis=1)[:, np.newaxis]
    mu_1 = np.repeat(
        [np.mean(np.sqrt(2) * erfinv_term[:, i:i+reps_per_condition], axis=1)
         for i in range(0, is_matrix.shape[1], reps_per_condition)],
        reps_per_condition, axis=0).T

    # define IS PDF
    def is_pdf(mu):
        return (np.log(2) / 5) * (2**(-(is_matrix / 10.) - 1)) * \
            np.exp(-(np.sqrt(2) * erfinv_term - mu)**2 / 2 + erfinv_term**2)

    # build likelihood ratio
    numerator = np.sum(np.log(is_pdf(mu_0)), axis=1)
    denominator = np.sum(np.log(is_pdf(mu_1)), axis=1)

    # return test statistic
    return -2 * (numerator - denominator)


def zscore_lrt_stat(zscore_matrix, reps_per_condition=2):
    """
    Computes the LRT statistic.

    Assumes that the columns of zscore_matrix are ordered by condition and that
    there are the same number of reps in each condition.

    Parameters
    ----------
    zscore_matrix : np.ndarray
        Columns are replicates, rows are FFLJs. Entries are z-scores.
    reps_per_condition : int
        Number of reps in each condition.

    Returns
    -------
    vector
        The vector of LRT statistics. Same length as zscore_df.
    """
    # prepare means
    mu_0 = np.mean(zscore_matrix, axis=1)[:, np.newaxis]
    mu_1 = [np.mean(zscore_matrix[:, i:i+reps_per_condition],
                    axis=1)[:, np.newaxis]
            for i in range(0, zscore_matrix.shape[1], reps_per_condition)]

    # build likelihood ratio
    numerator = np.sum(np.log(stats.norm.pdf(zscore_matrix, loc=mu_0)), axis=1)
    denom_parts = [np.sum(np.log(stats.norm.pdf(
        zscore_matrix[:, i:i+reps_per_condition],
        loc=mu_1[i/reps_per_condition])), axis=1)
        for i in range(0, zscore_matrix.shape[1], reps_per_condition)]

    # return test statistic
    return -2 * (numerator - reduce(np.add, denom_parts))


def max_d_stat(zscore_matrix, agg='min', reps_per_condition=2):
    """
    Computes the max_d statistic.

    Assumes that the columns of zscore_matrix are ordered by condition and that
    there are the same number of reps in each condition.

    Parameters
    ----------
    zscore_matrix : np.ndarray
        Columns are replicates, rows are FFLJs. Entries are z-scores or any kind
        of suitably transformed p-values (e.g., interaction scores).
    agg : {'min', 'mean'}
        The aggregating function to use within conditions.
    reps_per_condition : int
        Number of reps in each condition.

    Returns
    -------
    vector
        The vector of max_d statistics. Same length as zscore_df.
    """
    # resolve agg
    agg = np.min if agg == 'min' else np.mean

    # aggregate zscores by condition
    aggregated_zscores = [
        agg(zscore_matrix[:, i:i + reps_per_condition], axis=1)[:, np.newaxis]
        for i in range(0, zscore_matrix.shape[1], reps_per_condition)
    ]

    # prepare combinations of condition indices
    combs = np.array(list(
        itertools.combinations(range(len(aggregated_zscores)), 2)))

    # collect differences
    differences = np.hstack([np.abs(aggregated_zscores[combs[i][0]] -
                                    aggregated_zscores[combs[i][1]])
                             for i in range(len(combs))])

    # return max_d
    return np.max(differences, axis=1)


def anova_stat(zscore_matrix, reps_per_condition=2):
    """
    Performs ANOVA on each row of a zscore matrix.

    Assumes that the columns of zscore_matrix are ordered by condition and that
    there are the same number of reps in each condition.

    Parameters
    ----------
    zscore_matrix : np.ndarray
        Columns are replicates, rows are FFLJs. Entries are z-scores.
    reps_per_condition : int
        Number of reps in each condition.

    Returns
    -------
    vector
        The vector of ANOVA F-statistics. Same length as zscore_matrix.
    """
    return stats.f_oneway(
        *[zscore_matrix[:, i:i+reps_per_condition].T
          for i in range(0, zscore_matrix.shape[1], reps_per_condition)])[0]


def anova_pvalue(zscore_matrix, reps_per_condition=2):
    """
    Performs ANOVA on each row of a zscore matrix.

    Assumes that the columns of zscore_matrix are ordered by condition and that
    there are the same number of reps in each condition.

    Parameters
    ----------
    zscore_matrix : np.ndarray
        Columns are replicates, rows are FFLJs. Entries are z-scores.
    reps_per_condition : int
        Number of reps in each condition.

    Returns
    -------
    vector
        The vector of ANOVA p-values. Same length as zscore_matrix.
    """
    return stats.f_oneway(
        *[zscore_matrix[:, i:i+reps_per_condition].T
          for i in range(0, zscore_matrix.shape[1], reps_per_condition)])[1]


def bh_mtc(pvalue_matrix):
    """
    Performs Benjamini-Hochberg multiple testing correction on a matrix of
    p-values.

    Parameters
    ----------
    pvalue_matrix : np.ndarray
        The matrix of p-values, representing many tests that were performed.

    Returns
    -------
    np.ndarray
        The matrix of BH-adjusted p-values. Same size and shape as
        pvalue_matrix.
    """
    padj_matrix = np.zeros_like(pvalue_matrix) * np.nan
    padj_matrix[np.isfinite(pvalue_matrix)] = \
        multipletests(pvalue_matrix[np.isfinite(pvalue_matrix)], 0.1,
                      method='fdr_bh')[1]
    return padj_matrix


def determine_colors(matrix, conditions):
    """
    Determines the color of pixels using a logic thought to be equivalent to
    3DeFDR.

    Assumes exactly three conditions and equal numbers of replicates in each
    condition.

    Parameters
    ----------
    matrix : np.ndarray
        The rows are pixels, the columns are measurements in each replicate. The
        columns must be sorted by condition.
    conditions : list of str
        The condition names as strings, in the order of the columns of matrix.

    Returns
    -------
    np.ndarray
        The color categories for the pixels.
    """
    # sanity checks
    if len(conditions) != 3:
        raise ValueError('only supports exactly three conditions')
    if matrix.shape[1] % len(conditions) != 0:
        raise ValueError('unequal numbers of reps across conditions')

    # get number of rows
    n_rows = matrix.shape[0]

    # make conditions an array
    conditions = np.array(conditions, dtype='|S25')

    # infer number of reps per condition
    reps_per_condition = matrix.shape[1] / len(conditions)

    # prepare condition means
    means = np.array(
        [np.mean(matrix[:, i:i + reps_per_condition], axis=1)
         for i in range(0, matrix.shape[1], reps_per_condition)]).T

    # prepare combinations of condition indices
    combs = np.array(list(itertools.combinations(range(len(conditions)), 2)))

    # prepare differences
    diffs = np.array([means[:, c[1]] - means[:, c[0]] for c in combs]).T

    # identify the index of the minimum difference under absolute value
    min_index = np.argmin(np.abs(diffs), axis=1)

    # identify the "surviving" indices
    sig_indices = np.array([
        np.setdiff1d(np.arange(len(conditions)), min_index[i])
        for i in range(n_rows)])

    # make an array of string category names for the significantly up classes
    sig_up_classes = np.array(
        [conditions[combs[i][(diffs[:, i] > 0).astype(int)]]
         for i in range(diffs.shape[1])], dtype='|S25').T[
        np.arange(n_rows)[:, np.newaxis], sig_indices]

    # compile colors
    colors = []
    for i in range(n_rows):
        if sig_up_classes[i, 0] == sig_up_classes[i, 1]:
            colors.append('%s' % sig_up_classes[i, 0])
        else:
            colors.append('%s_%s' % tuple(np.sort(sig_up_classes[i, :])))

    return colors


def common_processing(dataset_filename,
                      transform, test_statistic, aggregator,
                      distance_threshold=None, sig_thresh=None):
    """
    Performs processing steps that are common across ANOVA and LRT, including
    (0) Loads a DataSet from file that contains logistic p-values for each replicate
    (1) Drops near diagonal interactions
    (2) Drops interactions with NaN values for any replicate
    (3) Transforms logistic p-values to z-scores or interaction scores as specified by transform parameter
    (4) Computes a test static of choice on the resulting transformed p-values

    :param dataset_filename: Path to stored DataSet containing p-values
    :param transform: 'zscore' or 'IS' - specifies which transformation of p-values to compute test statistic from
    :param test_statistic: 'ANOVA' or 'LRT' - specifies which test statistic to compute
    :param aggregator: parameter for use with max_d, an experimental test statistic
    :param distance_threshold: How many bins off the diagonal to trim
    :param sig_thresh: used in experimental versions of the code
    :return: DataSet object containing additional columns of test statistic values computed as specified
    """

    print 'loading DataSet'
    d = DataSet.load(dataset_filename)

    print 'total interactions: %i' % len(d.df)

    print 'applying pre-filtering'
    if distance_threshold is not None:
        filter_near_diagonal(d.df, k=distance_threshold)
    # if sig_thresh is not None:
    #     filter_by_min_max_pvalue(d.df, pvalue_threshold=sig_thresh)

    # Drop NaN p-values
    d.df.dropna(how='any', inplace=True)

    print 'surviving interactions: %i' % len(d.df)

    if transform == 'zscore':
        print 'transforming p-values to z-scores'
        d.apply_per_replicate(stats.norm.isf, 'pvalue', 'transformed_pvalue')
    elif transform == 'is':
        print 'transforming p-values to IS'
        d.apply_per_replicate(lambda x: -10 * np.log2(x), 'pvalue', 'transformed_pvalue')
    elif transform == 'mix':
        # Perform stat on z-score but choose loop color with IS
        print 'transforming p-values to z-scores'
        d.apply_per_replicate(stats.norm.isf, 'pvalue', 'transformed_pvalue')

    if test_statistic == 'LRT':
        if transform == 'is':
            print 'computing IS LRT statistics'
            d.df['test_statistic'] = is_lrt_stat(
                d.df['transformed_pvalue'].as_matrix())
        elif transform == 'zscore':
            print 'computing z-score LRT statistics'
            d.df['test_statistic'] = zscore_lrt_stat(
                d.df['transformed_pvalue'].as_matrix())
        elif transform == 'mix':
            print 'computing z-score LRT statistics'
            d.df['test_statistic'] = zscore_lrt_stat(
                d.df['transformed_pvalue'].as_matrix())
        else:
            raise ValueError('transform %s not recognized' % transform)

    elif test_statistic == 'ANOVA':
        print 'computing ANOVA statistics'
        d.df['test_statistic'] = anova_stat(
            d.df['transformed_pvalue'].as_matrix())

    else:
        print 'computing max_d statistics'
        d.df['test_statistic'] = max_d_stat(
            d.df['transformed_pvalue'].as_matrix(), agg=aggregator)

    return d


def benchmark_stats(dataset_filename, outfile, distance_threshold=6, sig_thresh=0.165,
                    test_statistic='max_d', transform='zscore', aggregator='min',
                    null_model=None, adaptive=True):
    """
    Main method for performing differential loop detection with benchmark approaches ANOVA and LRT
    :param dataset_name: Path to stored DataSet containing p-values
    :param outfile: Path at which to store DataSet containing output differential looping q-values
    :param distance_threshold: How many bins off the diagonal to trim to drop near-diagonal interactions from evaluation
    :param sig_thresh: Used in experimental versions of the code
    :param test_statistic: Which test statistic to compute ('ANOVA' or 'LRT')
    :param transform: Which transformation of p-values to compute test statistic from ('zscore' or 'is')
    :param aggregator: parameter for use with max_d, an experimental test statistic
    :param null_model: parameter for use with max_d, an experimental test statistic
    :param adaptive: True = control FDR separately for each differential loop class,
                     False = control FDR across all differential loop calls
    :return: Saves DataSet containing output differential looping q-values to path specified by outfile
    """
    # resolve outdir
    if outfile is None:
        outfile = 'output/%s' % os.path.split(dataset_filename)[1]

    # perform processing through test statistics
    d = common_processing(dataset_filename, transform, test_statistic, aggregator,
                          distance_threshold=distance_threshold, sig_thresh=sig_thresh)

    if null_model is None:
        if test_statistic == 'LRT':
            print 'computing chi2 p-values'
            d.df['differential_pvalue'] = stats.chi2.sf(
                d.df['test_statistic'], df=2)
        elif test_statistic == 'ANOVA':
            print 'computing ANOVA p-values'
            d.df['differential_pvalue'] = anova_pvalue(
                d.df['transformed_pvalue'].as_matrix())
        else:
            raise ValueError('no parametric null defined for max_d statistic')
    else:
        print 'preparing null data'
        d_null = common_processing(null_model, transform, test_statistic, aggregator,
                                   distance_threshold=distance_threshold, sig_thresh=sig_thresh)

        print 'computing empirical p-values'
        d.df['differential_pvalue'] = epvalue(
            d.df['test_statistic'].as_matrix(),
            d_null.df['test_statistic'].as_matrix())

    print 'determining colors'
    if transform == 'mix':
        d.apply_across_replicates(lambda x: -10 * np.log2(x), 'pvalue', 'is')
        d.df['color'] = determine_colors(
            d.df['is'].as_matrix(),
            [d.repinfo['condition'][d.df['is'].columns[i]]
             for i in range(0, len(d.df['is'].columns),
                            len(d.df['is'].columns)
                            / len(d.repinfo['condition'].unique()))])
    else:
        d.df['color'] = determine_colors(
            d.df['transformed_pvalue'].as_matrix(),
            [d.repinfo['condition'][d.df['transformed_pvalue'].columns[i]]
             for i in range(0, len(d.df['transformed_pvalue'].columns),
                            len(d.df['transformed_pvalue'].columns)
                            / len(d.repinfo['condition'].unique()))])

    if adaptive:
        print 'perfoming adaptive multiple testing correction'
        d.df['differential_qvalue'] = np.nan
        for color in d.df['color'].unique():
            d.df.loc[d.df['color'] == color, 'differential_qvalue'] = bh_mtc(
                d.df.loc[d.df['color'] == color, 'differential_pvalue'].as_matrix())
    else:
        print 'perfoming multiple testing correction'
        d.df['differential_qvalue'] = bh_mtc(
            d.df['differential_pvalue'].as_matrix())

    print 'saving DataSet'
    d.save(outfile)


def main():
    # argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset')
    parser.add_argument('-k', '--distance_threshold', type=int, default=6)
    parser.add_argument('-s', '--sig_thresh', type=float, default=0.165)
    parser.add_argument('-t', '--test_statistic', type=str,
                        choices=['max_d', 'LRT', 'ANOVA'], default='max_d')
    parser.add_argument('-x', '--transform', type=str,
                        choices=['is', 'zscore'], default='zscore')
    parser.add_argument('-a', '--aggregator', type=str,
                        choices=['min', 'mean'], default='min')
    parser.add_argument('-n', '--null_model', type=str)
    parser.add_argument('-o', '--outfile', type=str)
    parser.add_argument('-A', '--adaptive', action='store_true')
    args = parser.parse_args()

    benchmark_stats(args.dataset,
                    args.outfile,
                    distance_threshold=args.distance_threshold,
                    sig_thresh=args.sig_thresh,
                    test_statistic=args.test_statistic,
                    transform=args.transform,
                    aggregator=args.aggregator,
                    null_model=args.null_model,
                    adaptive=args.adaptive)
