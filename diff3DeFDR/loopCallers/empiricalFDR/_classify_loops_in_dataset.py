import sys

from itertools import combinations, permutations
import numpy as np
import pandas as pd

from diff3DeFDR.common import DataSet
from diff3DeFDR.loopCallers import size_threshold_loop_calls


class ClassifyLoops(object):
    """
    Class for managing classifying of differential loops via application of an interaction score thresholding
    procedure.
    """
    def __init__(self, dataset, background_threshold=None, significance_threshold=None, difference_threshold=None):
        """
        Constructor. Determines the differential looping class of each bin-level contact  included in dataset and adds
        them as the column 'color' in dataset.df.
        
        Parameters
        ----------
        dataset: DataSet
            Dataset object containing interaction scores for each bin-level contact for each replicate.
        background_threshold: float
            Background looping threshold in p-value units.
        significance_threshold: float
            Significance threshold in p-value units.
        difference_threshold: float
            Threshold on differences in counts across conditions in units of interaction score.
        """

        self.dataset = dataset
        condition_order = dataset.repinfo['condition']
        self.conds = np.unique(condition_order)
        self.cond_pairs = [(ci, cj) for (ci, cj) in combinations(self.conds, 2)]

        # Precompute differences in interaction score across conditions
        iscores = dataset.df['is' if 'is' in dataset.df else 'counts'].dropna(how='any')
        repinfo = dataset.repinfo
        reps_per_condition = {c: repinfo.index[repinfo['condition']==c] for c in self.conds}

        self.cond_min = pd.DataFrame({c: iscores[reps_per_condition[c]].min(axis=1) for c in self.conds})
        self.cond_max = pd.DataFrame({c: iscores[reps_per_condition[c]].max(axis=1) for c in self.conds})

        self.abs_min_differences = pd.DataFrame({(ci, cj): np.abs(self.cond_min[ci] - self.cond_min[cj])
                                                 for (ci, cj) in self.cond_pairs})

        self.min_differences = pd.DataFrame()
        self.max_abs_differences = pd.DataFrame()
        self.min_abs_differences = pd.DataFrame()

        for (a,b) in permutations(self.conds, 2):
            differences = pd.DataFrame()
            for rep_a in reps_per_condition[a]:
                for rep_b in reps_per_condition[b]:
                    differences[rep_a, rep_b] = iscores[rep_a] - iscores[rep_b]


            self.min_differences[a, b] = differences.min(axis=1)
            self.max_abs_differences[a, b] = differences.abs().max(axis=1)
            self.min_abs_differences[a, b] = differences.abs().min(axis=1)

        self.iscores = iscores

        if difference_threshold is not None:
            self.apply_thresholds(background_threshold, significance_threshold, difference_threshold)

    def apply_thresholds(self, background_thresh=0.8, sig_thresh=0.165,  difference_thresh=20):
        """
        Classifies a compiled list of interaction scores as background,
        constitutive, characteristic of only one condition, or or any two way
        combination of conditions.

        Parameters
        ----------
        compiled_interaction_scores : list[list[str]]
            A list of lists containing interaction scores across six replicates for all bin-bin pairs.
        sig_thresh : float
            Interaction scores above this threshold have the possibility to be classified
        difference_thresh : float
            The difference between the replicate interaction scores must be greater than this threshold to be
            classified as replicate specific

        Returns
        -------
        Dict[str, list[list[str]]]
            The keys are the category name, and the values are lists of lists
            of the first and second bin IDs of each categorized bin-bin pair.
        """
        # Convert p-value thresholds to interaction score thresholds
        background_thresh = -10 * np.log2(background_thresh)
        sig_thresh = -10 * np.log2(sig_thresh)

        # Classify each loop represented in interaction scores Dataframe iscores
        color = pd.Series(index=self.iscores.index)

        color[(self.cond_max < background_thresh).all(axis=1)] = 'background'
        color[(self.cond_min >= sig_thresh).all(axis=1) &
              (self.min_abs_differences <= difference_thresh).all(axis=1)] = 'constitutive'

        # Check if loop is differential and high in a single condition
        for ci in self.conds:
            remaining = np.setdiff1d(self.conds, ci)
            above_remaining = pd.DataFrame({ri: self.min_differences[ci, ri] > difference_thresh
                                            for ri in remaining}).all(axis=1)
            is_ci_loop = (self.cond_min[ci] >= sig_thresh).values & above_remaining
            color[is_ci_loop] = ci

        # Check if loop is differential and similarly high in two conditions
        if len(self.conds) > 2:
            for (ci, cj) in self.cond_pairs:
                remaining = np.setdiff1d(self.conds, [ci, cj])[0]
                above_remaining = pd.DataFrame({c: self.min_differences[c, remaining] > difference_thresh
                                                for c in [ci, cj]}).all(axis=1)
                ci_cj_close_enough = (self.max_abs_differences[ci, cj] <= difference_thresh).values
                both_significant = pd.DataFrame({c: self.cond_min[c] >= sig_thresh for c in [ci, cj]}).all(axis=1)
                is_ci_cj_loop = both_significant & ci_cj_close_enough & above_remaining
                color[is_ci_cj_loop] = '_'.join(np.sort([ci, cj]))

        self.recent_calls = color
        self.dataset.df['color'] = color

    def calls_dict(self):
        """
        Format resulting loop calls as a dictionary (for use with clustering code)
        """

        calls = {'background': self.recent_calls.index[self.recent_calls == 'background'],
                 'constitutive': self.recent_calls.index[self.recent_calls == 'constitutive']}

        for c in self.conds:
            calls[c] = self.recent_calls.index[self.recent_calls == c]

        for (ci, cj) in self.cond_pairs:
            class_name = '_'.join(np.sort([ci, cj]))
            calls[class_name] = self.recent_calls.index[self.recent_calls == class_name]

        # for k in calls:
        #     print '# of {} loops called = {}'.format(k, len(calls[k]))
            
        return calls

    def get_filtered_clusters(self, min_cluster_size=4):
        """
        Formats loop classifications results into list of clusters, filtering out clusters under a threshold size.
        
        Parameters
        ----------
        eFDR_results : pandas.Series
            Output list of loop classifications by diff3DeFDR.loopCallers.empiricalFDR.eFDR.control_fdr
        min_cluster_size : int
            The minimum size of clusters of points assigned to the same differential loop calls. This method will
            filter out all smaller clusters.
        
        Returns
        -------
        clusters: list[dict]
            Values are clusters represented as dictionaries
        filtered_calls: pandas.Series
            Identical to input calls, but loops captured in clusters of size < min_cluster_size are set to value NaN.
        """
        self.recent_filtered_calls, self.clusters = size_threshold_loop_calls(
            self.dataset, self.recent_calls, min_cluster_size=min_cluster_size)


if __name__ == '__main__':
    params_path = sys.argv[1]

    params = np.load(params_path, allow_pickle=True)
    set_name = params['set_name'][()]
    dataset_path = params['input_dataset_path'][()]
    output_calls_format = params['output_calls_format'][()]
    background_threshold = params['background_threshold'][()]
    significance_threshold = params['significance_threshold'][()]

    difference_thresholds = [float(sys.argv[2])]

    dataset = DataSet.load(dataset_path)

    # print set_name, difference_thresholds, dataset.df.head(0)
    classifier = ClassifyLoops(dataset)
    for difference_threshold in difference_thresholds:
        # Threshold to call differential loops
        classifier.apply_thresholds(background_thresh=background_threshold,
                                    sig_thresh=significance_threshold,
                                    difference_thresh=difference_threshold)

        # Save calls to file
        save_path = output_calls_format.format(difference_threshold)
        classifier.recent_calls.to_csv(save_path, sep='\t')
