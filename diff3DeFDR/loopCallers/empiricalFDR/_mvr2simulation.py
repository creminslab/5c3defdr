import sys
from diff3DeFDR.loopCallers import SimulationSet

if __name__ == '__main__':
    """
    Helper function to allow command line access to simulation generating function to allow parallel generation of 
    simulated replicates.
    """
    SimulationSet._mvr2simulation(*sys.argv[1:])