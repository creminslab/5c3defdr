"""
Main pipeline for differential looping analysis performed by 3DeFDR
"""

from eFDR import eFDR
from SimulationSet import SimulationSet
from group_by_distance_bins import *
from _postprocess_all_replicate_sets import postprocess_all_replicate_sets