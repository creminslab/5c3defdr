import numpy as np
from scipy.optimize import curve_fit
from statsmodels.nonparametric.smoothers_lowess import lowess

from group_by_distance_bins import group_fragments_by_distance_bins, group_binned_counts_by_distance_bins
from model_counts import nbinom_mvr_equation


def estimate_mvr(counts, distance_bins, is_binned_data=False, fragment_distances=None,
                 loess_smooth=0.5, globally_fit_dispersion=True, outlier_removal_method=None,
                 mean_outlier_limit=3, var_outlier_limit=None):
    """
    Estimate mean-variance relationship across replicates in the same condition.
    
    Parameters
    ----------
    counts : list[dict[str, numpy.ndarray(float)]]
        List of regional counts matrices to model.
    distance_bins : list[list[int]
        List of distance scale bins. A separate dispersion parameter will be fitted to points in each bin.
    is_binned_data : bool
        True = Use if input counts have been previously binned/smoothed.
        False = Use otherwise.
    fragment_distances : dict[str, numpy.ndarray(int)]
        Regional dict of distance between contacts. Will be used to determine which distance scale bin in which to
        place each contact.
    loess_smooth : float, optional
        frac parameter of statsmodels.nonparametric.smoothers_lowess.loess. If set to None, no loess smoothing is
        applied, else fitted dispersion parameters are smoothed with respect to distance scale.
    globally_fit_dispersion: bool
        True = Fit dispersion parameter across all genomic regions together.
        False = Fit separate dispersion parameters to each genomic region.
    outlier_removal_method : func
        Method for removing outlying contacts from consideration when fitting dispersion parameter to contacts captured
        in a single distance scale bin.
    mean_outlier_limit : float, optional
        Parameter to pass to outlier_removal_method
    var_outlier_limit : float, optional
        Parameter to pass to outlier_removal_method
    
    Returns
    -------
    means : dict
        {genomic region: mean counts across samples for each contact in region}
    vars : dict
        {genomic region: variance of counts across samples for each contact in region}
    mvrs : dict
        {distance scale bin, fitted MVR function for bin}
    mean_distance_groups : dict[dict]]
        {genomic region: {distance scale bin: Mean sample counts values captured in bin}}
    dispersion : dict[dict]]
        {genomic region: {distance scale bin: dispersion parameter fitted to counts values captured in bin}}
    smoothed_dispersion : dict[dict]]
        {genomic region: {distance scale bin: smoothed dispersion parameter fitted to counts values captured in bin}}
        This is set to None if loess_smooth is None.
    """
    remove_outliers = (mean_outlier_limit is not None) or (var_outlier_limit is not None)
    means = {}
    vars = {}
    mvrs = {}
    dispersion = {}
    dispersion_smoothed = {}
    mean_distance_groups = {}
    means_in_bin_dict = {}
    vars_in_bin_dict = {}

    # (1) Build mean and variance arrays for each region
    regions = counts[0].keys()
    for region in regions:

        region_list = np.array([rep[region] for rep in counts])  # Build array of reps in given region
        means[region] = np.mean(region_list, axis=0)             # Mean between reps in same region
        vars[region] = np.var(region_list, axis=0, ddof=1)       # Variance between reps in same region
        finite_points = np.isfinite(means[region]) & np.isfinite(vars[region])  # Mask to filter nans and infs

        # (2) Group (mean,var) points by kb-distance bin
        #     mean_distance_groups[region][bin][datatype]
        #     datatype='targets': returns region matrix indices of points in given distance bin
        if is_binned_data:
            mean_distance_groups[region] = group_binned_counts_by_distance_bins(means[region], distance_bins)
        else:
            mean_distance_groups[region] = group_fragments_by_distance_bins(means[region],
                                                                            fragment_distances[region],
                                                                            distance_bins)

        # (3) Estimate mean-variance relationship for each distance bin
        mvrs[region] = {}
        dispersion[region] = {} 
        means_in_bin_dict[region] = {}
        vars_in_bin_dict[region] = {}
        for (lower, upper) in mean_distance_groups[region]:
            in_bin = mean_distance_groups[region][lower, upper]['targets'] & finite_points  # Set infinite values to zero
            means_in_bin_dict[region][lower, upper] = means[region][in_bin]
            vars_in_bin_dict[region][lower, upper] = vars[region][in_bin]

    if globally_fit_dispersion:
        means_in_bin_all_regions = {}
        vars_in_bin_all_regions = {}

        for region in regions:
            for (lower, upper) in means_in_bin_dict[region]:
                if (lower, upper) in means_in_bin_all_regions:
                    means_in_bin_all_regions[lower, upper].append(means_in_bin_dict[region][lower, upper])
                    vars_in_bin_all_regions[lower, upper].append(vars_in_bin_dict[region][lower, upper])
                else:
                    means_in_bin_all_regions[lower, upper] = [means_in_bin_dict[region][lower, upper]]
                    vars_in_bin_all_regions[lower, upper] = [vars_in_bin_dict[region][lower, upper]]

        # De-nest
        for (lower, upper) in means_in_bin_all_regions:
            means_in_bin = np.concatenate(means_in_bin_all_regions[lower, upper])
            vars_in_bin = np.concatenate(vars_in_bin_all_regions[lower, upper])

            # if remove_outliers:
            print 'remove outliers'
            means_in_bin, vars_in_bin, _ = outlier_removal_method(
                means_in_bin, vars_in_bin, mean_outlier_limit=mean_outlier_limit,
                var_outlier_limit=var_outlier_limit)

            if len(means_in_bin) > 0:
                [A], _ = curve_fit(nbinom_mvr_equation, means_in_bin, vars_in_bin)
                for region in regions:
                    dispersion[region][lower, upper] = A
                    if loess_smooth == 0:
                        mvrs[region][lower, upper] = np.poly1d([A, 1, 0])

    else:
        for region in regions:
            for (lower, upper) in means_in_bin_dict[region]:
                means_in_bin = means_in_bin_dict[region][lower, upper]
                vars_in_bin = vars_in_bin_dict[region][lower, upper]

                # if remove_outliers:
                means_in_bin, vars_in_bin, _ = outlier_removal_method(
                    means_in_bin, vars_in_bin, mean_outlier_limit=mean_outlier_limit,
                    var_outlier_limit=var_outlier_limit)

                if len(means_in_bin) > 0:
                    [A], _ = curve_fit(nbinom_mvr_equation, means_in_bin, vars_in_bin)
                    dispersion[region][lower, upper] = A
                    if loess_smooth == 0:
                        mvrs[region][lower, upper] = np.poly1d([A, 1, 0])

    # (4) Loess smooth dispersion vs. distance scale trend (fit different regimes of bins separately)
    if loess_smooth != 0:
        for region in regions:
            dispersion_smoothed[region] = {}

            valid_bins = [ibin for ibin in distance_bins if tuple(ibin) in dispersion[region]]
            ds_dist_avg = np.array([np.mean(ibin) for ibin in valid_bins])
            ds_dispersion = np.array([dispersion[region][l, u] for (l, u) in valid_bins])

            ds_dispersion_smoothed = lowess(ds_dispersion, ds_dist_avg, frac=loess_smooth, return_sorted=False)

            for k, (lower, upper) in enumerate(valid_bins):
                dispersion_smoothed[region][lower, upper] = ds_dispersion_smoothed[k]
                mvrs[region][lower, upper] = np.poly1d([ds_dispersion_smoothed[k], 1, 0])
                # Usage: predicted_y = mvrs[region][lower, upper](x)

    return means, vars, mvrs, mean_distance_groups, dispersion, None if loess_smooth == 0 else dispersion_smoothed
