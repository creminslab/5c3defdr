import numpy as np


def group_fragments_by_distance_bins(counts, fragment_distances, distance_bins):
    """
    Groups elements in a regional counts matrix according to their fragment-fragment distance

    Parameters
    ----------
    counts : np.ndarray
        Matrix of counts to group by fragment-fragment distance
    fragment_distances : np.ndarray
        Corresponding matrix of fragment-fragment distance
    distance_bins : list[(int, int)]
        List of group lists e.g. [(0, 15), (15, 30)] returns two groups, one for points with fragment-fragment distances
        between 0 and 15 and one of points with fragment-fragment distances of 15 to 30.

    Returns
    -------
    groups : dict[(int,int), [dict[str, np.ndarray]]]
        Each inner dict represents one group and has the following structure::

            {
                'targets': np.ndarray,
                'values': np.ndarray
            }

        ``'targets'`` will be boolean arrays of the same size
        and shape as ``matrix``. ``'values'`` will be the values of ``matrix``
        refered to by ``'indices'`` as a one-dimensional array. The outer dict
        maps integer distances to the inner dict representing the group at that
        distance.

    Examples
    --------
    >>> bed_path = '../counts_data/preprocessed/BED_314-ES-NPC-LOCI_mm9_trimmed_rs.bed'
    >>> counts_path = '../counts_data/preprocessed/pNPC_rep1_raw_rs.counts'
    >>> bins, _ = generate_distance_bins()
    >>> primermap = load_primermap(bed_path)
    >>> counts = load_counts(counts_path, primermap)
    >>> regions = counts[0].keys()  # Use constant region order
    >>> fragment_distances = {region: make_distance_matrix(primermap[region]) for region in regions}
    >>> groups = group_fragments_by_distance_bins(counts[regions[0]], fragment_distances[regions[0]], bins)
    """
    groups = {}
    for (lower, upper) in distance_bins:
        indices = (fragment_distances >= lower) & (upper > fragment_distances) & (np.triu(fragment_distances) > 0)
        values = counts[indices]
        groups[lower, upper] = {'targets': indices, 'values': values}
    return groups


def group_binned_counts_by_distance_bins(counts, distance_bins, step_size=4000):
    """
    About the same as ``diff3DeFDR.loopCallers.empiricalFDR.group_fragments_by_distance_bins`` but for binned data
    (e.g. observed, observed over expected, interaction score).

    Examples
    --------
    >>> bed_path = '../counts_data/preprocessed/BED_314-ES-NPC-LOCI_mm9_trimmed_rs.bed'
    >>> counts_path = '../counts_data/preprocessed/pNPC_rep1_raw_rs.counts'
    >>> bins, _ = generate_distance_bins()
    >>> pixelmap = load_pixelmap(bed_path)
    >>> counts = load_counts(counts_path, pixelmap)
    >>> regions = counts[0].keys()  # Use constant region order
    >>> fragment_distances = {region: make_distance_matrix(primermap[region]) for region in regions}
    >>> groups = group_binned_counts_by_distance_bins(counts[regions[0]], fragment_distances[regions[0]], bins)
    """
    # Convert bp bins into bp step bins
    step_bins = []
    max_bin = len(counts)
    for (lower, upper) in distance_bins:
        step_lower = lower/step_size
        step_upper = upper/step_size
        if step_lower > max_bin:
            break
        step_bins += [(int(step_lower), int(step_upper) if step_upper <= max_bin else max_bin)]

    # Compute groupings
    groups = {}
    for (lower, upper) in step_bins:
        indices = _kth_diag_mask(counts, lower, upper-lower)
        groups[lower, upper] = {}
        groups[lower, upper]['targets'] = indices
        groups[lower, upper]['values'] = counts[indices]
    return groups


def _kth_diag_mask(matrix, k, w=1):
    """
    Filter matrix to diagonals beginning k pixels from the matrix diagonal and extending w diagonals outward.
    Only returns diagonals in upper triangle.

    Parameters
    ----------
    matrix : np.ndarray
    k : int
        Starting diagonal (0 -> main diagonal)
    w : int
        Width of diagonal (# of diagonals from k onward to go) to keep

    Returns
    -------
    Filtered matrix including only specified diagonals
    """
    a = np.ones_like(matrix, bool)
    return np.tril(a, k-1+w) & ~np.tril(a, k-1)

