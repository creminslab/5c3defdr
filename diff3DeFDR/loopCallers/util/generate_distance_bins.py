import numpy as np


def generate_distance_bins(bin_dimensions=[[0, 700000, 4000, 12000]], catchall=False):
    """
    Generates distance bins in units of bp.

    Parameters
    ----------
    bin_dimensions : list[list]
        Each element describes what binning (step_size, window_size) to use over what distance scale
        (lower_limit, upper_limit), where distance scales are listed in increasing order
        e.g. [[lower_limit_1, upper_limit_1, step_size_1, window_size_1], ... [lower_limit_n, ...]]
        for a single regime.
    catchall : bool
        If True, a single bin is appended to the output bins list to capture points occurring farther than upper_limit_n

    Returns
    -------
    bins : list[(int, int)]
        List of (lower, upper) limits of individual bins
    regimes : list
        List of same length as bins, contains integer label of the regime number of the corresponding bin
    """
    bins = []
    regimes = []
    for i, (lower_limit, upper_limit, step_size, window_size) in enumerate(bin_dimensions):
        offsets = range(lower_limit, upper_limit, step_size)  # The lower limit of every bin
        uppers = np.array(offsets) + window_size
        regime_bins = zip(offsets, uppers)  # Array of (lower bound, upper bound) for each bin
        bins += regime_bins
        regimes += list(np.ones(len(regime_bins)) * (i + 1))

    if catchall:
        bins += [(upper_limit, np.inf)]  # Append catchall bin to the end of bin list
        regimes += [len(bin_dimensions) + 1]

    return bins, regimes
