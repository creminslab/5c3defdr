import os

#from memory_profiler import profile
from matplotlib import pyplot as plt
import numpy as np
from scipy.stats import zscore
from scipy.optimize import curve_fit
from statsmodels.nonparametric.smoothers_lowess import lowess

from diff3DeFDR.common import check_dir, DataSet, load_counts_into_experimentset


def fragment_midpoint(dataset, fragment_name):
    region, index = dataset.reverse_pixelmap[fragment_name]
    frag = dataset.pixelmap[region][index]
    return np.abs(frag['end'] - frag['start'])/2.0 + frag['start']


def fragment_distance(dataset, fflj_id):
    left_name, right_name = DataSet._split_index(fflj_id)
    left_midpoint = fragment_midpoint(dataset, left_name)
    right_midpoint = fragment_midpoint(dataset, right_name)
    return abs(left_midpoint - right_midpoint)


def fragment_distance_matrix(dataset):
    return np.array([fragment_distance(dataset, i) for i in dataset.df.index])


def regimes_to_bins(regime_list, catchall=False):
    bins = []
    for regime in regime_list:
        bins += distance_bins(*regime)
    if catchall:
        bins += [regime_list[-1][1], np.inf]
    return bins


def distance_bins(start, end, step_size, window_size):
    lower_limits = range(start, end, step_size)
    upper_limits = np.array(lower_limits) + window_size
    return zip(lower_limits, upper_limits)


def bin_by_distance(distance_matrix, distance_bins):
    bin_matrix = np.zeros_like(distance_matrix)
    for i, (lower, upper) in enumerate(distance_bins):
        in_bin = (lower <= distance_matrix) & (distance_matrix < upper)
        bin_matrix[in_bin] = i
    return bin_matrix


def nbinom_mvr_equation(x, a):
    """
    Function used with scipy.optimize.curve_fit to fit y=Ax^2 + x, where x = means between replicates,
    and y = variance
    """
    return a*(x**2) + x


def nbiom_dispersion(counts, outlier_removal=None):
    if len(np.unique(counts)) < 2:
        return np.nan
    m = np.mean(counts, axis=1)
    v = np.var(counts, axis=1, ddof=1)

    if (outlier_removal is not None) & (m.size > 1):
        mask = (zscore(m) < outlier_removal['mean_limit']) | (zscore(v) < outlier_removal['var_limit'])
        m = m[mask]
        v = v[mask]
    return curve_fit(nbinom_mvr_equation, m, v)[0][0]


def apply_per_bin_per_condition(counts_matrix, bin_matrix, reps_per_condition, out_function, kwargs):
    out_matrix = np.zeros_like(counts_matrix)
    unique_bins = np.unique(bin_matrix)
    for bi in unique_bins:
        in_bin = bin_matrix == bi
        for i in xrange(0, counts_matrix.shape[1], reps_per_condition):
            out_matrix[in_bin, i:i+reps_per_condition] = \
                out_function(counts_matrix[in_bin, i:i+reps_per_condition], kwargs)
    return out_matrix

def apply_per_bin(counts_matrix, bin_matrix, out_function, kwargs):
    out_matrix = np.zeros(counts_matrix.shape[0])
    unique_bins = np.unique(bin_matrix)
    for bi in unique_bins:
        in_bin = bin_matrix == bi
        out_matrix[in_bin] = out_function(counts_matrix[in_bin, :], kwargs)
    return out_matrix

def apply_per_bin_per_region_per_condition(counts_matrix, bin_matrix, region_matrix, reps_per_condition, out_function, kwargs):
    out_matrix = np.zeros_like(counts_matrix)
    unique_bins = np.unique(bin_matrix)
    unique_regions = np.unique(region_matrix)
    for bi in unique_bins:
        in_bin = bin_matrix == bi
        for ri in unique_regions:
            in_region = region_matrix == ri
            for i in xrange(0, counts_matrix.shape[1], reps_per_condition):
                mask = np.logical_and(in_bin, in_region)
                if np.any(mask):
                    out_matrix[mask, i:i+reps_per_condition] = \
                        out_function(counts_matrix[mask, i:i+reps_per_condition], kwargs)
    return out_matrix

def apply_per_bin_per_region(counts_matrix, bin_matrix, region_matrix, out_function, kwargs):
    out_matrix = np.zeros(counts_matrix.shape[0])
    unique_bins = np.unique(bin_matrix)
    unique_regions = np.unique(region_matrix)
    for bi in unique_bins:
        in_bin = bin_matrix == bi
        for ri in unique_regions:
            in_region = region_matrix == ri
            mask = np.logical_and(in_bin, in_region)
            if np.any(mask):
                out_matrix[mask] = out_function(counts_matrix[mask, :], kwargs)
    return out_matrix


def smooth_matrix(x_matrix, y_matrix, loess_frac=0):
    return lowess(y_matrix, x_matrix, frac=loess_frac, return_sorted=False, delta=np.ptp(x_matrix)*0.01)


def weighted_variance(m_matrix, v_matrix, dispersion_matrix, alpha=0.5, beta=0.5):
    pred_v_matrix = dispersion_matrix*np.power(m_matrix, 2) + m_matrix
    return v_matrix*alpha + pred_v_matrix*beta


def add_pooled_dispersion(dataset, pooling_regimes, reps_per_condition,
                          per_region=False, loess_frac=0, outlier_removal=None):
    counts_matrix = dataset.df['counts'].as_matrix()
    distance_matrix = fragment_distance_matrix(dataset)
    bin_matrix = bin_by_distance(distance_matrix, regimes_to_bins(pooling_regimes))

    if per_region:
        dataset._add_region_column()
        region_matrix = dataset.df['region'].as_matrix()
        dispersion_matrix = apply_per_bin_per_region_per_condition(
            counts_matrix, bin_matrix, region_matrix, reps_per_condition, nbiom_dispersion, outlier_removal)
    else:
        dispersion_matrix = apply_per_bin_per_condition(
            counts_matrix, bin_matrix, reps_per_condition, nbiom_dispersion, outlier_removal)

    for i in xrange(0, counts_matrix.shape[1], reps_per_condition):
        condition = dataset.repinfo.loc[dataset.df['counts'].columns[i], 'condition']
        dataset.df['dispersion', condition] = dispersion_matrix[:, i]

    if loess_frac:
        for i, condition in enumerate(dataset.df['dispersion'].columns):
            dataset.df['smoothed_dispersion', condition] = \
                smooth_matrix(distance_matrix, dataset.df['dispersion', condition], loess_frac=loess_frac)
    return dataset


def model_counts(dataset, pooling_regimes, per_region=False, loess_frac=0, outlier_removal=None, alpha=0.5, beta=0.5):
    dataset.df.dropna(inplace=True)

    repinfo = dataset.repinfo
    reps_per_condition = repinfo.size/repinfo['condition'].unique().size
    dataset = add_pooled_dispersion(
        dataset, pooling_regimes, reps_per_condition,
        per_region=per_region, loess_frac=loess_frac, outlier_removal=outlier_removal)

    counts_matrix = dataset.df['counts'].as_matrix()
    dispersion_matrix = dataset.df['smoothed_dispersion' if loess_frac else 'dispersion'].as_matrix()

    m_matrix = np.zeros_like(dispersion_matrix)
    v_matrix = np.zeros_like(dispersion_matrix)
    for ci, i in enumerate(xrange(0, counts_matrix.shape[1], reps_per_condition)):
        m_matrix[:, ci] = np.mean(counts_matrix[:, i:i+reps_per_condition], axis=1)
        v_matrix[:, ci] = np.var(counts_matrix[:, i:i+reps_per_condition], axis=1)
    weighted_v_matrix = weighted_variance(m_matrix, v_matrix, dispersion_matrix, alpha=alpha, beta=beta)
    p_matrix = np.divide(m_matrix, weighted_v_matrix)
    n_matrix = np.multiply(m_matrix, np.divide(p_matrix, 1 - p_matrix))

    for i, condition in enumerate(dataset.df['dispersion'].columns):
        dataset.df['mean', condition] = m_matrix[:, i]
        dataset.df['variance', condition] = v_matrix[:, i]
        dataset.df['nbinom_p', condition] = p_matrix[:, i]
        dataset.df['weighted_variance', condition] = weighted_v_matrix[:, i]
        dataset.df['nbinom_n', condition] = n_matrix[:, i]
        dataset.df['nbinom_p', condition] = p_matrix[:, i]
    return dataset


def simulate_counts(dataset, condition):
    n_matrix = dataset.df['nbinom_n', condition] # TODO get for specified condition
    p_matrix = dataset.df['nbinom_p', condition]
    m_matrix = dataset.df['mean', condition]

    valid_p = (0 < p_matrix) & (p_matrix <= 1)

    sim_counts_matrix = np.zeros_like(n_matrix)
    sim_counts_matrix[valid_p] = np.random.negative_binomial(n_matrix[valid_p], p_matrix[valid_p])
    sim_counts_matrix[~valid_p] = np.random.poisson(m_matrix[~valid_p])
    return sim_counts_matrix

#@profile
def main():
    this_files_dir = '/Volumes/LF-Slim/3DeFDR/gb_resubmission/diff3defdr/generate_submission_figures/make_figures/generate_data_to_plot'
    input_dir = os.path.join(this_files_dir, 'input')
    output_dir = os.path.join(this_files_dir, 'output')
    intermediates_dir = os.path.join(this_files_dir, 'intermediates')

    repinfo_path = os.path.join(input_dir, 'rep_info.tsv')
    real_transforms_dir = check_dir(os.path.join(intermediates_dir, 'transforms_dir'))

    exp_preprocessed = load_counts_into_experimentset(
        bed_path=os.path.join(real_transforms_dir, 'trimmed.bed'),
        counts_path=real_transforms_dir,
        repinfo_path=repinfo_path,
        counts_ext='qn')
    dataset = exp_preprocessed.to_DataSet()


    # Fit data
    pooling_regimes = [[0, 150000, 4000, 12000],
                       [150000, 600000, 8000, 24000],
                       [600000, 1000000, 24000, 60000]]
    loess_frac = 0.5
    outlier_removal = {'mean_limit': 0, 'var_limit': 0}

    dataset = model_counts(dataset, pooling_regimes=pooling_regimes, per_region=False,
                           loess_frac=loess_frac, outlier_removal=outlier_removal)


    # Plot model
    # Fano factor v. distance scale
    dataset.df['fragment_distance'] = fragment_distance_matrix(dataset)
    bins = regimes_to_bins(pooling_regimes)
    dataset.df['bin_index'] = bin_by_distance(dataset.df['fragment_distance'], bins)

    plt.close()
    for bi, bin in enumerate(bins):
        bin_mask = dataset.df['bin_index'] == bi

        plt.plot(bin, dataset.df.loc[bin_mask, ('dispersion','es2i')][0:2], '.-', c='red', alpha=0.8, linewidth=0.75)
        plt.plot(bin, dataset.df.loc[bin_mask, ('dispersion','esserum')][0:2], '.-', c='blue', alpha=0.8, linewidth=0.75)
        plt.plot(bin, dataset.df.loc[bin_mask, ('dispersion','npc')][0:2], '.-', c='green', alpha=0.8, linewidth=0.75)

        plt.plot(bin, dataset.df.loc[bin_mask, ('smoothed_dispersion', 'es2i')][0:2], '.-', c='lightcoral', alpha=0.8, linewidth=0.75)
        plt.plot(bin, dataset.df.loc[bin_mask, ('smoothed_dispersion', 'esserum')][0:2], '.-', c='lightblue', alpha=0.8, linewidth=0.75)
        plt.plot(bin, dataset.df.loc[bin_mask, ('smoothed_dispersion', 'npc')][0:2], '.-', c='lightgreen', alpha=0.8, linewidth=0.75)

    plt.show()

    # Generate simulated counts
    null_condition = 'npc'
    num_sims = 18

    for i in xrange(num_sims):
        dataset.df['sim', 'npc_rep{}'.format(i)] = simulate_counts(dataset, null_condition)

if __name__ == '__main__':
    main()






