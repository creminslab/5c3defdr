import sys

import numpy as np
import pandas as pd

def compute_eFDR(real_calls_path, null_calls_paths, estimate_output_path, differential_classes):
    """
    Compute an empirical false discovery rate (eFDR) for each differential loop class given lists of differential
    loop calls on real datasets and null datasets.

    Parameters (in order)
    ----------
    real_calls_path : str
       Path to the list of loop calls on the real/experimental data set.
    null_calls_paths : list[str]
       Paths to the lists of loop calls from all null replicate sets (the number of loop called in each
       differential class will be averaged from multiple sets if multiple are matched)
    estimate_output_path : str
       Path at which to save a dict of the eFDR estimates computed for each loop class.
    differential_classes : list[str]
        List of differential class names to compute eFDR values for.

    Returns
    ----------
    Saves eFDR estimates for each differential loop class as an dict[str, float] to file as an .npz.
    """

    real_calls = pd.read_csv(real_calls_path, index_col=0, sep='\t', squeeze=True)

    num_calls_null = {lc: 0 for lc in differential_classes}
    for null_calls_path in null_calls_paths:
        null_calls = pd.read_csv(null_calls_path, index_col=0, sep='\t', squeeze=True)
        for lc in differential_classes:
            num_calls_null[lc] += np.sum(null_calls == lc)

    num_null_sets = len(null_calls_paths)
    fdr_dict = {}
    for lc in differential_classes:
        num_calls_real = float(np.sum(real_calls == lc))
        avg_calls_null = float(num_calls_null[lc]) / num_null_sets
        fdr_dict[lc] = avg_calls_null / num_calls_real if num_calls_real > 0 else np.nan

    np.savez(estimate_output_path, fdr_dict=fdr_dict)

if __name__ == '__main__':
    params_path = sys.argv[1]

    params = np.load(params_path, allow_pickle=True)
    real_calls_path = params['real_calls_path'][()]
    null_calls_paths = list(params['null_calls_paths'][()])
    estimate_output_path = params['estimate_output_path'][()]
    differential_classes = list(params['differential_classes'][()])

    compute_eFDR(real_calls_path, null_calls_paths, estimate_output_path, differential_classes)
