import os
import sys
import shutil

from copy import deepcopy
import configparser
import imp
import json
import numpy as np
from shutil import copyfile


import diff3DeFDR as dr

class RunFromConfig():
    def __init__(self, config_path):
        # Make output directories
        working_dir = os.path.dirname(config_path)
        intermediates_dir = dr.common.check_dir(os.path.join(working_dir, 'intermediates'))
        output_dir = dr.common.check_dir(os.path.join(working_dir, 'output'))
        transforms_dir = dr.common.check_dir(os.path.join(intermediates_dir, 'transformed_counts'))
        sims_dir = dr.common.check_dir(os.path.join(intermediates_dir, 'simulated_transformed_counts'))

        # Load input parameters from config file
        config = configparser.ConfigParser()
        config.read(config_path)

        countsfiles = json.loads(config['RawCounts']['countsfiles'])
        countsfiles = {label: os.path.join(working_dir, countsfiles[label]) for label in countsfiles}
        conditions = json.loads(config['RawCounts']['conditions'])
        primerfile = os.path.join(working_dir, str(config['PrimerFile']['primerfile']))

        name_parser = str(config['PrimerFile']['nameparser'])
        if name_parser == 'None':
            name_parser = None
        else:
            name_parser = imp.load_source('name_parser', os.path.join(working_dir, name_parser))
            name_parser = name_parser.name_parser

        compressed = config.getboolean('GeneralProcessing', 'compressed')
        parallelized = config.getboolean('GeneralProcessing', 'parallelized')
        transformOrder = np.array(config['GeneralProcessing']['transformOrder'].split(','))

        # Copy input into transformed_counts directory
        copyfile(primerfile, os.path.join(transforms_dir, os.path.basename(primerfile)))
        for label in countsfiles:
            dst = os.path.join(transforms_dir, os.path.basename(label + '.counts'))
            copyfile(countsfiles[label], dst)
            countsfiles[label] = dst

        # Set instance variables
        self.primerfile = primerfile
        self.name_parser = name_parser
        self.countsfiles = countsfiles
        self.conditions = conditions
        self.config = config
        self.compressed = compressed
        self.parallelized = parallelized
        self.default_transformOrder = transformOrder
        self.working_dir = working_dir
        self.intermediates_dir = intermediates_dir
        self.output_dir = output_dir
        self.real_transforms_dir = transforms_dir
        self.sim_transforms_dir = sims_dir


    def run5CProcessingPipeline(self, transformOrder, transforms_dir, countsfiles, input_suffix='', bed_suffix=''):
        primerfile = self.primerfile
        config = self.config
        conditions = self.conditions
        name_parser=self.name_parser

        condition_map = {label: [c for c in conditions if c in label][0] for label in countsfiles}
        labels = countsfiles.keys()

        # Run step if in transformOrder list
        if 'RegionSplit' in transformOrder:
            # Load input
            primermap, counts_superdict = _load_transform(transforms_dir, primerfile, countsfiles, condition_map,
                                                          bed_suffix, input_suffix, name_parser=name_parser)

            # Run transformation
            output_suffix = config['RegionSplit']['save_suffix']
            keep_regions = config['RegionSplit']['keep_regions']

            regions = counts_superdict[labels[0]].keys()
            keep_regions = regions if keep_regions == 'all' else json.loads(keep_regions)
            splits = config['RegionSplit']['splits']
            splits = None if splits == 'None' else json.loads(splits)
            dr.processing5C.region_split(primerfile, countsfiles.values(), transforms_dir, ext=output_suffix,
                                         keep_regions=keep_regions, splits=splits)

            # Set parameter to hand off to next step
            input_suffix = output_suffix
            bed_suffix = output_suffix

        if 'TrimPrimers' in transformOrder:
            # Load input
            primermap, counts_superdict = _load_transform(transforms_dir, primerfile, countsfiles, condition_map,
                                                          bed_suffix, input_suffix, name_parser=name_parser)

            # Run transformation
            output_suffix = config['TrimPrimers']['save_suffix']
            min_sum = float(config['TrimPrimers']['min_sum'])
            min_frac = config['TrimPrimers']['min_frac']
            min_frac = None if min_frac == 'None' else float(min_frac)

            trimmed_primermap = {}
            trimmed_indices = {}
            for region in primermap.keys():
                trimmed_primermap[region], trimmed_indices[region] = \
                    dr.common.trim_primers(primermap[region],
                                           {rep: counts_superdict[rep][region] for rep in counts_superdict},
                                           min_sum=min_sum, min_frac=min_frac)

            handpicked_ranges = config['TrimPrimers']['handpicked_ranges']
            handpicked_ranges = {} if handpicked_ranges == 'None' else json.loads(handpicked_ranges)

            for region in handpicked_ranges:
                if region in primermap.keys():
                    range_parts = handpicked_ranges[region].split(',')
                    to_remove = []
                    for r in range_parts:
                        if r[-1] == '-':
                            start = int(r[:-1])
                            to_remove.extend(range(start, trimmed_primermap[region][-1]['number'] + 1))
                        elif r[0] == '-':
                            end = int(r[1:])
                            to_remove.extend(range(0, end + 1))
                        else:
                            to_remove.append(int(r))
                    region_trimmed_primermap = deepcopy(trimmed_primermap[region])
                    trimmed_region_indices = []

                    for i, primer in enumerate(trimmed_primermap[region]):
                        if primer['number'] in to_remove:
                            region_trimmed_primermap.remove(primer)
                            trimmed_region_indices.append(i)
                    trimmed_primermap[region] = region_trimmed_primermap
                    trimmed_indices[region] = set(trimmed_region_indices)

            trimmed_counts_superdict = {rep: {} for rep in counts_superdict}
            for rep in counts_superdict.keys():
                for region in counts_superdict[rep].keys():
                    trimmed_counts_superdict[rep][region] = dr.common.trim_matrix(counts_superdict[rep][region],
                                                                                  trimmed_indices[region])

            # Save data
            trimmed_experiment = dr.common.ExperimentSet(counts_list=[trimmed_counts_superdict[l] for l in labels],
                                                         conditions=[condition_map[l] for l in labels],
                                                         mapping=trimmed_primermap, labels=labels)
            trimmed_experiment.save_counts(transforms_dir, ext=output_suffix)

            trimmed_primerfile = _get_transform_file(primerfile, transforms_dir, output_suffix, '.bed')
            dr.common.write_primermap(trimmed_primermap, trimmed_primerfile)

            # Set parameter to hand off to next step
            input_suffix = output_suffix
            bed_suffix = output_suffix

        if 'QuantileNormalization' in transformOrder:
            # Load input
            primermap, counts_superdict = _load_transform(transforms_dir, primerfile, countsfiles, condition_map,
                                                          bed_suffix, input_suffix, name_parser=name_parser)

            # Run transformation
            output_suffix = config['QuantileNormalization']['save_suffix']
            regional = config.getboolean('QuantileNormalization', 'regional')
            counts_superdict = dr.common.qnorm_counts_superdict(counts_superdict, primermap, regional=regional)

            # Save data
            qnormed_experiment = dr.common.ExperimentSet(counts_list=[counts_superdict[l] for l in labels],
                                                         conditions=[condition_map[l] for l in labels],
                                                         mapping=primermap, labels=labels)
            qnormed_experiment.save_counts(transforms_dir, ext=output_suffix)

            # Set parameter to hand off to next step
            input_suffix = output_suffix


        if 'JointExpressNormalization' in transformOrder:
            # Load input
            input_experiment = _load_transform_to_exp(transforms_dir, primerfile, countsfiles, condition_map,
                                                      bed_suffix, input_suffix, name_parser=name_parser)

            # Run transformation
            output_suffix = config['JointExpressNormalization']['save_suffix']
            jen_exp = dr.processing5C.joint_express_on_exp(input_experiment, save_intermediates=False)
            jen_exp.save_counts(transforms_dir, ext=output_suffix)

            # Set parameter to hand off to next step
            input_suffix = output_suffix

        # If we will be logging our observed and expected values
        logged = config['Logged']['logged']
        logged = None if logged == 'None' else int(logged)

        if 'BinSmooth' in transformOrder:
            # Load input
            input_experiment = _load_transform_to_exp(transforms_dir, primerfile, countsfiles, condition_map,
                                                      bed_suffix, input_suffix, name_parser=name_parser)

            # Run transformation
            output_suffix = config['BinSmooth']['save_suffix']
            bin_step = int(config['BinSmooth']['bin_step'])
            smoothing_window = int(config['BinSmooth']['smoothing_window'])
            wipe_window = int(config['BinSmooth']['wipe_window'])

            filter_function = config['BinSmooth']['filter_function']
            filter_function = dr.common.arithmetic_mean_filter() if filter_function == 'arithmetic' else \
                dr.common.geometric_mean_filter()

            binned_exp = dr.processing5C.bin_smooth_counts(input_experiment,
                                                           bin_step=bin_step,
                                                           neighbor_radius=smoothing_window/2,
                                                           wipe_window_width=wipe_window,
                                                           filter_function=filter_function,
                                                           logged=logged,
                                                           save_intermediates=False)

            # Save transformation
            binned_exp.save_counts(transforms_dir, ext=output_suffix)
            pixelfile = _get_transform_file(primerfile, transforms_dir, output_suffix, '.bed')
            dr.common.write_pixelmap(binned_exp.mapping, pixelfile)

            # Set parameter to hand off to next step
            input_suffix = output_suffix
            bed_suffix = output_suffix

        if '1DExpected' in transformOrder:
            # Load input
            input_suffix = config['BinSmooth']['save_suffix']
            bed_suffix = input_suffix
            input_experiment = _load_transform_to_exp(transforms_dir, self.primerfile, countsfiles, condition_map,
                                                      bed_suffix, input_suffix)

            # Run transformation
            output_suffix = config['1DExpected']['save_suffix']
            is_global = config.getboolean('1DExpected', 'is_global')
            exp_1D = dr.processing5C.calculate_1D_expected(input_experiment, save_intermediates=False,
                                                           logged=logged, is_global=is_global)

            # Save transformation
            exp_1D.save_counts(transforms_dir, ext=output_suffix)

            # Set parameter to hand off to next step
            input_suffix = output_suffix

        # Compute observed over expected
        if 'ObservedOverExpected' in transformOrder:
            # Load input
            observed_suffix = config['BinSmooth']['save_suffix']
            expected_suffix = config['1DExpected']['save_suffix']

            observed_exp = _load_transform_to_exp(transforms_dir, self.primerfile, countsfiles, condition_map,
                                                       observed_suffix, observed_suffix)
            expected_exp = _load_transform_to_exp(transforms_dir, self.primerfile, countsfiles, condition_map,
                                                       observed_suffix, expected_suffix)

            # Run transformation
            output_suffix = config['ObservedOverExpected']['save_suffix']
            ooe_exp = dr.processing5C.find_obs_over_max_donut_ll(
                observed_exp, expected_exp,
                save_intermediates=True, output_dir=transforms_dir,
                donut_ext=str(config['DonutExpected']['save_suffix']),
                lower_left_ext=str(config['LowerLeftExpected']['save_suffix']),
                max_ll_donut_ext=str(config['MaxLLDonutExpected']['save_suffix']),
                observed_over_expected_ext=str(config['ObservedOverExpected']['save_suffix']))

            # Save transformation
            ooe_exp.save_counts(transforms_dir, ext=output_suffix)

        if 'Pvalue' in transformOrder:
            input_suffix = config['ObservedOverExpected']['save_suffix']
            output_suffix = config['Pvalue']['save_suffix']
            dr.processing5C.get_logistic_pvalues(transforms_dir,
                                                 observed_over_expected_ext=input_suffix,
                                                 pvalues_ext=output_suffix)

        if 'InteractionScore' in transformOrder:
            input_suffix = config['Pvalue']['save_suffix']
            bed_suffix = config['BinSmooth']['save_suffix']

            output_suffix = config['InteractionScore']['save_suffix']
            pvalue_exp = _load_transform_to_exp(transforms_dir, self.primerfile, countsfiles, condition_map,
                                                bed_suffix, input_suffix)
            is_exp = dr.processing5C.get_interaction_scores(pvalue_exp, save_intermediates=False)
            is_exp.save_counts(transforms_dir, ext=output_suffix)

        if 'TrimDiagonal' in transformOrder:
            input_suffix = config['InteractionScore']['save_suffix']
            bed_suffix = config['BinSmooth']['save_suffix']

            output_suffix = config['TrimDiagonal']['save_suffix']
            distance_threshold = int(config['TrimDiagonal']['distance_threshold'])
            wipe_value = config['TrimDiagonal']['wipe_value']
            wipe_value = np.nan if wipe_value == 'NaN' else float(wipe_value)
            distance_span = str(config['TrimDiagonal']['distance_span'])

            is_exp = _load_transform_to_exp(transforms_dir, self.primerfile, countsfiles, condition_map,
                                            bed_suffix, input_suffix)
            trimmed_int_score_exp = dr.processing5C.trim_diagonal(is_exp, save_intermediates=False,
                                                                  distance_threshold=distance_threshold,
                                                                  wipe_value=wipe_value,
                                                                  distance_span=distance_span)
            trimmed_int_score_exp.save_counts(transforms_dir, ext=output_suffix)

    def processRealCounts(self):
        self.run5CProcessingPipeline(self.default_transformOrder, self.real_transforms_dir, self.countsfiles)

    def generateAndProcessSims(self):
        countsfiles = self.countsfiles
        config = self.config
        compressed = self.compressed
        parallelized = self.parallelized
        transformOrder = self.default_transformOrder
        real_transforms_dir = self.real_transforms_dir
        sim_transforms_dir = self.sim_transforms_dir
        conditions = self.conditions

        if 'Simulations' in config:
            counts_transform = config['Simulations']['counts_transform']
            counts_suffix = config[counts_transform]['save_suffix']

            bed_transform = config['Simulations']['bed_transform']
            bed_suffix = config[bed_transform]['save_suffix']

            condition_map = {label: [c for c in conditions if c in label][0] for label in countsfiles}
            input_exp = _load_transform_to_exp(real_transforms_dir, self.primerfile, countsfiles, condition_map,
                                               bed_suffix, counts_suffix)

            bin_properties = {'bin_dimensions': json.loads(config['Simulations']['bin_dimensions']),
                               'catchall': config.getboolean('Simulations','catchall')}
            globally_fit_dispersion = config.getboolean('Simulations','globally_fit_dispersion')
            mean_outlier_limit = config['Simulations']['mean_outlier_limit_prior_to_fitting_dispersion']
            mean_outlier_limit = None if mean_outlier_limit == 'None' else float(mean_outlier_limit)
            var_outlier_limit = config['Simulations']['var_outlier_limit_prior_to_fitting_dispersion']
            var_outlier_limit = None if var_outlier_limit == 'None' else float(var_outlier_limit)
            loess_smooth = config['Simulations']['loess_smooth']
            loess_smooth = None if loess_smooth == 'None' else float(loess_smooth)
            predicted_variance_weight = float(config['Simulations']['predicted_variance_weight'])
            observed_variance_weight = float(config['Simulations']['observed_variance_weight'])
            num_simulation_sets = int(config['Simulations']['num_simulation_sets'])

            simset = dr.loopCallers.SimulationSet(input_exp,
                                                  output_dir=sim_transforms_dir,
                                                  predicted_variance_weight=predicted_variance_weight,
                                                  observed_variance_weight=observed_variance_weight,
                                                  loess_smooth=loess_smooth,
                                                  bin_properties=bin_properties,
                                                  globally_fit_dispersion=globally_fit_dispersion,
                                                  mean_outlier_limit=mean_outlier_limit,
                                                  var_outlier_limit=var_outlier_limit,
                                                  compressed=compressed,
                                                  parallelized=parallelized)
            simset.compute_generative_model()
            simset.make_simulations(num_simulation_sets=num_simulation_sets)

            transformOrder = transformOrder[np.where(transformOrder == counts_transform)[0][0]+1:]
            input_suffix = config[counts_transform]['save_suffix']

            simset_countsfiles = simset.sim_countsfiles
            for set_path in simset_countsfiles:
                self.run5CProcessingPipeline(transformOrder, set_path, simset_countsfiles[set_path],
                                             input_suffix=input_suffix, bed_suffix='trim')

    def callLoops(self):
        countsfiles = self.countsfiles
        conditions = self.conditions
        config = self.config
        parallelized = self.parallelized
        output_dir = self.output_dir
        intermediates_dir = self.intermediates_dir
        real_transforms_dir = self.real_transforms_dir
        sim_transforms_dir = self.sim_transforms_dir

        condition_map = {label: [c for c in conditions if c in label][0] for label in countsfiles}

        if 'DiffLoopCalling' in config:
            background_threshold = float(config['DiffLoopCalling']['background_threshold'])
            significance_threshold = float(config['DiffLoopCalling']['significance_threshold'])
            difference_thresholds = config['DiffLoopCalling']['difference_thresholds']
            difference_thresholds = None if difference_thresholds == 'None' else json.loads(difference_thresholds)
            adaptive = config.getboolean('DiffLoopCalling', 'adaptive')
            fdr_thresholds = json.loads(config['DiffLoopCalling']['fdr_thresholds'])
            min_cluster_size = int(config['DiffLoopCalling']['min_cluster_size'])
            null_model_condition = config['DiffLoopCalling']['null_model_condition']
            overwrite = config.getboolean('DiffLoopCalling', 'overwrite')

            real_experiment = _load_transform_to_exp(transforms_dir=real_transforms_dir,
                                                     primerfile=self.primerfile,
                                                     countsfiles=countsfiles,
                                                     condition_map=condition_map,
                                                     bed_suffix=config['BinSmooth']['save_suffix'],
                                                     counts_suffix=config['TrimDiagonal']['save_suffix'])
            e = dr.loopCallers.eFDR(real_experiment=real_experiment,
                                    sim_transforms_dir=sim_transforms_dir,
                                    intermediates_dir=intermediates_dir,
                                    output_dir=output_dir,
                                    parallelized=parallelized)

            for fdr_threshold in fdr_thresholds:
                e.classify_loops(fdr_threshold=fdr_threshold,
                                 null_model_condition=null_model_condition,
                                 background_threshold=background_threshold,
                                 significance_threshold=significance_threshold,
                                 difference_thresholds=difference_thresholds,
                                 min_cluster_size=min_cluster_size,
                                 adaptive=adaptive,
                                 overwrite=overwrite)

    def plotResults(self):
        config = self.config
        output_dir = self.output_dir

        if 'PlotClassificationClusters' in config:
            windows=json.loads(config['PlotClassificationClusters']['windows'])
            color_dict=json.loads(config['PlotClassificationClusters']['color_dict'])
            categorial = config.getboolean('PlotClassificationClusters', 'categorical')
            refgene_stacks_dir = config['PlotClassificationClusters']['refgene_stacks_dir']
            refgene_stacks_dir = None if refgene_stacks_dir == 'None' else refgene_stacks_dir
            refgene_assembly_name = config['PlotClassificationClusters']['refgene_assembly_name']

            # Load loop classifications from file
            dataset_paths = dr.loopCallers.eFDR.previous_results_dataset_paths(output_dir)

            # Plot loop classification clusters found in each window for each results dataset
            cluster_plots_dir = dr.common.check_dir(os.path.join(output_dir, 'classification_cluster_plots'))
            for dataset_path in dataset_paths:
                dataset = dr.common.DataSet.load(dataset_path)
                dplots_dir = dr.common.check_dir(os.path.join(cluster_plots_dir,
                                                              os.path.basename(os.path.dirname(dataset_path))))
                for window in windows:
                    h = dr.plotters.plot_classification_clusters(
                        dataset, color_dict, window, categorical=categorial, refgene_stacks_dir=refgene_stacks_dir,
                        refgene_assembly_name=refgene_assembly_name)

                    plot_path = os.path.join(dplots_dir, window['name'] + '.png')
                    h.save(plot_path)
                    h.close()


def _load_transform_to_exp(transforms_dir, primerfile, countsfiles, condition_map, bed_suffix, counts_suffix,
                           name_parser=None):
    labels = countsfiles.keys()

    primerfile = _get_transform_file(primerfile, transforms_dir, bed_suffix, '.bed')
    primermap = dr.common.load_primermap(primerfile, name_parser=name_parser)
    counts_superdict = {label: dr.common.load_counts(
        _get_transform_file(countsfiles[label], transforms_dir, counts_suffix, '.counts'), primermap)
        for label in countsfiles}
    input_experiment = dr.common.ExperimentSet(counts_list=[counts_superdict[l] for l in labels],
                                               conditions=[condition_map[l] for l in labels],
                                               mapping=primermap, bed_path=str(primerfile), labels=labels,
                                               counts_ext=str(counts_suffix))
    return input_experiment

def _load_transform(transforms_dir, primerfile, countsfiles, condition_map, bed_suffix, counts_suffix,
                    name_parser=None):
    input_experiment = _load_transform_to_exp(transforms_dir, primerfile, countsfiles, condition_map, bed_suffix,
                                              counts_suffix, name_parser=name_parser)
    return input_experiment.mapping, input_experiment.superdict()


def _get_transform_file(infile, transforms_dir, suffix, ext):
    return os.path.join(transforms_dir, os.path.basename(infile).split('.')[0]) + ('_' if suffix else '') + suffix + ext


def runFromConfig(config_path):

    r = RunFromConfig(config_path)

    # Process real counts
    r.processRealCounts()

    # Generate simulated counts
    r.generateAndProcessSims()

    # Run 3DeFDR
    r.callLoops()

    # Plot differential loop classification clusters
    r.plotResults()
    

def main():
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
        runFromConfig(config_path)
    else:
        print('dropping default config file to current directory')
        shutil.copyfile(
            os.path.join(os.path.split(__file__)[0], 'diff3DeFDR.cfg'),
            'diff3DeFDR.cfg'
        )


if __name__ == '__main__':
    main()
