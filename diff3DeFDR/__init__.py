"""
A pipeline for detecting differential looping interactions in 5C data-sets.

Subpackage structure:

* :mod:`diff3DeFDR.common` - various utility functions
* :mod:`diff3DeFDR.loopCallers` - main pipeline for differential looping analysis
* :mod:`diff3DeFDR.processing5C` - transforming 5C counts to isolate looping signal
* :mod:`diff3DeFDR.plotters` - data visualization
"""

import warnings
warnings.filterwarnings("ignore")

import common
import processing5C
import plotters
import loopCallers

