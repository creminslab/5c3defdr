import os
import matplotlib as mpl

from BaseExtendableHeatmap import BaseExtendableHeatmap
from diff3DeFDR.common.modules2016 import check_intersect
from load_gene_table import load_gene_table


class GeneExtendableHeatmap(BaseExtendableHeatmap):
    def add_gene_tracks(self, genes, size='3%', pad=0.0, axis_limits=(0, 1),
                        intron_height=0.05, exon_height=0.5, colors=None):
        ax_h = self.add_gene_track(
            genes, loc='bottom', size=size, pad=pad, new_ax_name='h_genes',
            axis_limits=axis_limits, intron_height=intron_height,
            exon_height=exon_height, colors=colors)
        ax_v = self.add_gene_track(
            genes, loc='left', size=size, pad=pad, new_ax_name='v_genes',
            axis_limits=axis_limits, intron_height=intron_height,
            exon_height=exon_height, colors=colors)
        return [ax_h, ax_v]

    def add_gene_track(self, genes, loc='bottom', size='3%', pad=0.0,
                       new_ax_name='genes', axis_limits=(0, 1),
                       intron_height=0.05, exon_height=0.5, colors=None):
        # create new axis
        ax = self.add_margin_ax(loc=loc, size=size, pad=pad,
                                new_ax_name=new_ax_name,
                                axis_limits=axis_limits)

        # deduce orientation, either h or v, and save the correct grange
        if loc in ['bottom', 'top']:
            orientation = 'horizontal'
            grange = self.grange_x
        else:
            orientation = 'vertical'
            grange = self.grange_y

        # deduce midpoint of the non-genomic axis
        midpoint = (axis_limits[0] + axis_limits[1]) / 2.

        # compute track_width and track_height, assuming horizontal orientation
        track_width = grange['end'] - grange['start']
        track_height = abs(axis_limits[1] - axis_limits[0])

        # create patches for each gene
        for gene in genes:
            color = 'k'
            if colors is not None:
                if gene['id'] in colors:
                    color = colors[gene['id']]
                elif gene['name'] in colors:
                    color = colors[gene['name']]

            # skip genes that don't intersect the window
            if not check_intersect(gene, grange):
                continue

            # plot intron rectangle
            intron_coords = [gene['start'], midpoint - intron_height / 2.]
            intron_dims = [gene['end'] - gene['start'], intron_height]
            if orientation == 'vertical':
                intron_coords.reverse()
                intron_dims.reverse()
            ax.add_artist(mpl.patches.Rectangle(intron_coords, *intron_dims,
                                                fc=color, ec=color))

            # plot exon rectangles
            for block in gene['blocks']:
                exon_coords = [block['start'], midpoint - exon_height / 2.]
                exon_dims = [block['end'] - block['start'], exon_height]
                if orientation == 'vertical':
                    exon_coords.reverse()
                    exon_dims.reverse()
                ax.add_artist(mpl.patches.Rectangle(exon_coords, *exon_dims,
                                                    fc=color, ec=color))

            # plot a little arrow based on the strand information if present
            # TODO: figure out why this factor of 40 is necessary
            arrow_size = (exon_height / 60.) * (track_width / track_height)
            if gene['strand'] == '+':
                xs = [gene['start'], gene['start'] - arrow_size]
                upper_ys = [midpoint, midpoint + exon_height / 2.]
                lower_ys = [midpoint, midpoint - exon_height / 2.]
            elif gene['strand'] == '-':
                xs = [gene['end'], gene['end'] + arrow_size]
                upper_ys = [midpoint, midpoint + exon_height / 2.]
                lower_ys = [midpoint, midpoint - exon_height / 2.]
            else:
                return ax
            if orientation == 'vertical':
                ax.plot(upper_ys, xs, color=color)
                ax.plot(lower_ys, xs, color=color)
            else:
                ax.plot(xs, upper_ys, color=color)
                ax.plot(xs, lower_ys, color=color)

        return ax

    def add_gene_stacks(self, genes, size='3%', pad=0.0, axis_limits=(0, 1),
                        intron_height=0.05, exon_height=0.5, padding=1000,
                        colors=None):
        ax_hs = self.add_gene_stack(
            genes, loc='bottom', size=size, pad=pad, axis_limits=axis_limits,
            intron_height=intron_height, exon_height=exon_height,
            padding=padding, colors=colors)
        ax_vs = self.add_gene_stack(
            genes, loc='left', size=size, pad=pad, axis_limits=axis_limits,
            intron_height=intron_height, exon_height=exon_height,
            padding=padding, colors=colors)
        return [ax_hs, ax_vs]

    def add_gene_stack(self, genes, loc='bottom', size='3%', pad=0.0,
                       axis_limits=(0, 1), intron_height=0.05, exon_height=0.5,
                       padding=1000, colors=None):
        # deduce orientation, either h or v, and save the correct grange
        if loc in ['bottom', 'top']:
            orientation = 'horizontal'
            grange = self.grange_x
        else:
            orientation = 'vertical'
            grange = self.grange_y

        # initialize data structures for packing
        row_cursors = []
        rows = []

        # initialize the first row
        rows.append([])
        row_cursors.append(-10000)  # basically should be -Inf

        # main loop for packing genes
        for gene in genes:
            # skip genes that don't intersect the window
            if not check_intersect(gene, grange):
                continue

            gene_placed = False
            for i in range(len(rows)):
                if gene['start'] > row_cursors[i] + padding:
                    row_cursors[i] = gene['end']
                    rows[i].append(gene)
                    gene_placed = True
                    break
            if not gene_placed:
                rows.append([gene])
                row_cursors.append(gene['end'])

        # add a track for each row
        return [
            self.add_gene_track(
                rows[i], loc=loc, size=size, pad=pad, axis_limits=axis_limits,
                intron_height=intron_height, exon_height=exon_height,
                new_ax_name='%s_gene_track_%i' % (orientation, i),
                colors=colors)
            for i in range(len(rows))
        ]

    def add_refgene_stacks(self, assembly, size='3%', pad=0.0,
                           axis_limits=(0, 1), intron_height=0.05,
                           exon_height=0.5, padding=1000, colors=None, file_path=None):
        ax_hs = self.add_refgene_stack(
            assembly, loc='bottom', size=size, pad=pad, axis_limits=axis_limits,
            intron_height=intron_height, exon_height=exon_height,
            padding=padding, colors=colors, file_path=file_path)
        ax_vs = self.add_refgene_stack(
            assembly, loc='left', size=size, pad=pad, axis_limits=axis_limits,
            intron_height=intron_height, exon_height=exon_height,
            padding=padding, colors=colors, file_path=file_path)
        return [ax_hs, ax_vs]

    def add_refgene_stack(self, assembly, loc='bottom', size='3%', pad=0.0,
                          axis_limits=(0, 1), intron_height=0.05,
                          exon_height=0.5, padding=1000, colors=None, file_path=None):
        if assembly not in ['hg18', 'hg19', 'hg38', 'mm9', 'mm10']:
            raise ValueError('unrecognized assembly %s' % assembly)
        directory = os.path.join(os.path.split(__file__)[0], 'gene_tracks') if file_path is None else file_path
        if not directory:
            directory = '.'
        refgene_file_name = '%d/%g_refseq_genes.gz' \
            .replace('%d', directory) \
            .replace('%g', assembly) \
            .replace('%c', self.grange_x['chrom'])
        genes = load_gene_table(refgene_file_name)[self.grange_x['chrom']]

        return self.add_gene_stack(genes, loc=loc, size=size, pad=pad,
                                   axis_limits=axis_limits,
                                   intron_height=intron_height,
                                   exon_height=exon_height, padding=padding,
                                   colors=colors)

