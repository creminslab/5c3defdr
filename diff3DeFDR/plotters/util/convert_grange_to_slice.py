def convert_grange_to_slice(grange, regional_pixelmap):
    """
    Finds the slice of a regional pixelmap that covers a given grange.

    Parameters
    ----------
    grange : dict
        Should have the format

            {
                'chrom': str,
                'start': int,
                'end': int
            }

    regional_pixelmap : list of dict
        The dicts represent bins with at least the following structure

            {
                'start': int,
                'end': int
            }

    Returns
    -------
    slice
        Slicing into regional_pixelmap with this slice object will yield the
        smallest set of bins that covers grange.
    """
    min_index = len(regional_pixelmap) - 1
    while regional_pixelmap[min_index]['start'] > grange['start']:
        min_index -= 1
    max_index = 0
    while regional_pixelmap[max_index]['end'] < grange['end']:
        max_index += 1
    return slice(min_index, max_index + 1)