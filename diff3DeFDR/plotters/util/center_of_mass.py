import numpy as np

from get_vector import get_vector


def center_of_mass(cluster):
    """
    Computes the center of mass, or centroid, of a cluster.

    Parameters
    ----------
    cluster : cluster
        The cluster to consider.

    Returns
    -------
    1D numpy array of length 2
        The centroid of the cluster.

    Notes
    -----
    For the purpose of this calculation, the mass of a peak is taken to be its
    value.
    """
    m = 0
    vector_sum = np.array([0, 0])
    for peak in cluster:
        m += peak['value']
        vector_sum += peak['value'] * get_vector(peak)
    return vector_sum / m