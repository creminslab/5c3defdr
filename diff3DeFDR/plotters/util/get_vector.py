import numpy as np

def get_vector(peak):
    """
    Gets an array representing a peak's location.

    Parameters
    ----------
    peak : peak

    Returns
    -------
    1D numpy array of length 2
        The peak's location as an (x, y) ordered pair.
    """
    return np.array([peak['x'], peak['y']])