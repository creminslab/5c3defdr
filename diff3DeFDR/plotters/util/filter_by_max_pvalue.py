import numpy as np


def filter_by_max_pvalue(df, name='pvalue', pvalue_threshold=0.0625, drop=True):
    """
    Drops rows from df where a heirarchical column of p-values based on the
    maximum p-value across replicates is below a constitutive threshold.

    Parameters
    ----------
    df : pd.DataFrame
        Its name column must be hierarchical with the second level representing
        replicates.
    name : str
        The name of the column containing the p-values to threshold on.
    pvalue_threshold : float
        The p-value threshold to use.
    drop : bool
        Pass True to drop the filtered rows in-place. Pass False to return an
        index subset for the filtered rows instead.
    """
    # prepare p-value matrix
    pvalue_matrix = df[name].as_matrix()

    # make max_pvalues vector
    max_pvalues = np.amax(pvalue_matrix, axis=1)

    # drop based on max_pvalues vector
    index_subset = (df[max_pvalues < pvalue_threshold]).index
    if drop:
        df.drop(index_subset, inplace=True)
    else:
        return index_subset