import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

class ExtendableFigure(object):
    """
    Attributes
    ----------
    axes : dict of matplotlib.axes.Axes
        The collection of named Axes represented by this object.
    fig : matplotlib.figure.Figure
        The Figure instance this object represents.
    divider : mpl_toolkits.axes_grid1.axes_divider.AxesDivider
        This object serves as a coordinator for the allocation of new Axes to be
        appended to this ExtendableFigure.

    Examples
    --------
    >>> import numpy as np
    >>> xs = np.arange(0, 10)
    >>> f = ExtendableFigure()
    >>> f['root'].imshow(np.arange(100).reshape((10,10)))
    <matplotlib.image.AxesImage object at ...>
    >>> f.add_ax('sin')
    <matplotlib.axes._axes.Axes object at ...>
    >>> f['sin'].plot(xs, np.sin(xs))
    [<matplotlib.lines.Line2D object at ...>]
    >>> f.add_colorbar('root')
    >>> f.save('test/extendablefigure.png')
    """

    def __init__(self):
        self.axes = {}
        self.fig, self.axes['root'] = plt.subplots()
        self.divider = make_axes_locatable(self.axes['root'])

    def __getitem__(self, item):
        return self.axes[item]

    def add_ax(self, name, loc='bottom', size='10%', pad=0.1):
        if name in self.axes:
            raise ValueError('an axis with name %s already exists!' % name)
        self.axes[name] = self.divider.append_axes(loc, size, pad)
        return self.axes[name]

    def add_colorbar(self, source_ax_name, loc='right', size='10%', pad=0.1,
                     new_ax_name='colorbar'):
        self.add_ax(new_ax_name, loc, size, pad)
        self.fig.colorbar(self.axes[source_ax_name].images[0],
                          cax=self.axes[new_ax_name])

    def save(self, filename):
        self.fig.savefig(filename, dpi=800, bbox_inches='tight')

    def close(self):
        self.fig.clf()
        plt.close(self.fig)
