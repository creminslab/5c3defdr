from dblalt_primer_parser import dblalt_primer_parser
from default_primer_parser import default_primer_parser
from default_bin_parser import default_bin_parser


def guess_primer_name_parser(name):
    """
    Guesses the appropriate primer or bin name parser to use by looping through
    a list of possible parsers and testing if they work on a given primer name.

    Parameters
    ----------
    name : str
        The name of a primer to use for testing.

    Returns
    -------
    function
        The parser thought to be appropriate for this kind of primer name.
    """
    parsers = [dblalt_primer_parser, default_primer_parser, default_bin_parser]
    for parser in parsers:
        try:
            parser(name)
            return parser
        except (ValueError, IndexError):
            pass