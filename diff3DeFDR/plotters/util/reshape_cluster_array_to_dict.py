import numpy as np


def reshape_cluster_array_to_dict(cluster_array, ignored_values=None):
    """
    Reshapes loops dict structure into a nested dict structure.

    Parameters
    ----------
    cluster_array: np.ndarray
        The entries of this array are cluster ID's. Values that will be ignored
        include '', 'n.s.', 'NA', 'NaN', np.nan.
    ignored_values: set, optional
        Set of values in cluster_array that should not be treated as cluster
        ID's. By default this will be {'', 'n.s.', 'NA', 'NaN', np.nan}

    Returns
    --------
    Dict[Any, List[Dict[str, Any]]]
        The outer dict's keys are cluster ID's, its values are lists of points
        belonging to that cluster, with the points being provided as dicts with
        the following strucure::

            {
                'x': int,
                'y': int,
                'value': 0
            }

    Notes
    -----
    To rectangularize the returned data structure against a full list of cluster
    ID's, use something like

        cluster_dict = reshape_cluster_array_to_dict(cluster_array)
        for cluster_id in all_cluster_ids:
            if cluster_id not in cluster_dict:
                cluster_dict[cluster_id] = []

    Examples
    --------
    >>> import numpy as np
    >>> cluster_array = np.array([['', 'cow'], ['cow', 'grass']])
    >>> reshape_cluster_array_to_dict(cluster_array)
    {'grass': [{'y': 1, 'x': 1, 'value': 0}],
     'cow': [{'y': 1, 'x': 0, 'value': 0},
             {'y': 0, 'x': 1, 'value': 0}]}
    """
    # resolve ignored_values
    if ignored_values is None:
        ignored_values = {'', 'n.s.', 'NA', 'NaN', np.nan}

    # identify non-ignored cluster id's
    cluster_ids = set(np.unique(cluster_array)) - ignored_values

    # prepare data structure
    cluster_dict = {cluster_id: [] for cluster_id in cluster_ids}

    # fill in data structure
    for i in range(cluster_array.shape[0]):
        for j in range(cluster_array.shape[1]):
            cluster_id = cluster_array[i, j]
            if cluster_id in cluster_ids:
                cluster_dict[cluster_id].append({'x': i, 'y': j, 'value': 0})

    return cluster_dict
