import matplotlib as mpl
from BaseExtendableHeatmap import BaseExtendableHeatmap


class LegendExtendableHeatmap(BaseExtendableHeatmap):
    def add_legend(self, colors, **kwargs):
        legend_kwargs = {
            'loc': 'upper right',
            'bbox_to_anchor': (-0.1, -0.1),
            'borderaxespad': 0.,
            'fontsize': 5
        }
        legend_kwargs.update(kwargs)
        patches = [mpl.patches.Patch(color=colors[label], label=label)
                   for label in sorted(colors.keys())]
        return self['root'].legend(handles=patches, **legend_kwargs)
