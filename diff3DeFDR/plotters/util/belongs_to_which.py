from get_cluster import get_cluster

def belongs_to_which(peak, clusters):
    """
    Identifies which cluster out of a list of clusters, if any, a peak belongs
    to.

    Parameters
    ----------
    peak : peak
        The query peak to consider.
    clusters : list of clusters
        The clusters to look for the query peak in.

    Returns
    -------
    int
        The index of the cluster within the list of clusters which contains the
        query peak, or -1 if no cluster in the list of clusters contains the
        query peak.
    """
    return get_cluster(peak['x'], peak['y'], clusters)