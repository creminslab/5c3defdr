import gzip


def load_gene_table(tablefile):
    """
    Similar to ``load_genes()``, but reads in a gzipped UCSC table file instead.

    The main advantage of this approach is that genes parsed this way include
    human-readable gene symbols.

    Parameters
    ----------
    tablefile : str
        String reference to location of the gzipped table file to read.

    Returns
    -------
    dict of lists of dicts
        The keys are chromosome names. The values are lists of genes for that
        chromosome. The genes are represented as dicts with the following
        structure::

            {
                'start' : int,
                'end'   : int,
                'name'  : str,
                'id': str,
                'strand': '+' or '-',
                'blocks': list of dicts
            }

        Blocks typically represent exons and are represented as dicts with the
        following structure::

            {
                'start': int,
                'end'  : int
            }
    """
    # dict to store genes
    genes = {}

    # parse bedfile
    with gzip.open(tablefile, 'rb') as handle:
        for line in handle:
            # skip header
            if line.startswith('#'):
                continue

            # split line
            pieces = line.split('\t')

            # parse chromosome
            chromosome = pieces[2].strip()

            # add chromosome to dict if this is a new one
            if chromosome not in genes:
                genes[chromosome] = []

            # parse gene information
            start = int(pieces[4])
            end = int(pieces[5])
            id = pieces[1].strip()
            name = pieces[12]
            strand = pieces[3]

            # parse block information if applicable
            blocks = []
            cds_start = int(pieces[6])
            cds_end = int(pieces[7])
            if cds_start != cds_end:
                block_starts = [int(piece)
                                for piece in pieces[9].strip(',').split(',')]
                block_ends = [int(piece)
                              for piece in pieces[10].strip(',').split(',')]
                for i in range(len(block_starts)):
                    block = {'start': max(block_starts[i], cds_start),
                             'end': min(block_ends[i], cds_end)}
                    if block['end'] > block['start']:
                        blocks.append(block)

            # add this gene to the list
            genes[chromosome].append({'chrom': chromosome,
                                      'start': start,
                                      'end': end,
                                      'name': name,
                                      'id': id,
                                      'strand': strand,
                                      'blocks': blocks})
        for chrom in genes:
            genes[chrom].sort(key=lambda x: x['start'])

    return genes