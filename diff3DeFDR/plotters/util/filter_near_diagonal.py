def filter_near_diagonal(df, k=6, drop=True):
    """
    Drops rows from df where its 'distance' column is less than k.

    Dropping occurs in-place.

    Parameters
    ----------
    df : pd.DataFrame
        Must have a 'distance' column.
    k : int
        Threshold for distance.
    drop : bool
        Pass True to drop the filtered rows in-place. Pass False to return an
        index subset for the filtered rows instead.
    """
    index_subset = (df[df['distance'] < k]).index
    if drop:
        df.drop(index_subset, inplace=True)
    else:
        return index_subset