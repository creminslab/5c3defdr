from belongs_to_which import belongs_to_which

def belongs_to(peak, cluster):
    """
    Checks if a peak belongs to a cluster.

    Parameters
    ----------
    peak : peak
        The query peak.
    cluster : cluster
        The cluster to search for it in.

    Returns
    -------
    bool
        True if peak belongs to cluster, False otherwise.
    """
    return belongs_to_which(peak, [cluster]) == 0