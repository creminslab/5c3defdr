import re


def parse_grange(genomic_range_string):
    """
    Parses a genomic range string <chrom>:<start>-<end> into a grange dict.

    Parameters
    ----------
    genomic_range_string : str
        The string to parse.

    Returns
    -------
    dict
        Will have the format

            {
                'chrom': str,
                'start': int,
                'end': int
            }

    Examples
    --------
    >>> parse_grange('chr3:34520879-34688879')
    {'chrom': 'chr3', 'start': 34520879, 'end': 34688879}
    """
    pattern = re.compile('(\w+):(\d+)-(\d+)')
    chrom, start, end = pattern.match(genomic_range_string).groups()
    return {'chrom': chrom, 'start': int(start), 'end': int(end)}