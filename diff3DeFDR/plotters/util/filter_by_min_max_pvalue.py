import numpy as np


def filter_by_min_max_pvalue(df, name='pvalue', pvalue_threshold=0.165,
                             reps_per_condition=2, drop=True):
    """
    Drops rows from df where a heirarchical column of p-values based on the
    maximum p-value across replicates of the same condition (conservative) and
    the minimum of these max p-values across all conditions. In other words, the
    most conservative replicate in at least one condition must pass the p-value
    threshold.

    Dropping occurs in-place.

    This function assumes that there are the same number of reps for each
    condition (reps_per_condition).

    This function assumes that the p-values are present in df as a hierarchical
    column with its second level being the replicate names, ordered by
    condition.

    Parameters
    ----------
    df : pd.DataFrame
        Its name column must be hierarchical with the second level being rep
        names ordered by condition.
    name : str
        The name of the column containing the p-values to threshold on.
    pvalue_threshold : float
        The p-value threshold to use.
    reps_per_condition : int
        How many replicates there are in each condition.
    drop : bool
        Pass True to drop the filtered rows in-place. Pass False to return an
        index subset for the filtered rows instead.
    """
    # infer reps
    reps = list(df[name].columns)

    # prepare p-value matrix
    pvalue_matrix = df[name].as_matrix()

    # make min_max_pvalues vector
    min_max_pvalues = np.amin(np.array(
        [np.amax(pvalue_matrix[:, i:i + reps_per_condition], axis=1)
         for i in range(0, len(reps), reps_per_condition)]), axis=0)

    # drop based on min_max_pvalues vector
    index_subset = (df[min_max_pvalues > pvalue_threshold]).index
    if drop:
        df.drop(index_subset, inplace=True)
    else:
        return index_subset