import numpy as np


def _epvalue(real_statistics, null_statistics):
    total_real = len(real_statistics)
    total_null = len(null_statistics)

    return np.array([(np.sum(null_statistics > real_statistics[i])) / total_null
                     for i in range(total_real)])


def epvalue(real_statistics, null_statistics):
    """
    Returns empirical pvalues for an input pair of vectors of test statistics, one computed on real data and the other
    on null data
    :param real_statistics: Vector of test statistics computed on real or experimental data
    :param null_statistics: Vector of test statistics computed on null data (not necessarily the same length as
               real_statistics; order of this vector does not matter)
    :return: List of empirical pvalues of the same length and order as real_statistics
    """
    real_statistics = np.array(real_statistics)
    null_statistics = np.array(null_statistics)

    idx_real = np.isfinite(real_statistics)
    idx_null = np.isfinite(null_statistics)

    result = np.ones_like(real_statistics, dtype=float) * np.nan
    result[idx_real] = _epvalue(real_statistics[idx_real], null_statistics[idx_null])

    return result