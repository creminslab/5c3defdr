from interlap import InterLap


def features_to_interlaps(features, chroms=None):
    # resolve chroms
    if chroms is None:
        chroms = features.keys()

    return {chrom: InterLap([(f['start'], f['end'] - 1, f)
                             for f in features[chrom]])
            for chrom in chroms}


def query_interlap(interlap, query_feature):
    hits = interlap.find((query_feature['start'], query_feature['end'] - 1))
    return [hit[2] for hit in hits]