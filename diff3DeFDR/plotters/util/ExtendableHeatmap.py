from ChipSeqExtendableHeatmap import ChipSeqExtendableHeatmap
from CommunityExtendableHeatmap import CommunityExtendableHeatmap
from GeneExtendableHeatmap import GeneExtendableHeatmap
from LegendExtendableHeatmap import LegendExtendableHeatmap
from RulerExtendableHeatmap import RulerExtendableHeatmap
from ClusterExtendableHeatmap import ClusterExtendableHeatmap
from BaseExtendableHeatmap import BaseExtendableHeatmap

class ExtendableHeatmap(ChipSeqExtendableHeatmap, CommunityExtendableHeatmap,
                        GeneExtendableHeatmap, LegendExtendableHeatmap,
                        RulerExtendableHeatmap, ClusterExtendableHeatmap,
                        BaseExtendableHeatmap):
    """
    Examples
    --------
    >>> import numpy as np
    >>> import matplotlib.patches as patches
    >>> counts = load_counts('test/test.counts')['Sox2']
    >>> h = ExtendableHeatmap(
    ...     array=counts,
    ...     grange_x={'chrom': 'chr3', 'start': 34108879, 'end': 35104879},
    ...     colorscale=(-1, 1),
    ...     colormap='obs_over_exp'
    ... )
    >>> xs = np.arange(len(counts)) + 0.5
    >>> h.add_rulers()
    [<matplotlib.axes._axes.Axes object at ...>,
     <matplotlib.axes._axes.Axes object at ...>]
    >>> h.add_refgene_stacks('mm9', colors={'NM_011443': 'r', 'NR_015580': 'b'})
    [[<matplotlib.axes._axes.Axes object at ...>,
      <matplotlib.axes._axes.Axes object at ...>],
     [<matplotlib.axes._axes.Axes object at ...>,
      <matplotlib.axes._axes.Axes object at ...>]]
    >>> h.add_legend({'Sox2': 'red', 'Sox2 OT': 'blue'})
    <matplotlib.legend.Legend object at ...>
    >>> h.add_chipseq_tracks(load_features('test/tracks/CTCF_ES.bed')['chr3'],
    ...                      name='ES CTCF')
    [<matplotlib.axes._axes.Axes object at ...,
     <matplotlib.axes._axes.Axes object at ...>]
    >>> h.add_chipseq_tracks(load_features('test/tracks/CTCF_NPC.bed')['chr3'],
    ...                      name='NPC CTCF')
    [<matplotlib.axes._axes.Axes object at ...,
     <matplotlib.axes._axes.Axes object at ...>]
    >>> h.add_ax('sin')
    <matplotlib.axes._axes.Axes object at ...>
    >>> h['sin'].plot(xs, np.sin(xs))
    [<matplotlib.lines.Line2D object at ...>]
    >>> h['sin'].set_xlim((0, len(counts)))
    (0, 83)
    >>> h.add_colorbar()
    <matplotlib.colorbar.Colorbar object at ...>
    >>> h['horizontal_gene_track_1'].text(34558297, 0.5, 'Sox2', va='center',
    ...                                   ha='left', fontsize=5, color='r')
    <matplotlib.text.Text object at ...>
    >>> h['horizontal_gene_track_1'].add_patch(
    ...     patches.Rectangle([34541337, 0], 16960, 1, fill=False, ec='r')
    ... )
    <matplotlib.patches.Rectangle object at ...>
    >>> h.outline_communities(load_features('test/communities.bed')['chr3'])
    >>> h.add_clusters(
    ...     load_table('test/colors.tsv', get_pixelmap('test/bins_new.bed'),
    ...                dtype='|S25')['x']['Sox2'],
    ...     colors='random')
    >>> h.save('test/extendableheatmap.png')
    """
    pass