def parse_field(val):
    """
    Utility function for parsing a value that could be an int, a float, or a
    string.

    Parameters
    ----------
    val : str
        The value to parse.

    Returns
    -------
    Union[int, float, str]
        The parsed value.
    """
    try:
        return int(val)
    except ValueError:
        pass
    try:
        return float(val)
    except ValueError:
        return val