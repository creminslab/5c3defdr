from BaseExtendableHeatmap import BaseExtendableHeatmap

class CommunityExtendableHeatmap(BaseExtendableHeatmap):
    def outline_community(self, community, color='green', linewidth=2,
                          upper=True):
        x_coords = self.transform_feature(community, axis='x')
        y_coords = self.transform_feature(community, axis='y')
        xs = [x_coords['start'], x_coords['end']]
        ys = [y_coords['start'], y_coords['start']]
        if upper:
            self['root'].plot(xs, ys, color=color, linewidth=linewidth)
        else:
            self['root'].plot(ys, xs, color=color, linewidth=linewidth)
        xs = [x_coords['end'], x_coords['end']]
        ys = [y_coords['start'], y_coords['end']]
        if upper:
            self['root'].plot(xs, ys, color=color, linewidth=linewidth)
        else:
            self['root'].plot(ys, xs, color=color, linewidth=linewidth)

    def outline_communities(self, communities, color='green', linewidth=2,
                            upper=True):
        for community in communities:
            self.outline_community(community, color=color, linewidth=linewidth,
                                   upper=upper)