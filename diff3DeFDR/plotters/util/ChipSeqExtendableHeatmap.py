import numpy as np
import matplotlib.patches as patches

from BaseExtendableHeatmap import BaseExtendableHeatmap
from diff3DeFDR.common.modules2016 import check_intersect


class ChipSeqExtendableHeatmap(BaseExtendableHeatmap):
    def add_chipseq_tracks(self, features, size='10%', pad=0.05,
                           axis_limits=None, linewidth=0.4, name='chipseq',
                           color='k'):
        ax_h = self.add_chipseq_track(
            features, loc='bottom', size=size, pad=pad, axis_limits=axis_limits,
            linewidth=linewidth, name=name, color=color)
        ax_v = self.add_chipseq_track(
            features, loc='left', size=size, pad=pad, axis_limits=axis_limits,
            linewidth=linewidth, name=name, color=color)
        return [ax_h, ax_v]

    def add_chipseq_track(self, features, loc='bottom', size='10%', pad=0.05,
                          axis_limits=None, linewidth=0.4, name='chipseq',
                          color='k'):
        # deduce orientation, either h or v, and save the correct grange
        if loc in ['bottom', 'top']:
            orientation = 'horizontal'
            grange = self.grange_x
        else:
            orientation = 'vertical'
            grange = self.grange_y

        # compute track_width , assuming horizontal orientation
        track_width = grange['end'] - grange['start']

        # create the new axis
        ax = self.add_margin_ax(loc=loc, size=size, pad=pad,
                                new_ax_name='%s_%s' % (orientation, name),
                                axis_limits=axis_limits)

        features = [f if 'value' in f else dict(f, value=1) for f in features]

        # extract positions, widths, and heights for all features
        positions, widths, heights = zip(*[
            (f['start'], f['end'] - f['start'], f['value'])
            for f in features if check_intersect(f, grange) and f['value'] != 0
            and np.isfinite(f['value'])])

        # handle track height
        if axis_limits is not None:
            track_height = axis_limits[1]
        else:
            track_height = np.ceil(np.max(heights))
            if orientation == 'vertical':
                ax.set_xlim((0, track_height))
            else:
                ax.set_ylim((0, track_height))

        # prepare rectangle kwargs
        rect_kwargs = {'ec': color, 'fc': color, 'lw': linewidth}

        # plot
        for i in range(len(positions)):
            # add rectangle
            if orientation == 'horizontal':
                ax.add_patch(patches.Rectangle(
                    (positions[i], 0), widths[i], heights[i], **rect_kwargs))
            else:
                ax.add_patch(patches.Rectangle(
                    (0, positions[i]), heights[i], widths[i], **rect_kwargs))

        # add back only left y axis
        ax.axis('on')
        ax.set_frame_on(False)
        num_ticks = 3
        tick_positions = np.linspace(0, track_height, num_ticks)
        tick_labels = [''] * (num_ticks-1) + [int(track_height)]
        if orientation == 'vertical':
            ax.axes.get_yaxis().set_visible(False)
            ax.set_xticks(tick_positions)
            ax.set_xticklabels(tick_labels, rotation=270, fontsize=7)
            ax.xaxis.set_ticks_position('bottom')
            ax.get_xaxis().set_tick_params(direction='out')
        else:
            ax.axes.get_xaxis().set_visible(False)
            ax.set_yticks(tick_positions)
            ax.set_yticklabels(tick_labels, fontsize=7)
            ax.yaxis.set_ticks_position('left')
            ax.get_yaxis().set_tick_params(direction='out')

        # write name of track
        name_coords = [grange['start'] + track_width / 80, track_height]
        ha = 'left'
        va = 'top'
        rotation = 0
        if orientation == 'vertical':
            name_coords.reverse()
            ha = 'right'
            rotation = 270
        ax.text(name_coords[0], name_coords[1], name, fontsize=7, ha=ha, va=va,
                rotation=rotation)

        return ax
