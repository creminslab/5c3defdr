import numpy as np

from BaseExtendableHeatmap import BaseExtendableHeatmap


class RulerExtendableHeatmap(BaseExtendableHeatmap):
    def add_rulers(self, size='5%', pad=0.0, axis_limits=(1, 0),
                   ruler_tick_height=0.3, ruler_text_baseline=0.5):
        ax_h = self.add_ruler(loc='bottom', size=size, pad=pad,
                              new_ax_name='h_ruler', axis_limits=axis_limits,
                              ruler_tick_height=ruler_tick_height,
                              ruler_text_baseline=ruler_text_baseline)
        ax_v = self.add_ruler(loc='left', size=size, pad=pad,
                              new_ax_name='v_ruler', axis_limits=axis_limits,
                              ruler_tick_height=ruler_tick_height,
                              ruler_text_baseline=ruler_text_baseline)
        return [ax_h, ax_v]

    def add_ruler(self, loc='bottom', size='5%', pad=0.0, new_ax_name='ruler',
                  axis_limits=(1, 0), ruler_tick_height=0.3,
                  ruler_text_baseline=0.5):
        # create new axis
        ax = self.add_margin_ax(loc=loc, size=size, pad=pad,
                                new_ax_name=new_ax_name,
                                axis_limits=axis_limits)

        # deduce orientation, either h or v, and save the correct grange
        if loc in ['bottom', 'top']:
            orientation = 'horizontal'
            grange = self.grange_x
        else:
            orientation = 'vertical'
            grange = self.grange_y

        # compute tick positions
        tick_positions = np.linspace(grange['start'], grange['end'], 5)

        # compute track_width, assuming horizontal orientation
        track_width = grange['end'] - grange['start']

        # plot ticks and labels
        for i in range(1, 4):
            # ticks
            tick_coords = [(tick_positions[i], tick_positions[i]),
                           (0, ruler_tick_height)]
            if orientation == 'vertical':
                tick_coords.reverse()
            ax.plot(*tick_coords, c='k', lw=1.25)

            # labels
            label_coords = [tick_positions[i], ruler_text_baseline]
            ha = 'center'
            va = 'top'
            rotation = 0
            if orientation == 'vertical':
                label_coords.reverse()
                ha = 'right'
                va = 'center'
                rotation = 270
            ax.text(label_coords[0], label_coords[1],
                    str(int(tick_positions[i])),
                    fontsize=7, ha=ha, va=va, rotation=rotation)

        # write chromosome
        chrom_coords = [grange['start'] + track_width / 80, ruler_text_baseline]
        ha = 'left'
        va = 'top'
        rotation = 0
        if orientation == 'vertical':
            chrom_coords.reverse()
            ha = 'right'
            rotation = 270
        ax.text(chrom_coords[0], chrom_coords[1], grange['chrom'], fontsize=7,
                ha=ha, va=va, rotation=rotation)

        # write window width
        width_coords = [grange['end'] - track_width / 80, ruler_text_baseline]
        ha = 'right'
        va = 'top'
        rotation = 0
        if orientation == 'vertical':
            width_coords.reverse()
            va = 'bottom'
            rotation = 270
        ax.text(width_coords[0], width_coords[1], '%ikb' % (track_width / 1000),
                fontsize=7, ha=ha, va=va, rotation=rotation)

        return ax