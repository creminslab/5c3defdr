import numpy as np

from diff3DeFDR.common import make_clusters
from filter_near_diagonal import filter_near_diagonal


def get_background_calls(reshaped_loops):
    # Use same background calls in all benchmarks
    background_calls = {region: reshaped_loops[region] == 'background' for region in reshaped_loops}
    call_list = []
    for region in background_calls:
        x, y = np.where(background_calls[region])
        for i, j in zip(x, y):
            bin_ij_label = '{0}_BIN_{1:03d}_{0}_BIN_{2:03d}'.format(region, i, j)
            call_list.append(bin_ij_label)

    return call_list

def set_background_points_in_dataset(d, background_calls):
    for bin_ij in background_calls:
        d.df.loc[bin_ij, 'color'] = 'background'
    return d

def remove_small_clusters_from_dataset(d, R_is, region_list=None, min_cluster_size=8):
    """
    Helper method that removes very small clusters of significant points from list of points to be plotted
    :param d: DataSet object that contains a dataframe of interactions and assigned loop classifications
    :param R_is: counts superdict of interaction scores
    :param region_list: list of regions to which to apply this size filtering
    :param min_cluster_size: the minimum allowed number of points that can be plotted as a cluster. This method will
                             filter out all smaller clusters.
    :return: DataSet object d with loop classification assignments updated so that points inside of small clusters are
             ignored in plotting
    """
    if region_list is None:
        region_list = R_is.counts_list[0].keys()

    clusters = {}
    color_matrix = d.counts(name='color')
    for region in region_list:
        region_size = len(R_is.counts_list[0][region])
        color_list = []
        for i in xrange(region_size):
            for j in xrange(i):
                if not color_matrix[region][i, j] in ['', 'n.s.', 'below_significance', 'uncalled', 'nan']: #  'constitutive', 'background',
                    color_list.append({'x': i, 'y': j, 'color': color_matrix[region][i, j], 'value': 1})

        # Filter out small clusters
        all_clusters = np.array(make_clusters(color_list))
        large_mask = np.array([len(c) >= min_cluster_size for c in all_clusters])

        clusters[region] = all_clusters[large_mask]
        small_clusters_to_remove = all_clusters[np.logical_not(large_mask)]
        print '{} # of surviving clusters = {}'.format(region, len(clusters[region]))

        for c in small_clusters_to_remove:
            for_names = ['{0}_BIN_{1:03d}_{0}_BIN_{2:03d}'.format(region, p['x'], p['y']) for p in c]
            d.df.loc[for_names, 'color'] = 'n.s.'

    return d, clusters

def get_clusters_from_dataset(d, R_is, background_calls, Hdiff, fdr_threshold, constitutive_fdr_threshold,
                              min_cluster_size=8):
    """
    Helper method that removes very small clusters of significant points from list of points to be plotted
    :param d: DataSet object that contains a dataframe of interactions and assigned loop classifications
    :param R_is: counts superdict of interaction scores
    :param region_list: list of regions to which to apply this size filtering
    :param min_cluster_size: the minimum allowed number of points that can be plotted as a cluster. This method will
                             filter out all smaller clusters.
    :return: DataSet object d with loop classification assignments updated so that points inside of small clusters are
             ignored in plotting
    """
    filter_near_diagonal(d.df, k=6)

    d_const_mask = np.logical_and(d.df.isin(Hdiff)['color'],
                                  d.df['differential_qvalue'] > constitutive_fdr_threshold)
    d.df.loc[d_const_mask, 'color'] = 'constitutive'

    d_ns_mask = np.logical_and(
        np.logical_and(d.df.isin(Hdiff)['color'], d.df['differential_qvalue'] < constitutive_fdr_threshold),
        d.df['differential_qvalue'] > fdr_threshold)
    d.df.loc[d_ns_mask, 'color'] = 'n.s.'

    d = set_background_points_in_dataset(d, background_calls)

    d, clusters = remove_small_clusters_from_dataset(d, R_is, min_cluster_size=min_cluster_size)

    return clusters


