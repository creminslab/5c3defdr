import os

def check_outdir(outfile):
    if type(outfile) == dict:
        for r, f in outfile.iteritems():
            check_outdir(f)
    else:
        head, tail = os.path.split(outfile)
        if head and not os.path.exists(head):
            print('creating directory %s' % head)
            os.makedirs(head)