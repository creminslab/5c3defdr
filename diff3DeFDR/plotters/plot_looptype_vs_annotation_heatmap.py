import numpy as np
from matplotlib import pyplot as plt
import scipy.stats as stats

from diff3DeFDR.common import get_annotation_percentage_all, get_fisher_exact_pvalue_all

plt.rcParams["axes.grid"] = False


def plot_looptype_vs_annotation_heatmap(annotationmaps,
                                        looping_classes=None, constant_annotation=None,
                                        loop_type_order=None, foldchange_dict=None, hits_dict=None,
                                        annotation_order=None, threshold=0,
                                        margin=1, vmin=-2.0, vmax=2.0):
    """
    Plot a heatmap of enrichments for one fixed annotation, varying the loop
    category on the x-axis and the annotation on the other side on the y-axis.

    Parameters
    ----------
    filename : str
        String reference to a filename to save the plot to.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    looping_classes : dict of np.ndarray with str dtype
        The keys should be region names as strings, the values should be square,
        symmetric arrays of the same size and shape as the indicated region,
        with string loop category names in the positions of categorized loops.
    constant_annotation : str
        The annotation to hold constant throughout the heatmap.
    loop_type_order : list of str
        The loop categories to include on the x-axis, in order. If None, falls
        back to the sorted unique categories in ``looping_classes``.
    annotation_order : list of str, optional
        The annotations to include on the y-axis, in order. If None, falls back
        to ``sorted(annotationmap.keys())``.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.
    vmin : float
        The lowest fold change to show on the colorbar.
    vmax : float
        The highest fold change to show on the colorbar.

    Returns
    -------
    fig, ax : Matplotlib.pyplot handles
    """
    # resolve looping_classes
    if loop_type_order is None:
        loop_type_order = sorted(list(np.unique(np.concatenate(
            [looping_classes[region].flatten()
             for region in looping_classes]))))

    # resolve annotation_order
    if annotation_order is None:
        annotation_order = sorted(annotationmaps.keys())

    # prepare array for imshow
    array = []
    for i in xrange(len(annotation_order)):
        row = []
        for j in xrange(len(loop_type_order)):
            if foldchange_dict is None:
                selected_dict = get_annotation_percentage_all(
                    annotation_order[i], constant_annotation, loop_type_order[j],
                    annotationmaps, looping_classes, threshold=threshold,
                    margin=margin)
                background_dict = get_annotation_percentage_all(
                    annotation_order[i], constant_annotation, 'background',
                    annotationmaps, looping_classes, threshold=threshold,
                    margin=margin)

                if background_dict:
                    fold_enrichment = selected_dict / float(background_dict)
                    if fold_enrichment:
                        row.append(np.log2(fold_enrichment))
                    else:
                        row.append(np.log2(0.0001))
                else:
                    row.append(0)
            else:
                if loop_type_order[j] in foldchange_dict[annotation_order[i]]:
                    row.append(np.log2(foldchange_dict[annotation_order[i]][loop_type_order[j]]))
                else:
                    row.append(np.log2(0.0001))

        array.append(row)

    # prepare pvalue array
    p_values = []
    for i in xrange(len(annotation_order)):
        row = []
        for j in xrange(len(loop_type_order)):
            if hits_dict is None:
                selected_dict = get_fisher_exact_pvalue_all(
                    annotation_order[i], constant_annotation, loop_type_order[j],
                    annotationmaps, looping_classes, threshold=threshold,
                    margin=margin)
            else:
                if loop_type_order[j] in hits_dict[annotation_order[i]]:
                    category_loops_hit = hits_dict[annotation_order[i]][loop_type_order[j]]['hit']
                    category_loops_not_hit = hits_dict[annotation_order[i]][loop_type_order[j]]['not hit']
                else:
                    category_loops_hit = 0
                    category_loops_not_hit = 0

                bkgd_loops_hit = hits_dict[annotation_order[i]]['background']['hit']
                bkgd_loops_not_hit = hits_dict[annotation_order[i]]['background']['not hit']
                cont_table = [[category_loops_hit, bkgd_loops_hit],
                              [category_loops_not_hit, bkgd_loops_not_hit]]

                # return the smaller of the two single-tailed p-values
                selected_dict = min(stats.fisher_exact(cont_table, alternative='less')[1],
                                    stats.fisher_exact(cont_table, alternative='greater')[1])
            row.append(selected_dict)
        p_values.append(row)

    # plot heatmap
    fig, ax = plt.subplots(1, 1, squeeze=True)
    fig.set_size_inches(float(72) / 60.0 * len(annotation_order), float(40) / 60.0 * len(annotation_order))
    cmap = plt.get_cmap('bwr')
    im = ax.imshow(array, interpolation='none', cmap=cmap, origin='lower',
                    vmin=vmin, vmax=vmax)
    for i in xrange(len(annotation_order)):
        for j in xrange(len(loop_type_order)):
            p = p_values[i][j]
            text = ''
            if p >= 0.9:
                text = '1.0'
            elif p >= 0.1:
                text = '0.%i' % (int(10 * p + 1))
            elif p >= 0.01:
                if p >= 0.09:
                    text = '0.1'
                else:
                    text = '0.0%i' % (int(100 * p) + 1)
            else:
                text = 'E-{}'.format(int(-np.log10(p))) if np.isfinite(-np.log10(p)) else 'Inf' # Added Inf catch
            plt.text(j, i, text, ha='center', va='center')
    fig.colorbar(im)
    plt.xticks(np.arange(len(loop_type_order)), loop_type_order, rotation=45,
               ha='right')
    plt.yticks(np.arange(len(annotation_order)), annotation_order)

    return fig, ax
