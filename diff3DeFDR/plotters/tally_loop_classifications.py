
import numpy as np
import pandas as pd

from diff3DeFDR.common import get_differential_loop_class_names
import util.pyupset as pyu


def _format_loops_for_upsetr(repinfo, cluster_list):
    """
    Helper method that formats cluster colors for plotting with PyUpSet.

    Parameters
    ----------
    repinfo : DataFrame
        DataFrame created using ``diff3DeFDR.common.parse_repinfo``
    clusters : list[dict]
        List of clusters created using ``diff3DeFDR.common.make_clusters``

    Returns
    -------
    upset_dict_df : dict[str, DataFrame]

    """
    conditions = repinfo['condition'].unique()
    differential_classes = get_differential_loop_class_names(conditions)
    class2condmap = {lc: lc.split('_') for lc in differential_classes}
    class2condmap['constitutive'] = conditions

    upset_dict = {i: [] for i in conditions}
    cluster_colors = np.array([i['loop_class'] for i in cluster_list])

    for lc in class2condmap.keys():
        lc_index = np.where(cluster_colors==lc)[0]
        for cond in class2condmap[lc]:
            upset_dict[cond].extend(lc_index)

    print 'UpSet sizes:'
    for cond in upset_dict:
        rem = np.setdiff1d(upset_dict.keys(), cond)[0]
        print '  ', cond, len(np.setdiff1d(upset_dict[cond], upset_dict[rem]))
    print '  ', 'both', len(np.intersect1d(upset_dict[cond], upset_dict[rem]))

    upset_dict_df = {cond: pd.DataFrame(upset_dict[cond]) for cond in upset_dict}

    return upset_dict_df

def plot_upset_diagram(repinfo, cluster_list):
    """
    Plots an [UpSet](https://github.com/hms-dbmi/UpSetR) style venn-diagram of loop cluster classifications.

    Parameters
    ----------
    repinfo : DataFrame
        DataFrame created using ``diff3DeFDR.common.parse_repinfo``
    clusters : list[dict]
        List of clusters created using ``diff3DeFDR.common.make_clusters``

    Returns
    -------
    Matplotlib handle to plot
    """
    upsetr_dict = _format_loops_for_upsetr(repinfo, cluster_list)
    return pyu.plot(upsetr_dict, sort_by='degree')['figure']