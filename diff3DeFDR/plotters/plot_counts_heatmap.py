from diff3DeFDR.plotters import convert_grange_to_slice, ExtendableHeatmap, parse_grange

import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)


def plot_heatmap(dataset,
                 data_column='obs_over_exp',
                 rep_name=None,
                 window={},
                 colorscale=None,
                 colormap=None,
                 aggregate=None,
                 refgene_stacks_dir=None,
                 refgene_assembly_name='mm9'):
    """
    Plots a heatmap of 5C counts for any specified transformation of 5C data for a specific replicate and over a
    specific genomic region.
    
    Parameters
    ----------
    dataset : DataSet
        DataSet object containing Dataframe of counts.
    data_column : str
        Which column in dataset.df to plot.
    rep_name : str
        Which replicate in the dataset to plot. If none is specified, the first replicate in the list
        dataset.repinfo.index is plotted.
    window : dict
         Genomic coordinates over which to make this plot.
         Example: ``{'region': 'Fos', 'x': 'chr12:86201803-87697801', 'y': 'chr12:86201803-87697801'}``
    colorscale : list[int]
        Minimum and maximum counts values to map to color map
    colormap : A Matplotlib color map.
    refgene_stacks_dir : str
        Path to directory of reference gene assembly files.
    refgene_assembly_name : str
        Which assembly to use. Default is mm9.
    
    Returns
    -------
    ExtendableHeatmap handle of plot
    """
    if rep_name is None:
        rep_name = dataset.repinfo.index[0]

    # plot heatmaps
    region = window['region']
    grange_x = parse_grange(window['x'])
    grange_y = parse_grange(window['y'])

    slice_x = convert_grange_to_slice(grange_x, dataset.pixelmap[region])
    slice_y = convert_grange_to_slice(grange_y, dataset.pixelmap[region])


    if aggregate is 'min':
        cond = rep_name
        reps = dataset.repinfo.loc[dataset.repinfo['condition']==cond].index
        dataset.df[cond] = dataset.df['counts'][reps].min(axis=1)
        counts_array = dataset.counts(name=cond,
                                      region=region)
    else:
        counts_array = dataset.counts(rep=rep_name,
                                      name=data_column if dataset in dataset.df.columns else 'counts',
                                      region=region)

    # plot heatmap
    h = ExtendableHeatmap(
        array=counts_array[slice_x, slice_y],
        grange_x=grange_x,
        grange_y=grange_y,
        colorscale=colorscale[region],
        colormap=colormap
    )

    colorbar = h.add_colorbar()
    colorbar.set_ticks(colorscale[region])

    [gene_bottom, gene_left] = h.add_refgene_stacks(refgene_assembly_name,
                                                    file_path=refgene_stacks_dir,
                                                    size='10%',
                                                    intron_height=.1,
                                                    exon_height=0.5)

    for i, _ in enumerate(gene_bottom):
        gene_bottom[i].axis('on')
        gene_left[i].axis('on')

        gene_bottom[i].spines['top'].set_visible(False)
        gene_bottom[i].spines['bottom'].set_visible(False)

        gene_left[i].spines['left'].set_visible(False)
        gene_left[i].spines['right'].set_visible(False)

        gene_bottom[i].set_xticks([])
        gene_left[i].set_xticks([])

        gene_bottom[i].set_yticks([])
        gene_left[i].set_yticks([])

    gene_bottom[i].spines['bottom'].set_visible(True)
    gene_left[i].spines['left'].set_visible(True)

    [ruler_bottom, ruler_left] = h.add_rulers()
    ruler_bottom.axis('on')
    ruler_left.axis('on')

    ruler_bottom.spines['top'].set_visible(False)
    ruler_bottom.spines['bottom'].set_visible(False)

    ruler_left.spines['left'].set_visible(False)
    ruler_left.spines['right'].set_visible(False)

    ruler_bottom.set_xticks([])
    ruler_left.set_xticks([])

    ruler_bottom.set_yticks([])
    ruler_left.set_yticks([])

    return h
