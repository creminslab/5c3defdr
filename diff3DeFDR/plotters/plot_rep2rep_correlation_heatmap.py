from diff3DeFDR.common import make_pairwise_correlation_matrix, plot_correlation_matrix

def plot_rep2rep_correlation_heatmap(exp, outfile, rep_order, plot_labels=None):
    """
    Prints between replicate correlation grid.

    Parameters
    ----------
    exp : ExperimentSet
        ExperimentSet object carrying the replicate data set to assess
    rep_order : list[str]
        List of replicate label names (matching those in exp.labels) in the order they should appear in the plot
    plot_labels : list[str]
        How to label replicate names on the axes of the plot. If set to None, no labels will appear on the plot.
    outfile: str
        Filename to save the plot to

    """
    # Compute correlation matrix
    correlation_type = 'spearman'
    correlation_matrix = make_pairwise_correlation_matrix(exp.superdict(), correlation=correlation_type,
                                                          rep_order=rep_order)

    if plot_labels is None:
        plot_labels = rep_order

    # Plot correlations matrices
    plot_correlation_matrix(correlation_matrix, outfile, label_values=plot_labels)