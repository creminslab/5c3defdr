import numpy as np
import pandas as pd

from diff3DeFDR.plotters import parse_grange, convert_grange_to_slice, ExtendableHeatmap, get_colormap


def plot_classification_clusters(dataset, color_dict, window, categorical=False, refgene_stacks_dir=None,
                                 refgene_assembly_name='mm9'):
    """
    Creates a 'tetris plot' aka. plot showing outlines of classified loops that can be super imposed over a 5C counts
    heatmap.

    Parameters
    ----------
    dataset : DataSet
        object containing loop classifications in a column named 'color'
    color_dict : dict
        Mapping of loop classification to a Matplotlib compatible color.
    window : dict
        specifying genomic coordinates over which to make this plot.
        example: ``{'region': 'Fos', 'x': 'chr12:86201803-87697801', 'y': 'chr12:86201803-87697801'}``
    categorical: bool
        True = plot pixel-level loop classifications directly onto plot.
        False = Cluster adjacent pixels of the same class and plot only their outlines.
    refgene_stacks_dir : str
        Path to directory of reference gene assembly files.
    refgene_assembly_name : str
        specifying which assembly to use. Default is mm9.

    Returns
    -------
    ExtendableHeatmap handle of plot
    """

    # Ignore background and uncalled points when plotting clusters
    dataset.df.loc[pd.isnull(dataset.df['color']), 'color'] = 'n.s.'
    dataset.df.loc[dataset.df['color'] == 'background', 'color'] = 'n.s.'

    region = window['region']
    grange_x = parse_grange(window['x'])
    grange_y = parse_grange(window['y'])

    slice_x = convert_grange_to_slice(grange_x, dataset.pixelmap[region])
    slice_y = convert_grange_to_slice(grange_y, dataset.pixelmap[region])

    # plot heatmap
    if categorical:
        h = ExtendableHeatmap(
            array=dataset.counts('color', region=region, dtype='|S50')[slice_y, slice_x],
            grange_x=grange_x,
            grange_y=grange_y,
            colormap=color_dict
        )
    else:
        h = ExtendableHeatmap(
            array=np.zeros([len(dataset.pixelmap[region][slice_x]),
                            len(dataset.pixelmap[region][slice_y])]),
            grange_x=grange_x,
            grange_y=grange_y,
            colorscale=(0, 1),
            colormap=get_colormap('tetris', set_under='gray')
        )

        h.add_clusters(dataset.counts(name='color', region=region, dtype='|S50')[slice_x, slice_y],
                       colors=color_dict, weight='200x')

    [gene_bottom, gene_left] = h.add_refgene_stacks(refgene_assembly_name,
                                                file_path=refgene_stacks_dir,
                                                size='3%',
                                                intron_height=.1,
                                                exon_height=0.5)

    for i, _ in enumerate(gene_bottom):
        gene_bottom[i].axis('on')
        gene_left[i].axis('on')

        gene_bottom[i].spines['top'].set_visible(False)
        gene_bottom[i].spines['bottom'].set_visible(False)

        gene_left[i].spines['left'].set_visible(False)
        gene_left[i].spines['right'].set_visible(False)

        gene_bottom[i].set_xticks([])
        gene_left[i].set_xticks([])

        gene_bottom[i].set_yticks([])
        gene_left[i].set_yticks([])

    gene_bottom[i].spines['bottom'].set_visible(True)
    gene_left[i].spines['left'].set_visible(True)

    [ruler_bottom, ruler_left] = h.add_rulers()
    ruler_bottom.axis('on')
    ruler_left.axis('on')

    ruler_bottom.spines['top'].set_visible(False)
    ruler_bottom.spines['bottom'].set_visible(False)

    ruler_left.spines['left'].set_visible(False)
    ruler_left.spines['right'].set_visible(False)

    ruler_bottom.set_xticks([])
    ruler_left.set_xticks([])

    ruler_bottom.set_yticks([])
    ruler_left.set_yticks([])

    return h