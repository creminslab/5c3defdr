import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
import statsmodels.api as sm

from diff3DeFDR.loopCallers import SimulationSet

label_fontsize = 8


def _nb_mvr(x, a):
    """
    Mean-variance relationship of negative binomial distribution. This is a helper function used with
    scipy.optimize.curve_fit.
    """
    return a*(x**2) + x


def _gof(p, x, observed_y):
    """
    Compute measures of goodness of fit
    """

    gof_measures = {}

    # Compute R-squared
    predicted_y = p(x)  # aka predicted_y
    ybar = np.mean(observed_y)

    sstot = np.sum((observed_y - ybar) ** 2)
    ssreg = np.sum((predicted_y - ybar) ** 2)
    gof_measures['r_squared'] = ssreg / sstot

    sstot4 = np.sum(((observed_y ** 0.25) - (ybar ** 0.25)) ** 2)
    ssreg4 = np.sum(((predicted_y ** 0.25) - (ybar ** 0.25)) ** 2)
    gof_measures['r_squared4'] = ssreg4 / sstot4

    # Compute Pearson's correlation coefficient
    gof_measures['pcc'], _ = pearsonr(observed_y, predicted_y)  # Pearson's correlation coefficient

    # Compute mean squared error
    gof_measures['mse'] = np.mean((observed_y - predicted_y) ** 2)
    gof_measures['mse4'] = np.mean(((observed_y ** 0.25) - (predicted_y ** 0.25)) ** 2)

    return gof_measures


def fano_factor_versus_distance_scale_trend(simset, condition_to_plot, regions=None, show_labels=True):
    """
    Plots variance/mean for points captured at different distance scales in the input data set to be simulated.
    Useful for quickly assessing whether negative binomial distrubtion might be more approximate than Poisson for
    modeling your counts.

    Parameters
    ----------
    simset : SimulationSet
        SimulationSet object parameterized after running ``simset.get_generative_model``
    condition_to_plot : str
        Which single condition in your input set you would like to include in this plot.
    regions : list[str], optional
        What subset of regions to pool in each distance scale bin in the plot. If set to None, all regions are pooled
        in each bin.
    show_labels : bool
        If set to True, will show legend and axes labels on plot.

    Returns
    -------
    Matplotlib handles to figure and axes of plot
    """
    assert type(simset) == SimulationSet

    if regions is None or simset.globally_fit_dispersion:
        regions = simset.init_experiment.counts_list[0].keys()

    # Compute sample mean and variance across replicates
    means_dict = simset.means_dict
    vars_dict = simset.variances_dict
    mean_distance_groups_dict = simset.means_distance_groups_dict
    distance_bins = simset.distance_bins

    # Make plot and format axes
    fig, ax = plt.subplots(1, 1, squeeze=True, sharex='col')
    ax.tick_params(axis='x', labelsize=label_fontsize)
    ax.tick_params(axis='y', labelsize=label_fontsize)
    ax.set_xlabel('distance scale (bp)', fontsize=label_fontsize, alpha=int(show_labels))
    ax.set_ylabel(r'$\sigma^2/\mu$', fontsize=label_fontsize, alpha=int(show_labels))
    ax.set_axis_bgcolor('white')
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_visible(True)
        spine.set_linewidth(2)

    # Draw each subplot
    for ci, condition in enumerate([condition_to_plot]):
        cond_means = np.concatenate([means_dict[condition][r].ravel() for r in regions])
        cond_vars = np.concatenate([vars_dict[condition][r].ravel() for r in regions])

        # Remove non-finite points, outliers, and low counts points before fitting MVR
        finite_points = np.isfinite(cond_means) & np.isfinite(cond_vars)
        if simset.mean_outlier_limit is not None or simset.var_outlier_limit is not None:
            _, _, passing_index = SimulationSet._remove_outliers(cond_means, cond_means)
            passing_index &= finite_points
        means_passing = cond_means[passing_index]
        vars_passing = cond_vars[passing_index]

        # Add scatter plot of points color coded by distance scale to current subplot fitAx
        for bi, (lower, upper) in enumerate(distance_bins):
            in_bin = np.concatenate([mean_distance_groups_dict[condition][r][lower, upper]['targets'].ravel()
                                     for r in regions])
            in_bin = in_bin[passing_index]
            means_in_bin = means_passing[in_bin]
            vars_in_bin = vars_passing[in_bin]

            # Add scatter plot to current axes
            fano_index = np.mean(np.array(vars_in_bin, float) / np.array(means_in_bin, float))
            data_trend = ax.plot([lower, upper], [fano_index, fano_index], '-', color='orangered', alpha=0.8, linewidth=1)

    # Add "Poisson" line (flat line at y=1)
    regime_limits = [i[1] for i in simset.bin_properties['bin_dimensions']]
    poisson_trend = ax.plot([0, regime_limits[-1]], [1, 1], color='darkviolet', alpha=0.8, linewidth=1)

    # Tighten axis limits
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.set_xlim(xmin=0, xmax=regime_limits[-1])
    ax.set_xticks([0] + regime_limits)

    # Add legend
    if show_labels:
        ax.legend((data_trend, poisson_trend), ('Actual Data', 'Poisson'), loc='best')

    # Set figure to default large size
    fig.set_size_inches(6.25, 7.5)

    return fig, ax


def dispersion_versus_distance_scale(simset, conditions_to_plot, region_to_plot=None,
                                     show_labels=True, min_distance_scale=0):
    """
    Creates a plot of the fitted dispersion parameter of the negative binomial mean-variance relationship against
    distance scale.

    Parameters
    ----------
    simset : SimulationSet
        SimulationSet object parameterized after running ``simset.get_generative_model``
    conditions_to_plot : list[str]
        which conditions in your input set you would like to include in this plot
    region_to_plot : str
        Which region in your regional counts matrices you would like plot
    show_labels : bool
        True = will show legend and axes labels on plot, False = hide all labels on plot
    min_distance_scale : int
        Minimum distance scale in units of bp you would like to include in the plot

    Returns
    -------
    Matplotlib handles to figure and axes of plot
    """
    condition_colors = ['red', 'purple', 'blue']

    # Make plot and format axes
    regions = simset.init_experiment.mapping.keys()
    fig, ax = plt.subplots(1, 1, squeeze=True)

    ax.tick_params(axis='x', labelsize=label_fontsize)
    ax.tick_params(axis='y', labelsize=label_fontsize)
    ax.set_xlabel('distance scale (bp)', fontsize=label_fontsize, alpha=int(show_labels))
    ax.set_ylabel(r'fitted A of ' + r'$\sigma^2=A\mu^2 + \mu$', fontsize=label_fontsize,
                  alpha=int(show_labels))
    ax.set_title('Global fit' if simset.globally_fit_dispersion else 'Fit for region {}'.format(region_to_plot))

    ax.set_axis_bgcolor('white')
    for spine in ax.spines.values():
        spine.set_edgecolor('black')
        spine.set_visible(True)
        spine.set_linewidth(2)

    if simset.globally_fit_dispersion:
        region_to_plot = regions[0]
    elif region_to_plot is None:
        region_to_plot = regions[0]

    # Add dispersion vs. distance scale trend of current cellular condition to axes
    for ci, condition in enumerate(conditions_to_plot):
        distance_bins = simset.dispersion[condition][region_to_plot].keys()
        distance_bins = [(l, u) for l, u in distance_bins if l > min_distance_scale]
        for lower, upper in distance_bins:
            dispersion = simset.dispersion[condition][region_to_plot][lower, upper]
            plt.plot([lower, upper], [dispersion, dispersion], color=condition_colors[ci], alpha=0.8, linewidth=0.75,
                     label=None)

    if simset.loess_smooth > 0:
        for ci, condition in enumerate(conditions_to_plot):
            distance_bins = simset.dispersion_smoothed[condition][region_to_plot].keys()
            distance_bins = [(l, u) for l, u in distance_bins if l > min_distance_scale]
            distance_scale_average = [np.nanmean(bi) for bi in list(distance_bins)]
            smoothed_dispersion = [simset.dispersion_smoothed[condition][region_to_plot][lower, upper]
                          for lower, upper in distance_bins]

            d = pd.DataFrame.from_dict({'distance_scale_average': distance_scale_average,
                                        'smoothed_dispersion': smoothed_dispersion})
            d = d.sort_values('distance_scale_average')
            plt.plot(d['distance_scale_average'], d['smoothed_dispersion'], color=condition_colors[ci], linewidth=1.5,
                     label=condition)

    # Tighten axis limits
    regime_limits = [i[1] for i in simset.bin_properties['bin_dimensions']]
    ax.autoscale(enable=True, axis='x', tight=True)
    ax.set_xlim(xmin=0, xmax=regime_limits[-1])
    ax.set_xticks([0] + regime_limits)

    # ax.autoscale(enable=True, axis='x', tight=True)
    # ax.set_xlim(xmin=0, xmax=2500000)
    # ax.set_xticks([0, 150000, 600000, 2500000])

    # Add legend
    if show_labels:
        ax.legend()

    # Set the size of the entire figure to comfortably fit all the subplots
    fig.tight_layout()

    # Set figure to default large size
    fig.set_size_inches(6.25, 7.5)

    return fig, ax

def mean_variance_relationship_fitting(simset, distance_bins_to_plot=None, condition_to_plot=None, regions=None,
                                       plot_fits=None, xscale='log', yscale='4th-root', show_labels=True):
    """
    Plot mean-variance relation of counts in a specific distance scale bin and condition.

    Parameters
    ----------
    simset : SimulationSet
        SimulationSet object parameterized after running ``simset.get_generative_model``
    distance_bins_to_plot : list[int]
        Which distance scale bins to include in this plot. Should be indices into simset.distance_bins, the distance
        scale bins used when determining the counts generating function for each contact.
    conditions_to_plot : list[str]
        which conditions in your input set you would like to include in this plot
    regions : str
        Over which regions you would like to fit an MVR simultaneously
    plot_fits : list[str]
        Which distribution fits you would like to include in the plot legend. Options for now are just 'possion' and
        'negative binomial'.
    xscale : str
        'linear' or 'log'. What scaling of mean counts to put on the x-axis.
    yscale : str
        'linear', 'log', or '4th-root.' What scaling of counts sample variance to put on the y-axis. If '4th-root' is
        chosen, variance^(1/4) is plotted on a linear scale.
    show_labels : bool
        True = will show legend and axes labels on plot, False = hide all labels on plot. Legend will include fitted
        dispersion parameter value of negative binomial MVR as well as R^2 goodness of fit value for all fits included
        in plot_fits.

    Returns
    -------
    Matplotlib handles to figure and axes of plot

    """
    if regions is None or simset.globally_fit_dispersion:
        regions = simset.init_experiment.counts_list[0].keys()

    # Collect sample mean and variance across replicates
    means_dict = simset.means_dict
    vars_dict = simset.variances_dict
    mean_distance_groups_dict = simset.means_distance_groups_dict
    distance_bins = simset.distance_bins

    for bin in distance_bins:
        assert bin in distance_bins

    # Make plots
    fig, ax = plt.subplots(1, len(distance_bins_to_plot), squeeze=True, sharex='col', sharey='row')

    # Draw each subplot
    cond_means = np.concatenate([means_dict[condition_to_plot][r].ravel() for r in regions])
    cond_vars = np.concatenate([vars_dict[condition_to_plot][r].ravel() for r in regions])
    finite_points = np.isfinite(cond_means) & np.isfinite(cond_vars)

    for axbi, (lower, upper) in enumerate(distance_bins_to_plot):

        # Get points in distance scale bin across all regions for given condition
        in_bin = np.concatenate([mean_distance_groups_dict[condition_to_plot][r][lower, upper]['targets'].ravel()
                                 for r in regions])
        passing = in_bin & finite_points
        means_in_bin = cond_means[passing]
        vars_in_bin = cond_vars[passing]

        # Remove outliers before fitting MVR
        # if simset.mean_outlier_limit is not None or simset.var_outlier_limit is not None:
        means_in_bin, vars_in_bin, _ = SimulationSet._remove_outliers(means_in_bin, vars_in_bin,
                                                                      mean_outlier_limit=simset.mean_outlier_limit,
                                                                      var_outlier_limit=simset.var_outlier_limit)

        # Format current subplot axes cax
        cax = ax[axbi] if isinstance(ax, (np.ndarray, list)) else ax

        cax.tick_params(axis='x', labelsize=label_fontsize)
        cax.set_xscale(xscale)

        if axbi == 0:
            cax.set_ylabel('\n\nsqrt(std)', fontsize=label_fontsize)
            cax.set_yscale('linear' if yscale == '4th-root' else yscale)
            cax.tick_params(axis='y', labelsize=label_fontsize)
        else:
            cax.tick_params(axis='y', which='both', left='off', right='off', labelleft='off', labelright='off')

        bin_label = '[{0:d}kb, {1:d}kb)'.format(int(lower / 1000), int(upper / 1000))
        cax.set_title(bin_label + '\n', fontsize=label_fontsize)

        cax.set_axis_bgcolor('white')
        for spine in cax.spines.values():
            spine.set_edgecolor('black')
            spine.set_visible(True)
            spine.set_linewidth(2)

        # Add scatter plot of (mean, variance) values in given distance scale bin to current axes
        x = means_in_bin
        y = vars_in_bin
        cax.scatter(x, y ** 0.25 if yscale == '4th-root' else y, alpha=0.5, edgecolor='black',
                    facecolor='gray', linewidth=0.15)  # color_set[0]

        # Create dictionary of fittings to points in given bin
        fit_info = {}
        if 'poisson' in plot_fits:
            fit_info['Poisson'] = {'coeffs': [1, 0], 'color': 'darkviolet',
                                   'label': r'$\sigma^2 = \mu$'}

        if 'negative_binomial' in plot_fits:
            popt, _ = curve_fit(_nb_mvr, x, y)
            A = popt[0]
            fit_info['y=Ax^2+x'] = {'coeffs': [A, 1, 0],
                                    'color': 'orangered',
                                    'label': r'$\sigma^2 = ' + '{:.2f}'.format(A) + '\mu^2 + \mu$'}

        # Add fit lines to current axes
        line_handles = []  # Use array to keep track of line handles to put in legend
        for fit_name in fit_info:
            # Generate polynomial function with coefficients from the fitting
            p = np.poly1d(fit_info[fit_name]['coeffs'])

            # Compute measures of goodness of the fit
            gof_measures = _gof(p, x, y)
            fit_info[fit_name]['gof_measures'] = gof_measures

            # Plot fitting
            rx = np.linspace(np.min(x), np.max(x), 50)
            ry = p(rx)

            line_label = fit_info[fit_name]['label'] + '\n$R^2 = ' + '{:.2f}'.format(
                gof_measures['r_squared']) + '$'
            line_handle, = cax.plot(rx, ry ** 0.25 if yscale == '4th-root' else ry, '-',
                                    color=fit_info[fit_name]['color'], label=line_label, alpha=0.8,
                                    linewidth=3)

            # Add fit line's handle to our handles array (will use to generate the legend)
            line_handles.append(line_handle)

        # Add a legend to fitAx
        if show_labels:
            cax.legend(handles=line_handles, loc='upper left', fontsize=label_fontsize)

        # Tighten y axis limits
        cax.autoscale(enable=True, axis='x', tight=True)
        cax.autoscale(enable=True, axis='y', tight=True)

    # Set the size of the entire figure to comfortably fit all the subplots
    fig.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)

    # Set figure to default large size
    fig.set_size_inches(7*len(distance_bins_to_plot), 7)

    return fig, ax