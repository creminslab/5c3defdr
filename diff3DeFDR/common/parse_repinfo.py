import pandas as pd


def parse_repinfo(trigger_string):
    """
    Load tsv of replicate info as pandas.DataFrame.

    Parameters
    ----------
    trigger_string: str
        Path to tsv of replicate information.

    Returns
    -------
    df : pandas.DataFrame
        DataFrame containing information about a set of replicates.
    """
    df = pd.read_csv(trigger_string, index_col=0, sep='\s+')
    df.index = df.index.map(str)
    return df