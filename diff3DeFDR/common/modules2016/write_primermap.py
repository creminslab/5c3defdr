"""
Module for writing primermaps to bedfiles.
"""


def write_primermap(primermap, outfile, extra_column_names=None):
    """
    Write a primermap to a primer bedfile.

    Parameters
    ----------
    primermap : Dict[str, List[Dict[str, Any]]]
        The primermap to write. See ``lib5c.parsers.primers.get_primermap()``.
    outfile : str
        String reference to the file to write to.
    extra_column_names : Optional[List[str]]
        Names of additional columns to include in the bedfile.
    """
    # check if the object to be written has strand information
    has_strand = True if 'strand' in primermap[primermap.keys()[0]][0] else \
        False

    with open(outfile, 'w') as handle:
        handle.write('#chrom\tstart\tend\tname')
        if has_strand:
            handle.write('\tstrand')
        if extra_column_names is not None:
            for column_name in extra_column_names:
                handle.write('\t%s' % column_name)
        handle.write('\n')
        for region in primermap.keys():
            for i in range(len(primermap[region])):
                handle.write('%s\t%i\t%i\t%s'
                             % (primermap[region][i]['chrom'],
                                primermap[region][i]['start'],
                                primermap[region][i]['end'],
                                primermap[region][i]['name']))
                if has_strand:
                    handle.write('\t%s' % primermap[region][i]['strand'])
                if extra_column_names is not None:
                    for column_name in extra_column_names:
                        handle.write('\t%s' % primermap[region][i][column_name])
                handle.write('\n')
