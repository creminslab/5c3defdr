import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import seaborn as sns

from shifted_colormap import shifted_colormap
from get_colormap import get_colormap
from gmean import gmean
from heatmap import heatmap


def plot_bias_heatmap(obs_counts, exp_counts, primermap, factor, outfile,
                      bins=None, n_bins=None, cmap=None, vmin=None,
                      vmax=None, midpoint=None, log=True, region=None,
                      agg=gmean, asymmetric=False):
    """
    Plots a bias heatmap.

    Parameters
    ----------
    obs_counts : Dict[str, np.ndarray]
        The dict of observed counts.
    exp_counts : Dict[str, np.ndarray]
        The dict of expected counts.
    primermap : Dict[str, List[Dict[str, Any]]]
        Primermap or pixelmap describing the loci in ``obs_counts`` and
        ``exp_counts``.
    factor : str
        The bias factor to draw the bias heatmap for. This string must match a
        metadata key in ``primermap``. That is to say, if ``factor`` is
        ``'length'`` then we expect ``primermap[region][i]['length']`` to be a
        number representing the length of the ``i`` th fragment in the region
        specified by ``region``.
    outfile : str
        String reference to the file to write the heatmap to.
    bins : Optional[Sequence[numeric]]
        The endpoints of the bins to use to stratify the bias factor values.
        Either ``bins`` or ``n_bins`` must be specified.
    n_bins : Optional[int]
        The number of even-number bins to use to stratify the bias factor
        values. Either ``bins`` or ``n_bins`` must be specified.
    cmap : Optional[matplotlib.colors.Colormap]
        Pass a colormap to use for the heatmap. If this kwarg is not passed, the
        default 'bias' colormap is used.
    vmin : Optional[float]
        The minimum value to use for the heatmap. If this kwarg is not passed,
        the min of the data will be used.
    vmax : Optional[float]
        The maximum value to use for the heatmap. If this kwarg is not passed,
        the max of the data will be used.
    midpoint : Optional[float]
        The midpoint value to use for the colormap. If this kwarg is not passed,
        the colormap will be symmetric about its midpoint. This kwarg can be
        used to force the midpoint of the colormap to lie at a desired value,
        such as 0.
    log : bool
        Whether or not to show log-scale fold-enrichments in the heatmap.
    region : Optional[str]
        Pass a region name as a string to consider only the contacts in one
        particular region. If this kwarg is not passed, contacts for all regions
        in the input counts dicts will be used to generate the bias heatmap.
    agg : Callable[[np.ndarray], float]
        The aggregation function to use when summarizing the strata. This
        function should take in an array of floats and return a single summary
        value.
    asymmetric : bool
        Pass True to construct heatmaps using only the upper-triangular elements
        of the counts matrices, which can lead to asymmetric heatmaps. By
        default, the algorithm iterates over all elements of the counts
        matrices, enforcing symmetry in the bias models but incurring some
        redundancy in the actual counts information.
    """
    # resolve cmap
    if cmap is None:
        cmap = get_colormap('bias')

    # resolve regions
    regions = obs_counts.keys()
    if region is not None:
        regions = [region]

    # make dataframe
    list_of_dict = []
    for region in regions:
        for i in range(len(obs_counts[region])):
            if asymmetric:
                jrange = range(i + 1)
            else:
                jrange = range(len(obs_counts[region]))
            for j in jrange:
                if np.isfinite(obs_counts[region][i, j]):
                    list_of_dict.append(
                        {'upstream_factor'  : primermap[region][j][factor],
                         'downstream_factor': primermap[region][i][factor],
                         'obs'              : obs_counts[region][i, j],
                         'exp'              : exp_counts[region][i, j]})
    df = pd.DataFrame(list_of_dict)

    # cut/qcut
    if bins is not None:
        df['Upstream %s' % factor] = pd.cut(df.upstream_factor, bins)
        df['Downstream %s' % factor] = pd.cut(df.downstream_factor, bins)
    elif n_bins is not None:
        df['Upstream %s' % factor] = pd.qcut(df.upstream_factor, n_bins)
        df['Downstream %s' % factor] = pd.qcut(df.downstream_factor, n_bins)
    else:
        raise ValueError('must pass either bins or n_bins')

    # prepare plotting dataframe
    plot_df = (df.groupby(['Upstream %s' % factor,
                           'Downstream %s' % factor])['obs']
               .agg(lambda x: agg(x)) /
               df.groupby(['Upstream %s' % factor,
                           'Downstream %s' % factor])['exp']
               .agg(lambda x: agg(x))).unstack().iloc[::-1]
    if log:
        plot_df = np.log2(plot_df)

    # prepare cmap
    if midpoint is not None:
        cmap = shifted_colormap(cmap, midpoint=midpoint)

    # plot
    with sns.axes_style('darkgrid', {'text.color'     : 'black',
                                     'xtick.color'    : 'black',
                                     'ytick.color'    : 'black',
                                     'axes.labelcolor': 'black',
                                     'axes.edgecolor' : 'black'}), \
            sns.plotting_context(context='paper'):
        plt.clf()
        heatmap(plot_df, square=True, cmap=cmap, vmin=vmin, vmax=vmax)
        plt.yticks(rotation=0)
        plt.xticks(rotation=45, ha='right')
        plt.savefig(outfile, dpi=300, bbox_inches='tight')
