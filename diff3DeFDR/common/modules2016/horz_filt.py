from __future__ import division
import numpy as np

def horz_filt(m_star, e_star, w=15, p=5, min_percent=0.2,
              query_close_points=False):
    '''
    Computes horz filter for each pixel in the upper right part of matrix
    '''

    # w must be bigger than p
    # if p is bigger, assume user mixed up inputs
    if p > w:
        w, p = p, w
    elif p == w:
        raise Exception('w must be bigger than p')

    horz_expect = np.empty(np.shape(m_star))
    horz_expect[:] = np.NaN
    dim = len(m_star)
    max_area = (w-p)*3
    min_allowed = max_area * min_percent
    # need to make sure both sides are symmetric in size (as well as NaNs)
    for j in xrange(w, dim):
        l_bound = max(j-w, 0)
        if j + w <= dim - 1:
            r_bound = min(j+w, dim-1)
        else:
            continue
        lp_bound = max(0, j-p)
        for i in xrange(1, j-p-1):
            if i + 1 > dim - 1:
                continue
            # propagate NaNs across reflection

            # generate new numpy subarrays rather than just slicing
            m_1 = m_star[range(i-1, i+2), :][:, range(l_bound, lp_bound)]
            m_2 = m_star[range(i+1, i-2, -1), :][:, range(r_bound, j+p, -1)]
            e_1 = e_star[range(i-1, i+2), :][:, range(l_bound, lp_bound)]
            e_2 = e_star[range(i+1, i-2, -1), :][:, range(r_bound, j+p, -1)]

            # may want to change how NaNs are reflected
            # could do across diagonal rather than across line
            m_1[np.isnan(e_1)] = np.NaN
            m_2[np.isnan(e_2)] = np.NaN
            e_1[np.isnan(m_1)] = np.NaN
            e_2[np.isnan(m_2)] = np.NaN

            m_2[np.isnan(m_1)] = np.NaN
            e_2[np.isnan(e_1)] = np.NaN
            non_nan_count = np.count_nonzero(np.isfinite(m_2))

            if non_nan_count < min_allowed:
                continue

            m_1[np.isnan(m_2)] = np.NaN
            e_1[np.isnan(e_2)] = np.NaN

            numer = 0
            denom = 0

            numer += np.nansum(m_1)
            n1 = np.nansum(e_1)
            denom += n1

            numer -= np.nansum(m_2)
            n2 = np.nansum(e_2)
            denom -= n2

            horz_expect[i, j] = numer * e_star[i, j] / denom
            horz_expect[j, i] = horz_expect[i, j]

    return horz_expect
