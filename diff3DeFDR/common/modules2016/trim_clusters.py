import numpy as np
from copy import deepcopy

def trim_clusters(counts_dict,clusters_to_trim):
    """
    Wipes clusters that have been previously identified for
    removal. 

    Parameters
    ---------
    counts_dict : Dict[str,np.ndarray]
        The keys are the region name and the values are the counts matrices for
        that region
    clusters_to_trim : Dict[str,List[List[Dict[str,int]]]]
        The keys are the region names, and the values are lists of lists
        of dicts containing:

            {
             'x': int
             'y': int
             'value' : int
            }

    Returns
    -------
    Dict[str,np.ndarray]
        The keys are the region name and the values are the trimmed counts
        matrices for that region
    """
    for region in clusters_to_trim.keys():
        for i in range(len(clusters_to_trim[region])):
            for j in range(len(clusters_to_trim[region][i])):
                bin_x = clusters_to_trim[region][i][j]['x']
                bin_y = clusters_to_trim[region][i][j]['y']
                counts_dict[region][bin_x,bin_y] =  np.nan
                counts_dict[region][bin_y,bin_x] =  np.nan

    return counts_dict
