from ast import literal_eval
import numpy as np

def load_R_moments(moments_file,groups):
    """
    Loads statistical moments calculated in R based on distance or expected group values. 

    Parameters

    ----------
    moments_file : str
        String reference to location of .txt file to load moment data from.
    groups : Dict[str, Dict[float,[Dict[str, np.ndarray]]]]
        Groups dict to use in order to restore dimensions of data structures.
        the outermost keys are the region names, the second innermost keys are the group number or central expected value,
        and the innermost keys are the "targets" "indices" and "values" of each group where we want to call p-values,
        in order to fit statistical distributions.

    Returns
    -------
   Dict[str, Dict[float,[Dict[str, List[float]]]]]
        Nested dict where the outermost keys are the region names, the second innermost keys are the group number or central expected value,
        and the innermost keys are the precalculated moments.
    """
    # initialize groups data structure
    dict = {}
    for region in groups.keys():
        dict[region] = {}
        for center in sorted(groups[region].keys()):
            dict[region][center] = {}
            dict[region][center]['moment(s)'] = []

    with open(moments_file, 'r') as handle:
        for line in handle:
            moments = []
            # parse line information
            region = line.split('\t')[0].strip()
            center = literal_eval(line.split('\t')[1])
            moment1 = literal_eval(line.split('\t')[2])
            moments.append(moment1)
            if len(line.split('\t')) >= 4:
                moment2 = literal_eval(line.split('\t')[3])
                moments.append(moment2)
            dict[region][center]['moment(s)'] = moments

    return dict

