import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def trim_primers(primermap, counts_superdict, min_sum=100.0, min_frac=0.5):
    """
    Trim a primermap using counts information from many replicates.

    Parameters
    ----------
    primermap : List[Dict[str, Any]]
        The primermap to trim. See ``lib5c.parsers.primers.get_primermap()``.
    counts_superdict : Dict[str, np.ndarray]
        The keys are replicate names, the values are the counts for that rep.
    min_sum : Optional[float]
        Primers with a total cis sum lower than this value will be trimmed.
    min_frac : Optional[float]
        Primers with fewer than this fraction of nonzero interactions out of all
        their finite interactions will be trimmed.

    Returns
    -------
    Tuple[List[Dict[str, Any]], Set[int]]
        The first element is the trimmed primermap, the second is the set of
        indices of the original primermap which were removed.
    """
    # keep track of removed indices
    removed_indices = set()

    # each replicate gets its own chance to remove each primer
    for rep in counts_superdict.keys():
        # check min_sum
        if min_sum:
            for i in range(len(primermap)):
                column_sum = np.nansum(counts_superdict[rep][:, i])
                if column_sum < min_sum:
                    removed_indices.add(i)
        # check min_frac
        if min_frac:
            for i in range(len(primermap)):
                column = counts_superdict[rep][:, i]
                total_interactions = np.count_nonzero(~np.isnan(column))
                nonzero_interactions = np.count_nonzero(
                    column[np.isfinite(column)])
                fraction = nonzero_interactions / float(total_interactions)
                if fraction < min_frac:
                    removed_indices.add(i)

    # construct trimmed primermap
    trimmed_primermap = [primermap[i]
                         for i in range(len(primermap))
                         if i not in removed_indices]

    return trimmed_primermap, removed_indices
