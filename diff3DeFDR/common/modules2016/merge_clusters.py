def merge_clusters(clusters, merge_to_which):
    """
    Recursively merges clusters together from smallest to largest according to a
    specified merge function.

    Parameters
    ----------
    clusters : list of clusters
        The clusters to be merged. All elements will be removed from this list
        when this function is called.
    merge_to_which : function
        Function that takes in a list of clusters and returns the index of the
        cluster the first cluster in the list should be merged into. If the
        first cluster in the list should not be merged, this function should
        return -1.

    Returns
    -------
    list of clusters
        The list of merged clusters.
    """
    # list to store merged clusters
    merged_clusters = []

    while clusters:
        # sort the clusters from smallest to largest
        clusters.sort(key=lambda x: len(x), reverse=False)

        # attempt to merge this cluster into another cluster by adjacency
        merge_to = merge_to_which(clusters)
        if merge_to == -1:
            # this one didn't merge
            merged_clusters.append(clusters.pop(0))
        else:
            # merge this cluster into the chosen cluster
            clusters[merge_to].extend(clusters[0])
            clusters.pop(0)

    return merged_clusters