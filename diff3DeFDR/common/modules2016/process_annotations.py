from scipy.ndimage import generic_filter

from lru_cache import lru_cache


@lru_cache(maxsize=None, skip_dicts=True)
def process_annotations(annotation_label, region, annotationmaps, threshold=0,
                        margin=1):
    """
    Extracts one annotation and one region from a dict of annotationmaps and
    returns it in a vector form.

    This function should be called from within the bodies of vectorized
    enrichment functions that accept standard annotationmaps as arguments.

    Parameters
    ----------
    annotation_label : str
        The annotation for which a vector should be created. Must be a key of
        ``annotationmaps``.
    region : str
        The specific region for which a vector should be created. Must be a key
        of ``annotationmaps[annotation_label]``.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.

    Returns
    -------
    np.ndarray
        The processed vector representing the coverage of the selected
        annotation across the selected region, according to the definitions
        implied by the choise of ``threshold`` and ``margin``.

    Examples
    --------
    >>> from lib5c.algorithms.enrichment import clear_enrichment_caches
    >>> clear_enrichment_caches()
    >>> annotationmaps = {'a': {'r1': [0, 0, 2, 1]}}
    >>> process_annotations('a', 'r1', annotationmaps)
    array([0, 1, 1, 1])
    >>> process_annotations('a', 'r1', annotationmaps, threshold=1, margin=0)
    array([0, 0, 1, 0])
    """
    return generic_filter(annotationmaps[annotation_label][region],
                          lambda x: x.sum() > threshold, size=1+2*margin,
                          mode='constant')
