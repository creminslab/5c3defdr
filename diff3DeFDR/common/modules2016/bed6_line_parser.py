def bed6_line_parser(line):
    # split line on tab
    pieces = line.split('\t')

    # parse columns
    return {
        'chrom' : pieces[0].strip(),
        'start' : int(pieces[1]),
        'end'   : int(pieces[2]),
        'name'  : pieces[3].strip(),
        'score' : float(pieces[4]),
        'strand': pieces[5].strip()
    }
