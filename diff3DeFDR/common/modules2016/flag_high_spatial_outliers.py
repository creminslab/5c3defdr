import numpy as np
from scipy.ndimage.filters import generic_filter
from function_util import parallelize_regions

@parallelize_regions
def flag_high_spatial_outliers(array, size=5, fold_threshold=8.0):
    """
    Identifies which elements of an array are high spatial outliers.

    Parameters
    ----------
    array : np.ndarray
        The array to look for outliers in.
    size : int
        The size of the window to look in around each element when deciding if
        it is an outlier. Should be an odd integer.
    fold_threshold : float
        Elements will be flagged as outliers if they are greater than this
        number or greater than this many times the local median (as estimated
        using the window size in ``size``).

    Returns
    -------
    np.ndarray
        A matrix of the same size and shape as the input matrix, with 1's at
        positions flagged as high spatial outliers and 0's everywhere else.
    """
    median_filtered_array = generic_filter(array, np.nanmedian, size=size)
    flagged_indices = np.zeros_like(array)

    for i in range(len(array)):
        for j in range(i + 1):
            if array[i, j] > max(fold_threshold,
                                 fold_threshold * median_filtered_array[i, j]):
                flagged_indices[i, j] = 1
                flagged_indices[j, i] = 1

    return flagged_indices

