import numpy as np

def estimate_moments(group_values, dist_gen, log=False, pseudocount=1.0):
    if log:
        group_values = np.log(group_values + pseudocount)
    if dist_gen.name == 'norm':
        return np.nanmean(group_values), np.nanvar(group_values)
    elif hasattr(dist_gen, 'fit'):
        frozen_dist = dist_gen(*dist_gen.fit(group_values))
        return frozen_dist.stats(moments='mv')
    elif dist_gen.name == 'poisson':
        return np.nanmean(group_values), np.nanmean(group_values)
    else:
        return np.nanmean(group_values), np.nanvar(group_values)
