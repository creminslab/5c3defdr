import glob
import os

from load_features import load_features
from make_single_annotationmap import make_single_annotationmap

def make_annotationmaps(pixelmap, directory='./annotations', add_wildcard=True):
    """
    Gets a dict of annotationmaps, one for every BED file in a specified
    directory.

    Parameters
    ----------
    pixelmap: pixelmap
        The pixelmap to use to generate the annotationmap. See
        ``lib5c.parsers.bed.get_pixelmap()``.
    directory: str
        The directory to look in for BED files describing the annotations.
    add_wildcard : bool
        Pass True to add a 'wildcard' annotation that has 100 hits in every bin.
        Useful for doing "unsided" enrichments later.

    Returns
    -------
    dict of dict of lists
        The keys of the outer dict are annotation names as parsed from the names
        of the BED files in directory. The values are annotationmaps. See
        ``lib5c.util.annotationmap.get_single_annotatiomap()``.
    """
    # normalize directory
    directory = os.path.normcase(directory)

    # annotatopmaps to return
    annotationmaps = {}

    # make annotationmaps
    for path in glob.glob('%s/*.bed' % directory) + \
            glob.glob('%s/*.interval' % directory):
        annotation = load_features(path)
        name = os.path.splitext(os.path.split(path)[-1])[0]
        annotationmap = make_single_annotationmap(annotation, pixelmap)
        annotationmaps[name] = annotationmap

    # add wildcard if desired
    if annotationmaps and add_wildcard:
        any_key = annotationmaps.keys()[0]
        annotationmaps['wildcard'] = {}
        for region in annotationmaps[any_key].keys():
            annotationmaps['wildcard'][region] = \
                [100] * len(annotationmaps[any_key][region])

    return annotationmaps