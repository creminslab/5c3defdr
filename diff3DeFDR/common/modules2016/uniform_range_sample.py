import numpy as np


def uniform_range_sample(data, n_points, log_space=False):
    """
    A function that draws samples from the data, uniformly distributed across
    the range of the data, in linear space or log-space.

    Parameters
    ----------
    data : np.ndarray
        The data array to be sampled from.
    n_points : int
        The number of samples to generate.
    log_space : bool
        Determines whether the range should be sampled in log-space or linear
        space.

    Returns
    -------
    samples : np.ndarary
        An array containing uniformly sampled samples of the original data. In
        other words, this array should have a distribution resembling
        ``uniform(min(data), max(data))``, but it contains deterministically
        selected points from the real data in ``data``.

    Notes
    -----
    The sampling algorithm is not random.
    """
    if log_space:
        sorted_data = np.sort(data[data > 0.0])
    else:
        sorted_data = np.sort(data)

    if log_space:
        linspace = np.logspace(np.log10(sorted_data[0]),
                               np.log10(sorted_data[-1]),
                               n_points)
    else:
        linspace = np.linspace(sorted_data[0], sorted_data[-1], n_points)
    data_pointer = 0
    linspace_pointer = 0
    samples = []
    while data_pointer < len(sorted_data) and linspace_pointer < len(linspace):
        if sorted_data[data_pointer] >= linspace[linspace_pointer]:
            samples.append(sorted_data[data_pointer])
            linspace_pointer += 1
        else:
            data_pointer += 1
    return np.array(samples)
