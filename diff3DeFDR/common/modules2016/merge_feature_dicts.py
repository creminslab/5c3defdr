from merge_features import *
from check_intersect import *

def merge_feature_dicts(list_of_feature_dicts):
    """
    Merges all BED file dictionaries into a super dictionary. 

    Parameters
    ----------
    *list_of_feature_dicts : List[Dict[str, List[Dict[str, Any]]]]

       A list of BED file dictionaries generated from the function
       load_features. Each element in this list represents a different
       cell type (i.e. ESC, NPC, etc.)

    Returns
    -------
    Dict[str, List[Dict[str,Any]]
        Dictiionary of merged features across all cell types. 
    """
    # dict to store result of merge
    merged_features = {}

    for chrom in set().union(*list_of_feature_dicts):
        # create a list for this chromosome's merged features
        merged_features[chrom] = []

        # concatenate all features on this chromosome from all sources
        concatenated_features = sum(
            [feat_dict[chrom]
             for feat_dict in list_of_feature_dicts
             if chrom in feat_dict], [])

        # sort concatenated features
        concatenated_features.sort(key=lambda x: x['start'])

        # keep a pointer
        index = 0

        # loop across the merged features on this chromosome using the index
        while index < len(concatenated_features):
            # this will track the current merged clump
            current_feature = concatenated_features[index]

            # look at the next features to see if they overlap
            while index + 1 < len(concatenated_features) and \
                    check_intersect(current_feature,
                                    concatenated_features[index + 1]):
                # merge in the next feature
                current_feature = merge_features(
                    current_feature, concatenated_features[index + 1])
                index += 1

            # we are done merging this clump, toss it on the "done" list
            merged_features[chrom].append(current_feature)
            index += 1

    return merged_features
