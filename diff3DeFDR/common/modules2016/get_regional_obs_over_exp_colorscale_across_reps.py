import numpy as np

from flatten_counts_single_region_geometric import flatten_counts_single_region


def get_regional_obs_over_exp_colorscale_across_reps(regional_counts_superdict):
    """
    Computes a typical scale for visualizing observed over expected counts for a
    single region.

    Parameters
    ----------
    regional_counts_superdict : dict of 2d numpy arrays
        The keys of the outer dict are replicate names. The values are the
        square symmetric numpy arrays representing the counts in a certain
        region for that replicate.

    Returns
    -------
    tuple of float
        The first element of this list is the minimum value of the computed
        scale. The second element of this list is the maximum value of the
        computed scale.

    Notes
    -----
    The returned scale is computed as the mean of the observed over expected
    counts for the selected region across all replicates, plus and minus two and
    a half times the mean of the standard deviations for the selected region
    across all replicates for the maximum and the minimum, respectively.
    """
    means = []
    stds = []
    for rep in regional_counts_superdict.keys():
        flattened_counts = flatten_counts_single_region(
            regional_counts_superdict[rep], discard_nan=True)
        means.append(np.mean(flattened_counts))
        stds.append(np.std(flattened_counts))
    mean_of_means = np.mean(means)
    mean_of_stds = np.mean(stds)
    return mean_of_means - 2.5*mean_of_stds, mean_of_means + 2.5*mean_of_stds
