from deprecate import deprecate

from write_primermap import write_primermap


def write_pixelmap(pixelmap, outfile, extra_column_names=None):
    # deprecate('WRITE_PIXELMAP() IS DEPRECATED. TO AVOID THIS MESSAGE, CALL WRITE_PRIMERMAP() INSTEAD')
    return write_primermap(pixelmap, outfile, extra_column_names=extra_column_names)
