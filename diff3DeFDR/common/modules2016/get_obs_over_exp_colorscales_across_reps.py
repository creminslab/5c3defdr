from get_regional_obs_over_exp_colorscale_across_reps import \
    get_regional_obs_over_exp_colorscale_across_reps


def get_obs_over_exp_colorscales_across_reps(counts_superdict):
    """
    Computes a typical scale for visualizing observed over expected counts for
    each region.

    Parameters
    ----------
    counts_superdict : dict of dicts of 2d numpy arrays
        The keys of the outer dict are replicate names. The values are dicts
        corresponding to counts dicts (see
        ``lib5c.parsers.counts.load_counts()``), whose keys are region names and
        whose values are the arrays of counts values for that region. These
        arrays are square and symmetric.

    Returns
    -------
    dict of tuple of float
        The keys are region names as strings, the values are
        ``(lower_limit, upper_limit)`` tuples specifying the computed observed
        over expected scale for that region.
    """
    return {
        region: get_regional_obs_over_exp_colorscale_across_reps(
            {rep: counts_superdict[rep][region] for rep in counts_superdict})
        for region in counts_superdict[counts_superdict.keys()[0]].keys()}
