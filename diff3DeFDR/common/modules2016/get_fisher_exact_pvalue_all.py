from __future__ import division

import scipy.stats as stats
import numpy as np

from count_intersections_all import count_intersections_all
from lru_cache import lru_cache


@lru_cache(maxsize=None, skip_dicts=True)
def get_fisher_exact_pvalue_all(annotation_a, annotation_b, category,
                                annotationmaps, looping_classes, threshold=0,
                                margin=1):
    """
    Use Fisher's exact test to compute a one-sided p-value against the null
    hypothesis that the selected loop category's overlap with selected
    annotations across all regions is the same as the special "background"
    reference loop category's overlap with the same annotations.

    Parameters
    ----------
    annotation_a : str
        Annotation to look for on one side of the loop.
    annotation_b : str
        Annotation to look for on the other side of the loop.
    category : str
        The category of loops to consider.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    looping_classes : dict of np.ndarray with str dtype
        The keys should be region names as strings, the values should be square,
        symmetric arrays of the same size and shape as the indicated region,
        with string loop category names in the positions of categorized loops.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.

    Returns
    -------
    float
        The p-value.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.algorithms.enrichment import clear_enrichment_caches
    >>> clear_enrichment_caches()
    >>> annotationmaps = {'a': {'r1': [0, 1, 2], 'r2': [1, 0]},
    ...                   'b': {'r1': [1, 1, 0], 'r2': [0, 1]}}
    >>> looping_classes = {'r1': np.array([['npc', ''   , 'es' ],
    ...                                    [''   , ''   , 'npc'],
    ...                                    ['es' , 'npc', ''   ]],
    ...                                   dtype='a25'),
    ...                    'r2': np.array([['ips', 'es' ],
    ...                                    ['es' , ''   ]],
    ...                                   dtype='a25')}
    >>> looping_classes['r1'][looping_classes['r1'] == ''] = 'background'
    >>> looping_classes['r2'][looping_classes['r2'] == ''] = 'background'
    >>> get_fisher_exact_pvalue_all('a', 'b', 'es', annotationmaps,
    ...                             looping_classes, margin=0)
    0.3999999999999998
    >>> get_fisher_exact_pvalue_all('a', 'b', 'npc', annotationmaps,
    ...                             looping_classes, margin=0)
    0.7999999999999996
    >>> get_fisher_exact_pvalue_all('a', 'b', 'ips', annotationmaps,
    ...                             looping_classes, margin=0)
    0.60000000000000009
    """
    # count loops in the specified category
    category_loops_total = sum(
        [np.tril(looping_classes[region] == category).sum()
         for region in looping_classes])
    category_loops_hit = count_intersections_all(
        annotation_a, annotation_b, category, annotationmaps, looping_classes,
        threshold=threshold, margin=margin)
    category_loops_not_hit = category_loops_total - category_loops_hit

    # count loops in the background category
    bkgd_loops_total = sum(
        [np.tril(looping_classes[region] == 'background').sum()
         for region in looping_classes])
    bkgd_loops_hit = count_intersections_all(
        annotation_a, annotation_b, 'background', annotationmaps,
        looping_classes, threshold=threshold, margin=margin)
    bkgd_loops_not_hit = bkgd_loops_total - bkgd_loops_hit

    # short-circuit if neither category nor background have any hits
    if category_loops_hit == 0 and bkgd_loops_hit == 0:
        return 0.5

    # assemble contingency table
    cont_table = [[category_loops_hit, bkgd_loops_hit],
                  [category_loops_not_hit, bkgd_loops_not_hit]]

    # return the smaller of the two single-tailed p-values
    return min(stats.fisher_exact(cont_table, alternative='less')[1],
               stats.fisher_exact(cont_table, alternative='greater')[1])
