import numpy as np
from find_upstream_primers import find_upstream_primers
from find_nearby_fragments import find_nearby_fragments
from filter_selector import filter_selector
from function_util import parallelize_regions

@parallelize_regions
def bin_and_smooth_counts(counts, filter_function, regional_pixelmap,
                        regional_primermap, smoothing_window_width=16000,
                        filter_kwargs=None):
    """
    Convenience function for filtering a fragment-level matrix to a bin-level
    matrix.

    Parameters
    ----------
    counts : np.ndarray
        The counts matrix to filter.
    filter_function : Callable[[List[Dict[str, Any]]], float]
        The filter function to use when filtering. This function should take in
        a "neighborhood" and return the filtered value given that neighborhood.
        A neighborhood is represented as a list of "nearby points" where each
        nearby point is represented as a dict of the following form::

            {
                'value': float,
                'x_dist': int,
                'y_dist': int
            }

        where 'value' is the value at the point and 'x_dist' and 'y_dist' are
        its distances from the center of the neighborhood along the x- and
        y-axis, respectively, in base pairs. See
        ``lib5c.algorithms.filtering.filter_functions`` for examples of filter
        functions and how they can be created.
    regional_pixelmap : List[Dict[str, Any]]
        The bin map describing the bins for this region.
    regional_primermap : List[Dict[str, Any]]
        The primermap describing the primers for this region.
    smoothing_window_width : int
        This is total width of the smoothing window where nearby fragments
        within this window will be smoothed over by the ``filter_function``
    filter_kwargs : Optional[Dict[str, Any]]
        Kwargs to be passed to the ``filter_function``.

    Returns
    -------
    np.ndarray
        The filtered matrix.
    """
    # resolve function_kwargs
    if filter_kwargs is None:
        filter_kwargs = {}

    neighborhood_radius = 0.5*smoothing_window_width

    output = np.zeros((len(regional_pixelmap), len(regional_pixelmap)))

    upstream_primer_mapping = find_upstream_primers(regional_pixelmap,
                                                    regional_primermap)

    nearby_fragments = [find_nearby_fragments(i,
                                              regional_pixelmap,
                                              regional_primermap,
                                              upstream_primer_mapping,
                                              neighborhood_radius)
                        for i in range(len(output))]

    for i in range(len(output)):
        for j in range(i + 1):
            value = filter_function(filter_selector(
                counts, nearby_fragments[i], nearby_fragments[j]),
                **filter_kwargs)
            output[i, j] = value
            output[j, i] = value

    return output
