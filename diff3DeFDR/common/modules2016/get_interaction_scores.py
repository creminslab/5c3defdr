import numpy as np
from function_util import parallelize_regions

@parallelize_regions
def get_interaction_scores(pvalues):
    """
    Calculates interaction scores from p-values.

    Parameters
    ----------
    compiled_pvalues : np.ndarray
        An array of p-values for a single region

    Returns
    -------
    np.ndarray
        An array of interaction scores for a single region

    """
    return -10 * np.log2(pvalues)
