def default_bin_name_parser(name):
    """
    The default bin name parser.

    Parameters
    ---------
    name : str
        The name of the bin found in the appropriate column of the bin bedfile.

    Returns
    -------
    dict
        This dict has the following structure::

            {
                'region': str,
                'index': int
            }

        These fields are parsed from the bin name.

    Notes
    -----
    You can write other name parsers to accommodate different bin naming
    conventions.
    """
    pieces = name.split('_')
    region = pieces[0]
    if pieces[1] != 'BIN':
        raise ValueError('default bin name scheme violation')
    index = int(pieces[2])
    return {'region': region,
            'index': index}
