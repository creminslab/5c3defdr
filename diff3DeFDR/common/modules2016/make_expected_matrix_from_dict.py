import numpy as np

def make_expected_matrix_from_dict(distance_expected, distance_matrix):
    """
    Converts a fragment-level one-dimensional expected model into an expected
    matrix.

    Parameters
    ----------
    distance_expected : Dict[int, float]
        A mapping from interaction distances in units of base pairs to the
        expected value at that distance.
    distance_matrix : np.ndarray
        The pairwise distance matrix for the fragments in this region.

    Returns
    -------
    np.ndarray
        The expected matrix.
    """

    expected_matrix = np.zeros_like(distance_matrix, dtype=float)
    for i in range(len(expected_matrix)):
        for j in range(i + 1):
            expected_matrix[i, j] = distance_expected[distance_matrix[i, j]]
            expected_matrix[j, i] = distance_expected[distance_matrix[i, j]]
    return expected_matrix

