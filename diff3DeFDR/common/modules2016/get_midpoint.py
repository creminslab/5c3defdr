def get_midpoint(fragment, force_int=False):
    """
    Gets the midpoint of a fragment.

    Parameters
    ----------
    fragment : Dict[str, Any]
        The fragment to find the midpoint of. The fragment must be represented
        as a dict with at least the following keys::

            {
                'start': int,
                'end': int
            }
    force_int : bool
        Return an int rounded towards zero instead of a float.

    Returns
    -------
    float
        The midpoint of the fragment, rounded towards zero if force_int is True.

    Examples
    --------
    >>> fragment = {'start': 50, 'end': 100}
    >>> get_midpoint(fragment)
    75.0
    """
    if force_int:
        return int(get_midpoint(fragment))
    return (fragment['start'] + fragment['end']) / 2.0
