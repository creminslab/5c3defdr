def get_cluster(x, y, clusters):
    """
    Identifies which cluster, if any, a specified point belongs to.

    Parameters
    ----------
    x : int
        x-coordinate of the point to consider.
    y : int
        y-coordinate of the point to consider.
    clusters : list of clusters
        List of clusters to search.

    Returns
    -------
    int
        The index of the cluster which contains the point (x,y). If no cluster
        in the list contains the point, the value
        is -1.
    """
    for i in range(len(clusters)):
        for peak in clusters[i]:
            if peak['x'] == x and peak['y'] == y:
                return i
    return -1