"""
Module for resolving string identifiers to matplotlib colormaps.
"""

import matplotlib as mpl
import matplotlib.pyplot as plt


def get_colormap(name, reverse=False, set_under='green'):
    """
    Get a colormap given its name.

    Parameters
    ----------
    name : str
        The name of the colormap. See the Notes for special values.
    reverse : bool
        Pass True to reverse the colormap.
    set_under : str
        Color to set as the ``set_under`` color on the returned colormap. This
        is commonly used to represent NaN or undefined values.

    Returns
    -------
    matplotlib.colors.Colormap
        The requested colormap.

    Notes
    -----
    If ``name`` matches a built-in matplotlib colormap, that colormap will be
    returned. If ``name`` matches one of the following special values, the
    corresponding specialized colomap will be returned:
        * 'obs_over_exp': a colormap for visualizing fold changes in interaction
          frequencies
        * 'is': a colormap for plotting interaction score heatmaps
        * 'obs': a colormap for visualizing observed interaction frequencies
        * 'bias': a colormap for plotting bias factor heatmaps
    """
    # get base colormap
    if name == 'obs_over_exp':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'pvalue_obs_over_exp', colors=['darkblue', 'blue', 'lightblue',
                                           'white', 'white', 'white', 'white',
                                           'white', 'white', 'orange', 'red',
                                           'black'])
    elif name == 'is':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'buworb',colors=['#0049e6','#4d85ff','lightblue','white',
                             'white','white','white','white','white',
                             'white','orange','red','red','darkred','black'])
    elif name == 'tetris':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
                                'buworb', colors=['#333333','#333333'])

    elif name == 'obs':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'buworb',colors=['#666666','#858585', '#a6a6a6','#d9d9d9',
                            'white','#ffb833','red','black'])
    elif name == 'tnr':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'tnr', colors=['0.2', '0.4', '0.6', '0.8', 'white',
                            '#f97306', '#fd3c06', '#980002','black'])
    elif name == 'bias':
        cmap = mpl.colors.LinearSegmentedColormap.from_list(
            'bias', colors=['darkblue', 'mediumblue', 'deepskyblue', 'white',
                            'orange', 'orangered', 'red'])
    else:
        cmap = plt.cm.get_cmap(name)

    # reverse colormap
    if reverse:
        cmap._segmentdata = plt.cm.revcmap(cmap._segmentdata)

    # set under
    if set_under:
        cmap.set_under(set_under)

    return cmap
