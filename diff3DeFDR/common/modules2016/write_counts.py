def write_counts(counts, outfile, primermap):
    """
    Writes a standard counts file.

    Parameters
    ----------
    counts : dict of 2d arrays
        The counts to be written. The keys are the region names. The values are
        the arrays of counts values for that region. These arrays should be
        square and symmetric.
    outfile : str
        String reference to the file to write counts to.
    primermap : dict of lists of dicts
        The keys of the outer dict are region names. The values are lists, where
        the ``i`` th entry represents the ``i`` th primer or bin in that region.
        Primers or bins are represented as dicts with the following structure::

            {
                'chrom'       : str,
                'start'       : int,
                'end'         : int,
                'name'        : str,
                'orientation' : "3'" or "5'"
            }

        The orientation key is optional and only makes sense when writing
        primer-primer interaction counts. If present, impossible primer-primer
        combinations will be omitted from the output. See
        ``lib5c.parsers.primers.get_primermap()`` or
        ``lib5c.parsers.primers.get_pixelmap()``.
    """
    with open(outfile, 'wb') as handle:
        for region in counts:
            # print region, len(counts[region])
            # print primermap[region]
            for i in range(len(counts[region])):
                for j in range(i+1):
                    # skip impossible primer-primer combinations
                    if ('orientation' in primermap[region][i].keys() and
                                'orientation' in primermap[region][j].keys()):
                        if primermap[region][i]['orientation'] == \
                                primermap[region][j]['orientation']:
                            continue
                    handle.write('%s\t%s\t%s\n' % (primermap[region][i]['name'],
                                                   primermap[region][j]['name'],
                                                   counts[region][i, j]))
