import numpy as np

from donut_filt import donut_filt
from lower_left_filt import lower_left_filt
from function_util import parallelize_regions

@parallelize_regions
def get_max_donut_ll(observed,expected):

    """
    Generates a corrected expected matrix by assigning each element
    the maximum value between the donut expected and lower
    left donut expected

    Parameters
    ----------
    observed : np.ndarray
        The observed counts matrix to calculate the expected
    expected : np.ndarray
        The expected counts matrix to be corrected

    Returns
    -------
    max_expected : np.ndarray
        The corrected expected matrix that is maximized
        between the donut and lower left donut expected
 """
    donut_matrix = donut_filt(observed,expected)
    ll_matrix = lower_left_filt(observed,expected)

    max_expected = np.fmax(donut_matrix, ll_matrix)

    return max_expected
    
