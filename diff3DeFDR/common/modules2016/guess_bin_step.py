def guess_bin_step(regional_pixelmap):
    """
    Guesses the bin step from a regional pixelmap.

    Parameters
    ----------
    regional_pixelmap : List[Dict[str, Any]]
        Ordered list of bins for a single region.

    Returns
    -------
    int
        The guessed bin step for this pixelmap.
    """
    return regional_pixelmap[1]['start'] - regional_pixelmap[0]['start']
