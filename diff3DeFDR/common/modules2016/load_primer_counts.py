from deprecate import deprecate
from load_counts import load_counts

def load_primer_counts(countsfile, primermap, force_nan='always'):
    # deprecate('LOAD_PRIMER_COUNTS() IS DEPRECATED. TO AVOID THIS MESSAGE, USE LOAD_COUNTS() INSTEAD!')
    return load_counts(countsfile, primermap, force_nan=force_nan)
