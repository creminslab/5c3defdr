import numpy as np
from scipy.ndimage import generic_filter

from parallelize_regions import parallelize_regions


@parallelize_regions
def impute_values(regional_counts, size=5):
    """
    Impute missing (nan) values in a counts matrix using a local median
    estimate.

    Parameters
    ----------
    regional_counts : np.ndarray
        The counts matrix to imupte.
    size : int
        The size of the window used to compute the local median. Should be an
        odd integer.

    Returns
    -------
    np.ndarray
        The counts matrix with missing values filled in with the local median
        estimates.
    """
    imputed_matrix = np.copy(regional_counts)
    if size > 0:
        median_filtered_matrix = generic_filter(
            regional_counts, np.nanmedian, size=size)
        imputed_matrix[~np.isfinite(regional_counts)] = \
            median_filtered_matrix[~np.isfinite(regional_counts)]
    else:
        imputed_matrix[~np.isfinite(regional_counts)] = 0.0
    return imputed_matrix
