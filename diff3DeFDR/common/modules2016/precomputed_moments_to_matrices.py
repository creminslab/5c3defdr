from __future__ import division
import numpy as np
from function_util import parallelize_regions

@parallelize_regions
def precomputed_moments_to_matrices(precomputed_moments,distribution,groups):
    """
    Generates two moment matrices from precomputed moments from donut expected
    or distance based groupings.

    Parameters
    ----------
    precomputed_moments : Dict[str, Dict[float,[Dict[str, List[float]]]]]
        Precomputed moments to generate matrices from. These moments are the 
        output(s) of the ```fitdistr``` function in R. Depending on the
        distribution, precomputed moments may need to be transformed in order to
        call p-values downstream.
    distribution : str
        The string name of a statistical distribution of interest
    groups : Dict[str, Dict[float,[Dict[str, np.ndarray]]]]
        the outermost keys are the region names, the second innermost keys are
        the group number or central expected value, and the innermost keys are
        the "targets" "indices" and "values" of each group where we want to call
        p-values.

    Returns
    -------
    np.ndarray, np.ndarray
        Arrays encapsulating moment 1 and moment 2 at each [i,j] point
    """
    m1_matrix = np.zeros_like(groups[min(groups.keys())]['indices'], dtype=float)
    m2_matrix = np.zeros_like(groups[min(groups.keys())]['indices'], dtype=float)
    for group in sorted(groups.keys()):
        if distribution.lower() == 'poisson':
            m1 = precomputed_moments[group]['moment(s)'][0]
            m2 = precomputed_moments[group]['moment(s)'][0]
        elif distribution.lower() == 'nbinom':
            m1 = precomputed_moments[group]['moment(s)'][0]
            m2 = m1/(m1+ precomputed_moments[group]['moment(s)'][1])
        else:
            m1 = precomputed_moments[group]['moment(s)'][0]
            m2 = precomputed_moments[group]['moment(s)'][1]

        m1_matrix[groups[group]['targets']] = m1
        m2_matrix[groups[group]['targets']] = m2

    moment1 = m1_matrix.T + m1_matrix
    np.fill_diagonal(moment1, np.diag(m1_matrix))

    moment2 = m2_matrix.T + m2_matrix
    np.fill_diagonal(moment2, np.diag(m2_matrix))

    return moment1,moment2

