#! /usr/bin/Rscript
import subprocess
def call_R_function(function,*args):
    """
    Convenience function that calls an R script in Python along with its
    command-line args.

    Parameters
    ---------
    function : str
        The string name of the R script you'd like to call
        (i.e. 'logistic_separate_histograms.R')
    args : str
        The string name(s) of command-line arg(s). 
    
    """
    cmd = ['Rscript',function]
    for i in range(len(args)):
        cmd.append(args[i])
    subprocess.call(cmd)
