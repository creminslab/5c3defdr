def default_bin_namer(bin_index, region_name=None):
    """
    Names a bin given its index and, optionally, the name of the region.

    Parameters
    ----------
    bin_index : int
        The index of this bin, within the region if appropriate.
    region_name : Optional[str]
        The name of the region this bin is in.

    Returns
    -------
    str
        The name for this bin.

    Examples
    --------
    >>> default_bin_namer(3)
    'BIN_003'
    >>> default_bin_namer(123, region_name='Sox2')
    'Sox2_BIN_123'
    """
    if region_name is not None:
        return '%s_BIN_%03d' % (region_name, bin_index)
    return 'BIN_%03d' % bin_index
