from deprecate import deprecate
from load_counts import load_counts

def load_binned_counts(countsfile, name_parser=None,
                       pixelmap=None, force_nan='always'):
                       
    # deprecate('LOAD_BINNED_COUNTS() IS DEPRECATED. TO AVOID THIS MESSAGE, PLEASE USE LOAD_COUNTS() INSTEAD!')
    
    if pixelmap is None:
        raise TypeError('load_binned_counts() is no longer supported without passing a pixelmap.')

    return load_counts(countsfile,pixelmap,force_nan=force_nan)    
