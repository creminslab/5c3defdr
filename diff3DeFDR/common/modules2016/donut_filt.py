from __future__ import division
import numpy as np
from scipy.ndimage.filters import generic_filter

def donut_filt(m_star, e_star, w=15, p=5, min_percent=0.2, query_close_points=False):
    """
    Computes donut filter for each pixel in the upper right part of symmetric
    matrix for a static w and p.
    Note/Warning:
    This does not propagate NaNs across m_star to e_star (and vice versa)
    You will most likely want to do this before running this function.

    m_star is matrix of observed counts

    e_star is matrix of expected counts

    w is parameter for outer square

    p is parameter for inner square

    min_percent is the minimum percentage of donut that must fall in matrices
    and must be non-NaN for an expected value to be calculated.
    The minimum percentage is the primary method of determining whether or
    not a valid donut expected value can be computed.

    query_close_points determines whether or not to try to determine donut
    filter values at points that are less than 2 + p bins apart. This number
    comes from Rao et al. (2014) supplement pg. S58. The default, False,
    follows this guideline. Set to True (or some truthy value) to try to
    compute an expected value at these points

    Returns symmetric matrix of donut expected values at each point.
    All points not assigned an expected value will be a value of np.NaN
    """

    # w must be bigger than p
    # if p is bigger, assume user mixed up inputs
    if p > w:
        w, p = p, w
    elif p == w:
        raise Exception('w must be bigger than p')

    # maximum number of pixels that fall inside the filter
    max_in_filt = (2 * w + 1) ** 2 - (2 * p + 1) ** 2 - 4 * w + 4 * p
    # minimum number of non-NaN pixels for region to be allowed
    min_allowed = max_in_filt * min_percent

    # produces donut shape of 1s (inside) and 0s (outside)
    # is inside donut if column and row are not in inner p-square
    # unless it is the middle row or middle column
    footprint = [[1 if i != w and ((
                                       i > p + w or i < w - p) or (
                                   j < w - p or j > p + w)) and j != w else 0
                  for i in xrange(2 * w + 1)] for j in xrange(2 * w + 1)]

    # make array to sum number of NaN elements inside filter at a step
    nan_arr = np.zeros(m_star.shape)
    nan_arr[np.isfinite(m_star)] = 1

    ms = generic_filter(
        m_star, np.nansum, footprint=footprint, mode='constant')
    es = generic_filter(
        e_star, np.nansum, footprint=footprint, mode='constant')
    n = generic_filter(
        nan_arr, np.sum, footprint=footprint, mode='constant')

    don_expect = np.multiply(np.true_divide(ms, es), e_star)
    don_expect[np.where(n < min_allowed)] = np.nan

    # ensure symmetricality
    # - not actually necessary for donut since it is symmetric across diagonal
    for j in range(len(don_expect)):
        for i in range(j):
            don_expect[j, i] = don_expect[i, j]

    return don_expect

