import numpy as np
from scipy.stats import spearmanr,pearsonr,kendalltau

from counts_superdict_to_matrix import counts_superdict_to_matrix

def make_pairwise_correlation_matrix(counts,correlation='pearson',
                                     rep_order=None,
                                     lower_triangle_only=True):
    """
    Computes a matrix of pairwise correlation coefficients.

    Parameters
    ----------
    counts : np.ndarray or dict
        A 2 x 2 matrix or a counts_superdict where the outer keys are
        replicate names, the inner keys are region names, and the values
        are square-symmetric arrays containing counts data. 
    correlation : {'pearson', 'spearman','kendalltau'}
        Controls which correlation will be used.
    rep_order : Optional[List[str]]
        Pass a list of strings to specify the order of the replicates in the
        rows and columns of the returned correlation matrix. If this kwarg is
        omitted the columns and rows of the returned correlation matrix will be
        arranged in the iteration order of the keys of ``counts_superdict``.
    lower_triangle_only : bool
        If True, only the lower triangle of the correlation matrix will be
        filled in. 
    Returns
    -------
    np.ndarray
        The pairwise correlation matrix.
    """
    # resolve correlation
    corr_fn = None
    if correlation == 'pearson':
        corr_fn = pearsonr
    elif correlation == 'spearman':
        corr_fn = spearmanr
    elif correlation == 'kendalltau':
        corr_fn = kendalltau

    # make a matrix of all the counts
    if isinstance(counts,dict):
        counts_matrix = counts_superdict_to_matrix(
            counts, rep_order=rep_order, discard_nan=True)
    else:
        counts_matrix = counts

    # transpose column-major counts_matrix to row major
    if counts_matrix.shape[0] > counts_matrix.shape[1]:
        counts_matrix = counts_matrix.T

    # compute matrix of pairwise correlation coefficients
    correlation_matrix = np.zeros((len(counts_matrix), len(counts_matrix)))
    for i in range(len(correlation_matrix)):
        for j in range(i + 1):
            if i == j:
                correlation_matrix[i, j] = 1.0
            else:
                corr_value = corr_fn(counts_matrix[i], counts_matrix[j])[0]
                correlation_matrix[i, j] = corr_value
                if not lower_triangle_only:
                    correlation_matrix[j, i] = corr_value

    return correlation_matrix
