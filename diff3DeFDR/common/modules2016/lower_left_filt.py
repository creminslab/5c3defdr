from __future__ import division
import numpy as np
from scipy.ndimage.filters import generic_filter

def lower_left_filt(m_star, e_star, w=15, p=5, min_percent=0.2,
                    query_close_points=False):
    """
    Computes lower left filter for each pixel in the upper right
    part of matrix
    """

    # w must be bigger than p
    # if p is bigger, assume user mixed up inputs
    if p > w:
        w, p = p, w
    elif p == w:
        raise Exception('w must be bigger than p')

    # produces lower left shape of 1s (inside) and 0s (outside)
    # is inside donut if column and row are not in inner p-square
    # unless it is the middle row or middle column
    # includes 0-padding in upper right corner to make shape of footpring
    # rectangular
    footprint = [[1 if (j > w + p and i < w) or (j > w and i < w - p)
                  else 0 for i in xrange(2 * w + 1)] for j in xrange(2 * w + 1)]
    ll_expect = np.empty(np.shape(m_star))
    ll_expect[:] = np.NaN
    dim = len(m_star)
    max_in_filt = w ** 2 - p ** 2
    min_allowed = max_in_filt * min_percent

    nan_arr = np.zeros(m_star.shape)
    nan_arr[np.isfinite(m_star)] = 1

    ms = generic_filter(
        m_star, np.nansum, footprint=footprint, mode='constant')
    es = generic_filter(
        e_star, np.nansum, footprint=footprint, mode='constant')
    n = generic_filter(
        nan_arr, np.sum, footprint=footprint, mode='constant')

    ll_expect = np.multiply(np.true_divide(ms, es), e_star)
    ll_expect[np.where(n < min_allowed)] = np.nan

    for j in range(len(ll_expect)):
        for i in range(j):
            ll_expect[j, i] = ll_expect[i, j]

    return ll_expect
