from __future__ import division
import numpy as np

from function_util import parallelize_regions
from uniform_range_sample import uniform_range_sample

@parallelize_regions
def group_by_expected_value(obs_matrix, exp_matrix, fractional_tolerance=0.1,
                            expand_tolerance=True,group_size_threshold=20,
                            tolerance_multiplier=10,centers=None, n_centers=None,
                            log_space=True):
    """
    Groups elements in a matrix according to their expected value

    Parameters
    ----------
    obs_matrix : np.ndarray
        The matrix containing the actual values to group.
    exp_matrix : np.ndarray
        The matrix containing the expected values to be used to construct the
        groups.
    fractional_tolerance : float
        The fractional tolerance that controls how wide the ranges of expected
        values will be in each group.
    expand_tolerance : bool
        If True, will expand the fractional tolerance by a window multiplier
        when the number of expected values within a group falls below a threshold.
    group_size_threshold : int
        The minimum number of expected values each grouping must contain.
    tolerance_multiplier : float
        The multiplicative factor to increase the fractional tolerance by.
    centers : Optional[List[float]]
        If passed, specifies the expected values to be used as the centers for
        the groups. If not passed, a subsample of every unique expected value
        will be used as the center of its own group.

    Returns
    -------
    Dict[float, [Dict[str, np.ndarray]]]
        Each inner dict represents one group and has the following structure::

            {
                'indices': np.ndarray,
                'values': np.ndarray,
                'targets': np.ndarray
            }

        ``'indices'`` and ``'targets'`` will be boolean arrays of the same size
        and shape as ``matrix``. ``'values'`` will be the values of ``matrix``
        refered to by ``'indices'`` as a one-dimensional array. The outer dict
        maps float expected values to the inner dict representing the group at
        that expected value.
    """
    # this list will store information about the groups
    groups = {}

    # resolve n_centers
    if n_centers is None:
        n_centers = len(obs_matrix)

    # resolve centers
    if centers is None:
        centers = uniform_range_sample(
            np.unique(exp_matrix[np.isfinite(exp_matrix)]),
            n_centers,
            log_space=log_space
        )

    # there will be one group for every center
    for center in centers:
        # establish targets for this group
        group_targets = np.triu(exp_matrix == center)

        # establish indices for this group
        group_indices = np.triu(
            np.abs(exp_matrix-center)/center <= fractional_tolerance)

        # increase fractional tolerance when group indices are sparse
        if expand_tolerance:
            if len(obs_matrix[group_indices]) <= group_size_threshold:
                group_indices = np.triu(np.abs(exp_matrix-center)/center 
                                        <= fractional_tolerance*tolerance_multiplier)

        # save information about this group to a dict
        groups[center] = {'targets': group_targets,
                          'indices': group_indices,
                          'values' : obs_matrix[group_indices]}

    return groups
