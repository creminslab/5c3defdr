from __future__ import division
import numpy as np
from function_util import parallelize_regions

@parallelize_regions
def get_obs_over_exp(observed, expected, logged=True):
    """
    Convenience function that returns the observed over
    expected ratio for statistical modeling.
 
    Parameters
    ----------
    observed : np.ndarray
        The observed counts matrix of interest
    expected : np.ndarray
        The expected counts matrix of interest
    logged : bool
        Determines whether or not the observed and the
        counts data have been logged beforehand.

    Returns
    -------
    np.ndarray
        Array containing observed over expected ratios. 

    """

    if logged:
        return np.subtract(observed,expected)
    else:
        return np.divide(observed,expected)
