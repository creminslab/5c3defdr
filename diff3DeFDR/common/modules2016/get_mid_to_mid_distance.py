from get_midpoint import get_midpoint


def get_mid_to_mid_distance(fragment_a, fragment_b):
    """
    Gets the mid-to-mid distance between two fragments.

    Parameters
    ----------
    fragment_a, fragment_b : Dict[str, Any]
        The fragments to find the distance between. The fragments must be
        represented as dicts with at least the following keys::

            {
                'start': int,
                'end': int
            }

    Returns
    -------
    float
        The mid-to-mid distance
    """
    return abs(get_midpoint(fragment_a) - get_midpoint(fragment_b))
