from deprecate import deprecate
from bin_and_smooth_counts import bin_and_smooth_counts

def fragment_to_bin_filter(counts, filter_function, regional_pixelmap,
                        regional_primermap, neighborhood_radius,
                        filter_kwargs=None):
    # deprecate('FRAGMENT_TO_BIN_FILTER() IS DEPRECATED. TO AVOID THIS MESSAGE, USE BIN_AND_SMOOTH_COUNTS() INSTEAD!')
    return bin_and_smooth_counts(counts,filter_function,regional_pixelmap,
                                 regional_primermap,
                                 smoothing_window_width=2*neighborhood_radius,
                                 filter_kwargs=filter_kwargs)
