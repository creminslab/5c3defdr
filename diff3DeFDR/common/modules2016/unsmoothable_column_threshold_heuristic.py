def unsmoothable_column_threshold_heuristic(window_width, bin_step):
    """
    This function defines the heuristic that determines how long a run of
    fragment-less bins must be before it is considered "unsmoothable".

    Parameters
    ----------
    window_width : int
        The width of the filtering window in base pairs.
    bin_step : int
        The "sampling rate" or "bin step".

    Returns
    -------
    int
        The maximum length of a run of fragment-less bins must be before it is
        considered "unsmoothable".
    """
    return ((window_width / 2) / bin_step) + 1
