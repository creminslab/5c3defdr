from scipy.stats import gmean

from check_neighborhood_positive import check_neighborhood_positive
from make_filter_function import make_filter_function

def geometric_mean_filter(check_threshold=0.2,
                         check_function=check_neighborhood_positive):
    """
    Constructs a filter function that uses the unweighted geometric mean as the
    aggregating function.

    Parameters
    ----------
    check_threshold : float
        If less than this fraction of the values in a neighborhood are
        positive, the filter function will return NaN.

    Returns
    -------
    Callable[[List[Dict[str, Any]]], float]
        The constructed filter function. This function takes in a "neighborhood"
        and returns the filtered value given that neighborhood. A neighborhood
        is represented as a list of "nearby points" where each nearby point is
        represented as a dict of the following form::

            {
                'value': float,
                'x_dist': int,
                'y_dist': int
            }

        where 'value' is the value at the point and 'x_dist' and 'y_dist' are
        its distances from the center of the neighborhood along the x- and
        y-axis, respectively, in base pairs.
    """
    return make_filter_function(function=gmean,pseudocount=1,
                               check_function=check_function,
                               check_threshold=check_threshold)
