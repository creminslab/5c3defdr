import numpy as np

def reshape_clusters_into_matrix(clusters,counts_matrix):
    """
    Converts a list of clustered peaks into a np.array mask containing cluster
    IDs

    Parameters
    ---------
    clusters : List[List[Dict[str,int]]]
        List of clusters (equivalent to clusters[category][region])
    counts_matrix : Dict[str, np.ndarray(str)]
        A generic counts matrix used to initialize reshaped_clusters

    Returns
    -------
    np.ndarray
     Numpy array with same dimensions as counts_matrix. Each non-zero value is a cluster ID.  
    """
    reshaped_clusters = np.zeros_like(counts_matrix, dtype = np.int)
    for i in range(len(clusters)):
        for j in range(len(clusters[i])):
            x_bin = clusters[i][j]['x']
            y_bin = clusters[i][j]['y']
            reshaped_clusters[x_bin,y_bin] = i+1
            reshaped_clusters[y_bin,x_bin] = i+1

    return reshaped_clusters
