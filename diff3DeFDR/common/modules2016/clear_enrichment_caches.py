from process_annotations import process_annotations
from new_count_intersections import count_intersections
from count_intersections_all import count_intersections_all
from get_annotation_percentage import get_annotation_percentage
from get_annotation_percentage_all import get_annotation_percentage_all
from get_fold_change import get_fold_change
from get_fold_change_all import get_fold_change_all
from get_fisher_exact_pvalue import get_fisher_exact_pvalue
from get_fisher_exact_pvalue_all import get_fisher_exact_pvalue_all


def clear_enrichment_caches():
    """
    Clear all caches related to enrichment computations.

    This function should be called within a script whenever the content of
    ``annotationmaps`` or ``looping_classes`` changes.

    If this function is not called, any calls to enrichment computation
    functions that were made before the content of these dicts was modified will
    continue to return the old values.

    Examples
    --------
    >>> import numpy as np
    >>> annotationmaps = {'a': {'r1': [0, 0, 2, 1]},
    ...                   'b': {'r1': [1, 1, 0, 0]}}
    >>> looping_classes = {'r1': np.array([[''   , ''   , 'es' , 'ips'],
    ...                                    [''   , ''   , 'npc', 'npc'],
    ...                                    ['es' , 'npc', ''   , ''   ],
    ...                                    ['ips', 'npc', ''   , ''   ]],
    ...                                   dtype='a25')}
    >>> count_intersections('a', 'b', 'r1', 'es', annotationmaps,
    ...                     looping_classes, margin=0)
    1
    >>> looping_classes = {'r1': np.array([[''   , ''   , 'ips', 'ips'],
    ...                                    [''   , ''   , 'npc', 'npc'],
    ...                                    ['ips', 'npc', ''   , ''   ],
    ...                                    ['ips', 'npc', ''   , ''   ]],
    ...                                   dtype='a25')}
    >>> count_intersections('a', 'b', 'r1', 'es', annotationmaps,
    ...                     looping_classes, margin=0)
    1
    >>> clear_enrichment_caches()
    >>> count_intersections('a', 'b', 'r1', 'es', annotationmaps,
    ...                     looping_classes, margin=0)
    0
    """
    process_annotations.cache_clear()
    count_intersections.cache_clear()
    count_intersections_all.cache_clear()
    get_annotation_percentage.cache_clear()
    get_annotation_percentage_all.cache_clear()
    get_fold_change.cache_clear()
    get_fold_change_all.cache_clear()
    get_fisher_exact_pvalue.cache_clear()
    get_fisher_exact_pvalue_all.cache_clear()
