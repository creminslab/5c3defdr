import numpy as np

from flatten_counts_single_region_geometric import flatten_counts_single_region
from kr_balance import kr_balance
from balance_matrix import balance_matrix
from gmean import gmean
from impute_values import impute_values
from parallelize_regions import parallelize_regions


@parallelize_regions
def kr_balance_matrix(matrix, max_iter=3000, retain_scale=True,
                      imputation_size=0):
    """
    Convenience function for applying KR balancing to a counts matrix.

    Parameters
    ----------
    matrix : np.ndarray
        The matrix to balance.
    max_iter : int
        The maximum number of iterations to try.
    retain_scale : bool
        Pass True to rescale the results to the scale of the original matrix
        using a ratio of geometric means.
    imputation_size : int
        Pass an int greater than 0 to replace NaN's in the matrix with a local
        median approximation. Pass 0 to skip imputation.

    Returns
    -------
    Tuple[np.ndarray, np.ndarray, np.ndarray]
        The first array contains the balanced matrix. The second contains the
        bias vector. The third contains the residual.
    """
    if imputation_size > 0:
        imputed_matrix = impute_values(matrix, size=imputation_size)
    else:
        imputed_matrix = matrix
    bias_vector, errs = kr_balance(imputed_matrix, max_iter=max_iter)
    balanced = balance_matrix(matrix, bias_vector)
    if retain_scale:
        # temporary heuristic for scaling the kr results to the original scale
        factor = gmean(np.array(flatten_counts_single_region(matrix))) * \
                 len(matrix)
        balanced *= factor
        bias_vector *= np.sqrt(factor)
    balanced[~np.isfinite(matrix)] = np.nan
    return balanced, bias_vector[:, 0], errs
