import numpy as np

from function_util import parallelize_regions


@parallelize_regions
def group_by_distance(matrix, window_radius=1,expand_window=True,
                        group_size_threshold=20,window_expansion_factor=1):
    """
    Groups elements in a matrix according to their distance from the diagonal.

    Parameters
    ----------
    matrix : np.ndarray
        The matrix to group values in.
    window_radius : int
        How many bins away to include values when grouping.
    expand_window : bool
        If True, will add an expansion factor to the window radius
        when the number of indices falls below a threshold.
    group_size_threshold : int
        The minimum number of indices each grouping must contain.
    window_expansion_factor : int
        The number of bins to add to the window radius each time
        indices are sparse.

    Returns
    -------
    Dict[int, [Dict[str, np.ndarray]]]
        Each inner dict represents one group and has the following structure::

            {
                'indices': np.ndarray,
                'values': np.ndarray,
                'targets': np.ndarray
            }

        ``'indices'`` and ``'targets'`` will be boolean arrays of the same size
        and shape as ``matrix``. ``'values'`` will be the values of ``matrix``
        refered to by ``'indices'`` as a one-dimensional array. The outer dict
        maps integer distances to the inner dict representing the group at that
        distance.

    Examples
    --------
    >>> import numpy as np
    >>> a = np.arange(16).reshape((4,4))
    >>> a
    array([[ 0,  1,  2,  3],
           [ 4,  5,  6,  7],
           [ 8,  9, 10, 11],
           [12, 13, 14, 15]])
    >>> groups = group_by_distance(a, 1, expand_window=False)
    >>> groups[1]['indices']
    array([[ True,  True,  True, False],
           [False,  True,  True,  True],
           [False, False,  True,  True],
           [False, False, False,  True]], dtype=bool)
    >>> groups[1]['values']
    array([ 0,  1,  2,  5,  6,  7, 10, 11, 15])
    >>> groups[1]['targets']
    array([[False,  True, False, False],
           [False, False,  True, False],
           [False, False, False,  True],
           [False, False, False, False]], dtype=bool)
    """
    # this list will store information about the groups
    groups = {}
    # there will be one group for every row
    for i in range(len(matrix)):
        # establish targets for this group
        group_targets = np.zeros_like(matrix, dtype=bool)
        for j in range(len(matrix) - i):
            group_targets[j, i+j] = True

        # establish indices for this group
        group_indices = np.zeros_like(matrix, dtype=bool)
        for k in range(max(i-window_radius, 0),
                       min(i+window_radius+1, len(matrix))):
            for j in range(len(matrix) - k):
                group_indices[j, k+j] = True

        #expand window radius if group indices are sparse
        if expand_window:
            if len(matrix[group_indices]) <= group_size_threshold:
                window_radius += window_expansion_factor

        # save information about this group to a dict
        groups[i] = {'targets': group_targets,
                     'indices': group_indices,
                     'values' : matrix[group_indices]}

    return groups
