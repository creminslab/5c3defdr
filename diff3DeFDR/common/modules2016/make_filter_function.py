import numpy as np
from check_neighborhood_positive import check_neighborhood_positive

def make_filter_function(function, function_kwargs=None, pseudocount=0,
                          check_function=check_neighborhood_positive, check_threshold=0.2):
    """
    Constructs a filter function that passes the values in the neighborhood to
    an aggregation function.

    Parameters
    ----------
    function : Callable[Sequence[float], float]
        The aggregation function to use on the values in each neighborhood.
    function_kwargs : Optional[Dict[str, Any]]
        Kwargs to be passed to ``function``.
    pseudocount : float
        A pseudocount to be added to the values before applying the aggregation
        function. Useful if the aggregation function has catastrophic behavior
        when one input value is zero.
    check_function : Optional[Callable[[List[Dict[str, Any]], float], bool]]
        A function that takes in a neighborhood and a threshold value and
        performs some sort of test on the neighborhood, returning False if the
        filter function should return NaN for the neighborhood because it fails
        some critical condition.
    check_threshold : float
        The threshold to pass as the second arg to ``check_function``.

    Returns
    -------
    Callable[[List[Dict[str, Any]]], float]
        The constructed filter function. This function takes in a "neighborhood"
        and returns the filtered value given that neighborhood. A neighborhood
        is represented as a list of "nearby points" where each nearby point is
        represented as a dict of the following form::

            {
                'value': float,
                'x_dist': int,
                'y_dist': int
            }

        where 'value' is the value at the point and 'x_dist' and 'y_dist' are
        its distances from the center of the neighborhood along the x- and
        y-axis, respectively, in base pairs.
    """
    # resolve function_kwargs
    if function_kwargs is None:
        function_kwargs = {}

    def filter_function(neighborhood):
        # honor check API
        if check_function is not None:
            if not check_function(neighborhood, check_threshold):
                return np.nan
        #extract values while adding a pseudocount
        values = [i['value'] + pseudocount
                  for i in neighborhood
                  if np.isfinite(i['value'])]

        # compute result based on function
        if values:
            return function(values, **function_kwargs)
        else:
            return np.nan

    return filter_function

