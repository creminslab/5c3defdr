import numpy as np
from make_expected_matrix_from_list import make_expected_matrix_from_list


def make_expected_matrix_binned(obs_matrix, exp_model, is_global=True,
                                **kwargs):
    """
    Convenience function for quickly making an expected matrix for a
    bin-level observed counts matrix.

    Parameters
    ----------
    obs_matrix : Dict[str,np.nadarray] or np.ndarray
        The dict or matrix of observed counts.
    exp_model : Callable[List[float]]
        a function that generates an expected model from binned data
    is_global : bool
        sets the scale of the expected model. If False, the regional
        expected model will be determined. 

    Returns
    -------
    Dict[str, np.ndarray] or np.ndarray
        The expected dict or matrix.
    """
    if isinstance(obs_matrix, np.ndarray):
        expected_model = exp_model(obs_matrix, is_global=False, **kwargs)
        return make_expected_matrix_from_list(expected_model)
    elif isinstance(obs_matrix, dict):
        if is_global:
            expected_model = exp_model(obs_matrix, is_global=True, **kwargs)
            return {region: make_expected_matrix_from_list(expected_model)[
                            0:len(obs_matrix[region]),
                            0:len(obs_matrix[region])] for region in
                    obs_matrix.keys()}
        else:
            expected_model = {region: exp_model(
                obs_matrix[region], is_global=False, **kwargs)
                              for region in obs_matrix.keys()}
            return {region: make_expected_matrix_from_list(
                expected_model[region]) for region in obs_matrix.keys()}
    else:
        raise TypeError('obs_matrix must be either np.ndarray or dict')
