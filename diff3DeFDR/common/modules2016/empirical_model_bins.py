import numpy as np

from gmean import gmean


def empirical_model_bins(counts, log_transform=True, is_global=True):
    """
    Make a one-dimensional bin-level expected model by taking an average of the
    interaction values at each distance.

    Parameters
    ----------
    counts : Dict[str, np.ndarray] or np.ndarray
        The observed counts dict (if called with``is_global=True``) or matrix
        for this region (if called with ``is_global=False``).
    log_transform : bool
        Pass True to take the geometric mean instead of the arithmetic mean,
        which is equivalent to averaging log-transformed counts.
    is_global : bool
        Determines whether to compute a global or regional expected model. If
        set to False, the algorithm expects ``counts`` to be a ``np.ndarray``
        and will return a regional expected model.

    Returns
    -------
    List[float]
        The one-dimensional expected model. The ``i`` th element of the list
        corresponds to the expected value for interactions between loci
        separated by ``i`` bins. If called with ``is_global=True``, the length
        of this list will match the size of the largest region in the input
        counts dict.
    """
    # make offdiagonals
    if is_global:
        offdiagonals = [np.concatenate([np.diag(counts[region], k=i)
                                        for region in counts.keys()])
                        for i in range(max([len(counts[region])
                                            for region in counts.keys()]))]
    else:
        offdiagonals = [np.diag(counts, k=i)
                        for i in range(len(counts))]

    # pick appropriate mean function
    mean_function = np.nanmean
    if log_transform:
        mean_function = gmean

    # fill nans and return
    return [mean_function(offdiagonal)
            if np.any(np.isfinite(offdiagonal))
            else np.nan
            for offdiagonal in offdiagonals]
