from deprecate import deprecate
from default_primer_name_parser import default_primer_name_parser
from load_primermap import load_primermap

def generate_primermap(bedfile, name_parser=default_primer_name_parser,
                       strand_index=5, region_index=None, column_names=None):
    
    # deprecate('GENERATE_PRIMERMAP() IS DEPRECATED. TO AVOID THIS MESSAGE, USE LOAD_PRIMERMAP()!')
    
    return load_primermap(bedfile,name_parser=name_parser,strand_index=strand_index,
                             region_index=region_index, column_names=None)
