from get_cluster import get_cluster
def identify_nearby_clusters(cluster, clusters):
    """
    Figures out which other clusters from a list of clusters are adjacent to a
    query cluster.

    Parameters
    ----------
    cluster : cluster
        The query cluster to consider.
    clusters : list of clusters
        The clusters to check for adjacency to the query cluster.

    Returns
    -------
    list of int
        The indices of clusters within the list of clusters that were found to
        be adjacent to the query cluster.

    Notes
    -----
    This may include duplicates. To get rid of them, just use::

        set(identify_nearby_clusters(cluster, clusters))

    To identify one nearby cluster at random, use::

        nearby_clusters = identify_nearby_clusters(cluster, clusters)
        if nearby_clusters:
            nearby_clusters[0]

    To see what clusters are near a single peak, use::

        identify_nearby_clusters([peak], clusters)

    If cluster is in clusters, the cluster will be reported as adjacent to
    itself. As an example of how to avoid this in
    cases where it is undesirable, use::

        filter(lambda x: x > 0, identify_nearby_clusters(clusters[0], clusters))

    """
    nearby = []
    threshold = -1
    for peak in cluster:
        up = get_cluster(peak['x'], peak['y'] + 1, clusters)
        if up > threshold:
            nearby.append(up)
        down = get_cluster(peak['x'], peak['y'] - 1, clusters)
        if down > threshold:
            nearby.append(down)
        left = get_cluster(peak['x'] - 1, peak['y'], clusters)
        if left > threshold:
            nearby.append(left)
        right = get_cluster(peak['x'] + 1, peak['y'], clusters)
        if right > threshold:
            nearby.append(right)
        ne = get_cluster(peak['x'] + 1, peak['y'] + 1, clusters)
        if ne > threshold:
            nearby.append(ne)
        se = get_cluster(peak['x'] + 1, peak['y'] - 1, clusters)
        if se > threshold:
            nearby.append(se)
        nw = get_cluster(peak['x'] - 1, peak['y'] + 1, clusters)
        if nw > threshold:
            nearby.append(nw)
        sw = get_cluster(peak['x'] - 1, peak['y'] - 1, clusters)
        if sw > threshold:
            nearby.append(sw)
    return nearby
