import numpy as np
import scipy.stats as stats

def freeze_distribution(dist_gen, mu, sigma_2):
    """
    Create a frozen distribution of a given type, given a mean and variance.

    Parameters
    ----------
    dist_gen : scipy.stats.rv_generic
        The distribution to use.
    mu : float
        The desired mean.
    sigma_2 : float
        The desired variance.

    Returns
    -------
    scipy.stats.rv_frozen
        A frozen distribution of the specified type with specified mean and
        variance.

    Notes
    -----
    This function does not perform any fitting, because it assumes that the
    first two moments directly and uniquely identify the desired distribution.

    Examples
    --------
    >>> frozen_dist = freeze_distribution(stats.poisson, 4.0, 4.0)
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((frozen_dist.dist.name,) + frozen_dist.stats(moments='mv')))
    poisson distribution with mean 4.00 and variance 4.00

    >>> frozen_dist = freeze_distribution(stats.nbinom, 4.0, 3.0)
    falling back to Poisson 3.00 < 4.00
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((frozen_dist.dist.name,) + frozen_dist.stats(moments='mv')))
    poisson distribution with mean 4.00 and variance 4.00

    >>> frozen_dist = freeze_distribution(stats.nbinom, 3.0, 4.0)
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((frozen_dist.dist.name,) + frozen_dist.stats(moments='mv')))
    nbinom distribution with mean 3.00 and variance 4.00

    >>> frozen_dist = freeze_distribution(stats.norm, 4.0, 3.0)
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((frozen_dist.dist.name,) + frozen_dist.stats(moments='mv')))
    norm distribution with mean 4.00 and variance 3.00

    >>> frozen_dist = freeze_distribution(stats.logistic, 4.0, 3.0)
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((frozen_dist.dist.name,) + frozen_dist.stats(moments='mv')))
    logistic distribution with mean 4.00 and variance 3.00
    """
    if dist_gen.name == 'norm':
        frozen_dist = dist_gen(loc=mu, scale=np.sqrt(sigma_2))
    elif dist_gen.name == 'logistic':
        frozen_dist = dist_gen(loc=mu, scale=np.sqrt(3 * sigma_2) / np.pi)
    elif dist_gen.name == 'nbinom' and sigma_2 > mu:
        p = 1 - (sigma_2 - mu) / sigma_2
        n = mu ** 2 / (sigma_2 - mu)
        frozen_dist = dist_gen(n, p)
    elif dist_gen.name == 'nbinom' and not sigma_2 > mu:
        print('falling back to Poisson %0.2f < %0.2f' % (sigma_2, mu))
        frozen_dist = stats.poisson(mu=mu)
    elif dist_gen.name == 'poisson':
        frozen_dist = dist_gen(mu=mu)
    else:
        raise NotImplementedError('combination not supported')
    return frozen_dist
