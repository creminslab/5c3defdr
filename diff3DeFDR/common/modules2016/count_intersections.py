from check_intersect import check_intersect


def count_intersections(query_feature, feature_set):
    """
    Counts the number of times a query feature is hit by a set of other
    features.

    Parameters
    ----------
    query_feature : Dict[str, Any]
        The feature to count intersections for.
    feature_set : List[Dict[str, Any]]
        The set of features to intersect with the query feature.

    Returns
    -------
    int
        The number of intersections

    Notes
    -----
    Features are represented as dicts with the following structure::

            {
                'chrom': str
                'start': int,
                'end'  : int,
            }

    See ``lib5c.parsers.bed.load_features()``.
    """
    counter = 0
    for feature in feature_set:
        if check_intersect(query_feature, feature):
            counter += 1
    return counter
