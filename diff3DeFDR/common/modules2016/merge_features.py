def merge_features(a, b):
    """
    Merge two features, assuming that they intersect.

    Parameters
    ----------
    a, b : Dict[str, Any]
        The features to merge.

    Returns
    -------
    Dict[str, Any]
        The merged feature.
    """
    # track what original features were merged into this one
    a_ancestry = a['merged_from'] if 'merged_from' in a else [a]
    b_ancestry = b['merged_from'] if 'merged_from' in b else [b]
    merged_from = a_ancestry + b_ancestry

    # construct merged feature and return
    return {
        'chrom': a['chrom'],
        'start': min(a['start'], b['start']),
        'end': max(a['end'], b['end']),
        'merged_from': merged_from
    }

