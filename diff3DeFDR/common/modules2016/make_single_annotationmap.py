from count_intersections import count_intersections

def make_single_annotationmap(annotation, pixelmap):
    """
    Generates an annotationmap given an annotation and a pixelmap.

    Parameters
    ----------
    annotation : dict of lists of dicts
        The keys are chromosome names. The values are lists of features for that
        chromosome. The features are represented as dicts with the following
        structure::

            {
                'chrom': str
                'start': int,
                'end'  : int,
            }

        See ``lib5c.parsers.bed.load_features()``.
    pixelmap: pixelmap
        The pixelmap to use to generate the annotationmap. See
        ``lib5c.parsers.bed.get_pixelmap()``.

    Returns
    -------
    dict of lists
        The keys of the dictionary are region names. The values are lists, where
        the ``i`` th entry represents the number of intersections between the
        annotation and the ``i`` th bin of that region.
    """
    annotationmap = {}
    for region in pixelmap:
        annotationmap[region] = []
        for i in range(len(pixelmap[region])):
            if pixelmap[region][i]['chrom'] in annotation:
                annotationmap[region].append(
                    count_intersections(
                        pixelmap[region][i],
                        annotation[pixelmap[region][i]['chrom']]))
            else:
                annotationmap[region].append(0)
    return annotationmap