import numpy as np
from estimate_moments import estimate_moments
from function_util import parallelize_regions

@parallelize_regions
def make_moment_matrices(groups, dist_gen,log=False):
    """
    Generates mean and variance matrices from group "values"

    Parameters
    ----------
    groups : Dict[str, Dict[float,[Dict[str, np.ndarray]]]]
        the outermost keys are the region names, the second innermost keys are the group number or central expected value,
        and the innermost keys are the "targets" "indices" and "values" of each group where we want to call p-values.
    dist_gen : scipy.stats.distributions.dist_gen
        Instance of a scipy stats distribution (i.e. scipy.stats.norm or scipy.stats.poisson) to use in order
        to estimate mean and variance appropriately.
    log : bool
        if True, group "values" will be logged when finding mean and variance from group values.
    Returns
    -------
    np.ndarray, np.ndarray
        Arrays encapsulating mean and variance at each [i,j] point
    """
    expected_matrix = np.zeros_like(groups[min(groups.keys())]['indices'], dtype=float)
    variance_matrix = np.zeros_like(groups[min(groups.keys())]['indices'], dtype=float)
    
    for group in sorted(groups.keys()):
        mean, variance = estimate_moments(groups[group]['values'], dist_gen, log=log)
        expected_matrix[groups[group]['targets']] = mean
        variance_matrix[groups[group]['targets']] = variance

    expected = expected_matrix.T + expected_matrix
    np.fill_diagonal(expected, np.diag(expected_matrix))

    var = variance_matrix.T + variance_matrix
    np.fill_diagonal(var, np.diag(variance_matrix))

    return expected, var

