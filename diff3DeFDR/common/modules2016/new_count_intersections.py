import numpy as np

from process_annotations import process_annotations
from lru_cache import lru_cache


@lru_cache(maxsize=None, skip_dicts=True)
def count_intersections(annotation_a, annotation_b, region, category,
                        annotationmaps, looping_classes, threshold=0, margin=1):
    """
    Counts the number of times one annotation intersects another at a particular
    category of called loops within a specified region.

    Parameters
    ----------
    annotation_a : str
        The annotation to look for on one side of the loop. Must be a key into
        ``annotationmaps``.
    annotation_b : str
        The annotation to look for on the other side of the loop. Must be a key
        into ``annotationmaps``.
    region : str
        The region to count intersections over.
    category : str
        The loop category to count intersections for.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    looping_classes : dict of np.ndarray with str dtype
        The keys should be region names as strings, the values should be square,
        symmetric arrays of the same size and shape as the indicated region,
        with string loop category names in the positions of categorized loops.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.

    Returns
    -------
    int
        The total number of intersections.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.algorithms.enrichment import clear_enrichment_caches
    >>> clear_enrichment_caches()
    >>> annotationmaps = {'a': {'r1': [0, 0, 2, 1]},
    ...                   'b': {'r1': [1, 1, 0, 0]}}
    >>> looping_classes = {'r1': np.array([[''   , ''   , 'es' , 'ips'],
    ...                                    [''   , ''   , 'npc', 'npc'],
    ...                                    ['es' , 'npc', ''   , ''   ],
    ...                                    ['ips', 'npc', ''   , ''   ]],
    ...                                   dtype='a25')}
    >>> count_intersections('a', 'b', 'r1', 'es', annotationmaps,
    ...                     looping_classes, margin=0)
    1
    >>> count_intersections('a', 'b', 'r1', 'npc', annotationmaps,
    ...                     looping_classes, margin=0)
    2
    >>> count_intersections('a', 'b', 'r1', 'npc', annotationmaps,
    ...                     looping_classes, margin=0)
    2
    >>> count_intersections.cache_info()
    CacheInfo(hits=1, misses=2, maxsize=None, currsize=2)
    """
    temp = np.outer(
        process_annotations(annotation_a, region, annotationmaps,
                            threshold=threshold, margin=margin),
        process_annotations(annotation_b, region, annotationmaps,
                            threshold=threshold, margin=margin)) > 0
    return np.tril(looping_classes[region] == category)[temp | temp.T].sum()
