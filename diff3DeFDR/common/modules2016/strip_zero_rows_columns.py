import numpy as np

def strip_zero_rows_columns(sym_mat):
    """
    Given symmetric 2D numpy array sym_mat, removes rows and columns that do not
    contain any non-zero entries
    """
    zero_cols = np.where(np.all(sym_mat == 0, axis=1))
    reduced_mat = np.delete(sym_mat, zero_cols, axis=1)
    reduced_mat = np.delete(reduced_mat, zero_cols, axis=0)
    return reduced_mat

