import numpy as np
def write_group_values(groups,outfile):
    """
    Writes a dict of distance or expected based group values. 

    Parameters
    ----------
    groups : Dict[str, Dict[str, np.ndarray]]
        The outer keys are region names as strings, the inner keys are the group centers,
        and the values are arrays containing group distributions. 
    outfile : str
        A string reference to a file to write the group values to.
    """
    with open(outfile, 'w') as handle:
        for region in groups.keys():
            for center in sorted(groups[region].keys()):
                handle.write('%s\t%s\t%s\n' %
                            (region,
                             center,
                             str(list(groups[region][center]['values'])).replace('[','').replace(']','')))

