import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def log(array, pseudocount=1.0, base='e'):
    """
    Logs an array (element-wise).

    Parallelized via ``@parallelize_regions``.

    Emits nan when logging a negative number, and -inf when logging zero.

    Parameters
    ----------
    array : np.ndarray
        The array to log.
    pseudocount : float
        Psuedocount to add before logging.
    base : str or float
        The base to use when logging. Acceptable string values are 'e', '2', or
        '10'.

    Returns
    -------
    np.ndarray
        The logged array.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.util.mathematics import log
    >>> a = np.exp(np.array([[1, 2], [2, 4.]]))
    >>> log(a, pseudocount=0)
    array([[ 1.,  2.],
           [ 2.,  4.]])
    >>> a -= 1 #  the default pseudocount will add this back before logging
    >>> a[0, 0] = -2  # what happens to negative values?
    >>> log(a)
    array([[ nan,   2.],
           [  2.,   4.]])
    >>> b = np.power(42, np.array([[1, 2], [2, 4.]]))
    >>> log(b, base=42, pseudocount=0)
    array([[ 1.,  2.],
           [ 2.,  4.]])
    """
    log_fns = {'e': np.log, '2': np.log2, '10': np.log10}
    if str(base) not in log_fns.keys():
        log_fn = lambda x: np.log(x)/np.log(float(base))
    else:
        log_fn = log_fns[str(base)]
    return log_fn(array + pseudocount)
