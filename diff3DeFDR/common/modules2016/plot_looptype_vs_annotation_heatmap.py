from __future__ import division

import numpy as np
from matplotlib import pyplot as plt

from get_annotation_percentage_all import get_annotation_percentage_all
from get_fisher_exact_pvalue_all import get_fisher_exact_pvalue_all


def plot_looptype_vs_annotation_heatmap(filename, annotationmaps,
                                        looping_classes, constant_annotation,
                                        loop_type_order=None,
                                        annotation_order=None, threshold=0,
                                        margin=1, vmin=-2.0, vmax=2.0):
    """
    Plot a heatmap of enrichments for one fixed annotation, varying the loop
    category on the x-axis and the annotation on the other side on the y-axis.

    Parameters
    ----------
    filename : str
        String reference to a filename to save the plot to.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    looping_classes : dict of np.ndarray with str dtype
        The keys should be region names as strings, the values should be square,
        symmetric arrays of the same size and shape as the indicated region,
        with string loop category names in the positions of categorized loops.
    constant_annotation : str
        The annotation to hold constant throughout the heatmap.
    loop_type_order : list of str
        The loop categories to include on the x-axis, in order. If None, falls
        back to the sorted unique categories in ``looping_classes``.
    annotation_order : list of str, optional
        The annotations to include on the y-axis, in order. If None, falls back
        to ``sorted(annotationmap.keys())``.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.
    vmin : float
        The lowest fold change to show on the colorbar.
    vmax : float
        The highest fold change to show on the colorbar.
    """
    # resolve looping_classes
    if loop_type_order is None:
        loop_type_order = sorted(list(np.unique(np.concatenate(
            [looping_classes[region].flatten()
             for region in looping_classes]))))

    # resolve annotation_order
    if annotation_order is None:
        annotation_order = sorted(annotationmaps.keys())

    # prepare array for imshow
    array = []
    for i in range(len(annotation_order)):
        row = []
        for j in range(len(loop_type_order)):
            selected_dict = get_annotation_percentage_all(
                annotation_order[i], constant_annotation, loop_type_order[j],
                annotationmaps, looping_classes, threshold=threshold,
                margin=margin)
            background_dict = get_annotation_percentage_all(
                annotation_order[i], constant_annotation, 'background',
                annotationmaps, looping_classes, threshold=threshold,
                margin=margin)

            if background_dict:
                fold_enrichment = selected_dict/ float(background_dict)
                if fold_enrichment:
                    row.append(np.log2(fold_enrichment))
                else:
                    row.append(np.log2(0.0001))
            else:
                row.append(0)
        array.append(row)

    # prepare pvalue array
    p_values = []
    for i in range(len(annotation_order)):
        row = []
        for j in range(len(loop_type_order)):
            selected_dict = get_fisher_exact_pvalue_all(
                annotation_order[i], constant_annotation, loop_type_order[j],
                annotationmaps, looping_classes, threshold=threshold,
                margin=margin)
            row.append(selected_dict)
        p_values.append(row)

    # plot heatmap
    plt.clf()
    fig = plt.gcf()
    fig.set_size_inches(72 / 60.0 * len(annotation_order),
                        40 / 60.0 * len(annotation_order))
    cmap = plt.get_cmap('bwr')
    im = plt.imshow(array, interpolation='none', cmap=cmap, origin='lower',
                    vmin=vmin, vmax=vmax)
    for i in range(len(annotation_order)):
        for j in range(len(loop_type_order)):
            text = ''
            if p_values[i][j] >= 0.9:
                text = '1.0'
            elif p_values[i][j] >= 0.1:
                text = '0.%i' % (int(10 * p_values[i][j]) + 1)
            elif p_values[i][j] >= 0.01:
                if p_values[i][j] >= 0.09:
                    text = '0.1'
                else:
                    text = '0.0%i' % (int(100 * p_values[i][j]) + 1)
            else:
                text = 'E-%i' % int(-np.log10(p_values[i][j]))
            plt.text(j, i, text, ha='center', va='center')
    plt.colorbar(im)
    plt.xticks(np.arange(len(loop_type_order)), loop_type_order, rotation=45,
               ha='right')
    plt.yticks(np.arange(len(annotation_order)), annotation_order)
    plt.savefig(filename, bbox_inches='tight')
