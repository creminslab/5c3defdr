import numpy as np

from get_mid_to_mid_distance import get_mid_to_mid_distance
from parallelize_regions import parallelize_regions


@parallelize_regions
def make_distance_matrix(regional_primermap):
    """
    Construct a pairwise distance matrix for the fragments in a region from the
    primermap describing those fragments.

    Parameters
    ----------
    regional_primermap : List[Dict[str, Any]]
        The primermap for this region.

    Returns
    -------
    np.ndarray
        The pairwise distance matrix for all fragments in this region in units
        of base pairs.
    """
    return np.array([[get_mid_to_mid_distance(regional_primermap[i],
                                              regional_primermap[j])
                      for i in range(len(regional_primermap))]
                     for j in range(len(regional_primermap))], dtype=int)
