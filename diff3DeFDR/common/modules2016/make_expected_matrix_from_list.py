import numpy as np

def make_expected_matrix_from_list(distance_expected):
    """
    Converts a bin-level one-dimensional expected model into an expected matrix.

    Parameters
    ----------
    distance_expected : List[float]
        The one-dimensional distance expected model to make a matrix out of.

    Returns
    -------
    np.ndarray
        The expected matrix.
    """
    expected_matrix = np.zeros((len(distance_expected), len(distance_expected)),
                               dtype=float)
    for i in range(len(expected_matrix)):
        for j in range(i + 1):
            expected_matrix[i, j] = distance_expected[i - j]
            expected_matrix[j, i] = distance_expected[i - j]
    return expected_matrix
