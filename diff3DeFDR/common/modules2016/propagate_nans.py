import numpy as np
from function_util import parallelize_regions

@parallelize_regions
def propagate_nans(mat_1, mat_2):
    """
    Propagate nan values between two matrices.

    Parameters
    ----------
    mat_1, mat_2 : np.ndarray
        The matrices to propagate nan's between. These should have the same
        shape.

    Returns
    -------
    Tuple[np.ndarray, np.ndarray]
        The nan-propagated versions of the input matrices, in the order they
        were passed.
    """
    new_mat_1 = mat_1.copy()
    new_mat_2 = mat_2.copy()
    new_mat_1[~np.isfinite(mat_2)] = np.nan
    new_mat_2[~np.isfinite(mat_1)] = np.nan
    return new_mat_1, new_mat_2
