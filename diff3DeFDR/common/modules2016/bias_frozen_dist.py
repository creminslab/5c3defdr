import numpy as np
import scipy.stats as stats

from freeze_distribution import freeze_distribution

def bias_frozen_dist(frozen_dist, bias_factor, log=False):
    """
    Add a multiplicative bias to a frozen distribution, returning a new frozen
    distribution created through ``freeze_distribution()``.

    Parameters
    ----------
    frozen_dist : scipy.stats.rv_frozen
        The distribution to bias.
    bias_factor : float
        The multiplicative factor with which to bias the distribution.
    log : boolean, optional
        Whether or not the frozen distribtion should be biased in log-space.

    Returns
    -------
    scipy.stats.rv_frozen
        The biased distribution.

    Examples
    --------
    >>> frozen_dist = stats.norm(loc=5.0, scale=np.sqrt(3.0))
    >>> biased_dist = bias_frozen_dist(frozen_dist, 2.0)
    >>> print('%s distribution with mean %.2f and variance %.2f'
    ...       % ((biased_dist.dist.name,) + biased_dist.stats(moments='mv')))
    norm distribution with mean 10.00 and variance 12.00
    """
    # get mean and standard deviation from old
    mu, sigma_2 = frozen_dist.stats(moments='mv')

    # add bias
    if not log:
        mu *= bias_factor
        sigma_2 *= bias_factor ** 2
    else:
        mu += np.log(bias_factor)

    return freeze_distribution(frozen_dist.dist, mu, sigma_2)
