import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def wipe_matrix(matrix, indices, wipe_value=np.nan):
    """
    Wipes specified rows and columns of the counts matrix with a specified
    value.

    Parameters
    ----------
    matrix : np.ndarray
        The square symmetric counts matrix to wipe.
    indices : Iterable[int]
        The indices of the rows and columns to wipe.
    wipe_value : Optional[float]
        The value to wipe the selected indices with.

    Returns
    -------
    np.ndarray
        The wiped counts matrix.
    """
    for i in indices:
        matrix[:, i] = wipe_value
        matrix[i, :] = wipe_value
    return matrix
