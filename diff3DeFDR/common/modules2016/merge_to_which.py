from identify_nearby_clusters import identify_nearby_clusters

def merge_to_which(clusters):
    """
    Determines which other cluster, if any, the first cluster in a list of
    clusters should be merged into.

    Parameters
    ----------
    clusters : list of clusters
        The list of clusters to consider. Ideally, this list should be sorted in
        ascending order of cluster size.

    Returns
    -------
    int
        The index of the cluster that the first cluster should be merged into.
        If the cluster should not be merged, the value will be -1.

    Notes
    -----
    Under the adjacency heuristic, the condition for merging two clusters is
    that they must contain peaks that are immediately adjacent to each other in
    2-D space.
    """
    nearby_clusters = filter(lambda x: x > 0,
                             identify_nearby_clusters(clusters[0], clusters))
    if nearby_clusters:
        return nearby_clusters[0]
    return -1
