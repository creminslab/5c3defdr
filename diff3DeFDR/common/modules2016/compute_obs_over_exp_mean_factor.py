import numpy as np

from flatten_counts_single_region_geometric import flatten_counts_single_region


def compute_obs_over_exp_mean_factor(regional_obs, regional_exp, log=False):
    """
    Compute a mean scaling factor using the ``obs_over_exp`` approach.

    Parameters
    ----------
    regional_obs : np.ndarray
        The matrix of observed counts for this region.
    regional_exp : np.ndarray
        The matrix of expected counts for this region.
    log : Optional[bool]
        Whether or not the data should be logged.

    Returns
    -------
    float
        The mean scaling factor for this region.
    """
    obs_over_exp = regional_obs / regional_exp
    if log:
        obs_over_exp = np.log(obs_over_exp)
    flattened_obs_over_exp = flatten_counts_single_region(obs_over_exp)
    return np.nanmean(flattened_obs_over_exp)
