import numpy as np

from flatten_counts_single_region_geometric import flatten_counts_single_region


def get_regional_obs_colorscale_across_reps(
        regional_counts_superdict, min_function=np.min,
        max_function=lambda x: np.percentile(x, 98)):
    """
    Computes a typical scale for visualizing observed counts for a single
    region.

    Parameters
    ----------
    regional_counts_superdict : dict of 2d numpy arrays
        The keys of the outer dict are replicate names. The values are the
        square symmetric numpy arrays representing the counts in a certain
        region for that replicate.
    min_function : function
        The function that will compute the min for each replicate.
    max_function : function
        The function that will compute the max for each replicate.

    Returns
    -------
    tuple of float
        The first element of this list is the minimum value of the computed
        scale. The second element of this list is the maximum value of the
        computed scale.

    Notes
    -----
    By default, the returned scale is computed as ranging from the average of
    the minimum across the replicates to the average of the 98th percentiles
    across the replicates.

    To compute the scale for ``region`` given a counts superdict::

        get_regional_obs_colorscale_across_reps(
            {rep: counts_superdict[rep][region]
             for rep in counts_superdict})

    """
    lower_limits = []
    upper_limits = []
    for rep in regional_counts_superdict.keys():
        flattened_counts = flatten_counts_single_region(
            regional_counts_superdict[rep], discard_nan=True)
        lower_limits.append(min_function(flattened_counts))
        upper_limits.append(max_function(flattened_counts))
    return np.mean(lower_limits), np.mean(upper_limits)
