import numpy as np

from get_regional_obs_colorscale_across_reps import \
    get_regional_obs_colorscale_across_reps


def get_obs_colorscales_across_reps(
        counts_superdict, min_function=np.min,
        max_function=lambda x: np.percentile(x, 98)):
    """
    Computes a typical scale for visualizing observed counts for a each region
    across replicates.

    Parameters
    ----------
    counts_superdict : Dict[str, Dict[str, np.ndarrray]]
        A superdict where the outer keys are the replicate names, the inner keys
        are region names, and the values are the corresponding observed counts
        matrices.
    min_function : function
        The function that will compute the min for each replicate. The default
        is np.min
    max_function : function
        The function that will compute the max for each replicate. The default
        is a lambda expression of np.percentile where x is a placeholder for the
        the observed counts matrix and the second arg is the upper percentile.
        For more on lambda expressions, see
        https://docs.python.org/2/reference/expressions.html#lambda

    Returns
    -------
    Dict[str, Tuple[float]]
        The keys are region names as strings, the values are
        ``(lower_limit, upper_limit)`` tuples specifying the computed observed
        scale for that region.
    """
    return {
        region: get_regional_obs_colorscale_across_reps(
            {rep: counts_superdict[rep][region] for rep in counts_superdict},
            min_function=min_function, max_function=max_function
        )
        for region in counts_superdict[counts_superdict.keys()[0]].keys()}
