from copy import deepcopy
from identify_nearby_clusters import identify_nearby_clusters

def identify_clusters_to_trim(clusters_superdict,size_threshold=6):
    """
    Loops through clusters and iteratively trims clusters of clusters 
    that contain less than a certain number of pixels

    Parameters
    ----------
    clusters_superdict : Dict[str,Dict[str,List[List[Dict[str,int]]]]]
        The outer keys are the category names, the second innermost keys
        are the region names, and the values are lists of lists of
        dicts containing:
            
            {
              'x': int
              'y': int
              'value' : int
            }

    size_threshold : int
        Clusters of clusters that contain less pixels than this value will
        be appended to the list of clusters to be trimmed. 

    Returns
    ------
    Dict[str,List[List[Dict[str,int]]]]
        The keys are the region names and the values are lists of lists of
        of dicts in a similar format to the clusters superdict. 
    """
    clusters_to_trim = {region: [] for region in
                        clusters_superdict[clusters_superdict.keys()[0]].keys()}
    retained_clusters = {category:{region: [] for region in
                                  clusters_superdict[category].keys()} 
                        for category in clusters_superdict.keys()}
    counter = 1
    while counter > 0:
        counter = 0
        for category in clusters_superdict.keys():
            if category == 'background':
                continue
            else:                                                                                                    
                for region in clusters_superdict[category].keys():
                    for i in range(len(clusters_superdict[category][region])):
                        size_count = 0
                        for category2 in clusters_superdict.keys():
                            if category2 == category:
                                continue
                            elif category2 == 'background':
                                continue
                            elif region not in clusters_superdict[category2].keys():
                                continue
                            else:
                                nearby_clusters = list(set(identify_nearby_clusters(clusters_superdict[category][region][i],
                                                          clusters_superdict[category2][region])))
                                if len(nearby_clusters)>0:
                                    for j in range(len(nearby_clusters)):
                                        size_count += len(clusters_superdict[category2][region][nearby_clusters[j]])
                        if (len(clusters_superdict[category][region][i]) + size_count) <= size_threshold:
                            clusters_to_trim[region].append(clusters_superdict[category][region][i])
                            counter = 1
                        else:
                            retained_clusters[category][region].append(clusters_superdict[category][region][i])
        clusters_superdict = deepcopy(retained_clusters)
        retained_clusters = {category:{region: [] for region in clusters_superdict[category].keys()}
                                                    for category in clusters_superdict.keys()}
        if counter == 1:
            continue
        else:
            break

    return clusters_to_trim
