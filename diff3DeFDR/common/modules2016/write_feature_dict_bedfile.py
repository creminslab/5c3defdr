import re


_nsre = re.compile('([0-9]+)')


def natural_sort_key(s):
    """
    Function to enable natural sorting of alphanumeric strings.

    Parameters
    ----------
    s : str
        String being sorted.

    Returns
    -------
    List[Union[int, str]]
        This list is an alternative represenation of the input string that will
        sort in natural order.

    Notes
    -----
    Function written by SO user http://stackoverflow.com/users/15055/claudiu and
    provided in answer http://stackoverflow.com/a/16090640.
    """
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

def write_feature_dict_bedfile(feature_dict, filename):
    """
    Writes ".bed" files from BED dictionary data structure
    
    Parameters
    ---------
    feature_dict : Dict[str, List[Dict[str,Any]] 
        BED file dictionary generated from load_features or merge_feature_dicts
    filename : str
        Name of file output (it should have the extension ".bed") 
    """
    with open(filename, 'w') as handle:
        #file is sorted alphanumerically by "chrom"(e.g. chr1, chr2, chr3, etc.)
        for chrom in sorted(feature_dict.keys(),
                            key=lambda x: natural_sort_key(x)):
            for feat in feature_dict[chrom]:
                handle.write('%s\t%i\t%i\n' %
                             (feat['chrom'], feat['start'], feat['end']))

