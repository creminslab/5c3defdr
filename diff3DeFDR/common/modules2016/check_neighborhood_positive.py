def check_neighborhood_positive(neighborhood, threshold):
    """
    Check to see if a neighborhood clears as specified positive fraction
    threshold.

    Parameters
    ----------
    neighborhood : List[Dict[str, Any]]
        A list of "nearby points" where each nearby point is
        represented as a dict of the following form::

            {
                'value': float,
                'x_dist': int,
                'y_dist': int
            }

        where 'value' is the value at the point and 'x_dist' and 'y_dist' are
        its distances from the center of the neighborhood along the x- and
        y-axis, respectively, in base pairs.
    threshold : float
        If less than this fraction of the values in the neighborhood are
        positive, the neighborhood fails the check.

    Returns
    -------
    bool
        True if this neighborhood clears the threshold, otherwise False.
    """
    num_positive = len([1 for i in neighborhood if i['value'] > 0])
    total = len(neighborhood)

    if total == 0 or float(num_positive) / total <= threshold:
        return False
    return True