from deprecate import deprecate
from generate_primermap import generate_primermap
from default_bin_name_parser import default_bin_name_parser


def load_pixelmap(bedfile, name_parser=default_bin_name_parser):
    # deprecate('LOAD_PIXELMAP() IS DEPRECATED. TO AVOID THIS MESSAGE, USE LOAD_PRIMERMAP() WITH BIN_NAME_PARSER INSTEAD!')
    return load_primermap(bedfile, name_parser=name_parser)
