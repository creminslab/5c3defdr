from __future__ import division
import numpy as np

def vert_filt(m_star, e_star, w=15, p=5, min_percent=0.2,
              query_close_points=False):
    '''
    Computes vert filter for each pixel in the upper right part of matrix
    Not computed using ndimage filter due to reflecting propagation of NaNs

    ToDo: making reflection of NaNs an option (or at least the manner in which they are reflected)
    '''

    # w must be bigger than p
    # if p is bigger, assume user mixed up inputs

    if p > w:
        w, p = p, w
    elif p == w:
        raise Exception('w must be bigger than p')

    vert_expect = np.empty(np.shape(m_star))
    vert_expect[:] = np.NaN
    dim = len(m_star)
    max_area = 3*(w-p)
    min_allowed = min_percent * max_area
    for j in xrange(1, dim):
        if j + 1 > dim - 1:
            continue
        for i in xrange(w, j-p-1):

            u_bound = max(i-w, 0)
            if i + w <= dim - 1:
                d_bound = min(i+w, dim-1)
            else:
                continue
            up_bound = max(0, i-p)

            # propagate NaNs across reflection
            # copy area
            m_1 = m_star[range(u_bound, up_bound), :][:, range(j-1, j+2)]
            m_2 = m_star[range(d_bound, i+p, -1), :][:, range(j+1, j-2, -1)]
            e_1 = e_star[range(u_bound, up_bound), :][:, range(j-1, j+2)]
            e_2 = e_star[range(d_bound, i+p, -1), :][:, range(j+1, j-2, -1)]

            m_2[np.isnan(m_1)] = np.NaN
            e_2[np.isnan(e_1)] = np.NaN
            non_nan_count = np.count_nonzero(np.isfinite(m_2))

            if non_nan_count < min_allowed:
                continue

            m_1[np.isnan(e_1)] = np.NaN
            m_2[np.isnan(e_2)] = np.NaN
            e_1[np.isnan(m_1)] = np.NaN
            e_2[np.isnan(m_2)] = np.NaN

            m_1[np.isnan(m_2)] = np.NaN
            e_1[np.isnan(e_2)] = np.NaN

            numer = 0
            denom = 0

            numer += np.nansum(m_1)
            n1 = np.nansum(e_1)
            denom += n1

            numer -= np.nansum(m_2)
            n2 = np.nansum(e_2)
            denom -= n2

            vert_expect[i, j] = numer * e_star[i, j] / denom
            vert_expect[j, i] = vert_expect[i, j]
    return vert_expect

