import numpy as np

def quadratic_log_log_fit(x, y):
    """
    Fit a pure-quadratic function ``y = a * x**2`` using a loss function in
    log-log space.

    Parameters
    ----------
    x : np.ndarray
        Flat vector of ``x`` values to fit.
    y : np.ndarray
        Flat vector of ``y`` values to fit.

    Returns
    -------
    np.poly1d
        The fitted function.
    """
    a = np.exp(np.mean(np.log(y + 1) - 2 * np.log(x + 1)))
    return np.poly1d([a, 0, 0])
