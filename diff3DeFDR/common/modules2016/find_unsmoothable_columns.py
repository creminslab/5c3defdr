import itertools

from guess_bin_step import guess_bin_step
from unsmoothable_column_threshold_heuristic import unsmoothable_column_threshold_heuristic
from find_upstream_primers import find_upstream_primers
from find_nearby_fragments import find_nearby_fragments


def find_unsmoothable_columns(regional_primermap, regional_pixelmap,
                              window_width, upstream_primer_mapping=None):
    """
    Identifies the unsmoothable columns in a region assuming that the smoothing
    was a filtering operation applied on fragment-level data.

    Parameters
    ----------
    regional_primermap : List[Dict[str, Any]]
        The primermap describing the primers for this region.
    regional_pixelmap : List[Dict[str, Any]]
        The pixelmap describing the bins for this region.
    window_width : int
        The width of the filtering window in base pairs.
    upstream_primer_mapping : Dict[int, int]
        A mapping from each bin index to the index of its nearest upstream
        primer. See ``lib5c.algorithms.filtering.fragment_bin_filtering
        .find_upstream_primers()``.

    Returns
    -------
    List[bool]
        A list of boolean values with length equal to the number of bins in the
        region. The ``i`` th element of this list is True if the ``i`` th bin in
        the region is an "unsmoothable column".
    """
    bin_step = guess_bin_step(regional_pixelmap)
    threshold = unsmoothable_column_threshold_heuristic(window_width, bin_step)

    if upstream_primer_mapping is None:
        upstream_primer_mapping = find_upstream_primers(regional_pixelmap,
                                                        regional_primermap)

    # a list of bool which is True if the ith "small bin" is empty
    blank_columns = [not bool(len(find_nearby_fragments(i,
                                                        regional_pixelmap,
                                                        regional_primermap,
                                                        upstream_primer_mapping,
                                                        bin_step / 2)))
                     for i in range(len(regional_pixelmap))]

    # figure out which empty columns should be smoothed over
    unsmoothable_columns = []
    # group blank and nonblank columns
    for blank, run in itertools.groupby(blank_columns):
        # only write Trues if this is a group of more than threshold consecutive
        # blank columns
        run_as_list = list(run)
        if blank and len(run_as_list) > threshold:
            unsmoothable_columns.extend([True] * len(run_as_list))
        else:
            unsmoothable_columns.extend([False] * len(run_as_list))

    return unsmoothable_columns
