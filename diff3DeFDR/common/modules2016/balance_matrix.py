import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def balance_matrix(matrix, bias_vector, invert=False):
    """
    Balance a matrix given the appropriate multiplicative bias vector.

    Parameters
    ----------
    matrix : np.ndarray
    bias_vector : np.ndarray
    invert : Optional[bool]
        Pass True to invert the bias vector before balancing.

    Returns
    -------
    np.ndarray
        The balanced matrix.
    """
    if invert:
        bias_vector = 1.0 / bias_vector
    return bias_vector.T * matrix * bias_vector
