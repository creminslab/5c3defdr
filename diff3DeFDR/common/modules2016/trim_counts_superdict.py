from trim_matrix import trim_matrix


def trim_counts_superdict(counts_superdict, indices):
    """
    Applies ``trim_matrix()`` to each replicate in a ``counts_superdict``.

    Parameters
    ----------
    counts_superdict : Dict[str, np.ndarray]
        The keys are replicate names, the values are the counts for that rep.
    indices : Iterable[int]
        The indices to trim.

    Returns
    -------
    Dict[str, np.ndarray]
        The keys are replicate names, the values are the trimmed counts for that
        rep.
    """
    return {rep: trim_matrix(counts_superdict[rep], indices)
            for rep in counts_superdict.keys()}
