from __future__ import division

from get_annotation_percentage import get_annotation_percentage
from lru_cache import lru_cache


@lru_cache(maxsize=None, skip_dicts=True)
def get_fold_change(annotation_a, annotation_b, region, category,
                    annotationmaps, looping_classes, threshold=0, margin=1):
    """
    Computes the fold enrichment of the percentage of loops of a particular
    category in a particular region connecting specified annotations relative
    to the special "background" reference category.

    Parameters
    ----------
    annotation_a : str
        Annotation to look for on one side of the loop.
    annotation_b : str
        Annotation to look for on the other side of the loop.
    region : str
        The region to compute the fold enrichment within.
    category : str
        The category of loops to consider.
    annotationmaps : dict of annotationmap
        A dict describing the annotations. In total, it should have the
        following structure::

            {
                'annotation_a_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                 },
                'annotation_b_name': {
                    'region_1_name': list of int,
                    'region_2_name': list of int,
                    ...
                },
                ...
            }

        where ``annotationmaps['annotation_a']['region_r']`` should be a list of
        ints describing the number of ``'annotation_a'``s present in each bin of
        ``'region_r'``.
    looping_classes : dict of np.ndarray with str dtype
        The keys should be region names as strings, the values should be square,
        symmetric arrays of the same size and shape as the indicated region,
        with string loop category names in the positions of categorized loops.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.

    Returns
    -------
    float
        The fold enrichment.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.algorithms.enrichment import clear_enrichment_caches
    >>> clear_enrichment_caches()
    >>> annotationmaps = {'a': {'r1': [1, 0, 0, 1]},
    ...                   'b': {'r1': [1, 1, 0, 0]}}
    >>> looping_classes = {'r1': np.array([[''   , ''   , 'es' , 'ips'],
    ...                                    [''   , ''   , 'npc', 'npc'],
    ...                                    ['es' , 'npc', ''   , ''   ],
    ...                                    ['ips', 'npc', ''   , ''   ]],
    ...                                   dtype='a25')}
    >>> looping_classes['r1'][looping_classes['r1'] == ''] = 'background'
    >>> get_fold_change('a', 'b', 'r1', 'ips', annotationmaps, looping_classes,
    ...                 margin=0)
    3.0
    >>> get_fold_change('a', 'b', 'r1', 'npc', annotationmaps, looping_classes,
    ...                 margin=0)
    1.5
    """
    return (get_annotation_percentage(annotation_a, annotation_b, region,
                                      category, annotationmaps, looping_classes,
                                      threshold=threshold, margin=margin) /
            get_annotation_percentage(annotation_a, annotation_b, region,
                                      'background', annotationmaps,
                                      looping_classes, threshold=threshold,
                                      margin=margin))
