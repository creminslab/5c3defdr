"""
Module for assembling or merging clusters using a simple adjacency heuristic.
"""

from identify_nearby_clusters import identify_nearby_clusters
from merge_clusters import merge_clusters
from merge_to_which import merge_to_which

def make_clusters(peaks):
    """
    Clusters peaks by adjacency.

    Parameters
    ----------
    peaks : list of peaks
        The peaks to cluster.

    Returns
    -------
    list of clusters
        The clustered peaks.
    """

    clusters = []
    for peak in sorted(peaks, key=lambda x: x['value'], reverse=True):
        nearby_clusters = identify_nearby_clusters([peak], clusters)
        if nearby_clusters:
            clusters[nearby_clusters[0]].append(peak)
        else:
            clusters.append([peak])
    return merge_clusters(clusters, merge_to_which)
