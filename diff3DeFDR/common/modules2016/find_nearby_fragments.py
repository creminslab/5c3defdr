from get_midpoint import get_midpoint

def find_nearby_fragments(index, regional_pixelmap, regional_primermap,
                          upstream_primer_mapping, neighborhood_radius):
    """
    Finds the primers near a target bin as specified by an index.

    Parameters
    ----------
    index : int
        The index of the bin to look near.
    regional_pixelmap : List[Dict[str, Any]]
        The bin map describing the bins for this region.
    regional_primermap : List[Dict[str, Any]]
        The primermap describing the primers for this region.
    upstream_primer_mapping : Dict[int, int]
        A mapping from each bin index to the index of its nearest upstream
        primer. See ``lib5c.algorithms.filtering.fragment_bin_filtering
        .find_upstream_primers()``.
    neighborhood_radius : int
        The threshold for deciding if a fragment is "nearby" or not, as a
        distance in base pairs.

    Returns
    -------
    List[Dict[str, int]]
        A list of nearby fragments, where each nearby fragment is represented as
        a dict of the following form::

            {
                'index': int,
                'distance': int
            }

        where 'index' is the index of the fragment within the region and
        'distance' is the distance between this fragment and the target bin.
    """
    # establish key fragment
    key_fragment = regional_pixelmap[index]

    # establish coordinates
    coordinate = get_midpoint(key_fragment)

    # list of indices that are nearby
    nearby_fragments = []

    # step left
    i = upstream_primer_mapping[index]
    distance = coordinate - regional_primermap[i]['end']
    while distance < neighborhood_radius:
        nearby_fragments.append({'index': i, 'distance': distance})
        i -= 1
        if i < 0:
            break
        distance = coordinate - regional_primermap[i]['end']

    # step right
    i = upstream_primer_mapping[index] + 1
    distance = regional_primermap[i]['start'] - coordinate
    while distance < neighborhood_radius:
        nearby_fragments.append({'index': i, 'distance': distance})
        i += 1
        if i >= len(regional_primermap):
            break
        distance = regional_primermap[i]['start'] - coordinate

    return nearby_fragments

