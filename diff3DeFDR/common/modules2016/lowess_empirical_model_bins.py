import numpy as np

from statsmodels.nonparametric.smoothers_lowess import lowess

from empirical_model_bins import empirical_model_bins
from log import log
from unlog import unlog


def lowess_empirical_model_bins(counts, frac=0.8, log_base='e', pseudocount=1,
                                is_global=True):
    """
    Make a one-dimensional bin-level expected model by performing lowess
    regression in log-counts space, excluding the first third of the distance
    scales and only using the empirical geometric means there instead.

    Parameters
    ----------
    counts : Dict[str, np.ndarray] or np.ndarray
        The observed counts dict (if called with``is_global=True``) or matrix
        for this region (if called with ``is_global=False``).
    frac : float
        The lowess smoothing fraction parameter to use.
    log_base : int or str
        The base to use when logging and unlogging.
    pseudocount : int
        The pseudocount to add to the counts before logging. Will be subtracted
        away when unlogging.
    is_global : bool
        Determines whether to compute a global or regional expected model. If
        set to False, the algorithm expects ``counts`` to be a ``np.ndarray``
        and will return a regional expected model.

    Returns
    -------
    List[float]
        The one-dimensional expected model. The ``i`` th element of the list
        corresponds to the expected value for interactions between loci
        separated by ``i`` bins. If called with ``is_global=True``, the length
        of this list will match the size of the largest region in the input
        counts dict.
    """
    # log counts
    log_counts = log(counts, pseudocount=pseudocount, base=log_base)

    # make offdiagonals
    if is_global:
        offdiagonals = [np.concatenate([np.diag(log_counts[region], k=i)
                                        for region in log_counts.keys()])
                        for i in range(max([len(log_counts[region])
                                            for region in log_counts.keys()]))]
    else:
        offdiagonals = [np.diag(log_counts, k=i)
                        for i in range(len(log_counts))]

    # get empirical expected
    empirical = empirical_model_bins(log_counts, log_transform=False,
                                     is_global=is_global)

    # make data of the form [distance, count], ignoring nans
    data = np.asarray([[dist, count]
                       for dist in range(len(offdiagonals))
                       for count in offdiagonals[dist]
                       if np.isfinite(count)])

    # don't try to fit the first third of the region
    key_index = np.int(len(offdiagonals) / 3)
    filtered_data = np.asarray(filter(lambda x: x[0] >= key_index, data))

    # do the lowess fit
    fit = lowess(filtered_data[:, 1], filtered_data[:, 0], frac=frac, it=3)

    # filter the fit to just the region above the join point
    filtered_fit = np.asarray(filter(lambda x: x[0] > key_index, fit))

    # construct an array that will represent the joined fit
    joined_fit = np.zeros(len(offdiagonals))
    for i in range(key_index + 1):
        joined_fit[i] = empirical[i]
    for i in range(key_index + 1, len(offdiagonals)):
        query_result = filter(lambda x: x[0] == i, filtered_fit)
        if query_result:
            joined_fit[i] = query_result[0][1]
        else:
            joined_fit[i] = empirical[i]

    # unlog
    joined_fit = unlog(joined_fit, pseudocount=pseudocount, base=log_base)

    return joined_fit
