import warnings

def deprecate(message):
    warnings.simplefilter("default", DeprecationWarning)
    warnings.warn(message, DeprecationWarning, stacklevel=3)
