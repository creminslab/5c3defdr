import numpy as np

from log import log
from unlog import unlog


def polyfit_model_fragments(counts, distances, degree=1, log_base='e',
                            pseudocount=1, is_global=False):
    """
    Make a one-dimensional fragment-level expected model by fitting a
    polynomial in log-log space.

    Parameters
    ----------
    counts : Dict[str, np.ndarray] or np.ndarray
        The observed counts dict (if called with``is_global=True``) or matrix
        for this region (if called with ``is_global=False``).
    distances : Dict[str, np.ndarray]
        A dict of pairwise distance matrices describing the genomic distances
        between the elements of the matrices in ``counts``. The keys and array
        dimensions should match the keys and array dimensions of ``counts``.
    degree : int
        The degree of the polynomial to fit.
    log_base : int or str
        The base to use when logging and unlogging.
    pseudocount : int
        The pseudocount to add to the counts and distances before logging. Will
        be subtracted away when unlogging.
    is_global : bool
        Determines whether to compute a global or regional expected model. If
        set to False, the algorithm expects ``counts`` to be a ``np.ndarray``
        and will return a regional expected model.

    Returns
    -------
    Dict[int, float]
        A mapping from interaction distances in units of base pairs to the
        expected value at that distance.
    """
    # establish local definition of log and unlog
    log_fn = lambda x: log(x, pseudocount=pseudocount, base=log_base)
    unlog_fn = lambda x: unlog(x, pseudocount=pseudocount, base=log_base)

    # log transform
    log_counts = log_fn(counts)
    log_distances = log_fn(distances)

    # make data of the form [log_distance, log_count], ignoring nans
    if is_global:
        data = np.asarray([[log_distances[region][i, j],
                            log_counts[region][i, j]]
                           for region in log_counts.keys()
                           for i in range(len(log_counts[region]))
                           for j in range(i + 1)
                           if np.isfinite(log_counts[region][i, j])])
    else:
        data = np.asarray([[log_distances[i, j], log_counts[i, j]]
                           for i in range(len(log_counts))
                           for j in range(i + 1)
                           if np.isfinite(log_counts[i, j])])

    # do the linear fit
    fit = np.poly1d(np.polyfit(data[:, 0], data[:, 1], degree))

    # repackage
    if is_global:
        distance_expected = {
            dist: unlog_fn(fit(log_fn(dist)))
            for dist in np.unique(np.concatenate(
                [distances[region].flatten() for region in distances.keys()]))}
    else:
        distance_expected = {dist: unlog_fn(fit(log_fn(dist)))
                             for dist in np.unique(distances.flatten())}

    return distance_expected
