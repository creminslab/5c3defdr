import numpy as np
from get_mid_to_mid_distance import get_mid_to_mid_distance

def wipe_short_range_interactions(binned_counts, pixelmap, distance_threshold=20000, 
                                  wipe_value=np.nan, distance_span='start-to-end'):
    """
    Wipes binned counts indices when the genomic distance between two
    bins falls below a user-defined threshold. 

    Parameters
    ----------
    binned_counts : Dict[str, np.ndarray]
        The keys are region names, the values are the binned counts values for
        that region.
    pixelmap : Dict[str, List[Dict[str, Any]]]
        The bin map describing the bins for all 5C regions.
    distance_threshold : int
        Interacting bins whose genomic distance falls below this
        value will be classified as "short-range" and their indices will be wiped. 
    wipe_value = any
        The value that will fill in the index containing the short-range
        interaction
    distance_span = str
        Specifies how genomic distance between two interacting bins will be
        determined. 
        
    Returns
    -------
    Dict[str, np.ndarray], Dict[str, List[List[int,int]]]
        The keys are region names, the values are the trimmed counts for
        that region for the first dict. The second dict's values are list of
        lists containing trimmed indices
    """

    trimmed_bin_indices = []

    for i in range(len(binned_counts)):
        for j in range(i + 1):
            if distance_span == 'start-to-end':
                distance = abs(pixelmap[j]['start'] - pixelmap[i]['end'])
                pass
            elif distance_span == 'start-to-start':
                distance = abs(pixelmap[j]['start'] - pixelmap[i]['start'])
                pass
            elif distance_span == 'end-to-end':
                distance = abs(pixelmap[j]['end'] - pixelmap[i]['end'])
                pass
            elif distance_span == 'mid-to-mid':
                distance = get_mid_to_mid_distance({'start':pixelmap[j]['start'],'end' : pixelmap[j]['end']},
                                                   {'start':pixelmap[i]['start'],'end':pixelmap[i]['end']})
                pass

            if distance < distance_threshold:
                binned_counts[i, j] = wipe_value
                binned_counts[j, i] = wipe_value
                trimmed_bin_indices.append([i,j])

    return binned_counts,trimmed_bin_indices
