import numpy as np

from wipe_matrix import wipe_matrix


def wipe_counts_superdict(counts_superdict, indices, wipe_value=np.nan):
    """
    Applies ``wipe_matrix()`` to each replicate in a ``counts_superdict``.

    Parameters
    ----------
    counts_superdict : Dict[str, np.ndarray]
        The keys are replicate names, the values are the counts for that rep.
    indices : Iterable[int]
        The indices to wipe
    wipe_value : Optional[float]
        The value to wipe the selected indices with.

    Returns
    -------
    Dict[str, np.ndarray]
        The keys are replicate names, the values are the wiped counts for that
        rep.
    """
    return {rep: wipe_matrix(counts_superdict[rep], indices, wipe_value)
            for rep in counts_superdict.keys()}
