import numpy as np

def gmean(array, pseudocount=1):
    """
    Compute the geometric mean of an input array.

    Parameters
    ----------
    array : np.ndarray
        The array to take the geometric mean over.
    pseudocount : float
        The pseudocount to add to the elements of ``array`` before logging.

    Returns
    -------
    float
        The geometric mean.
    """
    return np.exp(np.nanmean(np.log(array + pseudocount))) - pseudocount

