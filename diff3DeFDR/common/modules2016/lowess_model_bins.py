import numpy as np

from statsmodels.nonparametric.smoothers_lowess import lowess

from log import log
from unlog import unlog


def lowess_model_bins(counts, frac=0.8, log_base='e', pseudocount=1,
                      is_global=True):
    """
    Make a one-dimensional bin-level expected model by performing lowess
    regression in log-log space.

    Parameters
    ----------
    counts : Dict[str, np.ndarray] or np.ndarray
        The observed counts dict (if called with``is_global=True``) or matrix
        for this region (if called with ``is_global=False``).
    frac : float
        The lowess smoothing fraction parameter to use.
    log_base : int or str
        The base to use when logging and unlogging.
    pseudocount : int
        The pseudocount to add to the counts and distances before logging. Will
        be subtracted away when unlogging.
    is_global : bool
        Determines whether to compute a global or regional expected model. If
        set to False, the algorithm expects ``counts`` to be a ``np.ndarray``
        and will return a regional expected model.

    Returns
    -------
    List[float]
        The one-dimensional expected model. The ``i`` th element of the list
        corresponds to the expected value for interactions between loci
        separated by ``i`` bins. If called with ``is_global=True``, the length
        of this list will match the size of the largest region in the input
        counts dict.
    """
    # establish local definition of log and unlog
    log_fn = lambda x: log(x, pseudocount=pseudocount, base=log_base)
    unlog_fn = lambda x: unlog(x, pseudocount=pseudocount, base=log_base)

    # log transform counts
    log_counts = log_fn(counts)

    # make data of the form [distance, count], ignoring nans
    if is_global:
        data = np.asarray([[log_fn(i - j), log_counts[region][i, j]]
                           for region in log_counts.keys()
                           for i in range(len(log_counts[region]))
                           for j in range(i + 1)
                           if np.isfinite(log_counts[region][i, j])])
    else:
        data = np.asarray([[log_fn(i - j), log_counts[i, j]]
                           for i in range(len(log_counts))
                           for j in range(i + 1)
                           if np.isfinite(log_counts[i, j])])

    # do the lowess fit
    fit = lowess(data[:, 1], data[:, 0], frac=frac, it=3)

    # unlog
    fit_dists = np.rint(unlog_fn(fit[:, 0])).astype(int)
    fit_counts = unlog_fn(fit[:, 1])

    # repackage
    distance_expected = {fit_dists[i]: fit_counts[i] for i in range(len(fit))}

    # fill nans
    if is_global:
        distance_expected = [distance_expected[i]
                             if i in distance_expected
                             else np.nan
                             for i in range(max([len(counts[region])
                                                 for region in counts.keys()]))]
    else:
        distance_expected = [distance_expected[i]
                             if i in distance_expected
                             else np.nan
                             for i in range(len(counts))]

    return distance_expected
