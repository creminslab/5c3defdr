from __future__ import division
import numpy as np
np.seterr(divide='ignore')
np.seterr(invalid='ignore')

def median_of_ratios(library_1,library_2):
    """
    Scales down the larger library by a median of ratios

    Parameters
    ----------
    library_1 : List[int] or 1D np.ndarray
        The counts for a given sequencing library.
    library_2 : List[int] or 1D np.ndarray
        The counts for a given sequencing library.

    Returns
    -------
    (List[float], List[float],float)
        A tuple that encapsulates the two libraries that have been
        corrected for sequencing depth, and the median of ratios. 
    """
        
    #calculate median of ratios
    ratios = [x / y for x, y in zip(library_1,library_2)]
    median_ratio = np.nanmedian(ratios)
        
    #scale down larger library
    if median_ratio < 1:
        library_2 = np.array(library_2) * median_ratio
    elif median_ratio > 1:
        library_1 = np.array(library_1) / median_ratio
    return library_1,library_2, median_ratio
