import numpy as np
from scipy.ndimage.filters import generic_filter

from flag_high_spatial_outliers import flag_high_spatial_outliers
from function_util import parallelize_regions
from make_expected_matrix_fragment import make_expected_matrix_fragment
from polyfit_model_fragments import polyfit_model_fragments
from make_expected_matrix_binned import make_expected_matrix_binned
from polyfit_model_bins import polyfit_model_bins
from express_normalize_matrix import express_normalize_matrix


@parallelize_regions
def remove_high_spatial_outliers(matrix, size=5, fold_threshold=8.0,
                                 overwrite_value='nan', primermap=None,
                                 level='fragment'):
    """
    Convenience function for removing high spatial outliers from counts
    matrices.

    Operates in-place.

    Parameters
    ----------
    matrix : np.ndarray
        The matrix to remove outliers from.
    size : int
        The size of the window to look in around each element when deciding if
        it is an outlier. Should be an odd integer.
    fold_threshold : float
        Elements will be flagged as outliers if they are greater than this
        number or greater than this many times the local median (as estimated
        using the window size in ``size``).
    overwrite_value : {'nan', 'zero', 'median'}
        The value to overwrite elements flagged as outliers with.
    primermap : List[Dict[str, Any]]
        The list of fragments for this region corresponding to ``counts``. Only
        required if ``overwrite_value`` is 'median'.
    level : {'fragment', 'bin'}
        Whether to interpret ``matrix`` as bin- or fragment-level. The
        difference is that bin-level matrices are assumed to have equal distance
        between elements. Only required if ``overwrite_value`` is 'median'.

    Returns
    -------
    np.ndarray
        The input matrix with all spatial outliers overwritten.
    """
    if overwrite_value == 'median':
        # precompute expected
        if level == 'fragment' and primermap is not None:
            expected = make_expected_matrix_fragment(matrix, primermap,
                                                     polyfit_model_fragments)
        else:
            expected = make_expected_matrix_binned(matrix, polyfit_model_bins)

        # express normalize
        normalized, _, _ = express_normalize_matrix(matrix, expected)
        normalized[~np.isfinite(matrix)] = np.nan
    else:
        normalized = matrix

    # flag outliers
    flagged_indices = flag_high_spatial_outliers(
        normalized, size=size, fold_threshold=fold_threshold)

    # overwrite outliers
    if overwrite_value == 'nan':
        matrix[flagged_indices == 1] = np.nan
    elif overwrite_value == 'zero':
        matrix[flagged_indices == 1] = 0
    elif overwrite_value == 'median':
        median_array = generic_filter(normalized, np.nanmedian, size=size)
        matrix[flagged_indices == 1] = median_array[flagged_indices == 1]

    return matrix
