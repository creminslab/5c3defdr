from get_midpoint import get_midpoint

def find_upstream_primers(regional_pixelmap, regional_primermap):
    """
    Creates a mapping from a bin index to the index of its nearest upstream
    primer.

    Parameters
    ----------
    regional_pixelmap : List[Dict[str, Any]]
        The bin map describing the bins for this region.
    regional_primermap : List[Dict[str, Any]]
        The primermap describing the primers for this region.

    Returns
    -------
    Dict[int, int]
        A map from each bin index to the index of its nearest upstream primer.
    """
    mapping = {}

    bin_index = 0
    primer_index = 0

    while bin_index < len(regional_pixelmap):
        bin_midpoint = get_midpoint(regional_pixelmap[bin_index])
        while (get_midpoint(regional_primermap[primer_index + 1]) < bin_midpoint
               and primer_index < len(regional_primermap)):
            primer_index += 1
        mapping[bin_index] = primer_index
        bin_index += 1

    return mapping

