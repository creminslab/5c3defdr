from bed3_line_parser import *
from bed4_line_parser import *
from bed5_line_parser import *
from bed6_line_parser import *


def guess_field_parser(line):
    """
    One of two functions that "guesses" which BED file parser to use in order to load BED file features

    Parameters
    ----------
    line : str

    Represents the ith row of the BED file

    Returns
    -------
    bed*_line_parser

    Appropriate parser function for extracting BED file features

    """

    # split line on tab
    pieces = line.split('\t')

    if len(pieces) == 3:
        return bed3_line_parser

    if len(pieces) == 4:
        return bed4_line_parser
    if len(pieces) == 5:
        try:
            float(pieces[4])
        except ValueError:
            return bed4_line_parser
        return bed5_line_parser
    if len(pieces) >= 6:
        try:
            float(pieces[4])
        except ValueError:
            return bed4_line_parser
        if pieces[5].strip() not in ['+', '-', '.']:
            return bed5_line_parser
        return bed6_line_parser
