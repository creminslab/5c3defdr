import numpy as np
from find_unsmoothable_columns import find_unsmoothable_columns
from wipe_matrix import wipe_matrix
from function_util import parallelize_regions


@parallelize_regions
def wipe_unsmoothable_columns(binned_counts, primermap, pixelmap, window_width):
    """
    Convenience function for wiping the unsmoothable columns in a binned counts
    matrix assuming that the smoothing was a filtering operation applied on
    fragment-level data.

    Parameters
    ----------
    binned_counts : np.ndarray
        The matrix of binned counts to wipe unsmoothable columns from.
    primermap : List[Dict[str, Any]]
        The primermap describing the primers for this region.
    pixelmap : List[Dict[str, Any]]
        The pixelmap describing the bins for this region.
    window_width : int
        The width of the filtering window in base pairs.

    Returns
    -------
    np.ndarray
        The wiped matrix of binned counts.
    """
    unsmoothable_columns = find_unsmoothable_columns(primermap, pixelmap,
                                                    window_width)
    return wipe_matrix(binned_counts, np.where(unsmoothable_columns))
