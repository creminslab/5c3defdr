import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def unlog(array, pseudocount=1.0, base='e'):
    """
    Unlogs an array (element-wise).

    Parallelized via ``@parallelize_regions``.

    Emits nan's when the input values are nan.

    Parameters
    ----------
    array : np.ndarray
        The array to unlog.
    pseudocount : float
        Psuedocount to subtract after unlogging.
    base : str or float
        The base to use when unlogging. Acceptable string values are 'e', '2',
        or '10'.

    Returns
    -------
    np.ndarray
        The unlogged array.

    Examples
    --------
    >>> import numpy as np
    >>> from lib5c.util.mathematics import log, unlog
    >>> a = np.array([[1, 2], [2, 4.]])
    >>> log(unlog(a))
    array([[ 1.,  2.],
           [ 2.,  4.]])
    >>> log(unlog(a, base=42), base=42)
    array([[ 1.,  2.],
           [ 2.,  4.]])
    """
    unlog_fns = {'e': np.exp, '2': np.exp2, '10': lambda x: np.power(10, x)}
    if str(base) not in unlog_fns.keys():
        unlog_fn = lambda x: np.power(float(base), x)
    else:
        unlog_fn = unlog_fns[str(base)]
    return unlog_fn(array) - pseudocount
