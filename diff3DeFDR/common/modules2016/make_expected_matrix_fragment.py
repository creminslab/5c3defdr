import numpy as np
from make_distance_matrix import make_distance_matrix
from make_expected_matrix_from_dict import make_expected_matrix_from_dict


def make_expected_matrix_fragment(observed, primermap, exp_model,
                                  is_global=False, **kwargs):
    """
    Convenience function for quickly making an expected matrix for a
    fragment-level observed counts matrix.

    Parameters
    ----------
    observed: np.ndarray or Dict[str,ndarray]
        The matrix or dict of observed counts.
    primermap : Dict[str, List[Dict[str, Any]]]
        Primermap describing the loci in one or more  regions. 
        Necessary to figure out distances between elements in
        the contact matrix.
    exp_model : Callable[Dict[int, float]]
        a function that generates an expected model from fragment level data
    is_global : bool
        sets the scale of the expected model. If False, the regional expected
        model will be determined. If observed is an np.ndarray this kwarg is
        ignored.

    Returns
    -------
    np.ndarray or Dict[str,ndarray]
        The expected matrix.
    """
    if isinstance(observed, dict):
        # determines expected dict
        distances = {region: make_distance_matrix(primermap[region]) for region
                     in primermap.keys()}
        if not is_global:
            distance_expected = {
                region: exp_model(observed[region], distances[region],
                                  is_global=False, **kwargs)
                for region in observed.keys()}
            return {
                region: make_expected_matrix_from_dict(
                    distance_expected[region], distances[region])
                for region in observed.keys()}
        else:
            distance_expected = exp_model(observed, distances, is_global=True,
                                          **kwargs)
            return {region: make_expected_matrix_from_dict(distance_expected,
                                                           distances[region])
                    for region in observed.keys()}
    elif isinstance(observed, np.ndarray) and not is_global:
        # determines single expected matrix
        distance_matrix = make_distance_matrix(primermap)
        distance_expected = exp_model(observed, distance_matrix,
                                      is_global=False, **kwargs)
        return make_expected_matrix_from_dict(distance_expected,
                                              distance_matrix)
    else:
        raise TypeError('observed must be either np.ndarray or dict')
