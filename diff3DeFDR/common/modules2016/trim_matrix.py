import numpy as np

from parallelize_regions import parallelize_regions


@parallelize_regions
def trim_matrix(matrix, indices):
    """
    Removes specified rows and columns from the counts matrix.

    Parameters
    ----------
    matrix : np.ndarray
        The square symmetric counts matrix to trim.
    indices : Iterable[int]
        The indices to wipe

    Returns
    -------
    np.ndarray
        The trimmed counts matrix.
    """
    indices = list(indices)
    matrix = np.delete(matrix, indices, axis=0)
    matrix = np.delete(matrix, indices, axis=1)
    return matrix
