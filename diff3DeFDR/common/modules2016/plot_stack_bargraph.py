from __future__ import division

import numpy as np
from matplotlib import pyplot as plt

from get_annotation_percentage_all import get_annotation_percentage_all


def plot_stack_bargraph(annotation_a, annotation_b, filename, loop_types,
                        labels, colors, annotationmaps, looping_classes,
                        threshold=0, margin=1):

    """
    Plots a bar graph with loop types arranged on the x-axis and the percentage
    of times ``annotation_a`` is interaction with ``annotation_b`` in all the
    loops of that loop type.

    Parameters
    ----------
    annotation_a : str
        First annotation you are intereted in.
    annotation_b : str
        Second annotation you are interested in.
    filename : str
        String reference to the filename to draw the heatmap to.
    loop_types : list of str
        The order in which to arrange the loop types along the x-axis, from left
        to right. If you exclude a loop type from this list, it will be excluded
        from the heatmap.
    labels : list of str
        The labels you want to be assigned on the x-axis to each of the loop
        types. The label order should correspond to the order of ``loop_types``.
    colors : list of valid matplotlib colors
        The colors to plot each bar with. The order should correspond to the
        order of ``loop_types``.
    threshold : int
        Bins are defined to contain an annotation if they are "hit" strictly
        more than ``threshold`` times by the annotation.
    margin : int
        A bin is defined to contain an annotation if any bin within ``margin``
        bins is "hit" by the annotation. Corresponds to a "margin for error" in
        the intersection precision.
    """
    # extract data
    array_data = [100*get_annotation_percentage_all(
        annotation_a, annotation_b, loop_type, annotationmaps, looping_classes,
        threshold=threshold, margin=margin) for loop_type in loop_types]
    
    # plot figure
    plt.clf()
    plt.figure(num=None, figsize=(5, 3.5), dpi=200, facecolor='w',
               edgecolor='w')
    xlocations = np.array(range(len(array_data)))+0.5
    width = 0.5
    plt.axhline(y=array_data[len(loop_types)-1], linestyle='--', linewidth=3.0,
                color='#666666')
    plt.bar(xlocations, array_data, width=width, color=colors)
    plt.xticks(xlocations+width/2, labels, fontsize=6)
    plt.xlim(0, xlocations[-1]+width*2)
    plt.ylabel('Percentage with %s against %s' % (annotation_a, annotation_b),
               fontsize=5)

    # save figure
    plt.savefig(filename, bbox_inches='tight')
    plt.close()
