import os


def check_dir(dirname):
    '''
    Make dir dirname if it does not already exist.

    Parameters
    ----------
    dirname : str
        Directory to create if it doesn't already exist

    Returns
    -------
    dirname : str
        Sends back input directory name to allow inline use
    '''
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    return dirname
