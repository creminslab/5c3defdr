import os

import glob
import numpy as np

from modules2016 import load_counts, load_primermap


def load_counts_matrices(bed_path, counts_path, counts_pre=None, counts_ext=None, not_ext=None, **kwargs):
    """
    Load set of counts matrices from file by matching {counts_path}/{counts_pre}*{counts_ext}.counts. or "".npz.

    Parameters
    ----------
    bed_path : str, optional
        Path to mapping of primers or bins to indices in counts matrices
    counts_path : str
        Directory frp, which to load counts matrices
    counts_pre : [str]
        Filename prefix to match, only loads replicate files including this prefix
    counts_ext : [str]
        Filename suffix to match, only loads replicate files including this suffix
    not_ext : [str]
        Filename substring to NOT match, leaves out replicate files including this suffix
    kwargs : dict
        Keyword arguments to pass on to ``diff3DeFDR.common.load_primermap``

    Returns
    -------
    counts_matrices : [list[dict[str,numpy.ndarray]]]
        List of regional counts matrices loaded from counts_path
    counts_fnames : [list[str]]
        List of corresponding filenames from which each counts matrix was loaded
    primermap : dict
        Primer to genomic coordinate mapping for the counts files. Created with ``diff3DeFDR.common.load_primermap``
    """

    primermap = load_primermap(bed_path, **kwargs)

    if counts_pre is None:
        counts_pre = ''
    if counts_ext is None:
        counts_ext = ''

    counts_match = os.path.join(counts_path, '{}*{}.counts'.format(counts_pre, counts_ext))
    counts_fnames = glob.glob(counts_match)

    is_npz = False
    if len(counts_fnames) == 0:
        # Try npz
        counts_match = os.path.join(counts_path, '{}*{}.npz'.format(counts_pre, counts_ext))
        counts_fnames = glob.glob(counts_match)
        if len(counts_fnames) == 0:
            raise ValueError('No files were found that match {0}*{1}.counts nor {0}*{1}.npz in directory {2}'.format(
                counts_pre, counts_ext, counts_path))
        else:
            is_npz = True
    if not_ext:
        counts_fnames = [i for i in counts_fnames if not_ext not in i]

    counts_matrices = []
    for counts_fname in counts_fnames:
        if is_npz:
            counts = np.load(counts_fname)
            if 'arr_0' in counts:
                counts = counts['arr_0'][()]
            else:
                counts = {region: counts[region][()] for region in counts}
        else:
            counts = load_counts(counts_fname, primermap)
        counts_matrices.append(counts)

    return counts_matrices, counts_fnames, primermap