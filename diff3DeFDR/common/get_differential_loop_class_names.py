from itertools import combinations

import numpy as np

def get_differential_loop_class_names(condition_list):
    """
    Get list of all possible differential loop classifications given list of input condition names

    Parameters
    ----------
    condition_list : list[str]
        List of condition names, usually specified in an input rep_info.tsv file

    Returns
    -------
    differential_classnames : list[str]
        List of all possible differential loop classifications
    """
    single_condition_classnames = np.sort(condition_list)

    if len(condition_list) == 2:
        return single_condition_classnames

    if len(condition_list) == 3:
        dual_condition_classnames = np.sort(['{}_{}'.format(i, j)
                                             for i, j in combinations(single_condition_classnames, 2)])
        differential_classnames = np.concatenate([single_condition_classnames, dual_condition_classnames])
        return differential_classnames