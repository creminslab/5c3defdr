import os

import glob
import numpy as np

from parse_repinfo import parse_repinfo
from modules2016 import load_counts, load_primermap
from DataSet import DataSet
from ExperimentSet import ExperimentSet

def _load_counts_by_name(counts_path, counts_prefix, counts_ext, primermap):
    """
    Helper method. Loads a single counts file by matching {counts_path}/{counts_prefix}*{counts_ext}.counts.

    Parameters
    ----------
    counts_path : str
        Directory holding counts file
    counts_prefix: str
        Filename prefix to match
    counts_ext: str
        Filename suffix to match
    primermap: dict
        Primer mapping for the counts file. Created with ``diff3DeFDR.common.load_primermap``

    Returns
    -------
    counts : dict[str,numpy.ndarray]
        Matrix of regional counts matrices loaded from file
    """
    counts_match = os.path.join(counts_path, '{}*{}.*'.format(counts_prefix, counts_ext))
    counts_fnames = glob.glob(counts_match)

    try:
        assert len(counts_fnames) == 1
    except AssertionError:
        print 'Error: Ambiguous or unmatched filename in rep_info.tsv'
        raise

    counts_fname = counts_fnames[0]

    if 'npz' in counts_fname:
        counts = np.load(counts_fname)
        if 'arr_0' in counts:
            counts = counts['arr_0'][()]
        else:
            counts = {region: counts[region][()] for region in counts}
    else:
        counts = load_counts(counts_fname, primermap)

    return counts

def load_counts_into_df(bed_path, counts_path, repinfo_path, counts_ext='', **kwargs):
    """
    Load real and simulated experiments from counts files into ExperimentSet object
    
    Parameters
    ----------
    bed_path: str
        path to bed file
    counts_path: str
        path to directory containing counts files
    counts_ext: str
        what data type of counts to load into the returned ExperimentSet objects
    
    Returns
    -------
    repinfo : pandas.DataFrame
        DataFrame from which counts matrix and loaded mapping (from bed file) are accessible
    primermap : dict
        Primer mapping for the counts file. Created with ``diff3DeFDR.common.load_primermap``
        Returned for convenience
    """
    repinfo = parse_repinfo(repinfo_path)
    primermap = load_primermap(bed_path, **kwargs)

    match_column = repinfo['filematch'] if 'filematch' in repinfo else repinfo.index

    repinfo['counts'] = np.vectorize(_load_counts_by_name)(counts_path, match_column, counts_ext, primermap)
    return repinfo, primermap

def load_counts_into_dataset(bed_path, counts_path, repinfo_path, counts_ext='', **kwargs):
    """
    Load real and simulated experiments from counts files into ExperimentSet object
    
    Parameters
    ----------
    bed_path :str
        path to bed file
    counts_path : str
        path to directory containing counts files
    counts_ext : str
        what data type of counts to load into the returned ExperimentSet objects
    
    Returns
    -------
    d : DataSet
        Dataset object from which counts matrix and loaded mapping (from bed file) are accessible
    """
    repinfo, mapping = load_counts_into_df(bed_path, counts_path, repinfo_path, counts_ext, **kwargs)

    d = DataSet.from_counts_superdict(repinfo['counts'].to_dict(),
                                      pixelmap=mapping,
                                      repinfo=repinfo.drop(['counts'], axis=1))
    return d

def load_counts_into_experimentset(bed_path, counts_path, repinfo_path=None, counts_ext='', **kwargs):
    """
    Load real and simulated experiments from counts files into ExperimentSet object
    
    Parameters
    ----------
    bed_path :str
        path to bed file
    counts_path : str
        path to directory containing counts files
    counts_ext : str
        what data type of counts to load into the returned ExperimentSet objects
    
    Returns
    -------
    exp : ExperimentSet
        ExperimentSet object from which counts matrix and loaded mapping (from bed file) are accessible
    """
    repinfo, mapping = load_counts_into_df(bed_path, counts_path, repinfo_path, counts_ext, **kwargs)

    exp = ExperimentSet(counts_list=repinfo['counts'].values,
                        conditions=repinfo['condition'].values,
                        labels=repinfo.index.values,
                        mapping=mapping,
                        bed_path=bed_path,
                        counts_path=counts_path,
                        counts_ext=counts_ext,
                        mapping_kwargs=kwargs)
    return exp