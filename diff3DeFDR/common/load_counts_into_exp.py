from os.path import basename

import numpy as np

from diff3DeFDR.common import ExperimentSet, load_counts_matrices


def load_counts_into_exp(bed_path, counts_path, counts_ext=None, not_ext=None, **kwargs):
    """
    Load real and simulated experiments from counts files into ExperimentSet object

    Parameters
    ----------
    bed_path: str
        Path to bed file
    counts_path: str
        Path to directory containing counts files
    counts_ext: str
        What data type of counts to load into the returned ExperimentSet objects
    kwargs : dict
        Keyword arguments to pass on to ``diff3DeFDR.common.load_primermap``

    Returns
    -------
    exp : ExperimentSet
        ExperimentSet object from which counts matrix and loaded mapping (from bed file) are accessible
    """
    x, counts_paths, bed_mapping = load_counts_matrices(bed_path, counts_path, counts_ext=counts_ext, not_ext=not_ext, **kwargs)

    sort_order = np.argsort(counts_paths)
    x = np.array(x)[sort_order]
    counts_paths = np.array(counts_paths)[sort_order]
    counts_fnames = [basename(f).split('.')[0] for f in counts_paths]

    conditions = [f.split('_')[0] for f in counts_fnames]
    labels = [f[:-len(counts_ext)-1] for f in counts_fnames] if counts_ext else counts_fnames

    exp = ExperimentSet(counts_list=x, conditions=conditions, labels=labels, mapping=bed_mapping, bed_path=bed_path,
                        counts_path=counts_path, counts_ext=counts_ext)
    return exp