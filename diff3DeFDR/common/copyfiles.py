import os
import shutil
import errno

def copyfiles(src, dst):
    '''
    Copies a file from source src to destination dst.

    Parameters
    ----------
    src : str
        Path to file to be copied
    dst : str
        Path to copy the file to
    '''
    try:
        if os.path.exists(dst):
            shutil.rmtree(dst)
        shutil.copytree(src, dst)
    except OSError as exc:  # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            raise