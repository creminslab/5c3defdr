import os

import gzip
from urllib import URLopener


def grab_geo_data(sample_links, output_dir):
    """
    Downloads all necessary input data files from GEO.

    Parameters
    ----------
    sample_links : list[dict]
        Each element in list is a dict with the replicate name and corresponding link from which to download its raw
        counts matrix, e.g. [{'name': 'es2i_rep1', 'url': 'http:/...'}, {'name': 'es2i_rep2', 'url': 'http:/...'}, ...]
    output_dir : str
        Directory in which to save downloaded data
    """
    print 'Downloading input 5C libraries from GEO.'
    for i, row in sample_links.iterrows():
        download_path = os.path.join(output_dir, row['name'])
        print '  Downloading {}'.format(row['name'])
        f = URLopener()
        f.retrieve(row['url'], download_path)
        f.close()

        g = gzip.open(download_path, 'rb').read()
        with open(download_path, 'w') as w:
            w.write(g)