import os

def check_outdir(outfile):
    """
    Create all directory levels in path outfile that do not already exist.

    Parameters
    ----------
    outfile : str
        Path, inclusive of base file name, to create

    Returns
    -------
    dirname : str
        Sends back input directory name to allow inline use
    """
    if type(outfile) == dict:
        for r, f in outfile.iteritems():
            check_outdir(f)
    else:
        head, tail = os.path.split(outfile)
        if head and not os.path.exists(head):
            print('creating directory %s' % head)
            os.makedirs(head)

    return outfile