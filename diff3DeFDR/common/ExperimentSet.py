from ntpath import basename

import numpy as np
import pandas as pd

from diff3DeFDR.common.modules2016 import write_counts
from DataSet import DataSet


class ExperimentSet(object):
    """
    Wrapper around set of counts matrices. Useful for carrying around counts matrices along with
    accompany primer or pixel mapping, and information about how they accessed from file.

    Attributes
    ----------
    counts_list : np.array
        Contains core list of replicate counts matrices
    conditions : np.array
        List of condition names corresponding to matrices in counts_list
    labels : np.array
        List of extract labels identifying replicates in counts_list
    mapping : primermap or pixelmap
        A mapping that provides information about the fragments or bins included in core matrices
    bed_path : str, optional
        Path that mapping was loaded from
    counts_path : str, optional
        Path to common directory that counts matrices were loaded from
    counts_ext : str, optional
        Suffix used to distinguish uploaded counts matrices from others in counts_path
    repinfo : pd.DataFrame, optional
        Its row index should be the replicate names, its columns can provide
        arbitrary information about each replicate, such as its condition, etc.
    mapping_kwargs : dict, optional
        Additonal arguments that were needed to load core counts matrices from file

    """
    def __init__(self, counts_list, conditions, labels=None, mapping=None, bed_path=None, counts_path=None,
                 counts_ext=None, mapping_kwargs=None):
        """
        Constructor for ExperimentSet class, which is a management class for 5C counts files.

        Parameters
        ----------
        counts_list : [list[dict[str,numpy.ndarray]]
            List of counts matrices.
        conditions : [list[str]]
            List of corresponding conditions for matrices in counts_list.
        labels :  [list[str]]
            List of corresponding replicate labels for matrices in counts_list
        mapping : [dict]
            Primer or bin mapping to genomic coordinates.
        bed_path : [str]
            Path to BED file that mapping was loaded from.
        counts_path : [str]
            Path to counts files that counts_list was loaded from.
        counts_ext : [str]
            Extension used when loading counts_list from counts_path.
        repinfo : [pandas.DataFrame]
            Replicate information loaded from input ``rep_info.tsv`` using ``diff3DeFDR.common.parse_repinfo``
        mapping_kwargs: [dict], optional
            Keyword arguments used when loading mapping from file.
        """
        assert type(counts_list) in [list, np.ndarray]
        assert type(conditions) in [list, np.ndarray]
        assert len(conditions) == len(counts_list)

        if labels is not None:
            assert len(labels) == len(counts_list)
        else:
            # Set labels from conditions list
            rep_tracker = {c: 0 for c in np.unique(conditions)}
            labels = []
            for c in conditions:
                rep_tracker[c] += 1
                new_label = '{}_rep{}'.format(c, rep_tracker[c])
                labels.append(new_label)

        if mapping is not None:
            assert type(mapping) is dict

        if bed_path is not None:
            assert type(bed_path) is str
            self.bed_name = basename(bed_path).split('.')[0]

        if counts_path is not None:
            assert type(counts_path) is str

        if counts_ext is not None:
            assert type(counts_ext) is str

        self.counts_list = np.array([{r: np.array(i[r], dtype=np.float64) for r in i} for i in counts_list])
        self.conditions = np.array(conditions)
        self.unique_conditions = np.unique(self.conditions)
        self.labels = np.array(labels)
        self.mapping = mapping
        self.bed_path = bed_path
        self.counts_path = counts_path
        self.counts_ext = counts_ext
        self.mapping_kwargs = mapping_kwargs

    def set_labels(self, labels):
        """
        Relabel the counts_list with new list of labels.

        Parameters
        ----------
        labels : list[str]
        """
        assert len(labels) == len(self.counts_list)
        self.labels = labels

    def set_mapping(self, mapping):
        """
        Set a new genomic coordinate mapping.

        Parameters
        ----------
        mapping : dict
            Primer or bin mapping to genomic coordinates. Should create by loading from file with ``diff3DeFDR.common.load_primermap``
        """
        assert type(mapping) is dict
        self.mapping = mapping

    def __len__(self):
        """
        This object's length is equal to the number of counts matrices it carries.
        :return: [int]
        """
        return len(self.counts_list)

    def superdict(self):
        """
        Obtain counts "superdict" representation of counts matrices carried in this object.

        Returns
        -------
        :return: [dict[str, dict[str, numpy.ndarray]] {replicate_label: regional counts matrix of replicate}
        """
        return {self.labels[i]: self.counts_list[i] for i in xrange(len(self))}

    def repinfo(self):
        condition_map = {l: self.conditions[i] for i, l in enumerate(self.labels)}
        info = pd.DataFrame.from_dict(condition_map, orient='index').rename(columns={0: 'condition'})
        info.index.name = 'replicate'
        return info

    def to_DataSet(self, column_name='counts'):
        """
        Convert ExperimentSet object to DataSet object
        :param column_name:
        :return:
        """
        dataset = DataSet.from_counts_superdict(counts_superdict=self.superdict(),
                                                pixelmap=self.mapping,
                                                repinfo=self.repinfo(),
                                                name=column_name)
        return dataset

    def save_counts(self, output_dir='../counts_data/preprocessed', ext=None):
        """
        Save counts matrices to counts files.

        Parameters
        ----------
        output_dir : str
            Directory to save counts files to.
        ext: str
            Extension to add to counts file names (which are replicate labels).

        Returns
        -------
        file_paths : list[str]
            List of file paths at which each counts matrix was saved as a .counts file.
        """
        file_paths = []
        for i, l in enumerate(self.labels):
            file_path = '{}/{}{}.counts'.format(output_dir, l, '_{}'.format(ext) if ext else '')
            file_paths.append(file_path)
            write_counts(self.counts_list[i], file_path, self.mapping)
        return file_paths

    def save_counts_as_npz(self, output_dir, ext=None):
        """
        Save counts matrices to npz files.

        Parameters
        ----------
        output_dir : str
            Directory to save npz files to.
        ext : str
            Extension to add to counts file names (which are replicate labels).

        Returns
        -------
        file_paths : list[str]
            List of file paths at which each counts matrix was saved as a .npz file.
        """
        file_paths = []
        for i, l in enumerate(self.labels):
            file_path = '{}/{}{}.npz'.format(output_dir, l, '_{}'.format(ext) if ext else '')
            file_paths.append(file_path)
            np.savez(file_path, self.counts_list[i])
        return file_paths

    def set_counts(self, counts_list):
        """
        Update the counts_list carried in this object.

        Parameters
        ----------
        counts_list : list[dict[str,numpy.ndarray]
            List of counts matrices.
        """
        assert len(self.conditions) == len(counts_list)
        self.counts_list = np.array([{r: np.array(i[r], dtype=np.float64) for r in i} for i in
                                     counts_list])  # TODO remove when origin of np.float32 conversion is found