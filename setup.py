from setuptools import setup, find_packages


extras_require = {
    'bsub': ['bsub>=0.3.5'],
    'iced': ['iced>=0.4.0'],
    'pyBigWig': ['pyBigWig>=0.3.10'],
    'multiprocess': ['multiprocess>=0.70.9']
}
extras_require['complete'] = sorted(set(sum(extras_require.values(), [])))

setup(
    name='diff3DeFDR',
    version='1.0',
    description='Identify differential looping interactions',
    author='Lindsey Fernandez',
    url='https://bitbucket.org/creminslab/diff3defdr',
    packages=find_packages(),
    install_requires=[
        'configparser>=3.5.0',
        'interlap>=0.2.6',
        'matplotlib>=2.1.0',
        'numpy>=1.12.1',
        'pandas>=0.22.0',
        'scipy>=1.0.0',
        'seaborn>=0.8.1',
        'statsmodels>=0.8.0,<=0.10.2',
        'subprocess32>=3.2.7',
        'xlrd>=1.1.0',
    ],
    extras_require=extras_require,
    package_data={
        'diff3DeFDR.processing5C.transformation': ['transformation_names.csv', 'find_logistic_pvalues.R'],
        'diff3DeFDR.plotters.util': ['gene_tracks/*.gz'],
        'diff3DeFDR': ['diff3DeFDR.cfg']
    },
    entry_points={
        'console_scripts': [
            'diff3defdr = diff3DeFDR.runFromConfig:main'
        ]
    },
    classifiers=[
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2.7.11',
        'License :: OSI Approved :: MIT License'
    ]
)
