FROM debian:jessie-slim

# build deps
RUN apt-get update && apt-get install -y \
    build-essential \
    python-minimal \
    r-base \
    r-base-core \
    r-recommended \
    python-dev \
    python-pip \
    zlib1g-dev \
    libcurl4-openssl-dev \
    wget

# python deps
RUN pip install -U pip
COPY requirements.txt .
RUN pip install $(grep numpy requirements.txt) && \
    pip install -r requirements.txt

# matplotlib config
RUN mkdir -p ~/.config/matplotlib && \
    echo backend: agg > /root/.config/matplotlib/matplotlibrc

# application code
COPY . /src/diff3defdr
RUN pip install /src/diff3defdr

ENTRYPOINT bash
