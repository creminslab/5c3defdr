Loop classification clusters
============================

.. image:: images/classification_clusters.png
   :align: center

For our team, perhaps the most interesting plots are those showing which
clusters of differential loops were called with our methods.

Exposed functionality
---------------------

The exposed method for creating these plots is:

* :func:`diff3DeFDR.plotters.plot_classification_clusters`

Workflow
--------

Like ``plot_heatmap()``, ``plot_classification_clusters()`` takes a ``DataSet``
object as input where the column 'color' is contains our final loop
classifications output by either 3DeFDR or its benchmark methods. Loop calls
from 3DeFDR and the benchmarks are all saved to file in a format that is readily
loaded to a ``DataSet`` in this way. The workflow for this is then:
::

    from diff3DeFDR.plotters import plot_classification_clusters

    color_dict = {'es2i': 'orange', 'esserum': 'green', 'npc': 'blue',
                  'es2i_esserum': 'yellow', 'esserum_npc': 'cyan', 'es2i_npc': 'magenta',
                  'constitutive': 'gray', 'background': 'black'}

    plotting_window = {'region': 'Olig1-Olig2',
                       'x': 'chr16:91135612-91330612',
                       'y': 'chr16:91135612-91330612'}

    h = plot_classification_clusters(
        dataset,
        window=plotting_window,
        color_dict=color_dict,
        categorical=False,
        refgene_stacks_dir=my_gene_tracks_dir,
        refgene_assembly_name='mm9')
    h.save(my_plot_path)
    h.close()

where ``dataset`` is accessible as the attribute ``final_calls`` of ``eFDR``
after running the instance method ``classify_loops()``:
::

    from diff3DeFDR.loopCallers.empiricalFDR import eFDR

    e = eFDR(real_is _experimentset, sim_transforms_dir, intermediates_dir, output_dir)
    e.classify_loops(
        null_model_condition='es2i'
        background_threshold=0.8,
        significance_threshold=0.165,
        difference_thresholds=None,
        fdr_threshold=0.01,
        adaptive=True,
        min_cluster_size=4
    )
    dataset = e.final_calls

Alternatively, datasets of final loop calls from previous runs of
``classify_loops()`` are also saved to file and accessible via a static
convenience method of eFDR:
::

    from diff3DeFDR.common import DataSet

    dataset = DataSet.load(eFDR.previous_results_dataset_paths(output_dir)[0])

While other parameters of ``plot_classification_clusters()`` match those of
``plot_heatmap()``, the ``color_dict`` and categorical parameters are unique to
``plot_classification_clusters()``. ``color_dict`` specifies which color each
loop class should be plotted as. If ``categorical`` is set to False, clusters
are computed from the input loop classifications in ``dataset`` and only the
outlines of these clusters are drawn and colored as specified with
``color_dict``. If set to True, no clusters are computed and instead each
interaction is plotted as an individual pixel color-coded according to
``color_dict``.
