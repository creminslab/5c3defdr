Counts heatmaps
===============

.. image:: images/counts_heatmaps.png
   :align: center

In the course of processing 5C counts, we generate many different transformed
data types. To help explore how these transformations affect your data, we
encourage you visualize your transformed contact maps as heatmaps, as shown
above.

Exposed functionality
---------------------

The exposed function for creating these plots is:

* :func:`diff3DeFDR.plotters.plot_heatmap`

Workflow
--------

Inputting a replicate set of the desired transformation as a DataSet object
dataset, we can plot heatmaps as follows:
::

    from diff3DeFDR.common import get_colormap
    from diff3DeFDR.plotters import plot_heatmap

    plotting_window = {'region': 'Olig1-Olig2',
                       'x': 'chr16:91135612-91330612',
                       'y': 'chr16:91135612-91330612'}

    h = plot_heatmap(
        dataset=dataset,
        rep_name='es2i_rep1',
        data_column='is',
        window=plotting_window,
        colorscale={region: [0, 60] for region in dataset.pixelmap},
        colormap=get_colormap('is')
        refgene_stacks_dir=my_gene_tracks_dir,
        refgene_assembly_name='mm9'
    )
    h.save(my_plot_path)
    h.close()

``DataSet`` objects can carry replicate sets at multiple levels of
transformation. Set ``data_column`` to the name of the data transformation or
whatever column name you used to upload your replicates as a ``DataSet``. If you
did not set a column name and your ``DataSet`` object carries data for only a
single transformation, set data_column to None and ``plot_heatmaps()`` will
choose the correct column name. Specify which replicate in dataset to plot with
rep_name, and what genomic range to plot with window, which must be in the
format:
::

    window = {'region': name, 'x': 'chr#:start-end', 'y': 'chr#:start-end'}

Finally to add gene tracks to your maps, you must point ``plot_heatmap()`` to a
directory refgene_stacks_dir containing your reference gene assembly files and
specify which files to use with ``refgene_assembly_name``.

``plot_heatmap`` ultimately returns a handle to an ``ExtendableHeatmap``, a
utility class for plotting heatmaps available in ``diff3DeFDR.plotters.util``.
You may use this handle to either immediately save your map as we did in the
example above, or to add more tracks along the axes of your plot as we did for
our manuscript figures, adding ChIP-Seq tracks from BigWig files. We encourage
you to explore the other ``*ExtentableHeatmap`` classes within the ``util``
subpackage which each contain methods for adding more features to these
heatmaps.
