Simulating 5C counts
====================

In our manuscript, we chose to simulate 5C counts from the level of quantile
normalized counts in the Beagan et al. 2017 pipeline. You may choose to mimic
our approach or simulate counts from another level of transformation. Whatever
the input, when using our simulation procedure, we recommend first exploring the
mean-variance relationship of your counts to ensure that they are in fact
overdispersed in a way that trends with distance scale as we determined ours to
be.

In this section, we provide a workflow in how to apply our simulation approach.
The algorithms that make up the 5C simulation framework can be found in the
subpackage ``diff3DeFDR.loopcallers.empiricalFDR`` and the most important class
within it is ``SimulationSet``. ``SimulationSet`` takes an ``ExperimentSet``
object as input, which contains the countsfiles and primerfile of the dataset to
be modeled and simulated.

Using ``SimulationSet``, we model every interaction in each condition in the
input ExperimentSet and can then generate as many simulations of each condition
as we want. With an ExperimentSet ``exp`` containing quantile normalized counts,
we can do this using the workflow:
::

    from diff3DeFDR.loopCallers.empiricalFDR import SimulationSet

    simset = SimulationSet(
        exp,
        output_dir=my_dir,
        distance_bins=my_distance_bins,
        globally_fit_dispersion=True,
        mean_outlier_limit=2.5,
        var_outlier_limit=None,
        loess_smooth=0.5,
        predicted_variance_weight=0.5,
        observed_variance_weight=0.5,
        compressed=True,
        parallelized=True
    )
    simset.compute_generative_model()
    simset.make_simulations(num_simulation_sets=1000)

which saves 1000 simulated replicate sets to ``output_dir``.

If we assume ``exp`` contains two replicates of each of three conditions
(``n_exp`` = six replicates total), each simulated replicate set will by default
contain six replicates of each of the three conditions so that if any single
condition is later chosen as a null model, there are enough simulated replicates
of that condition to create a null dataset of the same size as the input
experimental dataset (``n_exp`` = ``n_null``). We additionally chose to simulate
all conditions by default (before choosing a single condition for our null)
because we wanted to ensure that simulated counts are normalized similarly to
input experimental counts in the joint express normalization step of the Beagan
et al. 2017 5C processing pipeline.

``compressed`` specifies whether output simulated counts matrices will be stored
uncompressed as .counts files or compressed as .npz files. ``parallelized``
specifies whether jobs for creating each simulated replicate will be completed
in parallel or in sequence via the utility function
``diff3DeFDR.common.job_managment()``. The remaining parameters of
``SimulationSet`` guide how the simulated counts generating functions are
determined for each interaction. We discuss them conceptually in more detail
below.

Defining the mean-variance relationship (MVR) of input counts
-------------------------------------------------------------

The first step in constructing our simulation-generating functions is to compute
the sample mean and sample variance of every interaction in each condition as:
::

    condition_set = exp.unique_conditions
    region_set = exp.counts_list[0].keys()

    means = {c: {} for c in condition_set}
    vars = {c: {} for c in condition_set}

    for c in condition_set:
        condition_counts = exp.counts_list[exp.conditions == c]
        for r in region_set:
            region_counts = np.array([rep[r] for rep in condition_counts])
            m = np.mean(region_counts, 0)
            v = np.var(region_counts, 0)
            means[c][r], vars[c][r] = d.propogate_nans(m, v)

which corresponds to Equations 6 and 7 of our Supplementary Methods. We can plot
these values in a scatterplot to get a better sense of what mean-variance
relationship is most appropriate for modeling our counts.
::

    import matplotlib.pyplot as plt

    for c in condition_set:
        for r in region_set:
            m = means[c][r]
            v = vars[c][r]
            filtered_means = m[np.isfinite(m)]
            filtered_vars = v[np.isfinite(v)]

            plt.clf()
            ax = plt.gca()
            ax.scatter(np.log(filtered_means), np.log(filtered_vars))
            plt.xlabel('log mean')
            plt.ylabel('log variance')

            outfile = '_'.join(c, r) + '.png'
            plt.savefig(outfile, bbox_inches='tight')

We describe the workflow for using our functions in the library
``diff3DeFDR.plotters`` for visualizing the counts MVR in the subsection
Plotting.

We could directly use these values for each interaction to parameterize a
corresponding distribution from which we could generate simulated counts.
However, to create counts models that are more robust to noise in input samples,
we chose to first estimate a predicted sample variance computed by pooling
information from interactions occurring at similar distance scales.

Pooling information within distance scales
------------------------------------------

``SimulationSet`` groups points by distance scale, for the purpose of pooling
variance estimates, according to the parameters ``distance_bins`` and
``globally_fit_dispersion``. ``distance_bins`` has the structure:
::

    distance_bins = [[0, 16000], [4000, 20000],…]

where each element defines the start and end of a single distance scale bin
(e.g. the first bin here will contain values for interactions occurring at
fragment-to-fragment distances of 0 to 16 kb).

Alternatively, you can hand SimulationSet the parameter ``bin_properties``.
::

    simset = SimulationSet(
       exp,
       output_dir=my_dir,
       bin_properties={'bin_dimensions': [[0, 150000, 4000, 12000],
                                          [150000, 600000, 8000, 24000],
                                          [600000, 1000000, 24000, 60000]],
                       'catchall': False}
    )

which will then populate distance_bins using the exposed function:

* :func:`diff3DeFDR.loopCallers.empiricalFDR.generate_distance_bins`

For that function, bin_dimensions has the format:
::

    bin_dimensions = [[lower_limit_1, upper_limit_1, step_size_1, window_size_1], ... [lower_limit_n, ...]]

in which element defines what binning (``step_size``, ``window_size``) to use
over what regime of distance scales (``lower_limit``, ``upper_limit``).
Additionally, ``catchall`` specifies whether a final bin is added to
``distance_bins`` that captures all points occurring farther than the upper
limit of the last bin defined with ``bin_dimensions``.

Using the limits defined in distance_bins, SimulationSet groups the previously
computed means and variances. If ``globally_fit_dispersion`` is set to False,
these groupings will be separately performed for each region and predicted
variances will only be computed from interactions occurring in the same distance
bin in the same region. We recommend initially setting this parameter to False.
If you then find that your dispersion estimates in each region trend similarly
and have similar values with respect to distance scale, you might then consider
using the global fit. For the purpose of this exploration, dispersion estimates
(grouped by condition, region, and distance scale) are exposed from the instance
attribute ``SimulationSet.dispersion`` after running the instance method
``compute_generative_model()``.

Smoothing MVR parameter estimates
---------------------------------

To further curtail overfitting to noise or extreme values, we can remove
outliers prior to estimating our mean-variance relationship parameters for a
given distance scale bin. We do this with the ``SimulationSet`` parameters
``mean_outlier_limit`` and ``var_outlier_limit``, which each are values in units
of z-scores. In our example above, outlying points with means 2.5 STDs above
average for points captured in the same distance scale bin will be ignored in
subsequent MVR fitting. Removing these outliers may be especially useful when
attempting to estimate MVR parameters over small or sparsely populated distance
scale bins for which parameters could be incorrectly estimated in the presence
of a few extremely-high counts points.

After all of this processing, we finally can fit MVR parameters to the means and
variances of points that are captured in each distance scale bin. For our own
test dataset (Beagan et al. 2016), we determined that the negative binomial MVR
would be appropriate for our data which was overdispersed at every distance
scale in each region and condition, in a way that trended with distance scale.
Using this MVR, we implemented ``SimulationSet`` to obtain a dispersion
parameter estimate for each distance scale bin. These dispersion estimates are
finally smoothed with respect to distance scale according to the
``SimulationSet`` parameter ``loess_smooth``. If ``loess_smooth`` is set to 0,
no smoothing is performed, otherwise, smoothing is performed with
``statsmodels.nonparametric.smoothers_lowess.loess`` using ``loess_smooth`` is
as its ``frac`` parameter.

Final parameterization of simulated counts generating functions
---------------------------------------------------------------

Following the smoothing step above, we have now obtained a dispersion estimate
for each distance scale bin and can compute a predicted variance estimate for
every point in each region and condition (as in `Equation 8 of our Supplementary
Methods`).

We compute the variance used to parameterize our counts generating function as a
weighted average of the point's new predicted variance and its original observed
sample variance. How these values are weighted is specified using the
``SimulationSet`` parameters ``observed_variance_weight`` and
``predicted_variance_weight``, (which correspond to `α and β respectively in
Equation 9 of our Supplementary Methods`).

In our manuscript, we chose to equally weigh observed and predicted variances.
You may choose to more heavily weigh predicted values to allow per-distance bin
variance pooling to more strongly influence your counts models and this can be
especially useful when observed variances are believed to be unreliable (e.g.
when we have a low number of perhaps noisy replicates).

With these weighted variance estimates, we finally parameterize a negative
binomial distribution for every interaction and randomly draw samples from it to
obtain our simulated counts using the function
``numpy.random.negative_binomial``.
