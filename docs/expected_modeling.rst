Expected modeling and correction
================================

5C data exhibit a strong distance-dependent background signal which is also
influenced by local contact domain structure. diff3DeFDR includes a variety of
algorithms for modeling this background expected interaction frequency, which
can then be removed from the original data to obtain a background-corrected
"observed over expected" signal. For more details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/expected_modeling/>`_

One-dimensional expected modeling
---------------------------------

The exposed functions are:

* :func:`diff3DeFDR.common.make_expected_matrix_fragment`
* :func:`diff3DeFDR.common.make_expected_matrix_binned`

which can create global and regional expected matrices from unbinned counts and
binned counts respectively.

You can use these to obtain a global expected model via the following workflow:
::

    import diff3DeFDR.common as d

    global_expected = d.make_expected_matrix_binned(
        counts, empirical_model_bins, is_global=True)
    regional_expected = d.make_expected_matrix_binned(
        counts, empirical_model_bins, is_global=False)

where ``empirical_model_bins`` passes in one of the following exposed functions
for computing the expected model:

* :func:`diff3DeFDR.common.empirical_model_bins`
* :func:`diff3DeFDR.common.lowess_empirical_model_bins`
* :func:`diff3DeFDR.common.lowess_model_fragments`
* :func:`diff3DeFDR.common.lowess_model_bins`
* :func:`diff3DeFDR.common.polyfit_model_fragments`
* :func:`diff3DeFDR.common.polyfit_model_bins`

In our manuscript, we use ``lowess_empirical_model_bins()`` which makes a
bin-level expected model performing lowess regression in log-counts space,
excluding the first third of the distance scales and only calculating the
geometric means at each distance there instead.

``empirical_model_bins()`` makes a bin-level expected model by taking a
geometric or arithmetic mean of the interaction values at each distance for all
distance scales. ``lowess_model_fragments()`` makes a fragment level expected
model by performing lowess regression in log-log space
while``lowess_model_bins()`` does the same for bin level data.
``polyfit_model_fragments()`` makes a fragment level expected model by fitting a
polynomial in log-log-space while ``polyfit_model_bins()`` does the same for bin
level data.

Donut filtering
---------------

To account for both global and local background signal and so isolate looping
signal from that of TADs, we can compute a more advanced expected model based on
the Rao et al. 2014 donut filtering approach.

The exposed functions are:

* :func:`diff3DeFDR.common.donut_filt`
* :func:`diff3DeFDR.common.lower_left_filt`
* :func:`diff3DeFDR.common.vert_filt`
* :func:`diff3DeFDR.common.horz_filt`
* :func:`diff3DeFDR.common.propogate_nans`

``donut_filt()`` applies Rao et al.'s donut filter to each pixel in the upper
right part of a symmetric matrix while ``lower_left_filt()`` does this for only
the lower left part of the donut. Using the lower left filter in combination
with the donut expected filter (as presented in Beagan et al. 2017), we can more
strongly isolate loops occurring at the edges of TADs. We present the workflow
for computing this expected below:
::

    import numpy as np
    import diff3DeFDR.common as d

    counts, expected_counts = d.propagate_nans(counts, expected_counts)
    donut_expected = {region: d.donut_filt(counts[region], expected_counts[region])
                      for region in counts.keys()}
    lower_left_expected = {
        region: d.lower_left_filt(counts[region], expected_counts[region])
        for region in counts.keys()
    }
    max_donut_ll = {region: np.fmax(donut_expected[region], lower_left_expected[region])
                    for region in donut_expected.keys()}

Finally, we can correct our original "observed" counts from which we computed
our expected models by dividing out the expected signal (if our counts are
unlogged):
::

    obs_over_exp = {region: np.divide(observed_counts[region], max_donut_ll[region])
                    for region in max_donut_ll.keys()}
