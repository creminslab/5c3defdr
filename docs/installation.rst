Installation
============

.. toctree::
    :maxdepth: 1

diff3DeFDR can be installed in a Python 2.7 virtual environment using pip. We
also provide a Docker image that comes with diff3DeFDR and all dependencies
pre-installed.

Standard installation (using pip)
---------------------------------

Create a new Python 2.7 virtual environment
::

    $ virtualenv venv

Activate it
::

    $ source venv/bin/activate

or on Windows
::

    > venv\Scripts\activate

Ensure pip is up to date
::

    (venv)$ python -m pip install -U pip

Install diff3DeFDR
::

    (venv)$ pip install diff3DeFDR-1.0-py2-none-any.whl

Test the installation
::

    (venv)$ python
    >>> import diff3DeFDR

Additional requirements
~~~~~~~~~~~~~~~~~~~~~~~

diff3DeFDR requires R (>=3.4.2) to be installed and on your PATH. To check this,
run
::

    $ Rscript --version

Optional dependencies
~~~~~~~~~~~~~~~~~~~~~

To enable local parallelization of certain pipeline steps (recommended), run
::

    (venv)$ pip install multiprocess

To enable bigwig file support, run
::

    (venv)$ pip install pyBigWig

Note that this package may be difficult to install in Windows. Consider using
the Docker image as an alternative.

To enable iced matrix balancing, run
::

    (venv)$ pip install iced

Note that this package may be difficult to install in Windows. Consider using
the Docker image as an alternative.

If running diff3DeFDR on a cluster using the LSF/bsub job scheduling system,
enable support for parallelization by running
::

    (venv)$ pip install bsub

Docker installation
-------------------

Make sure Docker is installed and that the daemon is running. Then import the
Docker image
::

    $ docker load -i creminslab_diff3defdr_1.0.tar.gz

Run the Docker image and test diff3DeFDR
::

    $ docker run -it creminslab/diff3defdr:1.0
    root@<container_id># python
    >>> import diff3DeFDR

To make files from your local machine available for analysis from inside the
Docker container, include a bind mount in the ``docker run`` command above. For
example, to bind the current directory on the host to ``/data`` in the
container, run
::

    $ docker run -it -v ${PWD}:/data creminslab/diff3defdr:1.0
