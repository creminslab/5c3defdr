diff3DeFDR
==========

diff3DeFDR is an open-source Python package for identifying differential looping
interactions in 5C experiment sets capturing two or three cellular states. Here,
we describe how to install diff3DeFDR and walk through how to apply this tool to
your own data.

.. toctree::
   :maxdepth: 1

   installation
   introduction
   pipeline_tutorial
   conceptual

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
