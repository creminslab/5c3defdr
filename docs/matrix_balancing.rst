Bias mitigation
===============

5C read counts are strongly influenced by bias factors which are dictated by
intrinsic properties of the restriction fragments and primers involved in the
reactions. Moreover, the influence of these bias factors can vary from replicate
to replicate. For more in-depth exploration of the effect of these bias factors
on your data, we encourage you to explore the python package lib5c by Gilgenast
et al. which includes tools for visualizing bias factor profiles. For more
details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/bias_mitigation/>`_

Simple matrix balancing approaches
----------------------------------

Matrix balancing approaches attempt to equalize the row sums of the contact
matrix, without knowing anything about the intrinsic properties of the
restriction fragments. Within 3DeFDR, we provide convenience functions for
applying two such approaches, Knight-Ruiz balancing and ICED balancing, to your
own data.

The exposed functions are:

* :func:`diff3DeFDR.common.kr_balance_matrix`
* :func:`diff3DeFDR.common.iced_balance_matrix`

The Python package iced must be installed prior to running
``iced_balance_matrix()``:
::

    $ pip install iced

Note that this package may be difficult to install on Windows.

Express matrix balancing
------------------------

The Express matrix balancing algorithm takes into account a simple
one-dimensional distance-dependent expected model when balancing, which can
improve balancing performance given the wide dynamic range of interactions
across any given row of the interaction matrix.

The exposed functions are:

* :func:`diff3DeFDR.common.express_normalize_matrix`
* :func:`diff3DeFDR.common.joint_express_normalize`

In our manuscript, we specifically chose to use the joint express variant, first
made available in the Python library lib5C by Gilgenast et al. 2017.
