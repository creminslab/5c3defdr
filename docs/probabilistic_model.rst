Probabilistic model fitting and interaction score transformation
================================================================

In order to analyze p-values for individual 5C interactions, we model
observed/expected values within each region by parameterizing a log-logistic
distribution using maximum likelihood estimation in the style of the Beagan et
al. 2017.

P-values
--------

The exposed functions are:

* ``diff3DeFDR.common.find_logistic_pvalues.R``
* :func:`diff3DeFDR.common.call_R_function`

Passing in the path to our input observed/expected countsfile, we obtain these
p-values using:
::

    input_counts_path = 'my_dir/es2i_rep1_corrected.counts'
    output_pvalues_path = 'my_dir/es2i_rep1_pvalues.counts'
    d.call_R_function(d.find_logistic_pvalues, input_counts_path, output_pvalues_path)

which saves the resulting p-values to ``output_pvalue_path``.

Interaction scores
------------------

To enhance our ability to distinguish significant interactions by thresholding
their p-values, we convert them to interaction scores using the transformation
``-10*log2(pvalue)``. We can then apply interaction score thresholds to these
points to identify significant and differential interactions as described in our
manuscript.

In our manuscript, as a final step before thresholding, we additionally trimmed
interactions within a certain distance of the diagonal of each regional counts
matrix. In this way, we avoided further assessment of pixels at which it would
not be possible for us to detect loops given the resolution of the assay.

With the exposed function:

* :func:`diff3DeFDR.common.wipe_short_range_interactions`

we obtain our final interaction scores using this workflow:
::

    import numpy as np

    scores = {region: -10*np.log2(pvalues[region]) for region in pvalues.keys()}
    trimmed_scores = {
        region: d.wipe_short_range_interactions(
            scores[region], pixelmap[region], distance_span=20000,
            wipe_value=np.nan, distance_span='start-to-start')
        for region in scores.keys()
    }

which classifies interactions within 20 kb of the matrix diagonal as
"short-range" and wipes their values to ``numpy.nan``. The ``distance_span``
parameter specifies how genomic distance between bins will be determined, in
this case from the starting coordinate of the first to the starting coordinate
of the next.
