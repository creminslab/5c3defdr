Quantile normalization
======================

Quantile normalization is a method for bringing disparate 5C libraries (of
varying sequencing depth, library complexity, etc.) to a comparable scale. For
more details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/quantile_normalization/>`_

Exposed functionality

Two functions are exposed:

 * :func:`diff3DeFDR.common.qnorm`
 * :func:`diff3DeFDR.common.qnorm_counts_superdict`

``qnorm()`` performs quantile normalization on arbitrary data, and may be
re-used in other contexts. It operates on a table of input data, not the counts
dicts (and superdicts) which are commonly used in the library. Therefore, the
wrapper function ``qnorm_counts_superdict()`` is provided to make this step
easier for our 5C data.
