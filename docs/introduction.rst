Introduction
============

.. toctree::
    :maxdepth: 1

What is ``diff3DeFDR``? How is it laid out? What can I do with it? These are the
questions that will be answered in this section.

What is diff3DeFDR?
-------------------

``diff3DeFDR`` is a library for performing differential looping analysis on
high-resolution, multi-condition Chromosome Conformation Capture Carbon Copy
(5C) experiments.

How is it organized?
--------------------

``diff3DeFDR`` is currently organized as a processing pipeline emerging from a
function library consisting of interoperating subpackages. In the next sections,
we will walk through examples of both usage of the pipeline for classifying
differential loops and usage of the function library for creating your own
processing recipes.

What can I do with diff3DeFDR?
------------------------------

This list summarizes the things you can do with ``diff3DeFDR``:

* Analyze:

  * Simulate 5C replicates
  * Transform raw 5C counts into an interaction score that is representative of looping signal
  * Classify differential and invariant interactions from two- or three-condition experiments
  * Compute and control the false discovery rate of your differential loop calls

* Visualize:

  * Plot contact frequency heatmaps to see genome architecture and get an understanding for how transformations affect your data
  * Visualize counts modeling fits and get an understanding for how appropriate the model is for simulating your data
  * Visualize differential loop classifications
  * Visualize enrichments between looping classes and traditional genome annotations
