Trimming
========

An important early preprocessing step is the removal of low-quality primers from
the dataset. For more details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/trimming/>`_

Exposed functionality
---------------------

The core API is exposed in the following convenience functions:

 * :func:`diff3DeFDR.common.trim_primers`
 * :func:`diff3DeFDR.common.wipe_matrix`
 * :func:`diff3DeFDR.common.trim_matrix`

The functions ``wipe_matrix()`` and ``trim_matrix()`` also have convenience
wrappers which apply them over a counts superdict (dict of counts dicts, whose
first-level keys are replicate names), which are:

 * :func:`diff3DeFDR.common.wipe_counts_superdict`
 * :func:`diff3DeFDR.common.trim_counts_superdict`

Workflow
--------

The general workflow is to trim primers first (based on the quality of the
counts matrices in the dataset), and then either trim or wipe those counts
matrices:
::

    from diff3DeFDR.common import trim_primers, trim_counts_superdict

    trimmed_primermap, trimmed_indices = trim_primers(primermap, counts_superdict)
    trimmed_counts_superdict = trim_counts_superdict(counts_superdict, trimmed_indices)

The call to ``trim_primers()`` does not modify the ``counts_superdict``, leaving
the client to decide what to do next.

Trimming versus wiping
----------------------

``trim_matrix()`` removes rows and columns from the matrices in the counts dict,
with the result that the dimensions of these matrices will match the lengths of
the values of ``trimmed_primermap``. This is the recommended way to treat
removal of low-quality fragments.

``wipe_matrix()`` does not change the dimensions of any matrix, and instead
simply paints over the removed indices according to its kwarg ``wipe_value``.
This can be useful when removing low-quality regions from already-binned data,
for example:
::

    from diff3DeFDR.common import trim_primers, wipe_counts_superdict

    _, trimmed_indices = trim_primers(pixelmap, counts_superdict)
    wiped_counts_superdict = wipe_counts_superdict(counts_superdict, trimmed_indices)

Notice that we discard the ``trimmed_pixelmap`` from the first function call,
because this pixelmap's dimensions do not match any of the counts dicts.

Trimming options
----------------

There are two different ways to assess the quality of a primer: its total cis
contact count (row sum in the counts matrix) or the fraction of its possible
interactions which are nonzero. These two quality metrics are thresholded on by
the two kwargs of ``trim_primers()``: ``min_sum`` and ``min_frac``.
