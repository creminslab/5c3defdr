Transforming 5C counts to interaction scores
============================================

Before identifying differential interactions in a 5C data set, we process raw
the counts using each step in the following pipeline in sequence as listed
below. You may choose to use some or all of these on your own data set. The
algorithms that make up the 5C processing framework can be found in
``diff3DeFDR.common`` and in ``diff3DeFDR.processing5C``. ``processing5C``
specifically makes use of utility functions in common to run the entire pipeline
in sequence in the style of Beagan et al. 2017, handling countsfiles and
bedfiles using the ``ExperimentSet`` convenience class.

.. toctree::
    :maxdepth: 1

    trimming
    qnorm
    matrix_balancing
    binning_smoothing
    expected_modeling
    probabilistic_model
