Models for simulating counts
============================

.. image:: images/mvr_plots.png
   :align: center

The performance of our simulation technique is dependent on how well our counts
generating function can represent the true mean-variance relationship of an
interaction.  To determine how best to model counts, our team explored different
mean-variance relationships and different approaches for per-distance-scale
variance pooling, including different distance-scale binning schemes, pooling
bins across regions, and pooling bins across cell types. We ultimately
determined that the negative binomial MVR, and pooling bins across regions but
not cell types, worked well for the Beagan et al. 2016 dataset, as presented in
our manuscript. However, variations of this approach may be more appropriate for
other datasets. We encourage you to explore what models and pooling techniques
might help you better simulate interactions in your own data with our provided
MVR visualization tools as shown above.

Exposed functionality
---------------------

The exposed functions for creating these plots are:

* :func:`diff3DeFDR.plotters.fano_factor_versus_distance_scale_trend`
* :func:`diff3DeFDR.plotters.dispersion_versus_distance_scale`
* :func:`diff3DeFDR.plotters.mean_variance_relationship_fitting`

Each of these methods takes a ``SimulationSet`` object as input. The instance
``method compute_generative_model()`` should first be run so that the object can
provide values to be plotted including means and variances across samples in
each distance bin, fitted dispersion parameters in each bin, and smoothed
dispersions if the ``SimulationSet`` parameter ``loess_smooth`` is not set to
None. Each method returns handles to a ``matplotlib.pyplot`` figure and axes set
to allow customization of output plots. Each has the option to show or hide
default axis labels and the legend via the parameter ``show_labels``.

``fano_factor_versus_distance_scale_trend()`` simply plots the Fano factor (or
variance-mean ratio) of points capture in each distance-scale bin.
``dispersion_versus_distance_scale()`` and
``mean_variance_relationship_fitting()`` are more specifically intended to allow
comparison of Poisson and negative binomial MVR fittings to points across
different distance-scales, regions, and conditions.

Workflow
--------

The usage of each of these methods is demonstrated in the ``create_mvr_plots()``
method of ``diff3DeFDR/demo/demo_helpers.py`` and below:
::

    from diff3DeFDR.common import check_dir
    import diff3DeFDR.plotters as dplots

    simset = SimulationSet(
        exp_qnorm,
        output_dir=sim_transforms_dir,
        predicted_variance_weight=0.5,
        observed_variance_weight=0.5,
        loess_smooth=0.5,
        globally_fit_dispersion=True,
        mean_outlier_limit=2.5,
        var_outlier_limit=None
    )
    simset.compute_generative_model()
    condition_set = exp_qnorm.unique_conditions

    # 1. Fanofactor vs. distance scale trend
    fano_plot_dir = check_dir(os.path.join(my_plots_dir, 'fano_factor_v_distance_scale'))

    for condition in condition_set:
        fig, _ = dplots.fano_factor_versus_distance_scale_trend(
            simset, condition_to_plot=condition, show_labels=True)
        plot_path = os.path.join(fano_plot_dir, 'condition={}.png'.format(condition))
        fig.savefig(plot_path)

    # 2. Fitted negative binomial dispersion vs. distance scale trend
    fig, _ = dplots.dispersion_versus_distance_scale(
        simset, conditions_to_plot=condition_set, show_labels=True)
    plot_path = os.path.join(plots_dir, 'negative_binomial_dispersion_vs_distance_scale.png')
    fig.savefig(plot_path)

    # 3. Plot of mean vs. variance of raw counts with fittings of Poisson and Negative Binomial MVR equations
    mvr_plot_dir = check_dir(os.path.join(my_plots_dir, 'mean_vs_variance_per_distance_bin'))
    bin_indices = [10, 24, 60, 75, 96]
    bins = [simset.distance_bins[i] for i in bin_indices]
    for condition in condition_set:
        fig, _ = dplots.mean_variance_relationship_fitting(
            simset,
            distance_bins_to_plot=bins,
            condition_to_plot=condition,
            plot_fits=['poisson', 'negative_binomial'],
            xscale='log',
            yscale='4th-root',
            show_labels=True
        )
       plot_path = os.path.join(mvr_plot_dir, 'condition={}.png'.format(condition_to_plot))
       fig.savefig(plot_path)

where ``exp_qnorm`` is ``ExperimentSet`` object carrying counts matrices that
have been transformed via our 5C processing pipeline up through the step of
quantile normalization.
