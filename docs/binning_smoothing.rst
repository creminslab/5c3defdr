Binning and smoothing
=====================

To reduce spatial noise in 5C data, we can treat the 5C contact frequencies as a
2-D signal that can be smoothed with various filtering functions. Depending on
the original coordinates of the contact matrix and the coordinates we choose to
evaluate the filtered signal at, this process can be referred to as "binning" or
"smoothing". For more details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/binning_and_smoothing/>`_

Theoretical overview
--------------------

We will create "filtering functions" to pass over the contact matrices. For
generality in terms of the level of data the filtering functions can be applied
to, we will require that filtering functions compute values on "neighborhoods"
of points defined by spatial proximity to the at which point we want to evaluate
the filtered signal.

Exposed functionality
---------------------

The exposed functions are:

* :func:`diff3DeFDR.common.generate_pixelmap_from_primermap`
* :func:`diff3DeFDR.common.bin_and_smooth_counts`

Workflow
--------

With these functions, we roll the process of binning, smoothing, and filtering
into a single step. If we have a fragment-level regional counts matrix and a
primermap, we can run:
::

    import diff3DeFDR.common as d

    bin_step = 4000
    pixelmap = {
        region: d.generate_pixelmap_from_primermap(
            primermap[region], bin_step, region_name=region)
        for region in counts.keys()
    }

    filter_function = d.geometric_mean_filter()
    smooth_window_width = 16000
    binned_counts = {
        region: bin_and_smooth_counts(
            counts[region], filter_function, pixelmap[region],
            primermap[region] ,smoothing_window_width)
        for region in counts.keys()
    }

With this, we bin the counts using a 4 kb window width, smooth the resulting
bins with a 16 kb window width, and filter them using a geometric mean filter.
We discuss options for filter functions below.

Filter functions
----------------

Filter functions must take in a representation of a "neighborhood" around a
point and return a scalar value representing the evaluation of the filtered
signal at that point. A neighborhood is represented as a list of "nearby points"
where each nearby point is represented as a dict of the following form:
::

    {
        'value': float,
        'x_dist': int,
        'y_dist': int
    }

where 'value' is the value at the point and 'x_dist' and 'y_dist' are its
distances from the center of the neighborhood along the x- and y-axis,
respectively, in base pairs.

More formally, the Python type annotation for a filter function is:
::

    Callable[[List[Dict[str, Any]]], float]

In diff3DeFDR, we currently provide filter functions ``geometric_mean_filter()``
and ``arithmetic_mean_filter()``, and encourage users to explore the development
and application of more advanced filters with lib5c by Gilgenast et al.
