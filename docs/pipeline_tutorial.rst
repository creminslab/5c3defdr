Pipeline tutorial
=================

.. toctree::
    :maxdepth: 1


Here, we walk through the basic usage of the 3DeFDR pipeline described in our
manuscript.

Make a directory
----------------

Create a directory for this pipeline and change into it.
::

    $ mkdir diff3DeFDR-tutorial
    $ cd diff3DeFDR-tutorial

Get data
--------

First, download some data from GEO. For this tutorial, we will use the 5C data
set from GEO Series GSE85185.

The specific files we will need are:

* A primerfile describing the fragments in the 5C assay.

  * `primermap <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE85185&format=file&file=GSE85185%5FBED%5F314%2DES%2DNPC%2DLOCI%5Fmm9%2Ebed%2Egz>`_

* Six raw countsfiles containing the results of the two replicates each of three conditions (es2i, esserum, and npc):

  * `es2i rep 1 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259911&format=file&file=GSM2259911%5FES%5F2i%5FRep1%2Ecounts%2Etxt%2Egz>`_
  * `es2i rep 2 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259912&format=file&file=GSM2259912%5FES%5F2i%5FRep2%2Ecounts%2Etxt%2Egz>`_
  * `esserum rep 1 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259913&format=file&file=GSM2259913%5FES%5Fserum%5FRep1%2Ecounts%2Etxt%2Egz>`_
  * `esserum rep 2 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259914&format=file&file=GSM2259914%5FES%5Fserum%5FRep2%2Ecounts%2Etxt%2Egz>`_
  * `npc rep 1 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259915&format=file&file=GSM2259915%5FpNPC%5FRep1%2Ecounts%2Etxt%2Egz>`_
  * `npc rep 2 <https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259916&format=file&file=GSM2259916%5FpNPC%5FRep2%2Ecounts%2Etxt%2Egz>`_

Download the seven files using the links above, then unzip them into a new
directory, ``diff3DeFDR-tutorial/input``. Finally, strip all of the .txt
extensions and GEO indentifiers for brevity. If you use the bash shell, you can
do this by running:
::

    $ mkdir input
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE85185&format=file&file=GSE85185%5FBED%5F314%2DES%2DNPC%2DLOCI%5Fmm9%2Ebed%2Egz' | gunzip -c > input/primermap.bed
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259911&format=file&file=GSM2259911%5FES%5F2i%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/es2i_rep1.counts
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259912&format=file&file=GSM2259912%5FES%5F2i%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/es2i_rep2.counts
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259913&format=file&file=GSM2259913%5FES%5Fserum%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/esserum_rep1.counts
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259914&format=file&file=GSM2259914%5FES%5Fserum%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/esserum_rep2.counts
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259915&format=file&file=GSM2259915%5FpNPC%5FRep1%2Ecounts%2Etxt%2Egz' | gunzip -c > input/npc_rep1.counts
    $ wget -qO- 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM2259916&format=file&file=GSM2259916%5FpNPC%5FRep2%2Ecounts%2Etxt%2Egz' | gunzip -c > input/npc_rep2.counts

After renaming, the ``diff3DeFDR-tutorial/input`` directory should contain the
following files:

 * ``primermap.bed``
 * ``es2i_rep1.counts``
 * ``es2i_rep2.counts``
 * ``esserum_rep1.counts``
 * ``esserum_rep2.counts``
 * ``npc_rep1.counts``
 * ``npc_rep2.counts``

Get the default config file
---------------------------
To drop the example pipeline configuration file ``diff3DeFDR.cfg`` into the
current directory, run ``diff3defdr`` from the command line with no arguments:
::

    $ diff3defdr
    dropping default config file to current directory

The behavior of the entire pipeline is controlled using this one configuration
file.

Edit the config file
--------------------
Open ``diff3DeFDR.cfg`` in your favorite text editor and take a look inside.

The config file is organized into blocks that start with a header and then
define some config variables.

The first block is the ``[RawCounts]`` block, which specifies where the input
counts files are located on the disk and gives them the replicate names.

The second block is the ``[PrimerFile]`` block, which specifies where to find
the primerfile on the disk.

The remaining sections expose the default parameters for each step (e.g.
``[BinSmooth]`` describes the default parameters of the binning and smoothing
step),

We need to fill the ``[RawCounts]`` and ``[PrimerFile]`` blocks with the paths
to the data files we downloaded. For example:
::

    [RawCounts]
    countsfiles={
      'es2i_rep1': 'input/es2i_rep1.counts',
      'es2i_rep2': 'input/es2i_rep2.counts',
      'esserum_rep1': 'input/es2i_rep1.counts',
      'esserum_rep2': 'input/es2i_rep2.counts',
      'npc_rep1': 'input/es2i_rep1.counts',
      'npc_rep2': 'input/es2i_rep2.counts',
    }
    conditions=["es2i","esserum","npc"]

    [PrimerFile]
    primerfile=input/primermap.bed

The config parser is somewhat sensitive to indentation, so match this carefully.

The replicate names are the keys of the countsfiles dictionary. You can set them
to anything you like; however, the conditions (for the purposes of classifying
differential looping interactions) as defined in the conditions comma-separated
list above must be substrings of the replicate names. For convenience, by
default ``conditions=["es2i","esserum","npc"]`` which matches the replicate
names we have chosen above, but when you do your own analyses you may need to
change this.

The blocks from ``[GeneralProcessing]`` through ``[TrimDiagonal]`` specify
parameters to pass in for each transformation step in the 5C processing
pipeline. While the majority of the parameters for this pipeline are not
specific to the input dataset, ``[RegionSplit].splits`` and
``[TrimPrimer].handpicked_ranges`` guide how specific regions are split into
smaller regions and trimmed of specific bad primers respectively. You will need
to change these values to match your data (or set them both to None). For each
block, the resulting countsfiles from each transformation will be saved to
``diff3DeFDR-tutorial/intermediates/transforms_dir``. Each countsfile will be
saved in the format ``'es2i_rep1_{save_suffix}.counts'`` where ``save_suffix``
is a unique identifier of the transformation and set as a parameter of each
transformation block.

In the block ``[Simulations]``, we pass in parameters guiding how simulation
generating functions are computed for each interaction in the input data set and
how many simulated replcaite sets to generate. You may need to change the
parameter ``bin_properties`` to match the actual distance scales present in your
data. (For more information about ``bin_properties``, read about this parameter
in the section "Simulating 5C counts" below.) Simulated replicate sets will be
saved to ``diff3DeFDR-tutorial/intermediates/simulated_transforms_dir``.

In the block ``[DiffLoopCalling]``, we pass in parameters guiding how 3DeFDR
differential loop calling is applied to the experimental and simulated replicate
sets. You will need to change the parameter ``null_model_condition`` to be one
of the conditions specified earlier in the conditions list.

In the final block ``[PlotClassificationClusters]``, we specify how we would
like to visualize resulting differential loop classifications. You will need to
update the parameter windows and color_dict to specify what genomic windows you
would like to see results for and how you would like to color-code identified
loop classifications in those plots.

After specifying the input files and editing parameters to match your regions
and conditions, save the config file, then run the pipeline:
::

    $ diff3defdr diff3DeFDR.cfg

Output
------

Once the pipeline completes, intermediate and output files should be present in
the subfolders of the directory in which you ran the pipeline. Look around and
investigate the results of your analysis. A list of final loop calls will be
saved to ``diff3DeFDR-tutorial/output/classification_results`` and corresponding
classification cluster plots will be saved to
``diff3DeFDR-tutorial/output/classification_cluster_plots``.

While this default pipeline will produce interpretable results for all datasets,
the default parameters will not be ideal in all cases. In the next section, we
will break down each step of the pipeline for those who wish to adapt particular
parameters to better fit their data
