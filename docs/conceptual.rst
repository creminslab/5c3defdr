API specification and conceptual documentation
==============================================

This section describes the most important data structures and functions of ``diff3DeFDR``. We conceptually explain each step of the pipeline, and explain which functions are available to perform each step and what parameters can be changed to better apply 3DeFDR to your own data.

.. toctree::
    :maxdepth: 1

    data_structures_file_types
    transforming_5C_counts
    simulations
    classifying_differential_loops
    plotting
