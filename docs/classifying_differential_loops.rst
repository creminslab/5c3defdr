Classifying differential loops
==============================

After obtaining interaction scores, we want to combine this information across
all replicates to identify condition-specific and -invariant interactions.
Further, we could like to use a null replicate set to compute and control the
false discovery rate of our called interactions. This is the core methodology of
3DeFDR.

Conceptual background
---------------------

We assume that each replicate belongs to one of exactly three conditions, named
"A", "B", and "C." (In our manuscript, these were "ES-2i", "ES-Serum", and
"NPC.") With this number of conditions, there seven possible classes of
interactions that we wish to recover:

 * A only (looping in condition A but not in conditions B or C)
 * B only (looping in condition B but not in conditions A or C)
 * C only (looping in condition C but not in conditions A or B)
 * A & B only (looping in conditions A and B but not in condition C)
 * B & C only (looping in conditions B and C but not in condition A)
 * A & C only (looping in conditions A and C but not in condition B)
 * Constitutive (looping in all three conditions)

We will also recover a "background" class, which will be convenient when
assessing enrichments.

Exposed functionality
~~~~~~~~~~~~~~~~~~~~~

The subpackage for loop calling with 3DeFDR is

 * :mod:`diff3DeFDR.loopCallers.empiricalFDR`

in which the most important class is eFDR, which manages the entire 3DeFDR loop
calling pipeline.

We explain each step of our advanced differential loop calling procedure below.

Thresholding
------------

Given a set of sample interaction scores at a given point (or bin-bin pair), we
would like to determine whether the point is actually differential across
conditions and further to what class differential interactions it might belong.

We accomplish this in diff3DeFDR with the helper class

 * :class:`diff3DeFDR.loopCallers.empiricalFDR.ClassifyLoops`

which is called from the class eFDR. Through the instance method
``eFDR.classify_loops()``, we hand this helper class a replicate set of
interaction scores carried within an instance of the DataSet convenience class,
which contains our interaction score replicate set as well as the thresholds we
wish to apply to these scores. The workflow, if we have prepared this DataSet
``dataset`` (as is carried out in eFDR), is then:
::

    from diff3DeFDR.loopCallers.empiricalFDR import ClassifyLoops

    classifer = ClassifyLoops(dataset)
    classifier.apply_thresholds(
        background_threshold=0.8,
        significance_threshold=0.165,
        difference_threshold=25.0
    )

With this, the constructor method of ``ClassifyLoops`` precomputes minimums in
interaction scores within conditions and differences in interaction scores
across conditions. This is implemented for the sake of efficiency, so that these
values do not need to be recomputed as we apply potentially many different sets
of thresholds to our data in the course of our eFDR control procedure.

The instance method ``apply_thresholds()`` applies three thresholds to these
precomputed values. ``background_threshold`` is converted from units of p-value
to interaction score and then applied to all sample interaction scores as in
`Equation 12 of our Supplementary Methods`. If all sample score are less than
this threshold, the loop is said to be "background." ``significance_threshold``
is then converted from units of p-value to interaction and applied to all sample
interaction scores as in `Equation 13 of our Supplementary Methods`. To be
considered significantly looping, a point must have at least one condition in
which ALL of its replicate interaction scores are above this threshold. If this
is not the case, the point will uncalled and not be assessed further for
differential looping. Finally, the difference threshold is applied to
differences in scores across conditions as specified in `Supplementary Table 2`.
Following the application of the criteria listed in that table, we obtain a set
of loop classifications, thereafter available as a pandas Series from the
instance attribute ``classifier.recent_calls``.

Estimating empirical false discovery rate (eFDR)
------------------------------------------------

We would like to have a metric for determining what values we should use for our
thresholds to ensure that the loop calls we recover are bona fide differential
loops and not false positives.

In 3DeFDR, we chose to accomplish this by empirically estimating the false
discovery rate (eFDR) of loop calls on our input replicate set from the number
of calls we recovered on simulated null replicate set. This null replicate set
consists of simulated replicates modeled from a single condition and so any
differential loops called on it are necessarily false positives. To create this
null set, we simply generate simulations as described in the previous section
and choose replicates of one condition to populate our null.

To then compute an eFDR for a given set of loop calls on the input experimental
replicate set, we apply the same thresholds to our null replicate set. We then
obtain eFDR as ``eFDR = n_null / n_exp`` (Equation 1 of our manuscript) where
``n_null`` is the number of differential loops called on our null set and nexp
is the number of differential loops called on our input experimental set. If we
are concerned about variability in our eFDR estimates between different null
sample sets, we can generate many null sample sets and the pipeline class eFDR
will substitute nnull for its average value across the multiple null sample sets
when estimating eFDR with the helper method
:func:`diff3DeFDR.loopCallers.empiricalFDR._compute_FDR`.

Controlling eFDR
----------------

After estimating eFDR, we would like to use it to programmatically determine the
thresholds we apply to interaction scores to obtain our loop calls and in doing
so control the eFDR of our calls.

We chose to implement this by obtaining loop calls and eFDR estimates for each
value of ``difference_threshold`` across a sweep of values. The workflow for this
is:
::

    from diff3DeFDR.loopCallers.empiricalFDR import eFDR

    e = eFDR(real_is_exp, simulated_transforms_dir, intermediates_dir, output_dir, parallelized=True)
    e.classify_loops(
        null_model_condition='es2i',
        background_threshold=0.8,
        significance_threshold=0.165,
        difference_thresholds=None,
        fdr_threshold=0.01,
        adaptive=True,
        min_cluster_size=4,
        overwrite=False
    )

which performs the complete 3DeFDR differential loop calling procedure on
interaction score matrices of the input experimental replicate set as carried in
the ExperimentSet object ``real_is_exp`` and on simulated replicate set(s)
previously generated, transformed to interaction scores, and stored in
``simulated_transforms_dir``. It stores the final output loop call set as a csv
in ``output_dir`` and intermediate data generated in the process of computing
that loop set to ``intermediates_dir``. With the parameter
``null_model_condition``, we can specify which condition's simulated replicates
we would like to use for our null replicate set.

The ``difference_thresholds`` is the list of interaction score difference
thresholds at which we would like to compute loop calls and obtain an eFDR
estimate. If this parameter is set to None, the instance method
``classify_loops()`` will compute this list of values for you as 250 evenly
spaced values between the minimum and the 99.5 percentile interaction scores in
the input experimental replicate set. Using the internal helper method
``_get_fdr_to_diff_thresh_map()``, the ``eFDR`` class obtains an eFDR estimate
for every value in difference_thresholds, saving loop calls and these eFDR
estimates to the directory ``intermediates_dir/difference_threshold_sweep`` as
it proceeds. ``parallelized`` specifies whether jobs for computing each eFDR
estimate will be completed in parallel or in sequence via the utility function
:func:`diff3DeFDR.common.job_managment`. ``overwrite`` specifies whether to
recompute this intermediate data with each run of ``classify_loops()`` or to
just load those previously saved to the same directory.

We will obtain eFDR estimates that are computed across all differential loop
calls and another set of estimates computed individually in each loop class as
described in our `Supplemental Methods`. The ``adaptive`` parameter determines
what set of eFDR estimates are controlled. If the parameter is set to False,
only the eFDR estimate across all classes will be controlled to the value
``fdr_threshold`` and a single difference threshold will be used to classify
loops of all classes. If the parameter is set to True, the eFDR of each
differential loop class will be separately controlled and the loop call set of
an individual class will be returned at the difference threshold at which its
own class-specific eFDR estimate falls below ``fdr_threshold``.

Clustering loop calls
---------------------

In the final step of 3DeFDR, we group nearby interactions that have the same
loop class assignment into clusters. Clustering is an important step because it
allows us to identify groups of classified interactions that we think span a
large enough genomic range to constitute a real looping interaction. In the
instance method ``eFDR.classify_loops()``, the parameter ``min_cluster_size``
defines the minimum allowed size of clusters for called loops. Any called loops
that are part of clusters that are less than this size will be wiped from the
final loop call list.

Clustering and cluster thresholding are performed respectively with:

 * :func:`diff3DeFDR.common.make_clusters`
 * :func:`diff3DeFDR.loopCallers.size_threshold_loop_calls`

``make_clusters()`` may be flexibly used to cluster adjacent like values in any
matrix. ``eFDR.classify_loops()`` uses ``size_threshold_loop_calls()`` to
internally cluster loop calls with ``make_clusters()`` and apply
``min_cluster_size`` thresholding. The choice of ``min_cluster_size`` be the
smallest number of bin-bin pairs you expect to constitute a looping interaction
to further reduce false positives in your final call list.
 
Benchmarking with parametric approaches
---------------------------------------

To benchmark the performance of 3DeFDR, we chose to implement two parametric
approaches, ANOVA and 3DLRT, for calling differential loops.

These approaches do not use a simulated null model to estimate FDR, but instead
compute a differential looping (DL) p-value for every interaction using a
parametric test. These DL p-values are corrected for multiple testing using the
Benjamini-Hochberg (BH) step-up procedure and the resulting BH FDR adjusted
p-values can then be thresholded to control FDR while calling differential
loops.

Exposed functionality
~~~~~~~~~~~~~~~~~~~~~

These approaches are exposed in the subpackage:

 * :mod:`diff3DeFDR.loopCallers.benchmarks`

and the important libraries and methods in it are:

 * :func:`diff3DeFDR.loopCallers.benchmarks.benchmark_stats`
 * :func:`diff3DeFDR.loopCallers.benchmarks.generate_benchmark_pvalues`
 * :func:`diff3DeFDR.loopCallers.benchmarks.classify_loops_with_benchmark_pvalues`

``benchmark_stats`` contains utility methods for computing ANOVA and 3DLRT test
statistics from matrices of interaction scores or z-scores, an alternative
transformation of the modeled p-value that is normally distributed and perhaps
more appropriate for use with parametric approaches (``anova_pvalue()``,
``anova_stat()``, ``is_lrt_stat()``, ``zscore_lrt_stat()``). It also contains
``bh_mtc()`` for BH adjusting DL p-values and ``determine_colors()``, an
implementation of the approach for determining the class of a differential loop
without interaction score difference thresholding (as shown in `Supplementary
Figure 4`).

``generate_benchmark_pvalues()`` and ``classify_loops_with_benchmark_pvalues()``
are wrappers that can be used to obtain a list of differential loop calls from
these benchmarks in the same format as those output by 3DeFDR using the ``eFDR``
class.

Workflow
~~~~~~~~

We use this wrapper methods to obtain loop calls with ANOVA and 3DLRT as
follows:
::

    from diff3DeFDR.loopCallers.benchmarks import generate_benchmark_pvalues, classify_loops_with_benchmark_pvalues

    benchmarking_data_dir = os.path.join(my_output_dir, 'benchmarking_data')
    generate_benchmark_pvalues(
        input_dir=my_input_dir,
        output_dir=benchmarks_data_dir,
        exp_is=exp_is,
        pvalue_transform='is'
    )

    classify_loops_with_benchmark_pvalues(
        benchmarks_data_path=benchmark_data_dir,
        output_dir=my_output_dir,
        exp_postprocessed=exp_is,
        fdr_threshold=0.01,
        background_threshold=0.8,
        significance_threshold=0.165,
        min_cluster_size=4
    )

where ``exp_is`` is an ExperimentSet object of diagonal-trimmed interaction
scores and input_dir is used to load information about which replicate files
belong to which conditions. ``generate_benchmark_pvalues()`` saves the resulting
DL p-values for both ANOVA and 3DLRT to ``benchmarking_data/output`` and copies
over the input modeled p-values from which they were derived to
``benchmarking_data/input``. The saved files for each benchmark are csv's
generated using ``diff3DeFDR.common.DataSet.save()``, which are human-readable,
but can also be readily loaded back into DataSet objects using
``diff3DeFDR.common.DataSet.load()``. Whether input modeled p-values are
transformed into interaction scores or z-scores prior to applying the parametric
tests is specified with the parametric pvalue_transform (setting it to either
'is' or 'zscore').
