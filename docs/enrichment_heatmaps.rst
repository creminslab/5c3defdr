Enrichments
===========

.. image:: images/enrichments.png
   :align: center

An important final step in 5C analysis is the quantification of overlap between
specific interaction classes and traditional genomic annotations. ``diff3DeFDR``
provides a few tools for performing such an enrichment analysis and visualizing
the results in a heatmap. For more details, see `the lib5c docs <https://lib5c.readthedocs.io/en/stable/enrichments/>`_

Exposed functionality
---------------------

The exposed functions for performing and visualizing this analysis are:

 * :func:`diff3DeFDR.common.make_annotationmaps`
 * :func:`diff3DeFDR.plotters.plot_looptype_vs_annotation_heatmap`

Workflow
--------

``make_annotationmaps()`` loads our annotations from file and
``plot_looptype_vs_annotation_heatmap()`` plots a heatmap of enrichments for one
fixed annotation, varying the loop category on the x-axis and the annotation on
the other side on the y-axis. The workflow for this analysis is then:
::

    import matplotlib.pyplot as plt

    from diff3DeFDR.common import make_annnotationmaps
    from diff3DeFDR.plotters import plot_looptype_vs_annotation_heatmap

    dataset = e.final_calls

    # Load epigenetic annotations from file
    annotation_bed_reference = {
        'ES_Enhancer_MTD_160712_Galaxy1311': 'Enhancers ES',
        'NPC_Enhancer_MTD_160712_Galaxy1314': 'Enhancers NPC',
        'ES_Genes_1_8foldUpwAC_JB_2_9_17_plus_promotor': 'Genes ES',
        'NPC_Genes_1_8foldUpwAC_JB_2_9_17_plus_promotor': 'Genes NPC'
    }
    annotation_order = ['Enhancers ES', 'Enhancers NPC', 'Genes ES', 'Genes NPC']
    loop_type_order = ['constitutive', 'npc', 'es2i_esserum']

    annotation_maps = make_annotationmaps(dataset.pixelmap, directory=my_annotations_dir)
    annotation_maps = {(annotation_bed_reference[k] if k in annotation_bed_reference else k): annotation_maps[k] for k in annotation_maps}

    # Plot annotation enrichments as a heatmap
    loop_calls = dataset.counts(name='color', dtype=str)

    fig, ax = plot_looptype_vs_annotation_heatmap(
        annotationmaps=annotation_maps,
        looping_classes=loop_calls,
        constant_annotation='wildcard',
        loop_type_order=loop_type_order,
        annotation_order=annotation_order
    )
    fig.savefig(my_plot_path)
    plt.close('all')

where ``my_annotations_dir`` must contain bedfiles of the annotations listed in
``annotation_bed_reference``, which maps the filenames to the labels the
annotations will have in the final heatmap. ``annotation_order`` specifies the
order in which they will appear on the vertical axis in the heatmap from top to
bottom and ``loop_type_order`` specifies the order in which loop classes will be
included on its horizontal axis and the order from left to right. ``loop_calls``
passes in our final loop calls from runs of either 3DeFDR or its benchmark
approaches. ``constant_annotation`` specifies which annotation to hold constant
throughout the plot.
