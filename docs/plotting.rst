Plotting
========

``diff3DeFDR`` offers a few key plotting functions to support flexible exploration of
(1) the various data transformations it produces in its 5C processing pipeline,
(2) its models for simulating counts,
(3) its differential loop calls, and
(4) the enrichment of those calls in traditional genomic annotations.

The subpackage ``diff3DeFDR.plotters`` contains all of our ready-to-use plotting
functions and ``diff3DeFDR.plotters.util`` contains utility functions that you
can use to make your own alternative versions of these plotters.

.. toctree::
    :maxdepth: 1

    counts_heatmaps
    mvr_plots
    classification_clusters
    enrichment_heatmaps
